-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2015 at 07:02 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ab71407_airapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
`asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `friendly_name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `nfc_uuid` varchar(255) NOT NULL,
  `asset_key` varchar(255) NOT NULL,
  `device_udid` varchar(255) NOT NULL,
  `feature_fuel` tinyint(1) NOT NULL,
  `feature_maintenance` tinyint(1) NOT NULL,
  `feature_exception` tinyint(1) NOT NULL,
  `feature_location` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `deleted_on` datetime NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`asset_id`, `company_id`, `friendly_name`, `description`, `nfc_uuid`, `asset_key`, `device_udid`, `feature_fuel`, `feature_maintenance`, `feature_exception`, `feature_location`, `enabled`, `created_on`, `updated_on`, `deleted_on`, `asset_type_id`, `created_by`, `updated_by`, `deleted_by`) VALUES
(3, 17, 'search testing', '<b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><b>Description Demo</b><br>', 'Demo', 'Demo', '', 1, 1, 0, 1, 0, '2015-03-23 06:23:50', '2015-05-22 08:57:36', '0000-00-00 00:00:00', 3, 110, 110, 0),
(4, 17, 'Asset Name Neo 78', '<h1>Demo</h1><br>Testing Search<br>aslkjS<br><br>as<div></div>', '123', '', '1234965787', 1, 1, 1, 1, 0, '2015-03-24 18:08:15', '2015-05-22 05:41:05', '0000-00-00 00:00:00', 3, 110, 110, 0),
(5, 15, 'Demo', 'Demo', '123', '', '', 1, 1, 0, 1, 1, '2015-03-24 18:15:44', '2015-03-26 01:38:31', '2015-03-26 12:04:49', 1, 110, 110, 110),
(6, 15, 'Asset name', '<div>Asset name - 3</div>', '123', '', '', 1, 1, 0, 1, 1, '2015-03-25 21:10:57', '2015-03-25 21:11:32', '2015-03-25 21:28:18', 1, 110, 110, 110),
(7, 19, 'sdfsd', 'fsdfdsfsd', 'dfgsdfsdf', '', '', 1, 1, 0, 1, 1, '2015-03-27 03:16:31', '2015-03-27 03:17:17', '0000-00-00 00:00:00', 1, 120, 120, 0),
(8, 17, 'Asset name - neu', 'This is the description.', 'UDID', '', '5656', 1, 1, 1, 1, 1, '2015-03-28 11:10:02', '2015-05-29 17:09:05', '0000-00-00 00:00:00', 3, 110, 110, 0),
(9, 17, 'Bad Car', 'Which rarely works, unless pushed', '000006', '', '', 1, 1, 1, 1, 1, '2015-03-28 14:31:45', '2015-03-28 14:54:39', '0000-00-00 00:00:00', 1, 110, 110, 0),
(10, 17, 'Unit 36', '<b>Ford Territory 36</b>', '434', '', '', 1, 1, 1, 1, 1, '2015-03-29 16:13:12', '2015-05-02 11:48:23', '0000-00-00 00:00:00', 3, 110, 110, 0),
(11, 17, 'Small Test', '<b>There is not much<br></b>in this description<br><b>Apart from&nbsp;<i>this</i></b>', '567', '', '', 1, 1, 0, 0, 1, '2015-03-29 18:36:29', '2015-04-06 14:42:49', '0000-00-00 00:00:00', 1, 110, 110, 0),
(12, 19, 'fghgfh', 'fghfghfg', '', '', '', 1, 1, 0, 1, 1, '2015-04-01 05:31:14', '2015-04-01 05:31:14', '0000-00-00 00:00:00', 1, 120, 120, 0),
(13, 17, 'demo 89', 'demo', '', '', '', 1, 1, 1, 1, 1, '2015-04-01 07:58:08', '2015-04-01 07:58:08', '0000-00-00 00:00:00', 1, 110, 110, 0),
(14, 17, 'sdsd 897', 'dfsd', '', '', '8989', 1, 1, 1, 1, 1, '2015-04-01 02:46:23', '2015-04-01 02:46:23', '0000-00-00 00:00:00', 1, 110, 110, 0),
(15, 17, 'unit 88', 'Test', '', '', '', 0, 1, 1, 0, 1, '2015-04-01 05:50:01', '2015-04-01 05:50:01', '2015-04-27 11:43:50', 2, 110, 110, 110),
(16, 17, 'Testing Location', 'Testing Location', '', '', '', 1, 1, 1, 1, 1, '2015-04-04 09:31:46', '2015-04-25 06:40:08', '0000-00-00 00:00:00', 3, 110, 110, 0),
(17, 17, 'Testing Fuel', 'Testing Fuel', '123', '', '', 1, 1, 0, 1, 1, '2015-04-12 13:39:22', '2015-04-12 13:39:22', '0000-00-00 00:00:00', 1, 110, 110, 0),
(18, 17, 'Testing Location Feature', 'testing', 'klj', '', '234234243', 1, 1, 1, 1, 1, '2015-04-13 08:53:30', '2015-04-13 08:59:52', '0000-00-00 00:00:00', 3, 110, 110, 0),
(19, 15, 'dfgdfg', 'dfgdfg', 'dfgdfg', '', '', 1, 1, 1, 1, 1, '2015-04-15 10:06:44', '2015-05-05 12:51:10', '0000-00-00 00:00:00', 6, 110, 107, 0),
(20, 17, 'sas', 'asdas', 'asd', '7f043c78af7cb7e4f5180bf399fd5da25cbc2a8c1205e621f1080cf33724a4f7', '', 0, 1, 1, 1, 1, '2015-04-15 22:18:05', '2015-04-15 22:18:05', '0000-00-00 00:00:00', 3, 110, 110, 0),
(21, 17, 'Testing Maintenance', 'Testing Maintenance', 'Testing Maintenance', '3193f44a1a00d527e1bf74e049b368ff8fd3cdcb0fad3570fe68735736411844', '', 0, 1, 1, 1, 1, '2015-04-16 10:35:49', '2015-04-16 10:35:49', '0000-00-00 00:00:00', 3, 110, 110, 0),
(22, 17, 'Asset Manager', 'Asset Manager', 'aasdf', '58e68ba67ec0dd29ac212490b7384d9ec6f114cac09efb44ecf8a8f27d589f98', '', 1, 1, 1, 1, 1, '2015-04-16 13:53:35', '2015-04-16 13:53:35', '0000-00-00 00:00:00', 3, 110, 110, 0),
(23, 17, 'sdasd', 'asdasd', 'asd', '19207c88b8fefa6858048263c675973e0b36352b6ccc7fd8c434c092051e7525', '', 1, 1, 1, 1, 1, '2015-04-17 10:08:49', '2015-04-17 10:08:49', '0000-00-00 00:00:00', 3, 110, 110, 0),
(24, 17, 'sdad', 'asdasd', 'asdad', '3e49d99e06efd866477eb177474f1ecd6ac2c56472b7b7b43e9f259bae7781bd', '', 1, 1, 1, 1, 1, '2015-04-17 17:10:32', '2015-04-17 17:10:32', '0000-00-00 00:00:00', 3, 110, 110, 0),
(25, 17, 'asda', 'asd', 'asd', '5fe16af7220c69b913276ac42f6907fe929fd503055587fcd97537a75b3b86f3', '', 1, 1, 1, 1, 1, '2015-04-17 17:11:54', '2015-04-17 17:11:54', '0000-00-00 00:00:00', 3, 110, 110, 0),
(26, 17, 'qweqq', 'weqwe', 'qew', '52531556cb0042af8124f41529b1c6e145da39aa4c6ed8e17fe70cfb7b53198b', '', 0, 0, 1, 1, 1, '2015-04-17 17:13:15', '2015-04-17 17:13:15', '0000-00-00 00:00:00', 3, 110, 110, 0),
(27, 17, 'asda', 'asd', 'asd', '74ab5f7d9a9103064816df690f1aaa82db32b7f2f6d8a56761740784bfade6e7', '', 0, 0, 1, 1, 1, '2015-04-17 17:15:38', '2015-04-17 17:15:38', '0000-00-00 00:00:00', 3, 110, 110, 0),
(28, 17, 'as', 'aS', 'as', 'aebde9fc6c3052fb681d1949e0e0840b24a7cc0be9c80b68ec18ddb3615c1e07', '', 1, 1, 1, 1, 1, '2015-04-17 17:17:10', '2015-05-22 07:50:11', '0000-00-00 00:00:00', 3, 110, 138, 0),
(29, 17, 'asd', 'asd', 'asd', 'a0953d94f996cd0fe672142573d15d5f46bea2075887270342c78432c1349448', '', 0, 0, 1, 1, 1, '2015-04-24 11:26:53', '2015-04-24 11:26:53', '0000-00-00 00:00:00', 3, 110, 110, 0),
(30, 17, 'Testing Fuel', 'Testing Fuel', 'Testing Fuel', '5c4f99ef0ac3b42f09fe48704f0a36e4d005cd69266aba8d6b4077fb704db248', '', 1, 0, 1, 1, 1, '2015-04-25 06:16:27', '2015-04-25 06:16:27', '0000-00-00 00:00:00', 3, 110, 110, 0),
(31, 17, 'Fuel2', 'Testing Fuel', '', '524eb9b493503f75482ab7d80dc53991f37392f39ee97801cddd17101c6d297b', '', 1, 1, 1, 1, 1, '2015-04-25 06:23:26', '2015-04-25 06:33:12', '0000-00-00 00:00:00', 3, 110, 110, 0),
(32, 17, 'Fuel3', 'sdf', '', '21691b05c801e3f848c8ba1220b0cd30152327ba24a7cf0abebe0724f3f79234', '', 1, 0, 1, 1, 1, '2015-04-25 07:43:12', '2015-04-25 07:43:12', '0000-00-00 00:00:00', 3, 110, 110, 0),
(33, 17, 'testing portlet', 'testing portlet', '', '3a7950a292b4f6701fd9fdada503e3dd9643b62a10a5ae6f61656b1a55e9bb3a', '', 1, 0, 1, 1, 1, '2015-04-25 08:14:37', '2015-04-25 08:17:09', '0000-00-00 00:00:00', 8, 110, 110, 0),
(34, 17, 'sdad', 'asd', 'as', '336da317bca85bbf3924df6641c404dd2ccba6cfeb7ed25543394ad32dbd2145', '', 0, 0, 0, 0, 1, '2015-04-27 10:56:50', '2015-04-27 10:56:50', '0000-00-00 00:00:00', 3, 110, 110, 0),
(35, 17, 'asd', '<div>asdasdas</div>', 'asd', 'f227e2b1c25c64ab622af68e8621278ef52d747e34794ac42379e97e4481a3d6', '', 0, 0, 0, 0, 1, '2015-04-27 10:58:57', '2015-04-27 10:58:57', '0000-00-00 00:00:00', 3, 110, 110, 0),
(36, 17, 'asD', 'ad', 'adS', 'd13f92be70e5c1a38e5885dfc82f51d45a0c1a7e70b21bbfd8feb14bcb39d2ca', '', 0, 1, 1, 0, 1, '2015-04-27 11:23:19', '2015-04-27 11:23:41', '2015-04-27 11:42:38', 3, 110, 110, 110),
(37, 17, 'unit78', 'jkhkj', 'jkhjk', '5800a3f00e74811ef38d4faa6bea3d5daddc736fb1e677d2a81f14bccda9db66', '', 0, 0, 1, 1, 1, '2015-04-27 11:43:14', '2015-04-27 11:43:14', '2015-04-27 11:43:24', 3, 110, 110, 110),
(38, 15, 'wqeqwe', 'qweqwe', 'qweqwe', '681a32ee98a536576a2436f77af3a88a746919e48a0af50264831346dcf9f2b5', '', 0, 1, 1, 1, 1, '2015-04-27 13:42:42', '2015-04-27 13:42:42', '0000-00-00 00:00:00', 10, 110, 110, 0),
(39, 17, 'demo', 'ee', 'ee', '496e53bb7f1350f0332fda65bfd6917196bedca667037bc45ce2ae340cf97af2', '', 0, 0, 1, 1, 1, '2015-04-29 11:11:50', '2015-04-29 11:11:50', '0000-00-00 00:00:00', 3, 110, 110, 0),
(40, 17, 'd', 'd', '', 'b708ee84bbb5ca2c697facce29629fb3c840fe605c5c82ef37f7118e122eae10', '', 1, 1, 1, 1, 1, '2015-04-29 15:51:24', '2015-04-29 15:51:24', '0000-00-00 00:00:00', 7, 110, 110, 0),
(41, 17, 'jjkh', 'hkjh', '', '19dba4b71ee4222a8cdf34ff779c29a660b5a41fec86a42f2b5b65631b12bece', '', 1, 0, 1, 0, 1, '2015-04-30 14:35:28', '2015-04-30 14:35:40', '0000-00-00 00:00:00', 3, 110, 110, 0),
(42, 17, 'asdf', 'asfd', '', '22f2e9e1ee2ff020be495db34c6b0bb421d3600ff679f9ab2fd2b045b9840a2a', '', 1, 1, 1, 1, 1, '2015-04-30 14:42:24', '2015-04-30 14:42:24', '0000-00-00 00:00:00', 8, 110, 110, 0),
(43, 17, 'qww', 'qwe', 'qweqwe', '1b6c9b5f343438282a7e529ab7b585c0b061c9b44681c73e48250fb50e551437', '', 0, 0, 1, 1, 1, '2015-05-04 07:47:25', '2015-05-04 07:47:25', '0000-00-00 00:00:00', 3, 110, 110, 0),
(44, 17, 'Testing -100', 'asdasd', '', '6a168dae8b3bba9e537016d44cb43d55d5245df7736bc526b472367406eddbbd', '', 0, 0, 1, 1, 1, '2015-05-04 08:07:31', '2015-05-04 08:07:31', '0000-00-00 00:00:00', 3, 110, 110, 0),
(45, 17, 'testing all', 'testing all', 'testing all', 'bdb320f2812f1d59c0ced8acf95a1fdc2f9d1e508ec330f0c6e258ef169e5b38', '', 1, 1, 1, 1, 1, '2015-05-04 09:59:04', '2015-05-04 09:59:04', '0000-00-00 00:00:00', 3, 110, 110, 0),
(46, 17, 'Demo Timezone', 'Demo', '', '54a7dba1168a0f7ef6fc08f38f8cf3e5dd47c83b08b5b48aab4e92966eeda7ee', '', 1, 1, 1, 1, 1, '2015-05-05 07:23:16', '2015-05-05 07:23:48', '0000-00-00 00:00:00', 3, 110, 110, 0),
(47, 17, 'Redirect test', 'lkj', 'hkjh', 'b71ec0ea950bee4dc93bb24a2011a9636167b1481bfa9ca68be29960c1d34a3b', '', 1, 1, 1, 0, 1, '2015-05-05 10:08:16', '2015-05-05 10:08:16', '0000-00-00 00:00:00', 9, 110, 110, 0),
(48, 17, 'Demo', 'sdasd', '', 'df271cac15521c94a14843dbc5f5d39b952ca9270450e863af4a657e55ce889a', '', 0, 0, 1, 1, 1, '2015-05-09 06:41:55', '2015-05-09 06:41:55', '0000-00-00 00:00:00', 3, 110, 110, 0),
(49, 17, 'Testing Crons', 'Testing Crons', '', '29cf4d7bcc56ae30860bdb92bf576812f01de872f441666d1e18ffe8d6b0bf85', '', 0, 1, 1, 1, 1, '2015-05-11 13:59:17', '2015-05-11 14:00:28', '0000-00-00 00:00:00', 3, 110, 110, 0),
(50, 17, 'demo cron', 'demo', '', '75f3667913d9a09b848eee5e247f598b5d5dda1253672e17ebcf7b98bdad2fc6', '', 0, 1, 1, 1, 1, '2015-05-12 07:22:36', '2015-05-12 07:22:36', '0000-00-00 00:00:00', 3, 110, 110, 0),
(51, 17, 'DEMO CRON - 2', 'sadasd', '', '1e27a6067f78e7103758056d3023a6c03cb414ae9aa9694b1dbf65ffc4d4791a', '', 1, 1, 1, 1, 1, '2015-05-12 07:27:08', '2015-05-12 07:27:08', '0000-00-00 00:00:00', 3, 110, 110, 0),
(52, 17, 'demo cron 3', 'demo', '', '11793b535033e3e45cff690ef405cf70e00732b505c7b876c741c221236cb750', '', 0, 1, 1, 1, 1, '2015-05-12 10:12:08', '2015-05-12 10:12:08', '0000-00-00 00:00:00', 3, 110, 110, 0),
(53, 17, 'Demo', 'sadasd', 'ad', '6a869e585fe10780164ed46c2c35f8688540120213ec06c7d911388328fe650d', '', 0, 1, 1, 1, 1, '2015-05-12 10:37:46', '2015-05-12 10:37:46', '0000-00-00 00:00:00', 3, 110, 110, 0),
(54, 17, 'cron - 4', 'dasd', '', 'd692f4f5d57b8d6b1b217d96536421afc3e776a2f4f50e816502e7eec8820606', '', 0, 1, 1, 1, 1, '2015-05-12 10:55:25', '2015-05-12 10:55:25', '0000-00-00 00:00:00', 3, 110, 110, 0),
(55, 17, 'cron - 5', 'asd', '', '494d485df73312a9b75b7df90403824ae3b7d0c810b2558682142197087012cc', '', 0, 1, 1, 1, 1, '2015-05-12 10:55:40', '2015-05-12 10:55:40', '0000-00-00 00:00:00', 3, 110, 110, 0),
(56, 17, 'cron - 6', 'demo', 'demo', '09689474fa079b5c033c2dbb30dd1cc91bd559bfcaedde56e848e5d17b3738e3', '', 1, 1, 1, 1, 1, '2015-05-12 10:58:57', '2015-05-12 10:58:57', '0000-00-00 00:00:00', 12, 110, 110, 0),
(57, 17, 'cron - 6', 'asd', 'asd', 'd960748c5604c7ef2da30ff1318d123077d65e934e14c4fc249b6ee9f0530286', '', 1, 1, 1, 1, 1, '2015-05-12 12:06:10', '2015-05-12 12:06:10', '0000-00-00 00:00:00', 12, 110, 110, 0),
(58, 17, 'fuel invoice testing', 'qwer', 'wqr', '20203d79ea6b87a320239a7ac428c927ba0a10c535ebd48fb4272fbf7327c0ea', '', 1, 0, 0, 0, 1, '2015-05-20 04:56:57', '2015-05-20 04:56:57', '0000-00-00 00:00:00', 8, 110, 110, 0),
(59, 17, 'testing report image', 'asdad', '', '8cc47617c427b3791b72bb72bb43e0f8f2eabfa11e057eb4123e97aff3fcd131', '', 1, 1, 1, 0, 1, '2015-05-21 09:57:09', '2015-05-21 09:57:09', '0000-00-00 00:00:00', 7, 110, 110, 0),
(60, 17, 'maintenance test', 'maintenance test', '123', '34fd7189223773c793fa207172fd2f4fb4a223af2f7a24f329323568476f90b8', '', 0, 1, 0, 0, 1, '2015-05-27 20:20:44', '2015-05-27 20:20:44', '0000-00-00 00:00:00', 9, 110, 110, 0),
(61, 17, 'maintenance test', 'w', '', 'd2a5a3a2ad7e150819b1213ffcdaae21ea537bbf4d08075630fbde2e61e379d9', '', 0, 1, 1, 1, 1, '2015-05-28 09:53:46', '2015-05-28 09:53:46', '0000-00-00 00:00:00', 3, 110, 110, 0),
(62, 17, 'Demo', 'demo', 'demo', '3fb7da40c140b43ab4d58d0cef6eaea480b746594311437ce8dba2b7d62d2b40', '', 0, 1, 0, 1, 1, '2015-05-28 15:54:33', '2015-05-28 15:54:48', '0000-00-00 00:00:00', 3, 110, 110, 0);

-- --------------------------------------------------------

--
-- Table structure for table `asset_documents`
--

CREATE TABLE IF NOT EXISTS `asset_documents` (
`asset_document_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `friendly_name` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_documents`
--

INSERT INTO `asset_documents` (`asset_document_id`, `title`, `filename`, `friendly_name`, `created_on`, `updated_on`, `asset_id`, `company_id`, `created_by`, `updated_by`) VALUES
(1, 'LandingPage.png', '0371375cb13009eae40c58c0d8199e78.png', '', '2015-03-24 18:13:55', '2015-03-24 18:13:55', 3, 17, 110, 110),
(2, 'Screen Shot 2015-03-25 at 8.39.00 pm.png', '58954a232a690735c3b0a5d68518bbc6.png', '', '2015-03-25 21:11:32', '2015-03-25 21:11:32', 6, 15, 110, 110),
(3, 'license.txt', 'a54022da6ce8256eeefb60698c7c495a.txt', '', '2015-03-26 01:38:31', '2015-03-26 01:38:31', 5, 15, 110, 110),
(5, 'zero_results_20150124_0924.csv', 'b9296f2ec40962a109f0736bbb696a47.csv', '', '2015-03-27 03:17:17', '2015-03-27 03:17:17', 7, 19, 120, 120),
(7, 'AutomationDirectissue.PNG', 'cbcc48d304324e9713090682259d1963.PNG', '', '2015-03-28 14:49:28', '2015-03-28 14:49:28', 9, 17, 110, 110),
(8, 'warrack - dodgy code.PNG', '36d002e2765f17d0dfcdbc012a73e125.PNG', '', '2015-03-28 14:50:12', '2015-03-28 14:50:12', 9, 17, 110, 110),
(9, 'Developer Report.pdf', 'd35304c40c5d48ae803639fd8fdd6da7.pdf', 'jkdgajh', '2015-03-28 15:11:36', '2015-03-28 15:11:36', 8, 17, 110, 110),
(41, 'examples.xlsx', '48e97fbe7464060a8fc5cb48602d4a16.xlsx', 'Demo', '2015-05-20 04:53:11', '2015-05-20 04:53:11', 8, 17, 110, 110),
(39, 'permissions.xlsx', 'fd27a6d1749f1b158a931721c5bd3e2e.xlsx', '', '2015-04-14 07:08:34', '2015-04-14 07:08:34', 4, 17, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `asset_expenses`
--

CREATE TABLE IF NOT EXISTS `asset_expenses` (
`asset_expense_id` int(255) NOT NULL,
  `job_number` varchar(255) NOT NULL,
  `notes` varchar(2000) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `reimburse` int(11) NOT NULL,
  `cost` decimal(12,4) NOT NULL,
  `invoice` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_expenses`
--

INSERT INTO `asset_expenses` (`asset_expense_id`, `job_number`, `notes`, `purchase_date`, `reimburse`, `cost`, `invoice`, `status`, `asset_id`, `company_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, '22', '', '2015-06-04 20:30:00', 0, '32.0000', '', 1, 42, 17, '2015-06-05 13:40:27', '2015-06-05 13:40:27', 110, 110),
(2, '24', '', '2015-06-02 20:30:00', 1, '34.0000', '5b5600af79d6fcc7db2d63eddd93570c.jpg', 1, 29, 17, '2015-06-05 13:41:34', '2015-06-05 13:41:34', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `asset_favourites`
--

CREATE TABLE IF NOT EXISTS `asset_favourites` (
  `asset_favourite_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `asset_managers`
--

CREATE TABLE IF NOT EXISTS `asset_managers` (
`asset_manager_id` int(11) NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_managers`
--

INSERT INTO `asset_managers` (`asset_manager_id`, `asset_type_id`, `company_id`, `user_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(13, 3, 17, 130, '2015-04-17 17:17:47', '2015-04-17 17:17:47', 110, 110),
(22, 9, 17, 110, '2015-05-05 10:08:01', '2015-05-05 10:08:01', 110, 110),
(17, 11, 17, 131, '2015-04-29 11:11:38', '2015-04-29 11:11:38', 110, 110),
(18, 4, 15, 107, '2015-05-04 07:19:17', '2015-05-04 07:19:17', 110, 110),
(23, 12, 17, 138, '2015-05-12 10:58:37', '2015-05-12 10:58:37', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `asset_meta`
--

CREATE TABLE IF NOT EXISTS `asset_meta` (
`asset_meta_id` int(255) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `purchase_value` decimal(12,4) NOT NULL,
  `effective_life` int(11) NOT NULL,
  `depreciation_method` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted-at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_meta`
--

INSERT INTO `asset_meta` (`asset_meta_id`, `asset_id`, `company_id`, `purchase_date`, `purchase_value`, `effective_life`, `depreciation_method`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted-at`) VALUES
(1, 29, 17, '2015-06-17 20:15:00', '23.0000', 2, 't2', 110, 110, 0, '2015-06-02 14:54:28', '2015-06-03 14:31:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `asset_photos`
--

CREATE TABLE IF NOT EXISTS `asset_photos` (
`asset_photo_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_photos`
--

INSERT INTO `asset_photos` (`asset_photo_id`, `filename`, `created_on`, `updated_on`, `asset_id`, `company_id`, `created_by`, `updated_by`) VALUES
(1, '870955e72a481dd8b785666c3d3c6757.jpg', '2015-03-28 14:54:39', '2015-03-28 14:54:39', 9, 17, 110, 110),
(3, 'c727603a2fd4b8660ec45c3b37efea6b.jpg', '2015-03-29 18:36:29', '2015-03-29 18:36:29', 11, 17, 110, 110),
(7, '7bba36990bf35f63bcfdd760a25a0bbf.jpg', '2015-04-13 08:56:47', '2015-04-13 08:56:47', 18, 17, 110, 110),
(8, '1d558be8ce1dc5592e8e45571a040204.jpg', '2015-04-15 05:22:24', '2015-04-15 05:22:24', 10, 17, 110, 110),
(9, '6fda3979f7e74bbcbb48bd78dd888856.png', '2015-04-25 05:59:27', '2015-04-25 05:59:27', 16, 17, 110, 110),
(10, '6d8672000f7e58dcec46bba5942b6c54.png', '2015-05-21 09:57:09', '2015-05-21 09:57:09', 59, 17, 110, 110),
(11, 'b954134fb60d15cb41fd15a18bd5732a.png', '2015-05-28 15:53:04', '2015-05-28 15:53:04', 10, 17, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `asset_types`
--

CREATE TABLE IF NOT EXISTS `asset_types` (
`asset_type_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `feature_fuel` tinyint(1) NOT NULL,
  `feature_maintenance` tinyint(1) NOT NULL,
  `feature_exception` tinyint(1) NOT NULL,
  `feature_location` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_types`
--

INSERT INTO `asset_types` (`asset_type_id`, `company_id`, `name`, `created_on`, `updated_on`, `created_by`, `updated_by`, `feature_fuel`, `feature_maintenance`, `feature_exception`, `feature_location`) VALUES
(1, 0, 'Car', '2015-03-19 00:00:00', '2015-03-19 00:00:00', 0, 0, 1, 1, 0, 1),
(2, 0, 'Machine', '2015-03-19 00:00:00', '2015-03-19 00:00:00', 0, 0, 0, 1, 1, 0),
(3, 17, 'Machine', '2015-03-19 00:00:00', '2015-04-17 17:17:47', 0, 110, 0, 0, 1, 1),
(4, 15, 'Car', '2015-04-15 10:01:05', '2015-05-04 07:19:17', 110, 110, 1, 1, 1, 1),
(5, 15, 'Demo', '2015-04-15 10:01:25', '2015-04-15 10:01:25', 110, 110, 1, 0, 1, 0),
(6, 15, 'rreytret', '2015-04-15 10:06:30', '2015-04-15 10:06:30', 110, 110, 1, 1, 1, 0),
(7, 17, 'de', '2015-04-24 08:55:07', '2015-04-24 08:55:07', 110, 110, 1, 0, 0, 0),
(8, 17, 'de', '2015-04-24 08:55:49', '2015-04-24 08:55:49', 110, 110, 1, 0, 0, 0),
(9, 17, 'Redirect testing', '2015-04-24 09:00:39', '2015-05-05 10:08:01', 110, 110, 0, 0, 0, 0),
(10, 15, 'Computer', '2015-04-27 13:42:26', '2015-05-02 05:55:14', 110, 107, 0, 0, 1, 1),
(11, 17, 'de', '2015-04-29 11:11:38', '2015-04-29 11:11:38', 110, 110, 1, 0, 0, 0),
(12, 17, 'testing cron', '2015-05-12 10:58:31', '2015-05-12 10:58:37', 110, 110, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `asset_valuations`
--

CREATE TABLE IF NOT EXISTS `asset_valuations` (
`asset_valuation_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `valuation_date` datetime NOT NULL,
  `valuation_comment` varchar(4000) NOT NULL,
  `valuation_amount` decimal(12,4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_valuations`
--

INSERT INTO `asset_valuations` (`asset_valuation_id`, `asset_id`, `company_id`, `valuation_date`, `valuation_comment`, `valuation_amount`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 39, 17, '2015-06-05 00:00:00', 'sd fdhg &nbsp;dfgfgds gfd gdf gfd sgfs d', '246.0000', 110, 110, 0, '2015-06-05 09:10:50', '2015-06-05 09:10:50', '0000-00-00 00:00:00'),
(2, 39, 17, '2015-06-05 00:00:00', 'One more', '23.0000', 110, 110, 0, '2015-06-05 09:35:49', '2015-06-05 09:35:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `authentications`
--

CREATE TABLE IF NOT EXISTS `authentications` (
`authentication_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `account_type` varchar(45) NOT NULL,
  `token` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authentications`
--

INSERT INTO `authentications` (`authentication_id`, `email`, `account_type`, `token`, `user_id`, `profile_pic`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(165, 'service@xyrintech.com', 'Live', '', 106, '', '2015-03-22 23:44:08', '2015-03-22 23:44:08', 106, 106),
(166, 'xyrintechnologies@gmail.com', 'Google', '', 106, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '2015-03-22 23:52:04', '2015-03-22 23:52:04', 106, 106),
(168, 'davinder17s@outlook.com', 'Live', '', 112, '', '2015-03-24 00:44:00', '2015-03-24 00:44:00', 112, 112),
(171, 'andrew.frahn@chsgroup.com.au', 'Google', '', 125, 'https://lh4.googleusercontent.com/-fXm6sfZXsOU/AAAAAAAAAAI/AAAAAAAAAHE/5_kcLS65Lik/photo.jpg', '2015-03-31 16:03:51', '2015-03-31 16:03:51', 125, 125),
(172, 'development@xyrintech.com', 'Live', '', 126, '', '2015-04-02 02:03:51', '2015-04-02 02:03:51', 126, 126),
(173, 'xyrindploy@gmail.com', 'Google', '', 130, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '2015-04-10 11:07:24', '2015-04-10 11:07:24', 130, 130),
(180, 'service@xyrintech.com', 'Facebook', '', 106, 'https://graph.facebook.com/1606911302855966/picture?width=150&height=150', '2015-04-11 20:53:01', '2015-04-11 20:53:01', 106, 106);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
`company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `cms_dashboard_notice` text NOT NULL,
  `cms_dashboard_notice_title` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `deleted_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `user_id`, `name`, `email`, `active`, `cms_dashboard_notice`, `cms_dashboard_notice_title`, `created_on`, `updated_on`, `deleted_on`, `created_by`, `updated_by`, `deleted_by`) VALUES
(14, 107, 'Xyrin Technologies', 'xyrintech@outlook.com', 1, '', '', '2015-03-22 23:45:21', '2015-03-22 23:50:22', '2015-03-22 23:50:22', 106, 106, 106),
(15, 110, 'Xyrin Technologies', 'xyrintechnologies@outlook.com', 1, '', '', '2015-03-22 23:51:19', '2015-05-05 09:34:38', '0000-00-00 00:00:00', 106, 106, 0),
(16, 110, 'Testing Company - 1', 'company@company.com', 1, '', '', '2015-03-23 00:16:11', '2015-03-23 05:46:46', '2015-03-23 05:46:46', 106, 106, 106),
(17, 110, 'Company - 3', 'company@testing.com', 1, ' <p>Please contact us at service@xyrintech.com in order to begin using this software.&nbsp;</p> ', ' Welcome to AirApp!', '2015-03-23 00:16:36', '2015-05-05 09:34:46', '0000-00-00 00:00:00', 106, 106, 0),
(18, 110, 'Deleted user', 'davinder17s@gmail.coms', 1, '', '', '2015-03-23 05:38:58', '2015-03-23 05:39:23', '2015-03-23 05:39:23', 109, 109, 109),
(19, 120, 'Prince Business', 'demohkjh@kjh.com', 1, '', '', '2015-03-27 00:03:45', '2015-04-03 07:57:30', '2015-04-03 07:57:30', 106, 106, 106),
(20, 110, 'One', 'emmertex@gmail.com', 1, '', '', '2015-03-31 17:15:18', '2015-03-31 17:15:18', '0000-00-00 00:00:00', 109, 109, 0),
(21, 125, 'CHS', 'andrew.frahn@chsgroup.com.au', 1, '', '', '2015-03-31 17:15:37', '2015-03-31 17:15:37', '0000-00-00 00:00:00', 109, 109, 0),
(22, 113, 'trterytre', 'eeretre@dfgfdg.com', 1, '', '', '2015-04-03 05:11:21', '2015-04-03 07:57:20', '2015-04-03 07:57:20', 106, 106, 106),
(23, 113, 'sfsdf', 'sdfsdf@dfg.sd', 1, '', '', '2015-04-03 05:14:10', '2015-04-03 07:57:25', '2015-04-03 07:57:25', 106, 106, 106),
(24, 129, 'Demo Company', 'demo@user.com', 1, '', '', '2015-04-08 14:09:17', '2015-04-09 05:12:04', '0000-00-00 00:00:00', 106, 106, 0),
(25, 106, 'Demo', 'servi@nklj.com', 1, '', '', '2015-04-29 11:38:19', '2015-04-29 11:40:54', '2015-04-29 11:40:54', 106, 106, 106),
(26, 106, 'Testing Script', 'testing@lklkj.com', 1, '', '', '2015-04-30 09:38:32', '2015-04-30 14:30:54', '2015-04-30 14:30:54', 106, 106, 106),
(27, 148, 'Company Demo', 'p.tintumol@yahoo.com', 1, '', '', '2015-05-06 15:32:20', '2015-05-07 06:54:14', '2015-05-07 06:54:14', 106, 106, 106),
(28, 107, 'Elance', 'te@tes.com', 1, '', '', '2015-05-19 04:18:15', '2015-05-19 04:55:58', '2015-05-19 04:55:58', 106, 106, 106);

-- --------------------------------------------------------

--
-- Table structure for table `company_admins`
--

CREATE TABLE IF NOT EXISTS `company_admins` (
`company_admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_admins`
--

INSERT INTO `company_admins` (`company_admin_id`, `user_id`, `company_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(8, 107, 15, '2015-05-04 12:13:18', '2015-05-04 12:13:18', 110, 110),
(9, 0, 17, '2015-05-04 12:52:05', '2015-05-04 12:52:05', 110, 110),
(10, 109, 17, '2015-05-04 12:53:56', '2015-05-04 12:53:56', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `company_preferences`
--

CREATE TABLE IF NOT EXISTS `company_preferences` (
`company_preference_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_preferences`
--

INSERT INTO `company_preferences` (`company_preference_id`, `theme_id`, `created_on`, `updated_on`, `company_id`, `user_id`, `created_by`, `updated_by`) VALUES
(1, 2, '2015-04-25 11:54:22', '2015-04-25 11:54:22', 17, 110, 110, 110),
(2, 2, '2015-04-27 12:22:33', '2015-04-27 12:22:33', 15, 110, 110, 110),
(3, 2, '2015-04-27 15:37:29', '2015-04-27 15:37:29', 17, 107, 107, 107),
(4, 2, '2015-04-28 11:54:46', '2015-04-28 11:54:46', 21, 125, 125, 125),
(5, 2, '2015-05-02 05:11:55', '2015-05-02 05:11:55', 15, 107, 107, 107);

-- --------------------------------------------------------

--
-- Table structure for table `cron_emails`
--

CREATE TABLE IF NOT EXISTS `cron_emails` (
`cron_email_id` int(255) NOT NULL,
  `exception_id` int(11) NOT NULL,
  `exception_note_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `cc` varchar(255) NOT NULL,
  `bcc` varchar(255) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status_sent` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cron_emails`
--

INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(39, 107, 187, 130, 'xyrindata@outlook.com', 'New comment on exception: Exception for Maintenance entry #28', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=2.1" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Xyrin Dploy,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Exception for Maintenance entry #28</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">Odometer : 100000 <br>\r\nFueled Date : May 30, 2015 <br>\r\nVolume : 12 <br>\r\nCost : 12 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/18">Testing Location Feature</a> <br>\r\n</div>\r\n                                        <div class="datetime">\r\n                                            11:00 am on May 30                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/18">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/18 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 18, 17, 0, '2015-05-30 11:00:09');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(38, 106, 186, 130, 'xyrindata@outlook.com', 'New comment on exception: Exception for Maintenance Entry #67', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=2.0" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Xyrin Dploy,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Exception for Maintenance Entry #67</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Nerd All:</div>\r\n                                        <div class="comment"><p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>22 May 2015, 10:50 pm</strong>, <strong>6</strong> days time.</p></div>\r\n                                        <div class="datetime">\r\n                                            9:57 am on May 28                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/28">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/28 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 28, 17, 0, '2015-05-28 09:57:40');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(36, 43, 185, 110, 'company@xyrintech.com', 'New comment on exception: Demo Demo', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=1.1.4" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Company Testing User,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Demo Demo</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">demo</div>\r\n                                        <div class="datetime">\r\n                                            5:53 am on May 23                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/28">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/28 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 28, 17, 0, '2015-05-23 05:53:56');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(37, 43, 185, 130, 'xyrindata@outlook.com', 'New comment on exception: Demo Demo', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=1.1.4" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Xyrin Dploy,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Demo Demo</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">demo</div>\r\n                                        <div class="datetime">\r\n                                            5:53 am on May 23                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/28">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/28 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 28, 17, 0, '2015-05-23 05:53:56');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(35, 43, 184, 130, 'xyrindata@outlook.com', 'New comment on exception: Demo Demo', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=1.1.4" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Xyrin Dploy,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Demo Demo</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">kjashdkjhasd</div>\r\n                                        <div class="datetime">\r\n                                            3:44 pm on May 22                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/28">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/28 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 28, 17, 0, '2015-05-22 15:44:20');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(33, 66, 183, 130, 'xyrindata@outlook.com', 'New comment on exception: Exception for Maintenance Entry #8', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=1.0" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Xyrin Dploy,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Exception for Maintenance Entry #8</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">test</div>\r\n                                        <div class="datetime">\r\n                                            11:48 am on May 21                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/8">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/8 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 8, 17, 0, '2015-05-21 11:48:52');
INSERT INTO `cron_emails` (`cron_email_id`, `exception_id`, `exception_note_id`, `user_id`, `to`, `subject`, `message`, `cc`, `bcc`, `asset_id`, `company_id`, `status_sent`, `created_on`) VALUES
(34, 43, 184, 110, 'company@xyrintech.com', 'New comment on exception: Demo Demo', '<html xmlns="http://www.w3.org/1999/xhtml"><head>\r\n    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n    <title>Email</title>\r\n    <!--IMPORTANT:\r\n    Before deploying this email template into your application make sure you convert all the css code in <style> tag using http://beaker.mailchimp.com/inline-css.\r\n    Chrome and other few mail clients do not support <style> tag so the above converter from mailchip will make sure that all the css code will be converted into inline css.\r\n    -->\r\n    <meta name="viewport" content="width=device-width">\r\n    <style type="text/css">\r\n        /*********************************************************************\r\n        Ink - Responsive Email Template Framework Based: http://zurb.com/ink/\r\n        *********************************************************************/\r\n        #outlook a {\r\n            padding:0;\r\n        }\r\n        body{\r\n            width:100% !important;\r\n            min-width: 100%;\r\n            -webkit-text-size-adjust:100%;\r\n            -ms-text-size-adjust:100%;\r\n            margin:0;\r\n            padding:0;\r\n        }\r\n        img {\r\n            outline:none;\r\n            text-decoration:none;\r\n            -ms-interpolation-mode: bicubic;\r\n            width: auto;\r\n            height: auto;\r\n            max-width: 100%;\r\n            float: left;\r\n            clear: both;\r\n            display: block;\r\n        }\r\n        @media screen and (min-width:0\\0) {\r\n            /* IE9 and IE10 rule sets go here */\r\n            img.ie10-responsive {\r\n                width: 100% !important;\r\n            }\r\n        }\r\n        center {\r\n            width: 100%;\r\n            min-width: 580px;\r\n        }\r\n        a img {\r\n            border: none;\r\n        }\r\n        p {\r\n            margin: 0 0 0 10px;\r\n        }\r\n        table {\r\n            border-spacing: 0;\r\n            border-collapse: collapse;\r\n        }\r\n        td {\r\n            word-break: break-word;\r\n            -webkit-hyphens: auto;\r\n            -moz-hyphens: auto;\r\n            hyphens: auto;\r\n            border-collapse: collapse !important;\r\n        }\r\n        table, tr, td {\r\n            padding: 0;\r\n            vertical-align: top;\r\n            text-align: left;\r\n        }\r\n        hr {\r\n            color: #d9d9d9;\r\n            background-color: #d9d9d9;\r\n            height: 1px;\r\n            border: none;\r\n        }\r\n        /* Responsive Grid */\r\n        table.body {\r\n            height: 100%;\r\n            width: 100%;\r\n        }\r\n        table.container {\r\n            width: 580px;\r\n            margin: 0 auto;\r\n            text-align: inherit;\r\n        }\r\n        table.row {\r\n            padding: 0px;\r\n            width: 100%;\r\n            position: relative;\r\n        }\r\n        table.container table.row {\r\n            display: block;\r\n        }\r\n        td.wrapper {\r\n            padding: 10px 20px 0px 0px;\r\n            position: relative;\r\n        }\r\n        table.columns,\r\n        table.column {\r\n            margin: 0 auto;\r\n        }\r\n        table.columns td,\r\n        table.column td {\r\n            padding: 0px 0px 10px;\r\n        }\r\n        table.columns td.sub-columns,\r\n        table.column td.sub-columns,\r\n        table.columns td.sub-column,\r\n        table.column td.sub-column {\r\n            padding-right: 10px;\r\n        }\r\n        td.sub-column, td.sub-columns {\r\n            min-width: 0px;\r\n        }\r\n        table.row td.last,\r\n        table.container td.last {\r\n            padding-right: 0px;\r\n        }\r\n        table.one { width: 30px; }\r\n        table.two { width: 80px; }\r\n        table.three { width: 130px; }\r\n        table.four { width: 180px; }\r\n        table.five { width: 230px; }\r\n        table.six { width: 280px; }\r\n        table.seven { width: 330px; }\r\n        table.eight { width: 380px; }\r\n        table.nine { width: 430px; }\r\n        table.ten { width: 480px; }\r\n        table.eleven { width: 530px; }\r\n        table.twelve { width: 580px; }\r\n        table.one center { min-width: 30px; }\r\n        table.two center { min-width: 80px; }\r\n        table.three center { min-width: 130px; }\r\n        table.four center { min-width: 180px; }\r\n        table.five center { min-width: 230px; }\r\n        table.six center { min-width: 280px; }\r\n        table.seven center { min-width: 330px; }\r\n        table.eight center { min-width: 380px; }\r\n        table.nine center { min-width: 430px; }\r\n        table.ten center { min-width: 480px; }\r\n        table.eleven center { min-width: 530px; }\r\n        table.twelve center { min-width: 580px; }\r\n        table.one .panel center { min-width: 10px; }\r\n        table.two .panel center { min-width: 60px; }\r\n        table.three .panel center { min-width: 110px; }\r\n        table.four .panel center { min-width: 160px; }\r\n        table.five .panel center { min-width: 210px; }\r\n        table.six .panel center { min-width: 260px; }\r\n        table.seven .panel center { min-width: 310px; }\r\n        table.eight .panel center { min-width: 360px; }\r\n        table.nine .panel center { min-width: 410px; }\r\n        table.ten .panel center { min-width: 460px; }\r\n        table.eleven .panel center { min-width: 510px; }\r\n        table.twelve .panel center { min-width: 560px; }\r\n        .body .columns td.one,\r\n        .body .column td.one { width: 8.333333%; }\r\n        .body .columns td.two,\r\n        .body .column td.two { width: 16.666666%; }\r\n        .body .columns td.three,\r\n        .body .column td.three { width: 25%; }\r\n        .body .columns td.four,\r\n        .body .column td.four { width: 33.333333%; }\r\n        .body .columns td.five,\r\n        .body .column td.five { width: 41.666666%; }\r\n        .body .columns td.six,\r\n        .body .column td.six { width: 50%; }\r\n        .body .columns td.seven,\r\n        .body .column td.seven { width: 58.333333%; }\r\n        .body .columns td.eight,\r\n        .body .column td.eight { width: 66.666666%; }\r\n        .body .columns td.nine,\r\n        .body .column td.nine { width: 75%; }\r\n        .body .columns td.ten,\r\n        .body .column td.ten { width: 83.333333%; }\r\n        .body .columns td.eleven,\r\n        .body .column td.eleven { width: 91.666666%; }\r\n        .body .columns td.twelve,\r\n        .body .column td.twelve { width: 100%; }\r\n        td.offset-by-one { padding-left: 50px; }\r\n        td.offset-by-two { padding-left: 100px; }\r\n        td.offset-by-three { padding-left: 150px; }\r\n        td.offset-by-four { padding-left: 200px; }\r\n        td.offset-by-five { padding-left: 250px; }\r\n        td.offset-by-six { padding-left: 300px; }\r\n        td.offset-by-seven { padding-left: 350px; }\r\n        td.offset-by-eight { padding-left: 400px; }\r\n        td.offset-by-nine { padding-left: 450px; }\r\n        td.offset-by-ten { padding-left: 500px; }\r\n        td.offset-by-eleven { padding-left: 550px; }\r\n        td.expander {\r\n            visibility: hidden;\r\n            width: 0px;\r\n            padding: 0 !important;\r\n        }\r\n        /* Alignment & Visibility Classes */\r\n        table.center, td.center {\r\n            text-align: center;\r\n        }\r\n        h1.center,\r\n        h2.center,\r\n        h3.center,\r\n        h4.center,\r\n        h5.center,\r\n        h6.center {\r\n            text-align: center;\r\n        }\r\n        span.center {\r\n            display: block;\r\n            width: 100%;\r\n            text-align: center;\r\n        }\r\n        img.center {\r\n            margin: 0 auto;\r\n            float: none;\r\n        }\r\n        /* Typography */\r\n        body, table.body, h1, h2, h3, h4, h5, h6, p, td {\r\n            color: #222222;\r\n            font-family: "Helvetica", "Arial", sans-serif;\r\n            font-weight: normal;\r\n            padding:0;\r\n            margin: 0;\r\n            text-align: left;\r\n            line-height: 1.3;\r\n        }\r\n        h1, h2, h3, h4, h5, h6 {\r\n            word-break: normal;\r\n        }\r\n        h1 {font-size: 40px;}\r\n        h2 {font-size: 36px;}\r\n        h3 {font-size: 32px;}\r\n        h4 {font-size: 28px;}\r\n        h5 {font-size: 24px;}\r\n        h6 {font-size: 20px;}\r\n        body, table.body, p, td {font-size: 14px;line-height:19px;}\r\n        p.lead, p.lede, p.leed {\r\n            font-size: 18px;\r\n            line-height:21px;\r\n        }\r\n        p {\r\n            margin-bottom: 10px;\r\n        }\r\n        small {\r\n            font-size: 10px;\r\n        }\r\n        a {\r\n            color: #2ba6cb;\r\n            text-decoration: none;\r\n        }\r\n        a:hover {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:active {\r\n            color: #2795b6 !important;\r\n        }\r\n        a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a,\r\n        h2 a,\r\n        h3 a,\r\n        h4 a,\r\n        h5 a,\r\n        h6 a {\r\n            color: #2ba6cb;\r\n        }\r\n        h1 a:active,\r\n        h2 a:active,\r\n        h3 a:active,\r\n        h4 a:active,\r\n        h5 a:active,\r\n        h6 a:active {\r\n            color: #2ba6cb !important;\r\n        }\r\n        h1 a:visited,\r\n        h2 a:visited,\r\n        h3 a:visited,\r\n        h4 a:visited,\r\n        h5 a:visited,\r\n        h6 a:visited {\r\n            color: #2ba6cb !important;\r\n        }\r\n        /* Panels */\r\n        .panel {\r\n            background: #f2f2f2;\r\n            border: 1px solid #d9d9d9;\r\n            padding: 10px !important;\r\n        }\r\n        table.radius td {\r\n            -webkit-border-radius: 3px;\r\n            -moz-border-radius: 3px;\r\n            border-radius: 3px;\r\n        }\r\n        table.round td {\r\n            -webkit-border-radius: 500px;\r\n            -moz-border-radius: 500px;\r\n            border-radius: 500px;\r\n        }\r\n        /* Outlook First */\r\n        body.outlook p {\r\n            display: inline !important;\r\n        }\r\n        /*  Media Queries */\r\n        @media only screen and (max-width: 600px) {\r\n            table[class="body"] img {\r\n                width: auto !important;\r\n                height: auto !important;\r\n            }\r\n            table[class="body"] center {\r\n                min-width: 0 !important;\r\n            }\r\n            table[class="body"] .container {\r\n                width: 95% !important;\r\n            }\r\n            table[class="body"] .row {\r\n                width: 100% !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper {\r\n                display: block !important;\r\n                padding-right: 0 !important;\r\n            }\r\n            table[class="body"] .columns,\r\n            table[class="body"] .column {\r\n                table-layout: fixed !important;\r\n                float: none !important;\r\n                width: 100% !important;\r\n                padding-right: 0px !important;\r\n                padding-left: 0px !important;\r\n                display: block !important;\r\n            }\r\n            table[class="body"] .wrapper.first .columns,\r\n            table[class="body"] .wrapper.first .column {\r\n                display: table !important;\r\n            }\r\n            table[class="body"] table.columns td,\r\n            table[class="body"] table.column td {\r\n                width: 100% !important;\r\n            }\r\n            table[class="body"] .columns td.one,\r\n            table[class="body"] .column td.one { width: 8.333333% !important; }\r\n            table[class="body"] .columns td.two,\r\n            table[class="body"] .column td.two { width: 16.666666% !important; }\r\n            table[class="body"] .columns td.three,\r\n            table[class="body"] .column td.three { width: 25% !important; }\r\n            table[class="body"] .columns td.four,\r\n            table[class="body"] .column td.four { width: 33.333333% !important; }\r\n            table[class="body"] .columns td.five,\r\n            table[class="body"] .column td.five { width: 41.666666% !important; }\r\n            table[class="body"] .columns td.six,\r\n            table[class="body"] .column td.six { width: 50% !important; }\r\n            table[class="body"] .columns td.seven,\r\n            table[class="body"] .column td.seven { width: 58.333333% !important; }\r\n            table[class="body"] .columns td.eight,\r\n            table[class="body"] .column td.eight { width: 66.666666% !important; }\r\n            table[class="body"] .columns td.nine,\r\n            table[class="body"] .column td.nine { width: 75% !important; }\r\n            table[class="body"] .columns td.ten,\r\n            table[class="body"] .column td.ten { width: 83.333333% !important; }\r\n            table[class="body"] .columns td.eleven,\r\n            table[class="body"] .column td.eleven { width: 91.666666% !important; }\r\n            table[class="body"] .columns td.twelve,\r\n            table[class="body"] .column td.twelve { width: 100% !important; }\r\n            table[class="body"] td.offset-by-one,\r\n            table[class="body"] td.offset-by-two,\r\n            table[class="body"] td.offset-by-three,\r\n            table[class="body"] td.offset-by-four,\r\n            table[class="body"] td.offset-by-five,\r\n            table[class="body"] td.offset-by-six,\r\n            table[class="body"] td.offset-by-seven,\r\n            table[class="body"] td.offset-by-eight,\r\n            table[class="body"] td.offset-by-nine,\r\n            table[class="body"] td.offset-by-ten,\r\n            table[class="body"] td.offset-by-eleven {\r\n                padding-left: 0 !important;\r\n            }\r\n            table[class="body"] table.columns td.expander {\r\n                width: 1px !important;\r\n            }\r\n        }\r\n    </style>\r\n    <style>\r\n        /**************************************************************\r\n        * Custom Styles *\r\n        ***************************************************************/\r\n        /***\r\n        Reset & Typography\r\n        ***/\r\n        body {\r\n            direction: ltr;\r\n            background: #f6f8f1;\r\n        }\r\n        a:hover {\r\n            text-decoration: underline;\r\n        }\r\n        h1 {font-size: 34px;}\r\n        h2 {font-size: 30px;}\r\n        h3 {font-size: 26px;}\r\n        h4 {font-size: 22px;}\r\n        h5 {font-size: 18px;}\r\n        h6 {font-size: 16px;}\r\n        h4, h3, h2, h1 {\r\n            display: block;\r\n            margin: 5px 0 15px 0;\r\n        }\r\n        h7, h6, h5 {\r\n            display: block;\r\n            margin: 5px 0 5px 0 !important;\r\n        }\r\n        /***\r\n        Buttons\r\n        ***/\r\n        .btn td {\r\n            background: #e5e5e5 !important;\r\n            border: 0;\r\n            font-family: "Segoe UI", Helvetica, Arial, sans-serif;\r\n            font-size: 14px;\r\n            padding: 7px 14px !important;\r\n            color: #333333 !important;\r\n            text-align: center;\r\n            vertical-align: middle;\r\n        }\r\n        .btn td a {\r\n            display: block;\r\n            color: #fff;\r\n        }\r\n        .btn td a:hover,\r\n        .btn td a:focus,\r\n        .btn td a:active {\r\n            color: #fff !important;\r\n            text-decoration: none;\r\n        }\r\n        .btn td:hover,\r\n        .btn td:focus,\r\n        .btn td:active {\r\n            background: #d8d8d8 !important;\r\n        }\r\n        /*  Yellow */\r\n        .btn.yellow td {\r\n            background: #ffb848 !important;\r\n        }\r\n        .btn.yellow td:hover,\r\n        .btn.yellow td:focus,\r\n        .btn.yellow td:active {\r\n            background: #eca22e !important;\r\n        }\r\n        .btn.red td{\r\n            background: #d84a38 !important;\r\n        }\r\n        .btn.red td:hover,\r\n        .btn.red td:focus,\r\n        .btn.red td:active {\r\n            background: #bb2413 !important;\r\n        }\r\n        .btn.green td {\r\n            background: #35aa47 !important;\r\n        }\r\n        .btn.green td:hover,\r\n        .btn.green td:focus,\r\n        .btn.green td:active {\r\n            background: #1d943b !important;\r\n        }\r\n        /*  Blue */\r\n        .btn.blue td {\r\n            background: #4d90fe !important;\r\n        }\r\n        .btn.blue td:hover,\r\n        .btn.blue td:focus,\r\n        .btn.blue td:active {\r\n            background: #0362fd !important;\r\n        }\r\n        .template-label {\r\n            color: #ffffff;\r\n            font-weight: bold;\r\n            font-size: 11px;\r\n        }\r\n        /***\r\n        Note Panels\r\n        ***/\r\n        .note .panel {\r\n            padding: 10px !important;\r\n            background: #ECF8FF;\r\n            border: 0;\r\n        }\r\n        /***\r\n        Header\r\n        ***/\r\n        .page-header {\r\n            width: 100%;\r\n            background: #1f1f1f;\r\n        }\r\n        /***\r\n        Social Icons\r\n        ***/\r\n        .social-icons {\r\n            float: right;\r\n        }\r\n        .social-icons td {\r\n            padding: 0 2px !important;\r\n            width: auto !important;\r\n        }\r\n        .social-icons td:last-child {\r\n            padding-right: 0 !important;\r\n        }\r\n        .social-icons td img {\r\n            max-width: none !important;\r\n        }\r\n        /***\r\n        Content\r\n        ***/\r\n        table.container.content > tbody > tr > td{\r\n            background: #fff;\r\n            padding: 15px !important;\r\n        }\r\n        /***\r\n        Footer\r\n        ***/\r\n        .page-footer  {\r\n            width: 100%;\r\n            background: #2f2f2f;\r\n        }\r\n        .page-footer td {\r\n            vertical-align: middle;\r\n            color: #fff;\r\n        }\r\n        /***\r\n        Content devider\r\n        ***/\r\n        .devider {\r\n            border-bottom: 1px solid #eee;\r\n            margin: 15px -15px;\r\n            display: block;\r\n        }\r\n        /***\r\n        Media Item\r\n        ***/\r\n        .media-item img {\r\n            display: block !important;\r\n            float: none;\r\n            margin-bottom: 10px;\r\n        }\r\n        .vertical-middle {\r\n            padding-top: 0;\r\n            padding-bottom: 0;\r\n            vertical-align: middle;\r\n        }\r\n        /***\r\n        Utils\r\n        ***/\r\n        .align-reverse {\r\n            text-align: right;\r\n        }\r\n        .border {\r\n            border: 1px solid red;\r\n        }\r\n        .hidden-mobile {\r\n            display: block;\r\n        }\r\n        .visible-mobile {\r\n            display: none;\r\n        }\r\n        @media only screen and (max-width: 600px) {\r\n            /***\r\n            Reset & Typography\r\n            ***/\r\n            body {\r\n                background: #fff;\r\n            }\r\n            h1 {font-size: 30px;}\r\n            h2 {font-size: 26px;}\r\n            h3 {font-size: 22px;}\r\n            h4 {font-size: 20px;}\r\n            h5 {font-size: 16px;}\r\n            h6 {font-size: 14px;}\r\n            /***\r\n            Content\r\n            ***/\r\n            table.container.content > tbody > tr > td{\r\n                padding: 0px !important;\r\n            }\r\n            table[class="body"] table.columns .social-icons td {\r\n                width: auto !important;\r\n            }\r\n            /***\r\n            Header\r\n            ***/\r\n            .header {\r\n                padding: 10px !important;\r\n            }\r\n            /***\r\n            Content devider\r\n            ***/\r\n            .devider {\r\n                margin: 15px 0;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .media-item {\r\n                border-bottom: 1px solid #eee;\r\n                padding: 15px 0 !important;\r\n            }\r\n            /***\r\n            Media Item\r\n            ***/\r\n            .hidden-mobile {\r\n                display: none;\r\n            }\r\n            .visible-mobile {\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .comment-box {\r\n            margin-bottom: 9px;\r\n            margin-top: 16px;\r\n            padding-left: 5px;\r\n        }\r\n        .comment-box .username {\r\n            font-weight: 700;\r\n            color: #116DA5;\r\n            font-size: 14px;\r\n            font-family: sans-serif;\r\n        }\r\n        .comment-box .comment {\r\n            font-size: 13px;\r\n            color: #666;\r\n            font-weight: normal;\r\n            font-family: sans-serif;\r\n            margin: 5px 0;\r\n        }\r\n        .comment-box .datetime {\r\n            font-size: 11px;\r\n            color: #888;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<table class="body">\r\n    <tbody><tr>\r\n        <td class="center" align="center" valign="top">\r\n            <!-- BEGIN: Header -->\r\n            <table class="page-header" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <!-- BEGIN: Header Container -->\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <table class="row ">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper vertical-middle">\r\n                                                <!-- BEGIN: Logo -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            <a href="http://airapp.xyrintechnologies.com/">\r\n                                                                <img src="http://airapp.xyrintechnologies.com/public/img/logo-big.png?v=1.1.4" width="86" height="14" border="0" alt="">\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Logo -->\r\n                                            </td>\r\n                                            <td class="wrapper vertical-middle last">\r\n                                                <!-- BEGIN: Social Icons -->\r\n                                                <table class="six columns">\r\n                                                    <tbody><tr>\r\n                                                        <td style="height:40px">\r\n\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                                <!-- END: Social Icons -->\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                        <!-- END: Header Container -->\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Header -->\r\n            <!-- BEGIN: Content -->\r\n            <table class="container content" align="center">\r\n                <tbody><tr>\r\n                    <td>\r\n                        <table class="row note">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <h4>New exception comment received!</h4>\r\n                                    <p>\r\n                                        Hello Company Testing User,\r\n                                        </p>\r\n                                    <hr/>\r\n                                    <p>\r\n                                        You have received a new comment on a exception:\r\n                                        <strong>Demo Demo</strong>\r\n                                    </p>\r\n                                    <div class="comment-box">\r\n                                        <div class="username">Company Testing User:</div>\r\n                                        <div class="comment">kjashdkjhasd</div>\r\n                                        <div class="datetime">\r\n                                            3:44 pm on May 22                                        </div>\r\n                                    </div>\r\n                                    <br/>\r\n                                    <p>\r\n\r\n                                        Please click the following URL to read more:\r\n                                    </p>\r\n                                    <!-- BEGIN: Note Panel -->\r\n                                    <table class="twelve columns" style="margin-bottom: 10px">\r\n                                        <tbody><tr>\r\n                                            <td class="panel">\r\n                                                <a href="http://airapp.xyrintechnologies.com/cadmin/assets/view/28">\r\n                                                    http://airapp.xyrintechnologies.com/cadmin/assets/view/28 </a>\r\n                                            </td>\r\n                                            <td class="expander">\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <p>\r\n                                        If clicking the URL above does not work, copy and paste the URL into a browser window.\r\n                                    </p>\r\n                                    <!-- END: Note Panel -->\r\n\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n				<span class="devider">\r\n				</span>\r\n                        <table class="row">\r\n                            <tbody><tr>\r\n                                <td class="wrapper last">\r\n                                    <!-- BEGIN: Disscount Content -->\r\n\r\n                                    <!-- END: Disscount Content -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Content -->\r\n            <!-- BEGIN: Footer -->\r\n            <table class="page-footer" align="center">\r\n                <tbody><tr>\r\n                    <td class="center" align="center">\r\n                        <table class="container" align="center">\r\n                            <tbody><tr>\r\n                                <td>\r\n                                    <!-- BEGIN: Unsubscribet -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper last">\r\n								<span style="font-size:12px;">\r\n								<i>This ia a system generated email and reply is not required.</i>\r\n								</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Unsubscribe -->\r\n                                    <!-- BEGIN: Footer Panel -->\r\n                                    <table class="row">\r\n                                        <tbody><tr>\r\n                                            <td class="wrapper">\r\n                                                <table class="eight columns">\r\n                                                    <tbody><tr>\r\n                                                        <td class="vertical-middle">\r\n                                                            &copy; 2015 All Rights Reserved.\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                            <td class="wrapper last">\r\n                                                <table class="four columns">\r\n                                                    <tbody><tr>\r\n\r\n                                                    </tr>\r\n                                                    </tbody></table>\r\n                                            </td>\r\n                                        </tr>\r\n                                        </tbody></table>\r\n                                    <!-- END: Footer Panel List -->\r\n                                </td>\r\n                            </tr>\r\n                            </tbody></table>\r\n                    </td>\r\n                </tr>\r\n                </tbody></table>\r\n            <!-- END: Footer -->\r\n        </td>\r\n    </tr>\r\n    </tbody></table>\r\n\r\n</body></html>\r\n', '', '', 28, 17, 0, '2015-05-22 15:44:20');

-- --------------------------------------------------------

--
-- Table structure for table `device_json_payload`
--

CREATE TABLE IF NOT EXISTS `device_json_payload` (
`device_json_payload_id` int(11) NOT NULL,
  `device_udid` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `json_payload` varchar(6000) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `device_json_payload`
--

INSERT INTO `device_json_payload` (`device_json_payload_id`, `device_udid`, `type`, `json_payload`, `created_on`) VALUES
(1, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 12:57:08'),
(2, '78687678', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":78687678}', '2015-04-06 12:57:35'),
(3, '9.82731983719e24', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":9827319837189379813789173}', '2015-04-06 12:57:43'),
(4, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJHKagsdkhj}', '2015-04-06 12:57:51'),
(5, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJHKagsdkhj}', '2015-04-06 12:59:40'),
(6, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJHKagsdkhj}', '2015-04-06 12:59:43'),
(7, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJkhhksjhHKagsdkhj}', '2015-04-06 12:59:51'),
(8, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJkhhksjhHKagsdkhj}', '2015-04-06 06:00:10'),
(9, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJkhhksjhHKagsdkhj}', '2015-04-06 13:00:31'),
(10, '', '', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":jasGDJkhhksjhHKagsdkhj}', '2015-04-06 13:05:37'),
(11, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 13:05:52'),
(12, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 13:59:18'),
(13, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 14:09:13'),
(14, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 14:09:17'),
(15, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 14:09:19'),
(16, '123456', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":123456}', '2015-04-06 14:12:07'),
(17, '', '', '', '2015-04-06 07:15:37'),
(18, '5656', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":5656}', '2015-04-06 14:17:00'),
(19, '5656', 'location', '{"_type": "location", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":5656}', '2015-04-06 14:17:44'),
(20, '5656', 'test', '{"_type": "test", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":5656}', '2015-04-06 14:18:19'),
(21, '44548', 'location', '{"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid": "44548"}', '2015-04-06 14:24:38'),
(22, '8989', 'test', '{"_type": "test", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":8989}', '2015-04-07 06:11:39'),
(23, '8989', 'location', '{"_type": "location", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":8989}', '2015-04-07 06:11:52'),
(24, '8989', '', '{ "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":8989}', '2015-04-07 06:14:34'),
(25, '8989', 'location', '{"_type": "location", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":8989}', '2015-04-07 06:15:00'),
(26, '8989', 'location', '{"_type": "location", "lat": "-35.7277507", "lon": "141.1988267", "tst": "1427854340", "acc": "20.0", "device_udid":8989}', '2015-04-07 06:17:27'),
(27, '', '', '', '2015-04-07 08:28:50'),
(28, '', '', '', '2015-04-08 04:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `exceptions`
--

CREATE TABLE IF NOT EXISTS `exceptions` (
`exception_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` varchar(45) NOT NULL,
  `closed_datetime` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_user_id` int(11) NOT NULL,
  `closed_user_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exceptions`
--

INSERT INTO `exceptions` (`exception_id`, `title`, `status`, `closed_datetime`, `created_on`, `updated_on`, `created_user_id`, `closed_user_id`, `asset_id`, `company_id`, `created_by`, `updated_by`) VALUES
(1, 'Broken Headlight', 'new', '0000-00-00 00:00:00', '2015-03-31 15:23:41', '2015-03-31 15:23:41', 110, 110, 10, 17, 110, 110),
(9, 'adsasd', 'new', '0000-00-00 00:00:00', '2015-04-02 13:23:13', '2015-04-02 13:23:13', 110, 110, 4, 17, 110, 110),
(12, 'asdad', 'new', '0000-00-00 00:00:00', '2015-04-02 13:23:33', '2015-04-02 13:23:33', 110, 110, 4, 17, 110, 110),
(15, 'asdad', 'new', '0000-00-00 00:00:00', '2015-04-02 13:23:50', '2015-04-02 13:23:50', 110, 110, 4, 17, 110, 110),
(18, 'adsad', 'new', '0000-00-00 00:00:00', '2015-04-02 13:24:08', '2015-04-02 13:24:08', 110, 110, 4, 17, 110, 110),
(22, 'demo', 'closed', '0000-00-00 00:00:00', '2015-04-04 00:43:00', '2015-04-04 00:43:00', 110, 110, 8, 17, 110, 110),
(24, 'Exception for Fuel Entry #11', 'new', '0000-00-00 00:00:00', '2015-04-06 09:29:12', '2015-04-06 09:29:12', 110, 110, 4, 17, 110, 110),
(25, 'Exception for Maintenance Entry #16', 'new', '0000-00-00 00:00:00', '2015-04-06 12:01:20', '2015-04-06 12:01:20', 110, 110, 4, 17, 110, 110),
(26, 'Exception for Maintenance Entry #4', 'new', '0000-00-00 00:00:00', '2015-04-06 07:16:37', '2015-04-06 07:16:37', 110, 110, 8, 17, 110, 110),
(27, 'Something Else', 'in_progress', '0000-00-00 00:00:00', '2015-04-06 14:27:53', '2015-04-06 14:27:53', 110, 110, 10, 17, 110, 110),
(28, 'Exception 3', 'new', '0000-00-00 00:00:00', '2015-04-06 19:24:46', '2015-04-06 19:24:46', 110, 110, 10, 17, 110, 110),
(29, 'demo', 'new', '0000-00-00 00:00:00', '2015-04-08 01:30:43', '2015-04-08 01:30:43', 107, 107, 4, 17, 107, 107),
(30, 'Exception for Maintenance Entry #18', 'new', '0000-00-00 00:00:00', '2015-04-12 13:38:37', '2015-04-12 13:38:37', 110, 110, 4, 17, 110, 110),
(31, 'demo', 'new', '0000-00-00 00:00:00', '2015-04-13 12:46:47', '2015-04-13 12:46:47', 110, 110, 4, 17, 110, 110),
(32, 'demo', 'new', '0000-00-00 00:00:00', '2015-04-13 09:00:08', '2015-04-13 09:00:08', 110, 110, 18, 17, 110, 110),
(33, 'Exception for Maintenance Entry #22', 'new', '0000-00-00 00:00:00', '2015-04-15 06:02:25', '2015-04-15 06:02:25', 110, 110, 4, 17, 110, 110),
(34, 'asd', 'new', '0000-00-00 00:00:00', '2015-04-16 13:53:53', '2015-04-16 13:53:53', 110, 110, 22, 17, 110, 110),
(35, 'sad', 'new', '0000-00-00 00:00:00', '2015-04-16 06:58:14', '2015-04-16 06:58:14', 110, 110, 22, 17, 110, 110),
(36, 'ASP', 'new', '0000-00-00 00:00:00', '2015-04-17 00:31:48', '2015-04-17 00:31:48', 110, 110, 22, 17, 110, 110),
(37, 'sd', 'new', '0000-00-00 00:00:00', '2015-04-17 00:52:27', '2015-04-17 00:52:27', 110, 110, 22, 17, 110, 110),
(38, 'asdasd', 'new', '0000-00-00 00:00:00', '2015-04-17 17:10:46', '2015-04-17 17:10:46', 110, 110, 24, 17, 110, 110),
(39, 'sadasd', 'new', '0000-00-00 00:00:00', '2015-04-17 17:12:07', '2015-04-17 17:12:07', 110, 110, 25, 17, 110, 110),
(40, 'qwqe', 'new', '0000-00-00 00:00:00', '2015-04-17 17:13:25', '2015-04-17 17:13:25', 110, 110, 26, 17, 110, 110),
(41, 'asdadsad', 'new', '0000-00-00 00:00:00', '2015-04-17 17:15:50', '2015-04-17 17:15:50', 110, 110, 27, 17, 110, 110),
(42, 'asdf', 'new', '0000-00-00 00:00:00', '2015-04-17 17:17:21', '2015-04-17 17:17:21', 110, 110, 28, 17, 110, 110),
(43, 'Demo Demo', 'new', '0000-00-00 00:00:00', '2015-04-17 17:18:01', '2015-04-17 17:18:01', 110, 110, 28, 17, 110, 110),
(44, 'Exception for Maintenance Entry #20', 'new', '0000-00-00 00:00:00', '2015-04-18 22:06:00', '2015-04-18 22:06:00', 110, 110, 8, 17, 110, 110),
(45, 'Exception for Maintenance Entry #21', 'new', '0000-00-00 00:00:00', '2015-04-19 22:43:57', '2015-04-19 22:43:57', 110, 110, 10, 17, 110, 110),
(46, 'kjhjk', 'new', '0000-00-00 00:00:00', '2015-04-22 09:02:24', '2015-04-22 09:02:24', 110, 110, 10, 17, 110, 110),
(47, 'qweqwe', 'new', '0000-00-00 00:00:00', '2015-04-22 09:02:42', '2015-04-22 09:02:42', 110, 110, 10, 17, 110, 110),
(48, 'demo', 'new', '0000-00-00 00:00:00', '2015-04-25 06:18:52', '2015-04-25 06:18:52', 110, 110, 30, 17, 110, 110),
(49, 'demo - 2', 'new', '0000-00-00 00:00:00', '2015-04-25 06:20:21', '2015-04-25 06:20:21', 110, 110, 30, 17, 110, 110),
(50, 'hjh', 'new', '0000-00-00 00:00:00', '2015-04-25 06:33:37', '2015-04-25 06:33:37', 110, 110, 31, 17, 110, 110),
(51, 'demo - 2', 'new', '0000-00-00 00:00:00', '2015-04-25 08:15:45', '2015-04-25 08:15:45', 110, 110, 33, 17, 110, 110),
(52, 'Exception for Maintenance Entry #29', 'new', '0000-00-00 00:00:00', '2015-04-27 11:40:40', '2015-04-27 11:40:40', 110, 110, 18, 17, 110, 110),
(53, 'Exception for Maintenance Entry #27', 'new', '0000-00-00 00:00:00', '2015-05-01 05:24:09', '2015-05-01 05:24:09', 110, 110, 8, 17, 110, 110),
(54, 'Exception for Maintenance Entry #27', 'new', '0000-00-00 00:00:00', '2015-05-01 05:24:09', '2015-05-01 05:24:09', 110, 110, 8, 17, 110, 110),
(55, 'qwe', 'new', '0000-00-00 00:00:00', '2015-05-04 07:48:04', '2015-05-04 07:48:04', 110, 110, 43, 17, 110, 110),
(56, 'asd', 'new', '0000-00-00 00:00:00', '2015-05-04 08:07:51', '2015-05-04 08:07:51', 110, 110, 44, 17, 110, 110),
(57, '12', 'new', '0000-00-00 00:00:00', '2015-05-04 10:00:23', '2015-05-04 10:00:23', 110, 110, 45, 17, 110, 110),
(58, 'demo', 'new', '0000-00-00 00:00:00', '2015-05-05 07:26:24', '2015-05-05 07:26:24', 110, 110, 46, 17, 110, 110),
(59, 'lkjlkj', 'new', '0000-00-00 00:00:00', '2015-05-05 10:08:26', '2015-05-05 10:08:26', 110, 110, 47, 17, 110, 110),
(60, 'asd', 'new', '0000-00-00 00:00:00', '2015-05-09 06:42:07', '2015-05-09 06:42:07', 110, 110, 48, 17, 110, 110),
(61, 'Exception for Maintenance Entry #6', 'new', '0000-00-00 00:00:00', '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110, 9, 17, 110, 110),
(62, 'Exception for Maintenance Entry #24', 'new', '0000-00-00 00:00:00', '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110, 21, 17, 110, 110),
(63, 'Exception for Maintenance Entry #25', 'new', '0000-00-00 00:00:00', '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110, 21, 17, 110, 110),
(64, 'Exception for Maintenance Entry #7', 'new', '0000-00-00 00:00:00', '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110, 9, 17, 110, 110),
(65, 'Exception for Maintenance Entry #17', 'new', '0000-00-00 00:00:00', '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110, 4, 17, 110, 110),
(66, 'Exception for Maintenance Entry #8', 'new', '0000-00-00 00:00:00', '2015-05-11 13:51:33', '2015-05-11 13:51:33', 110, 110, 8, 17, 110, 110),
(67, 'Exception for Maintenance Entry #23', 'new', '0000-00-00 00:00:00', '2015-05-11 13:53:48', '2015-05-11 13:53:48', 110, 110, 21, 17, 110, 110),
(68, 'Exception for Maintenance Entry #30', 'new', '0000-00-00 00:00:00', '2015-05-11 14:01:53', '2015-05-11 14:01:53', 110, 110, 38, 15, 110, 110),
(69, 'Exception for Maintenance Entry #37', 'new', '0000-00-00 00:00:00', '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110, 49, 17, 110, 110),
(70, 'Exception for Maintenance Entry #38', 'new', '0000-00-00 00:00:00', '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110, 49, 17, 110, 110),
(71, 'Exception for Maintenance Entry #39', 'new', '0000-00-00 00:00:00', '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110, 49, 17, 110, 110),
(72, 'Exception for Maintenance Entry #36', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 10, 17, 110, 110),
(73, 'Exception for Maintenance Entry #32', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 18, 17, 110, 110),
(74, 'Exception for Maintenance Entry #34', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 45, 17, 110, 110),
(75, 'Exception for Maintenance Entry #35', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 46, 17, 110, 110),
(76, 'Exception for Maintenance Entry #40', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 49, 17, 110, 110),
(77, 'Exception for Maintenance Entry #48', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 52, 17, 110, 110),
(78, 'Exception for Maintenance Entry #47', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 51, 17, 110, 110),
(79, 'Exception for Maintenance Entry #49', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 52, 17, 110, 110),
(80, 'Exception for Maintenance Entry #50', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 52, 17, 110, 110),
(81, 'Exception for Maintenance Entry #51', 'new', '0000-00-00 00:00:00', '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110, 52, 17, 110, 110),
(82, 'Exception for Maintenance Entry #33', 'new', '0000-00-00 00:00:00', '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110, 10, 17, 110, 110),
(83, 'Exception for Maintenance Entry #52', 'new', '0000-00-00 00:00:00', '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110, 53, 17, 110, 110),
(84, 'Exception for Maintenance Entry #31', 'new', '0000-00-00 00:00:00', '2015-05-12 10:39:22', '2015-05-12 10:39:22', 110, 110, 10, 17, 110, 110),
(85, 'Exception for Maintenance Entry #15', 'new', '0000-00-00 00:00:00', '2015-05-12 10:39:31', '2015-05-12 10:39:31', 110, 110, 10, 17, 110, 110),
(86, 'Exception for Maintenance Entry #19', 'new', '0000-00-00 00:00:00', '2015-05-12 10:39:33', '2015-05-12 10:39:33', 110, 110, 10, 17, 110, 110),
(87, 'Exception for Maintenance Entry #53', 'new', '0000-00-00 00:00:00', '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110, 53, 17, 110, 110),
(88, 'Exception for Maintenance Entry #55', 'new', '0000-00-00 00:00:00', '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110, 53, 17, 110, 110),
(89, 'Exception for Maintenance Entry #56', 'new', '0000-00-00 00:00:00', '2015-05-12 10:51:23', '2015-05-12 10:51:23', 110, 110, 53, 17, 110, 110),
(90, 'Exception for Maintenance Entry #54', 'new', '0000-00-00 00:00:00', '2015-05-12 10:51:23', '2015-05-12 10:51:23', 110, 110, 53, 17, 110, 110),
(91, 'Exception for Maintenance Entry #57', 'new', '0000-00-00 00:00:00', '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110, 54, 17, 110, 110),
(92, 'Exception for Maintenance Entry #58', 'new', '0000-00-00 00:00:00', '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110, 55, 17, 110, 110),
(93, 'Exception for Maintenance Entry #59', 'new', '0000-00-00 00:00:00', '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110, 56, 17, 110, 110),
(94, 'Exception for Maintenance Entry #60', 'new', '0000-00-00 00:00:00', '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110, 56, 17, 110, 110),
(95, 'Exception for Maintenance Entry #61', 'new', '0000-00-00 00:00:00', '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110, 56, 17, 110, 110),
(96, 'Exception for Maintenance Entry #62', 'new', '0000-00-00 00:00:00', '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110, 56, 17, 110, 110),
(97, 'Exception for Maintenance Entry #46', 'new', '0000-00-00 00:00:00', '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110, 51, 17, 110, 110),
(98, 'Exception for Maintenance Entry #65', 'new', '0000-00-00 00:00:00', '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110, 57, 17, 110, 110),
(99, 'Exception for Maintenance Entry #66', 'new', '0000-00-00 00:00:00', '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110, 57, 17, 110, 110),
(100, 'Exception for Maintenance Entry #41', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 50, 17, 110, 110),
(101, 'Exception for Maintenance Entry #63', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 57, 17, 110, 110),
(102, 'Exception for Maintenance Entry #42', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 50, 17, 110, 110),
(103, 'Exception for Maintenance Entry #64', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 57, 17, 110, 110),
(104, 'Exception for Maintenance Entry #43', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 50, 17, 110, 110),
(105, 'Exception for Maintenance Entry #44', 'new', '0000-00-00 00:00:00', '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110, 50, 17, 110, 110),
(106, 'Exception for Maintenance Entry #67', 'new', '0000-00-00 00:00:00', '2015-05-28 09:57:39', '2015-05-28 09:57:39', 138, 138, 28, 17, 138, 138),
(107, 'Exception for Maintenance entry #28', 'new', '0000-00-00 00:00:00', '2015-05-30 11:00:08', '2015-05-30 11:00:08', 110, 110, 18, 17, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `exception_notes`
--

CREATE TABLE IF NOT EXISTS `exception_notes` (
`exception_note_id` int(11) NOT NULL,
  `comments` mediumtext NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `attachment_name` varchar(255) NOT NULL,
  `exception_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exception_notes`
--

INSERT INTO `exception_notes` (`exception_note_id`, `comments`, `attachment`, `attachment_name`, `exception_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Broken Headlight', '', '', 1, '2015-03-31 15:23:41', '2015-03-31 15:23:41', 110, 110),
(6, 'test', '2ada6403dde7e58fc5ef6b8eeb2ccbf7.png', 'Untitled.png', 1, '2015-04-01 06:15:59', '2015-04-01 06:15:59', 110, 110),
(14, 'asdasd', '', '', 9, '2015-04-02 13:23:13', '2015-04-02 13:23:13', 110, 110),
(17, 'adad', '', '', 12, '2015-04-02 13:23:33', '2015-04-02 13:23:33', 110, 110),
(20, 'adadad', '', '', 15, '2015-04-02 13:23:50', '2015-04-02 13:23:50', 110, 110),
(23, 'adsad', '', '', 18, '2015-04-02 13:24:08', '2015-04-02 13:24:08', 110, 110),
(31, 'demo', '', '', 22, '2015-04-04 00:43:00', '2015-04-04 00:43:00', 110, 110),
(39, 'Odometer : 100 <br>\r\nFueled Date : Apr 06, 2015 <br>\r\nVolume : 200 <br>\r\nCost : 20000 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/4">Asset Name Neoo 78</a> <br>\r\n', '', '', 24, '2015-04-06 09:29:12', '2015-04-06 09:29:12', 110, 110),
(40, '<p class=''text-danger''>Maintenance is scheduled on 101 Km, But current odometer reading is 200</p>', '', '', 25, '2015-04-06 12:01:20', '2015-04-06 12:01:20', 110, 110),
(41, '<p class=''text-danger''>Maintenance is scheduled on 12 Km, But current odometer reading is 2147483647</p>', '', '', 26, '2015-04-06 07:16:37', '2015-04-06 07:16:37', 110, 110),
(42, 'Happened', '', '', 27, '2015-04-06 14:27:53', '2015-04-06 14:27:53', 110, 110),
(43, 'okujd', '', '', 27, '2015-04-06 19:22:43', '2015-04-06 19:22:43', 110, 110),
(44, 'ty', '', '', 28, '2015-04-06 19:24:46', '2015-04-06 19:24:46', 110, 110),
(45, 'Plokij', '', '', 27, '2015-04-06 19:26:55', '2015-04-06 19:26:55', 110, 110),
(46, 'dgfhj', '', '', 27, '2015-04-06 19:27:06', '2015-04-06 19:27:06', 110, 110),
(47, 'demo', '', '', 9, '2015-04-08 08:09:41', '2015-04-08 08:09:41', 107, 107),
(48, 'Demo', '', '', 9, '2015-04-08 01:11:11', '2015-04-08 01:11:11', 107, 107),
(49, 'Demo', '', '', 9, '2015-04-08 01:11:19', '2015-04-08 01:11:19', 107, 107),
(50, 'Demno', '', '', 9, '2015-04-08 01:11:46', '2015-04-08 01:11:46', 107, 107),
(51, 'demo', '', '', 29, '2015-04-08 01:30:43', '2015-04-08 01:30:43', 107, 107),
(52, 'Hello', '', '', 9, '2015-04-08 04:15:08', '2015-04-08 04:15:08', 107, 107),
(53, 'demo', '', '', 24, '2015-04-11 08:07:50', '2015-04-11 08:07:50', 110, 110),
(54, 'demo', '', '', 9, '2015-04-11 09:08:16', '2015-04-11 09:08:16', 110, 110),
(55, '<p class=''text-danger''>Maintenance is due on <strong>201 Km</strong> time</p>', '', '', 30, '2015-04-12 13:38:37', '2015-04-12 13:38:37', 110, 110),
(56, 'Odometer : 900 <br>\r\nFueled Date : Apr 13, 2015 <br>\r\nVolume : 123 <br>\r\nCost : 123 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/4">Asset Name Neoo 78</a> <br>\r\n', '', '', 31, '2015-04-13 12:46:48', '2015-04-13 12:46:48', 110, 110),
(57, 'jkkjhd', '', '', 31, '2015-04-13 12:48:40', '2015-04-13 12:48:40', 110, 110),
(58, 'Odometer : 2 <br>\r\nFueled Date : Apr 13, 2015 <br>\r\nVolume : 2 <br>\r\nCost : 2 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintech.biz/cadmin/assets/view/18">Testing Location Feature</a> <br>\r\n', '', '', 32, '2015-04-13 09:00:08', '2015-04-13 09:00:08', 110, 110),
(59, 'jhgh', '7d07c0cb236a30dc56769d32f73d13ed.xlsx', 'permissions.xlsx', 30, '2015-04-14 07:09:49', '2015-04-14 07:09:49', 110, 110),
(60, '<p class=''text-danger''>Maintenance is due on <strong>2015-04-14 12:38:12</strong>, <strong>0</strong> days time.</p>', '', '', 33, '2015-04-15 06:02:25', '2015-04-15 06:02:25', 110, 110),
(61, 'asdad', '', '', 33, '2015-04-15 04:55:24', '2015-04-15 04:55:24', 110, 110),
(62, 'asdadasdad', '', '', 33, '2015-04-15 04:55:53', '2015-04-15 04:55:53', 110, 110),
(63, 'dfgdfgfd', '', '', 30, '2015-04-15 04:58:46', '2015-04-15 04:58:46', 110, 110),
(64, 'asd', '', '', 34, '2015-04-16 13:53:53', '2015-04-16 13:53:53', 110, 110),
(65, 'asd', '', '', 35, '2015-04-16 06:58:14', '2015-04-16 06:58:14', 110, 110),
(66, 'sdfsdf', '', '', 30, '2015-04-16 22:10:07', '2015-04-16 22:10:07', 138, 138),
(67, 'demo comment', '', '', 30, '2015-04-16 22:14:09', '2015-04-16 22:14:09', 138, 138),
(68, 'sdfsd', '', '', 36, '2015-04-17 00:31:48', '2015-04-17 00:31:48', 110, 110),
(69, 'sdf', '', '', 37, '2015-04-17 00:52:27', '2015-04-17 00:52:27', 110, 110),
(70, 'asdasd', '', '', 38, '2015-04-17 17:10:46', '2015-04-17 17:10:46', 110, 110),
(71, 'adasd', '', '', 39, '2015-04-17 17:12:07', '2015-04-17 17:12:07', 110, 110),
(72, 'qew', '', '', 40, '2015-04-17 17:13:25', '2015-04-17 17:13:25', 110, 110),
(73, 'asdadsadad', '', '', 41, '2015-04-17 17:15:50', '2015-04-17 17:15:50', 110, 110),
(74, 'asdasd', '', '', 41, '2015-04-17 17:16:18', '2015-04-17 17:16:18', 110, 110),
(75, 'asdf', '', '', 42, '2015-04-17 17:17:21', '2015-04-17 17:17:21', 110, 110),
(76, 'Demo Demo', '', '', 43, '2015-04-17 17:18:01', '2015-04-17 17:18:01', 110, 110),
(77, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 44, '2015-04-18 22:06:00', '2015-04-18 22:06:00', 110, 110),
(78, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 45, '2015-04-19 22:43:57', '2015-04-19 22:43:57', 110, 110),
(79, 'kjhkhjk', '', '', 46, '2015-04-22 09:02:24', '2015-04-22 09:02:24', 110, 110),
(80, 'qweqwe', '', '', 47, '2015-04-22 09:02:42', '2015-04-22 09:02:42', 110, 110),
(81, 'asd', '', '', 32, '2015-04-23 07:31:28', '2015-04-23 07:31:28', 110, 110),
(82, 'Odometer : 20 <br>\r\nFueled Date : Apr 25, 2015 <br>\r\nVolume : 20 <br>\r\nCost : 20 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/30">Testing Fuel</a> <br>\r\n', '', '', 48, '2015-04-25 06:18:52', '2015-04-25 06:18:52', 110, 110),
(83, 'Odometer : 50 <br>\r\nFueled Date : Apr 25, 2015 <br>\r\nVolume : 50 <br>\r\nCost : 50 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/30">Testing Fuel</a> <br>\r\n', '', '', 49, '2015-04-25 06:20:21', '2015-04-25 06:20:21', 110, 110),
(84, 'Odometer : 400 <br>\r\nFueled Date : Apr 25, 2015 <br>\r\nVolume : 99 <br>\r\nCost : 99 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/31">Fuel2</a> <br>\r\n', '', '', 50, '2015-04-25 06:33:37', '2015-04-25 06:33:37', 110, 110),
(85, 'Odometer : 1000 <br>\r\nFueled Date : Apr 25, 2015 <br>\r\nVolume : 1000 <br>\r\nCost : 1000 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/33">testing portlet</a> <br>\r\n', '', '', 51, '2015-04-25 08:15:45', '2015-04-25 08:15:45', 110, 110),
(86, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>25 Apr, 2015 11:21 pm</strong>, <strong>-2</strong> days time.</p>', '', '', 52, '2015-04-27 11:40:40', '2015-04-27 11:40:40', 110, 110),
(87, '<p class=''text-danger''>Maintenance - Testing (Date) is due on <strong>30 Apr, 2015 11:04 pm</strong>, <strong>-1</strong> days time.</p>', '', '', 53, '2015-05-01 05:24:09', '2015-05-01 05:24:09', 110, 110),
(88, '<p class=''text-danger''>Maintenance - Testing (Date) is due on <strong>30 Apr, 2015 11:04 pm</strong>, <strong>-1</strong> days time.</p>', '', '', 54, '2015-05-01 05:24:09', '2015-05-01 05:24:09', 110, 110),
(89, 'qwe', '', '', 55, '2015-05-04 07:48:04', '2015-05-04 07:48:04', 110, 110),
(90, 'qwe', '', '', 55, '2015-05-04 07:48:11', '2015-05-04 07:48:11', 110, 110),
(91, 'ads', '', '', 56, '2015-05-04 08:07:51', '2015-05-04 08:07:51', 110, 110),
(92, 'asd', '', '', 56, '2015-05-04 08:07:58', '2015-05-04 08:07:58', 110, 110),
(93, '12', '', '', 57, '2015-05-04 10:00:23', '2015-05-04 10:00:23', 110, 110),
(94, 'demo', '', '', 58, '2015-05-05 07:26:24', '2015-05-05 07:26:24', 110, 110),
(95, 'asd', '', '', 58, '2015-05-05 07:27:14', '2015-05-05 07:27:14', 110, 110),
(96, 'lkjkl', '', '', 59, '2015-05-05 10:08:26', '2015-05-05 10:08:26', 110, 110),
(97, 'sAas', '', '', 59, '2015-05-05 10:08:37', '2015-05-05 10:08:37', 110, 110),
(98, 'sda', '', '', 59, '2015-05-05 10:13:13', '2015-05-05 10:13:13', 107, 107),
(99, 'demo', '', '', 59, '2015-05-05 10:59:05', '2015-05-05 10:59:05', 107, 107),
(100, 'demo', '', '', 59, '2015-05-05 11:00:27', '2015-05-05 11:00:27', 107, 107),
(101, 'demo', '', '', 59, '2015-05-05 11:01:57', '2015-05-05 11:01:57', 107, 107),
(102, 'asd', '', '', 60, '2015-05-09 06:42:07', '2015-05-09 06:42:07', 110, 110),
(103, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 61, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(104, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 61, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(105, '<p class=''text-danger''>Maintenance - Service (Date) is due on <strong>18 Apr, 2015 4:05 pm</strong>, <strong>-23</strong> days time.</p>', '', '', 62, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(106, '<p class=''text-danger''>Maintenance - Service (Date) is due on <strong>18 Apr, 2015 4:05 pm</strong>, <strong>-23</strong> days time.</p>', '', '', 62, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(107, '<p class=''text-danger''>Maintenance - Testing (Date) is due on <strong>19 Apr, 2015 4:05 pm</strong>, <strong>-22</strong> days time.</p>', '', '', 63, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(108, '<p class=''text-danger''>Maintenance - Testing (Date) is due on <strong>19 Apr, 2015 4:05 pm</strong>, <strong>-22</strong> days time.</p>', '', '', 63, '2015-05-11 12:45:58', '2015-05-11 12:45:58', 110, 110),
(109, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 64, '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110),
(110, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 64, '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110),
(111, '<p class=''text-danger''>Maintenance - Service (Date) is due on <strong>06 Apr, 2015 3:14 pm</strong>, <strong>-35</strong> days time.</p>', '', '', 65, '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110),
(112, '<p class=''text-danger''>Maintenance - Service (Date) is due on <strong>06 Apr, 2015 3:14 pm</strong>, <strong>-35</strong> days time.</p>', '', '', 65, '2015-05-11 12:48:31', '2015-05-11 12:48:31', 110, 110),
(113, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 66, '2015-05-11 13:51:33', '2015-05-11 13:51:33', 110, 110),
(114, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>01 Jan, 1970 12:00 am</strong>, <strong></strong> days time.</p>', '', '', 66, '2015-05-11 13:51:33', '2015-05-11 13:51:33', 110, 110),
(115, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>17 Apr, 2015 4:05 pm</strong>, <strong>-24</strong> days time.</p>', '', '', 67, '2015-05-11 13:53:48', '2015-05-11 13:53:48', 110, 110),
(116, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>17 Apr, 2015 4:05 pm</strong>, <strong>-24</strong> days time.</p>', '', '', 67, '2015-05-11 13:53:48', '2015-05-11 13:53:48', 110, 110),
(117, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>30 Apr, 2015 7:12 pm</strong>, <strong>-11</strong> days time.</p>', '', '', 68, '2015-05-11 14:01:53', '2015-05-11 14:01:53', 110, 110),
(118, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>30 Apr, 2015 7:12 pm</strong>, <strong>-11</strong> days time.</p>', '', '', 68, '2015-05-11 14:01:53', '2015-05-11 14:01:53', 110, 110),
(119, '<p class=''text-danger''>Maintenance - Warranty (Date) is due on <strong>11 May, 2015 7:29 pm</strong>, <strong>0</strong> days time.</p>', '', '', 69, '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110),
(120, '<p class=''text-danger''>Maintenance - Service (Date) is due on <strong>11 May, 2015 7:29 pm</strong>, <strong>0</strong> days time.</p>', '', '', 70, '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110),
(121, '<p class=''text-danger''>Maintenance - Testing (Date) is due on <strong>11 May, 2015 7:29 pm</strong>, <strong>0</strong> days time.</p>', '', '', 71, '2015-05-12 04:43:27', '2015-05-12 04:43:27', 110, 110),
(122, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 4:50 am</strong>, <strong>1</strong> days time.</p>', '', '', 72, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(123, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 4:50 am</strong>, <strong>1</strong> days time.</p>', '', '', 72, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(124, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:52 am</strong>, <strong>11</strong> days time.</p>', '', '', 73, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(125, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:52 am</strong>, <strong>11</strong> days time.</p>', '', '', 73, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(126, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>05 May 2015, 12:59 am</strong>, <strong>8</strong> days time.</p>', '', '', 74, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(127, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>05 May 2015, 12:59 am</strong>, <strong>8</strong> days time.</p>', '', '', 74, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(128, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>05 May 2015, 10:24 pm</strong>, <strong>7</strong> days time.</p>', '', '', 75, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(129, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>05 May 2015, 10:24 pm</strong>, <strong>7</strong> days time.</p>', '', '', 75, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(130, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 7:43 pm</strong>, <strong>0</strong> days time.</p>', '', '', 76, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(131, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 7:43 pm</strong>, <strong>0</strong> days time.</p>', '', '', 76, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(132, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 77, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(133, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 77, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(134, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 4:57 pm</strong>, <strong>0</strong> days time.</p>', '', '', 78, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(135, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 4:57 pm</strong>, <strong>0</strong> days time.</p>', '', '', 78, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(136, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 79, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(137, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 79, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(138, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 80, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(139, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 80, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(140, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 81, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(141, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 7:41 pm</strong>, <strong>0</strong> days time.</p>', '', '', 81, '2015-05-12 10:36:31', '2015-05-12 10:36:31', 110, 110),
(142, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:52 am</strong>, <strong>11</strong> days time.</p>', '', '', 82, '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110),
(143, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:52 am</strong>, <strong>11</strong> days time.</p>', '', '', 82, '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110),
(144, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:06 pm</strong>, <strong>0</strong> days time.</p>', '', '', 83, '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110),
(145, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:06 pm</strong>, <strong>0</strong> days time.</p>', '', '', 83, '2015-05-12 10:38:26', '2015-05-12 10:38:26', 110, 110),
(146, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:05 am</strong>, <strong>11</strong> days time.</p>', '', '', 84, '2015-05-12 10:39:22', '2015-05-12 10:39:22', 110, 110),
(147, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>02 May 2015, 3:05 am</strong>, <strong>11</strong> days time.</p>', '', '', 84, '2015-05-12 10:39:22', '2015-05-12 10:39:22', 110, 110),
(148, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>01 Jan 1970, 9:30 am</strong>, <strong></strong> days time.</p>', '', '', 85, '2015-05-12 10:39:31', '2015-05-12 10:39:31', 110, 110),
(149, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>01 Jan 1970, 9:30 am</strong>, <strong></strong> days time.</p>', '', '', 85, '2015-05-12 10:39:31', '2015-05-12 10:39:31', 110, 110),
(150, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>01 Jan 1970, 9:30 am</strong>, <strong></strong> days time.</p>', '', '', 86, '2015-05-12 10:39:33', '2015-05-12 10:39:33', 110, 110),
(151, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>01 Jan 1970, 9:30 am</strong>, <strong></strong> days time.</p>', '', '', 86, '2015-05-12 10:39:33', '2015-05-12 10:39:33', 110, 110),
(152, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 87, '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110),
(153, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 87, '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110),
(154, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 88, '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110),
(155, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 88, '2015-05-12 10:43:19', '2015-05-12 10:43:19', 110, 110),
(156, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 89, '2015-05-12 10:51:23', '2015-05-12 10:51:23', 110, 110),
(157, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 89, '2015-05-12 10:51:23', '2015-05-12 10:51:23', 110, 110),
(158, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 90, '2015-05-12 10:51:24', '2015-05-12 10:51:24', 110, 110),
(159, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:10 pm</strong>, <strong>0</strong> days time.</p>', '', '', 90, '2015-05-12 10:51:24', '2015-05-12 10:51:24', 110, 110),
(160, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 91, '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110),
(161, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 91, '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110),
(162, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 92, '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110),
(163, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:13 pm</strong>, <strong>0</strong> days time.</p>', '', '', 92, '2015-05-12 10:56:44', '2015-05-12 10:56:44', 110, 110),
(164, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 93, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(165, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 93, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(166, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 94, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(167, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 94, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(168, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 95, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(169, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 95, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(170, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 96, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(171, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 8:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 96, '2015-05-12 11:00:14', '2015-05-12 11:00:14', 110, 110),
(172, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 9:27 pm</strong>, <strong>0</strong> days time.</p>', '', '', 97, '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110),
(173, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 9:36 pm</strong>, <strong>0</strong> days time.</p>', '', '', 98, '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110),
(174, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 9:36 pm</strong>, <strong>0</strong> days time.</p>', '', '', 99, '2015-05-12 12:07:36', '2015-05-12 12:07:36', 110, 110),
(175, 'dasda', '', '', 66, '2015-05-20 06:10:37', '2015-05-20 06:10:37', 110, 110),
(176, 'asd', '', '', 66, '2015-05-20 06:10:44', '2015-05-20 06:10:44', 110, 110),
(177, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 10:22 pm</strong>, <strong>8</strong> days time.</p>', '', '', 100, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(178, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>12 May 2015, 10:06 pm</strong>, <strong>8</strong> days time.</p>', '', '', 101, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(179, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 10:22 pm</strong>, <strong>8</strong> days time.</p>', '', '', 102, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(180, '<p class=''text-danger''>Maintenance - Service (Date) was due on <strong>12 May 2015, 10:06 pm</strong>, <strong>8</strong> days time.</p>', '', '', 103, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(181, '<p class=''text-danger''>Maintenance - Testing (Date) was due on <strong>12 May 2015, 10:22 pm</strong>, <strong>8</strong> days time.</p>', '', '', 104, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(182, '<p class=''text-danger''>Maintenance - End of Life (Date) was due on <strong>12 May 2015, 10:22 pm</strong>, <strong>8</strong> days time.</p>', '', '', 105, '2015-05-20 13:33:59', '2015-05-20 13:33:59', 110, 110),
(183, 'test', '', '', 66, '2015-05-21 11:48:51', '2015-05-21 11:48:51', 110, 110),
(184, 'kjashdkjhasd', '', '', 43, '2015-05-22 15:44:20', '2015-05-22 15:44:20', 110, 110),
(185, 'demo', '', '', 43, '2015-05-23 05:53:56', '2015-05-23 05:53:56', 110, 110),
(186, '<p class=''text-danger''>Maintenance - Warranty (Date) was due on <strong>22 May 2015, 10:50 pm</strong>, <strong>6</strong> days time.</p>', '', '', 106, '2015-05-28 09:57:39', '2015-05-28 09:57:39', 138, 138),
(187, 'Odometer : 100000 <br>\r\nFueled Date : May 30, 2015 <br>\r\nVolume : 12 <br>\r\nCost : 12 <br>\r\nAsset : <a target="_blank" href="http://airapp.xyrintechnologies.com/cadmin/assets/view/18">Testing Location Feature</a> <br>\r\n', '', '', 107, '2015-05-30 11:00:08', '2015-05-30 11:00:08', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `exception_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exception_subscriptions` (
`exception_subscription_id` int(11) NOT NULL,
  `exception_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exception_subscriptions`
--

INSERT INTO `exception_subscriptions` (`exception_subscription_id`, `exception_id`, `user_id`, `company_id`, `created_on`) VALUES
(2, 1, 110, 17, '2015-04-07 03:12:18'),
(4, 9, 110, 17, '2015-04-07 12:36:34'),
(5, 15, 110, 17, '2015-04-07 05:59:46'),
(6, 25, 110, 17, '2015-04-07 05:59:52'),
(7, 26, 110, 17, '2015-04-07 06:00:06'),
(8, 9, 108, 17, '2015-04-08 08:08:21'),
(9, 9, 107, 17, '2015-04-08 08:09:35'),
(10, 29, 107, 17, '2015-04-08 01:30:43'),
(11, 34, 110, 17, '2015-04-16 13:53:54'),
(12, 35, 110, 17, '2015-04-16 06:58:23'),
(13, 36, 110, 17, '2015-04-17 00:31:49'),
(14, 37, 110, 17, '2015-04-17 00:52:28'),
(15, 38, 110, 17, '2015-04-17 17:10:46'),
(16, 39, 110, 17, '2015-04-17 17:12:07'),
(17, 40, 110, 17, '2015-04-17 17:13:25'),
(18, 41, 110, 17, '2015-04-17 17:15:50'),
(19, 42, 110, 17, '2015-04-17 17:17:21'),
(20, 43, 110, 17, '2015-04-17 17:18:02'),
(21, 46, 110, 17, '2015-04-22 09:02:25'),
(22, 47, 110, 17, '2015-04-22 09:02:43'),
(23, 55, 110, 17, '2015-05-04 07:48:04'),
(24, 56, 110, 17, '2015-05-04 08:07:51'),
(25, 57, 110, 17, '2015-05-04 10:00:23'),
(26, 58, 110, 17, '2015-05-05 07:26:24'),
(27, 59, 110, 17, '2015-05-05 10:08:27'),
(28, 60, 110, 17, '2015-05-09 06:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE IF NOT EXISTS `favourites` (
`favourite_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`favourite_id`, `asset_id`, `user_id`, `created_by`, `updated_by`, `created_on`, `updated_on`) VALUES
(8, 5, 110, 110, 110, '2015-03-26 04:43:05', '2015-03-26 04:43:05'),
(9, 5, 107, 107, 107, '2015-03-26 12:03:49', '2015-03-26 12:03:49'),
(11, 3, 107, 107, 107, '2015-03-27 14:49:27', '2015-03-27 14:49:27'),
(34, 8, 110, 110, 110, '2015-05-29 17:09:30', '2015-05-29 17:09:30'),
(32, 3, 110, 110, 110, '2015-05-19 06:36:27', '2015-05-19 06:36:27'),
(18, 4, 107, 107, 107, '2015-04-04 07:19:30', '2015-04-04 07:19:30'),
(33, 4, 110, 110, 110, '2015-05-19 06:38:17', '2015-05-19 06:38:17'),
(20, 4, 108, 108, 108, '2015-04-08 07:55:54', '2015-04-08 07:55:54'),
(29, 48, 110, 110, 110, '2015-05-09 06:42:34', '2015-05-09 06:42:34'),
(22, 18, 110, 110, 110, '2015-04-13 08:59:37', '2015-04-13 08:59:37'),
(28, 10, 110, 110, 110, '2015-05-02 13:13:31', '2015-05-02 13:13:31'),
(25, 36, 110, 110, 110, '2015-04-27 11:23:30', '2015-04-27 11:23:30'),
(26, 40, 110, 110, 110, '2015-04-29 15:51:33', '2015-04-29 15:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `fuel`
--

CREATE TABLE IF NOT EXISTS `fuel` (
`fuel_id` int(11) NOT NULL,
  `supplier_id` int(255) NOT NULL,
  `odometer` int(11) NOT NULL,
  `fueled_date` datetime NOT NULL,
  `volume` float NOT NULL,
  `cost` float NOT NULL,
  `pre_purchased` tinyint(1) NOT NULL,
  `towing` tinyint(1) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fuel`
--

INSERT INTO `fuel` (`fuel_id`, `supplier_id`, `odometer`, `fueled_date`, `volume`, `cost`, `pre_purchased`, `towing`, `photo`, `created_on`, `updated_on`, `asset_id`, `company_id`, `created_by`, `updated_by`) VALUES
(7, 0, 2147483647, '2015-04-11 00:00:00', 123424, 1234120000, 0, 0, '', '2015-04-06 07:19:28', '2015-04-06 07:19:28', 8, 17, 110, 110),
(9, 0, 1212, '2015-04-11 00:00:00', 1212, 1212, 0, 0, '', '2015-04-06 07:39:01', '2015-04-06 07:39:01', 10, 17, 110, 110),
(10, 0, 1212, '2015-04-18 00:00:00', 1212, 1212, 0, 0, '', '2015-04-06 07:39:10', '2015-04-06 07:39:10', 10, 17, 110, 110),
(13, 2, 876, '2015-04-06 00:00:00', 1000, 10000, 0, 0, 'd8eba2465c27d61369e1e9a81240f2fb.png', '2015-04-06 09:44:55', '2015-05-01 14:15:03', 4, 17, 110, 110),
(14, 0, 200, '2015-04-06 00:00:00', 20, 200000, 0, 0, '', '2015-04-06 09:45:42', '2015-04-06 09:45:42', 4, 17, 110, 110),
(15, 0, 1000, '2015-04-06 00:00:00', 10, 10, 0, 0, '', '2015-04-06 14:42:59', '2015-04-06 14:42:59', 11, 17, 110, 110),
(16, 0, 2000, '2015-04-06 00:00:00', 10, 10, 0, 0, '', '2015-04-06 14:43:07', '2015-04-06 14:43:07', 11, 17, 110, 110),
(17, 0, 2500, '2015-04-06 00:00:00', 10, 10, 0, 0, '', '2015-04-06 14:43:19', '2015-04-06 14:43:19', 11, 17, 110, 110),
(18, 0, 3500, '2015-04-06 00:00:00', 10, 10, 0, 0, '', '2015-04-06 14:43:26', '2015-04-06 14:43:26', 11, 17, 110, 110),
(19, 1, 500, '2015-04-15 00:00:00', 898, 898, 1, 1, '', '2015-04-12 13:37:46', '2015-04-12 13:37:46', 4, 17, 110, 110),
(20, 1, 700, '2015-04-24 00:00:00', 345, 345, 0, 0, '', '2015-04-12 13:38:05', '2015-04-12 13:38:05', 4, 17, 110, 110),
(21, 1, 100, '2015-04-12 00:00:00', 100, 100, 0, 0, '', '2015-04-12 13:39:39', '2015-04-12 13:39:39', 17, 17, 110, 110),
(22, 1, 200, '2015-04-14 00:00:00', 200, 200, 0, 0, '', '2015-04-12 13:39:52', '2015-04-12 13:39:52', 17, 17, 110, 110),
(23, 1, 300, '2015-04-16 00:00:00', 300, 300, 0, 0, '', '2015-04-12 13:40:04', '2015-04-12 13:40:04', 17, 17, 110, 110),
(24, 1, 900, '2015-04-13 00:00:00', 123, 123, 0, 0, '', '2015-04-13 12:46:47', '2015-04-13 12:46:47', 4, 17, 110, 110),
(25, 3, 1000, '2015-04-13 00:00:00', 23, 23, 0, 0, '', '2015-04-13 08:48:15', '2015-04-13 08:48:15', 4, 17, 110, 110),
(26, 2, 2, '2015-04-13 00:00:00', 2, 2, 0, 0, '', '2015-04-13 09:00:08', '2015-04-13 09:00:08', 18, 17, 110, 110),
(27, 2, 123123, '2015-04-25 00:00:00', 897, 13213100000, 0, 0, '637f7f57e24e38884c4498f8a7672d93.png', '2015-04-25 05:48:19', '2015-04-25 05:48:19', 10, 17, 110, 110),
(28, 2, 2147483647, '2015-04-24 00:00:00', 1.23123, 12312300000, 0, 0, 'e7c77f13b7f05d48db18edda5ccdc3fa.png', '2015-04-24 22:49:51', '2015-04-24 22:49:51', 10, 17, 110, 110),
(29, 2, 3423423, '2015-04-25 00:00:00', 27987, 23423400, 0, 0, 'b3d9762a73221e6ddd5c3a4359901b84.png', '2015-04-25 05:53:40', '2015-04-25 05:53:40', 4, 17, 110, 110),
(30, 2, 999999999, '2015-04-25 00:00:00', 1000000000, 1000000000, 0, 0, '2a0fc14cf68a58708fe3864c6c36b21f.png', '2015-04-25 05:55:48', '2015-04-25 05:55:48', 14, 17, 110, 110),
(31, 2, 2147483647, '2015-04-25 00:00:00', 9.89081e15, 9.89081e15, 0, 0, '6bd447ae0f5a11f54c6d39540f4a17ef.png', '2015-04-25 05:59:49', '2015-04-25 05:59:49', 16, 17, 110, 110),
(32, 2, 900, '2015-04-25 00:00:00', 9999, 20, 0, 0, '', '2015-04-25 06:18:52', '2015-04-25 06:18:52', 30, 17, 110, 110),
(33, 2, 8888, '2015-04-25 00:00:00', 999999, 50, 0, 0, '', '2015-04-25 06:20:21', '2015-04-25 06:20:21', 30, 17, 110, 110),
(34, 2, 201, '2015-04-25 00:00:00', 100, 100, 0, 0, '', '2015-04-25 06:23:42', '2015-04-25 06:23:42', 31, 17, 110, 110),
(35, 2, 200, '2015-04-25 00:00:00', 200, 200, 0, 0, '', '2015-04-25 06:25:23', '2015-04-25 06:25:23', 31, 17, 110, 110),
(36, 2, 300, '2015-04-25 00:00:00', 99, 88, 0, 0, '', '2015-04-25 06:29:24', '2015-04-25 06:29:24', 31, 17, 110, 110),
(37, 2, 400, '2015-04-25 00:00:00', 99, 99, 0, 0, '', '2015-04-25 06:33:36', '2015-04-25 06:33:36', 31, 17, 110, 110),
(38, 2, 800, '2015-04-25 00:00:00', 800, 800, 0, 0, '', '2015-04-25 06:33:51', '2015-04-25 06:33:51', 31, 17, 110, 110),
(39, 2, 199, '2015-04-25 07:43:34', 100, 100, 0, 0, '', '2015-04-25 07:43:34', '2015-04-25 07:43:34', 32, 17, 110, 110),
(40, 2, 200, '2015-04-25 07:43:46', 200, 200, 0, 0, '', '2015-04-25 07:43:46', '2015-04-25 07:43:46', 32, 17, 110, 110),
(41, 2, 900, '2015-04-25 08:15:14', 900, 900, 0, 0, '', '2015-04-25 08:15:14', '2015-04-25 08:15:14', 33, 17, 110, 110),
(42, 2, 1000, '2015-04-25 08:15:45', 898, 1000, 0, 0, '', '2015-04-25 08:15:45', '2015-04-25 08:15:45', 33, 17, 110, 110),
(43, 2, 11189, '2015-04-27 11:49:34', 6, 11111100, 0, 0, '', '2015-04-27 11:49:34', '2015-04-27 11:49:34', 3, 17, 110, 110),
(44, 2, 100, '2015-04-30 14:35:51', 100, 0, 1, 0, '', '2015-04-30 14:35:51', '2015-04-30 14:35:51', 41, 17, 110, 110),
(45, 2, 10, '2015-04-30 14:42:46', 100, 0, 1, 0, '', '2015-04-30 14:42:46', '2015-04-30 14:42:46', 42, 17, 110, 110),
(46, 2, 2000, '2015-04-30 14:48:58', 100, 2000, 0, 0, '', '2015-04-30 14:48:58', '2015-04-30 14:48:58', 42, 17, 110, 110),
(47, 2, 12, '2015-05-04 10:00:12', 12, 12, 0, 0, '', '2015-05-04 10:00:12', '2015-05-04 10:00:12', 45, 17, 110, 110),
(48, 2, 1, '2015-05-05 07:24:05', 1, 1, 0, 0, '', '2015-05-05 07:24:05', '2015-05-05 07:24:05', 46, 17, 110, 110),
(49, 2, 1, '2015-05-20 04:57:18', 23, 23, 0, 0, '8a49bd5a420d804aa211c1f74c4dc9ef.png', '2015-05-20 04:57:18', '2015-05-20 04:57:18', 58, 17, 110, 110),
(50, 2, 100000, '2015-05-29 18:30:00', 12, 12, 0, 0, '5e5869e0627bd9cd8ca259c1889581c5.png', '2015-05-30 11:00:07', '2015-05-30 11:00:07', 18, 17, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `invites`
--

CREATE TABLE IF NOT EXISTS `invites` (
`invite_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `accepted` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `invited_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invites`
--

INSERT INTO `invites` (`invite_id`, `first_name`, `last_name`, `email`, `user_id`, `company_id`, `accepted`, `role_id`, `invited_by`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, '', '', 'xyrindata@outlook.com', 108, 14, 1, 13, 107, '2015-03-22 23:47:24', '2015-03-22 23:47:24', 107, 107),
(2, '', '', 'abhijeetthakur26@gmail.com', 111, 15, 1, 16, 110, '2015-03-23 11:27:52', '2015-03-23 11:27:52', 110, 110),
(38, 'Nitin', 'Abrol', 'nitin@xyrintech.com', 107, 15, 1, 16, 110, '2015-05-04 07:45:59', '2015-05-04 07:45:59', 110, 110),
(12, 'Davinder', 'Singh', 'davinder17s@gmail.com', 0, 19, 0, 19, 120, '2015-04-03 05:09:49', '2015-04-03 05:09:49', 120, 120),
(44, '', '', 'jeremy@amazingkidsparties.com.au', 149, 17, 1, 25, 110, '2015-05-16 04:40:12', '2015-05-16 04:40:12', 110, 110),
(19, 'Admin', 'User', 'admin@admin.com', 109, 17, 1, 21, 110, '2015-04-09 08:06:03', '2015-04-09 08:06:03', 110, 110),
(40, 'Nerd', 'All', 'service@nerdforall.com', 138, 17, 1, 17, 110, '2015-05-04 07:56:33', '2015-05-04 07:56:33', 110, 110),
(22, '', '', 'xyrin.dploy@gmail.com', 131, 17, 1, 21, 110, '2015-04-10 11:10:44', '2015-04-10 11:10:44', 110, 110),
(27, 'fone', 'lone', 'xyrintechnologie.s@gmail.com', 141, 26, 1, 23, 0, '2015-04-30 09:42:59', '2015-04-30 09:42:59', 0, 0),
(28, 'ftwo', 'ltwo', 'xyrin.technologies@gmail.com', 142, 26, 1, 23, 0, '2015-04-30 09:42:59', '2015-04-30 09:42:59', 0, 0),
(29, 'fone', 'lone', 'xyrintechnologi.es@gmail.com', 143, 26, 1, 23, 0, '2015-04-30 09:57:35', '2015-04-30 09:57:35', 0, 0),
(30, 'ftwo', 'ltwo', 'xyri.ntechnologies@gmail.com', 144, 26, 1, 23, 0, '2015-04-30 09:57:35', '2015-04-30 09:57:35', 0, 0),
(31, 'test', 'one', 'help@airapp.io', 145, 26, 1, 23, 0, '2015-04-30 10:31:08', '2015-04-30 10:31:08', 0, 0),
(32, 'test', 'two', 'emmertextesttwo@gmail.com', 146, 26, 1, 23, 0, '2015-04-30 10:31:09', '2015-04-30 10:31:09', 0, 0),
(42, 'Xyrin', 'Dploy', 'xyrindata@outlook.com', 130, 17, 1, 17, 110, '2015-05-05 06:46:02', '2015-05-05 06:46:02', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `locations_geo`
--

CREATE TABLE IF NOT EXISTS `locations_geo` (
`location_geo_id` int(11) NOT NULL,
  `device_udid` varchar(255) NOT NULL,
  `location_geo` varchar(255) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `hdop` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations_geo`
--

INSERT INTO `locations_geo` (`location_geo_id`, `device_udid`, `location_geo`, `longitude`, `latitude`, `created_on`, `updated_on`, `asset_id`, `company_id`, `hdop`, `created_by`, `updated_by`) VALUES
(1, '', 'Shahid Jasdev Singh Nagar, Ludhiana, Punjab, India', 75.8592, 30.8522, '2015-03-27 05:29:10', '2015-03-27 05:29:10', 7, 19, 0, 120, 120),
(3, '', 'Horsham VIC 3400, Australia', 142.199, -36.7276, '2015-03-28 14:47:09', '2015-03-28 14:47:09', 9, 17, 0, 110, 110),
(6, '', 'Horsham VIC 3400, Australia', 142.208, -36.7065, '2015-03-29 18:33:57', '2015-03-29 18:33:57', 10, 17, 0, 110, 110),
(9, '123456', '', 142.199, -36.7277, '2015-04-06 12:57:08', '2015-04-06 12:57:08', 3, 17, 20, 0, 0),
(10, '78687678', '', 142.199, -36.7277, '2015-04-06 12:57:35', '2015-04-06 12:57:35', 3, 17, 20, 0, 0),
(11, '9.82731983719e24', '', 142.199, -36.7277, '2015-04-06 12:57:44', '2015-04-06 12:57:44', 3, 17, 20, 0, 0),
(12, '', '', 0, 0, '2015-04-06 12:57:51', '2015-04-06 12:57:51', 3, 17, 0, 0, 0),
(13, '', '', 0, 0, '2015-04-06 12:59:40', '2015-04-06 12:59:40', 3, 17, 0, 0, 0),
(14, '', '', 0, 0, '2015-04-06 12:59:44', '2015-04-06 12:59:44', 3, 17, 0, 0, 0),
(15, '', '', 0, 0, '2015-04-06 12:59:51', '2015-04-06 12:59:51', 3, 17, 0, 0, 0),
(16, '', '', 0, 0, '2015-04-06 06:00:10', '2015-04-06 06:00:10', 3, 17, 0, 0, 0),
(17, '', '', 0, 0, '2015-04-06 13:00:31', '2015-04-06 13:00:31', 3, 17, 0, 0, 0),
(18, '123456', '', 142.199, -36.7277, '2015-04-06 13:05:52', '2015-04-06 13:05:52', 4, 17, 20, 0, 0),
(19, '123456', '', 142.199, -36.7277, '2015-04-06 13:59:18', '2015-04-06 13:59:18', 4, 17, 20, 0, 0),
(20, '123456', '', 142.199, -36.7277, '2015-04-06 14:09:13', '2015-04-06 14:09:13', 4, 17, 20, 0, 0),
(21, '123456', '', 142.199, -36.7277, '2015-04-06 14:09:17', '2015-04-06 14:09:17', 4, 17, 20, 0, 0),
(22, '123456', '', 142.199, -36.7277, '2015-04-06 14:09:19', '2015-04-06 14:09:19', 4, 17, 20, 0, 0),
(23, '123456', '', 142.199, -36.7277, '2015-04-06 14:12:08', '2015-04-06 14:12:08', 4, 17, 20, 0, 0),
(24, '5656', '', 142.199, -36.7277, '2015-04-06 14:17:00', '2015-04-06 14:17:00', 8, 17, 20, 0, 0),
(25, '5656', '', 141.199, -35.7277, '2015-04-06 14:17:44', '2015-04-06 14:17:44', 8, 17, 20, 0, 0),
(26, '5656', '', 141.199, -35.7277, '2015-04-06 14:18:19', '2015-04-06 14:18:19', 8, 17, 20, 0, 0),
(27, '8989', '', 141.199, -35.7277, '2015-04-07 06:11:53', '2015-04-07 06:11:53', 14, 17, 20, 0, 0),
(28, '8989', '', 141.199, -35.7277, '2015-04-07 06:15:00', '2015-04-07 06:15:00', 14, 17, 20, 0, 0),
(29, '8989', '', 141.199, -35.7277, '2015-04-07 06:17:28', '2015-04-07 06:17:28', 14, 17, 20, 0, 0),
(30, '', 'Shahid Jasdev Singh Nagar, Ludhiana, Punjab, India', 75.8592, 30.8522, '2015-04-17 22:06:10', '2015-04-17 22:06:10', 10, 17, 0, 110, 110),
(31, '', 'Rajesh Nagar, New Tagore Nagar, Haibowal Kalan, Ludhiana, Punjab 141001, India', 75.8224, 30.9215, '2015-04-18 06:21:12', '2015-04-18 06:21:12', 18, 17, 0, 110, 110),
(32, '', '37B, Sector 37, Chandigarh, Chandigarh, India', 76.7567, 30.74, '2015-06-03 13:48:39', '2015-06-03 13:48:39', 8, 17, 0, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `locations_text`
--

CREATE TABLE IF NOT EXISTS `locations_text` (
`location_text_id` int(11) NOT NULL,
  `location_text` varchar(300) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations_text`
--

INSERT INTO `locations_text` (`location_text_id`, `location_text`, `created_on`, `updated_on`, `asset_id`, `company_id`, `created_by`, `updated_by`) VALUES
(3, 'hjjh', '2015-03-28 03:05:30', '2015-03-28 03:05:30', 3, 17, 110, 110),
(6, '', '2015-03-28 14:47:13', '2015-03-28 14:47:13', 9, 17, 110, 110),
(7, 'Warehouse', '2015-03-28 14:47:22', '2015-03-28 14:47:22', 9, 17, 110, 110),
(8, '', '2015-03-29 16:35:53', '2015-03-29 16:35:53', 4, 17, 110, 110),
(9, 'Warehouse', '2015-03-29 16:36:08', '2015-03-29 16:36:08', 4, 17, 110, 110),
(10, '', '2015-03-29 18:33:32', '2015-03-29 18:33:32', 10, 17, 110, 110),
(11, 'Yard', '2015-03-29 18:33:39', '2015-03-29 18:33:39', 10, 17, 110, 110),
(14, 'demo', '2015-04-08 01:16:18', '2015-04-08 01:16:18', 4, 17, 107, 107),
(15, '', '2015-04-08 01:16:24', '2015-04-08 01:16:24', 4, 17, 107, 107),
(16, '', '2015-04-13 04:33:43', '2015-04-13 04:33:43', 14, 17, 110, 110),
(17, '', '2015-04-13 08:53:55', '2015-04-13 08:53:55', 18, 17, 110, 110),
(18, '', '2015-05-04 10:00:31', '2015-05-04 10:00:31', 45, 17, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE IF NOT EXISTS `maintenance` (
`maintenance_id` int(11) NOT NULL,
  `comments` mediumtext NOT NULL,
  `logged_datetime` datetime NOT NULL,
  `warranty_date` datetime NOT NULL,
  `odo_hour` int(11) NOT NULL,
  `due_date` datetime NOT NULL,
  `testing_date` datetime NOT NULL,
  `end_of_life_date` datetime NOT NULL,
  `maintenance_type` int(11) NOT NULL,
  `notes_next_service` varchar(2000) NOT NULL,
  `exception_created` int(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maintenance`
--

INSERT INTO `maintenance` (`maintenance_id`, `comments`, `logged_datetime`, `warranty_date`, `odo_hour`, `due_date`, `testing_date`, `end_of_life_date`, `maintenance_type`, `notes_next_service`, `exception_created`, `created_on`, `updated_on`, `company_id`, `asset_id`, `created_by`, `updated_by`) VALUES
(4, 'Demo', '2015-03-28 11:18:41', '0000-00-00 00:00:00', 12, '2015-04-23 23:46:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-03-28 11:18:41', '2015-04-01 02:52:35', 17, 8, 110, 110),
(5, 'ghjk', '2015-03-28 14:44:43', '0000-00-00 00:00:00', 5001, '2015-09-29 08:44:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 0, '2015-03-28 14:44:43', '2015-03-28 14:56:33', 17, 9, 110, 110),
(6, 'Test', '2015-03-28 14:58:23', '0000-00-00 00:00:00', 0, '2015-03-31 08:58:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-03-28 14:58:23', '2015-03-28 14:58:23', 17, 9, 110, 110),
(7, 'Test2', '2015-03-28 14:59:23', '0000-00-00 00:00:00', 0, '2015-03-29 08:59:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-03-28 14:59:23', '2015-03-28 14:59:50', 17, 9, 110, 110),
(8, 'Demo', '2015-03-28 15:12:31', '0000-00-00 00:00:00', 0, '2015-03-29 03:40:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-03-28 15:12:31', '2015-03-28 15:12:31', 17, 8, 110, 110),
(9, 'Serviced', '2015-03-29 16:14:58', '0000-00-00 00:00:00', 90000, '2015-09-30 10:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 0, '2015-03-29 16:14:58', '2015-03-29 16:14:58', 17, 10, 110, 110),
(10, 'Serviced', '2015-03-29 18:33:03', '0000-00-00 00:00:00', 250000, '2015-09-30 12:32:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 0, '2015-03-29 18:33:03', '2015-04-01 06:02:33', 17, 10, 110, 110),
(15, 'Test attachment', '2015-04-01 06:05:04', '0000-00-00 00:00:00', 0, '2015-12-02 00:04:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-01 06:05:04', '2015-04-01 06:05:04', 17, 10, 110, 110),
(16, 'd', '2015-04-06 09:29:28', '0000-00-00 00:00:00', 101, '2015-04-06 14:58:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-04-06 09:29:28', '2015-04-06 09:29:28', 17, 4, 110, 110),
(17, 'Testing Fuel Entry', '2015-04-06 09:45:26', '0000-00-00 00:00:00', 101, '2015-04-06 15:14:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-04-06 09:45:26', '2015-04-06 09:45:26', 17, 4, 110, 110),
(18, 'Testing fuel entry - 2', '2015-04-06 09:46:09', '0000-00-00 00:00:00', 201, '2015-04-06 15:15:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-04-06 09:46:09', '2015-04-06 09:46:09', 17, 4, 110, 110),
(19, 'Registration', '2015-04-06 14:41:22', '0000-00-00 00:00:00', 0, '2015-11-07 07:41:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-06 14:41:22', '2015-04-06 14:41:22', 17, 10, 110, 110),
(20, 'ujn', '2015-04-06 19:03:34', '0000-00-00 00:00:00', 0, '2015-05-07 12:03:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-06 19:03:34', '2015-04-06 19:03:34', 17, 8, 110, 110),
(21, 'f', '2015-04-08 05:54:06', '0000-00-00 00:00:00', 0, '2015-10-08 22:53:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-08 05:54:06', '2015-04-08 05:54:06', 17, 10, 110, 110),
(22, 'asd', '2015-04-14 07:09:20', '0000-00-00 00:00:00', 0, '2015-04-14 12:38:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-14 07:09:20', '2015-04-14 07:09:20', 17, 4, 110, 110),
(23, 'sdf', '2015-04-16 10:36:19', '2015-04-17 16:05:55', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-16 10:36:19', '2015-04-16 10:36:19', 17, 21, 110, 110),
(24, 'asd', '2015-04-16 10:36:34', '0000-00-00 00:00:00', 0, '2015-04-18 16:05:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-04-16 10:36:34', '2015-04-16 10:36:34', 17, 21, 110, 110),
(25, 'asdad', '2015-04-16 10:36:45', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-04-19 16:05:55', '0000-00-00 00:00:00', 3, '', 1, '2015-04-16 10:36:45', '2015-04-16 10:36:45', 17, 21, 110, 110),
(26, 'Gh', '2015-04-16 13:05:22', '0000-00-00 00:00:00', 2147483647, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '', 0, '2015-04-16 13:05:22', '2015-04-16 13:05:22', 17, 8, 110, 110),
(27, 'Bhb', '2015-04-16 13:06:08', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-04-30 23:04:16', '0000-00-00 00:00:00', 3, '', 1, '2015-04-16 13:06:08', '2015-04-16 13:06:08', 17, 8, 110, 110),
(28, 'asdasd', '2015-04-18 17:51:36', '0000-00-00 00:00:00', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, '', 0, '2015-04-18 17:51:36', '2015-04-18 17:51:36', 17, 18, 110, 110),
(29, 'asdads', '2015-04-18 17:51:51', '2015-04-25 23:21:13', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-18 17:51:51', '2015-04-18 17:51:51', 17, 18, 110, 110),
(30, 'wqweqwe', '2015-04-27 13:43:20', '2015-04-30 19:12:45', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-04-27 13:43:20', '2015-04-27 13:43:20', 15, 38, 110, 110),
(31, 'sadasd', '2015-05-01 12:05:26', '2015-05-01 17:35:09', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-01 12:05:26', '2015-05-01 12:05:26', 17, 10, 110, 110),
(32, 'dfsfdsf', '2015-05-01 12:52:20', '2015-05-01 18:22:10', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-01 12:52:20', '2015-05-01 12:52:20', 17, 18, 110, 110),
(33, 'sdaasd', '2015-05-01 12:52:45', '2015-05-01 18:22:37', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-01 12:52:45', '2015-05-01 12:52:45', 17, 10, 110, 110),
(34, '12', '2015-05-04 10:00:18', '2015-05-04 15:29:52', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-04 10:00:18', '2015-05-04 10:00:18', 17, 45, 110, 110),
(35, 'de', '2015-05-05 07:24:43', '2015-05-05 12:54:35', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-05 07:24:43', '2015-05-05 07:24:43', 17, 46, 110, 110),
(36, 'Demo', '2015-05-11 13:50:58', '2015-05-11 19:20:07', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-11 13:50:58', '2015-05-11 13:50:58', 17, 10, 110, 110),
(37, 'demo', '2015-05-11 14:00:42', '2015-05-11 19:29:21', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-11 14:00:42', '2015-05-11 14:00:42', 17, 49, 110, 110),
(38, 'demo`', '2015-05-11 14:01:24', '0000-00-00 00:00:00', 0, '2015-05-11 19:29:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-11 14:01:24', '2015-05-11 14:01:24', 17, 49, 110, 110),
(39, 'demo', '2015-05-11 14:01:43', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-11 19:29:21', '0000-00-00 00:00:00', 3, '', 1, '2015-05-11 14:01:43', '2015-05-11 14:01:43', 17, 49, 110, 110),
(40, 'demo', '2015-05-12 04:44:09', '2015-05-12 10:13:23', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 04:44:09', '2015-05-12 04:44:09', 17, 49, 110, 110),
(41, 'demo<br><br>', '2015-05-12 07:22:58', '2015-05-12 12:52:39', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 07:22:58', '2015-05-12 07:22:58', 17, 50, 110, 110),
(42, 'demo', '2015-05-12 07:23:16', '0000-00-00 00:00:00', 0, '2015-05-12 12:52:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 07:23:16', '2015-05-12 07:23:16', 17, 50, 110, 110),
(43, 'demo', '2015-05-12 07:23:34', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-12 12:52:39', '0000-00-00 00:00:00', 3, '', 1, '2015-05-12 07:23:34', '2015-05-12 07:23:34', 17, 50, 110, 110),
(44, 'aasdjkh', '2015-05-12 07:23:46', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-05-12 12:52:39', 4, '', 1, '2015-05-12 07:23:46', '2015-05-12 07:23:46', 17, 50, 110, 110),
(45, 'dsadadad', '2015-05-12 07:26:14', '2015-05-12 11:54:47', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 07:26:14', '2015-05-12 07:26:14', 17, 50, 110, 110),
(46, 'sadhakjsh', '2015-05-12 07:27:23', '2015-05-12 11:57:13', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 07:27:23', '2015-05-12 07:27:23', 17, 51, 110, 110),
(47, 'sadad', '2015-05-12 07:28:04', '0000-00-00 00:00:00', 0, '2015-05-12 07:27:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 07:28:04', '2015-05-12 07:28:04', 17, 51, 110, 110),
(48, 'dss', '2015-05-12 10:12:31', '2015-05-12 10:11:28', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:12:31', '2015-05-12 10:12:31', 17, 52, 110, 110),
(49, 'werwer', '2015-05-12 10:12:46', '0000-00-00 00:00:00', 0, '2015-05-12 10:11:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 10:12:46', '2015-05-12 10:12:46', 17, 52, 110, 110),
(50, 'werwer', '2015-05-12 10:13:01', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-12 10:11:28', '0000-00-00 00:00:00', 3, '', 1, '2015-05-12 10:13:01', '2015-05-12 10:13:01', 17, 52, 110, 110),
(51, 'qweqe', '2015-05-12 10:13:16', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-05-12 10:11:28', 4, '', 1, '2015-05-12 10:13:16', '2015-05-12 10:13:16', 17, 52, 110, 110),
(52, 'sadad', '2015-05-12 10:38:16', '2015-05-12 10:36:55', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:38:16', '2015-05-12 10:38:16', 17, 53, 110, 110),
(53, 'asdasd', '2015-05-12 10:40:47', '2015-05-12 10:40:02', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:40:47', '2015-05-12 10:40:47', 17, 53, 110, 110),
(54, 'afasd', '2015-05-12 10:40:57', '2015-05-12 10:40:02', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:40:57', '2015-05-12 10:40:57', 17, 53, 110, 110),
(55, 'sadasd', '2015-05-12 10:41:52', '0000-00-00 00:00:00', 0, '2015-05-12 10:40:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 10:41:52', '2015-05-12 10:41:52', 17, 53, 110, 110),
(56, 'wqdadas', '2015-05-12 10:51:14', '2015-05-12 10:43:42', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:51:14', '2015-05-12 10:51:14', 17, 53, 110, 110),
(57, 'demo', '2015-05-12 10:56:04', '2015-05-12 10:43:42', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:56:04', '2015-05-12 10:56:04', 17, 54, 110, 110),
(58, 'demo', '2015-05-12 10:56:29', '2015-05-12 10:43:42', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:56:29', '2015-05-12 10:56:29', 17, 55, 110, 110),
(59, 'demo', '2015-05-12 10:59:22', '2015-05-12 10:57:08', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 10:59:22', '2015-05-12 10:59:22', 17, 56, 110, 110),
(60, 's', '2015-05-12 10:59:36', '0000-00-00 00:00:00', 0, '2015-05-12 10:57:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 10:59:36', '2015-05-12 10:59:36', 17, 56, 110, 110),
(61, 's', '2015-05-12 10:59:47', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-12 10:57:08', '0000-00-00 00:00:00', 3, '', 1, '2015-05-12 10:59:47', '2015-05-12 10:59:47', 17, 56, 110, 110),
(62, 'w', '2015-05-12 10:59:57', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-05-12 10:57:08', 4, '', 1, '2015-05-12 10:59:57', '2015-05-12 10:59:57', 17, 56, 110, 110),
(63, 'aas', '2015-05-12 12:06:29', '2015-05-12 12:36:15', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-12 12:06:29', '2015-05-12 12:06:29', 17, 57, 110, 110),
(64, 'asd', '2015-05-12 12:06:44', '0000-00-00 00:00:00', 0, '2015-05-12 12:36:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, '2015-05-12 12:06:44', '2015-05-12 12:06:44', 17, 57, 110, 110),
(65, 'asd', '2015-05-12 12:07:10', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-12 12:06:15', '0000-00-00 00:00:00', 3, '', 1, '2015-05-12 12:07:10', '2015-05-12 12:07:10', 17, 57, 110, 110),
(66, 'saa', '2015-05-12 12:07:27', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-05-12 12:06:15', 4, '', 1, '2015-05-12 12:07:27', '2015-05-12 12:07:27', 17, 57, 110, 110),
(67, 'asd', '2015-05-22 07:50:23', '2015-05-22 13:20:01', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '2015-05-22 07:50:23', '2015-05-22 07:50:23', 17, 28, 138, 138),
(68, 'demo', '2015-05-27 20:21:26', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-06-08 01:50:52', '0000-00-00 00:00:00', 3, '', 0, '2015-05-27 20:21:26', '2015-05-27 20:21:26', 17, 60, 110, 110),
(69, 'demo', '2015-05-27 20:21:41', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-06-15 01:50:52', '0000-00-00 00:00:00', 3, '', 0, '2015-05-27 20:21:41', '2015-05-27 20:21:41', 17, 60, 110, 110),
(70, 'Demo', '2015-05-28 09:54:12', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2015-05-28 15:23:54', '0000-00-00 00:00:00', 3, 'demo', 0, '2015-05-28 09:54:12', '2015-05-28 09:54:12', 17, 61, 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_attachments`
--

CREATE TABLE IF NOT EXISTS `maintenance_attachments` (
`maintenance_attachment_id` int(11) NOT NULL,
  `maintenance_id` int(11) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `attachment_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maintenance_attachments`
--

INSERT INTO `maintenance_attachments` (`maintenance_attachment_id`, `maintenance_id`, `attachment`, `attachment_name`, `created_by`, `updated_by`, `created_on`, `updated_on`) VALUES
(1, 4, '1f542cc4ffcc126ae181bf5829702507.csv', '297458683.csv', 110, 110, '2015-04-01 02:52:35', '2015-04-01 02:52:35'),
(2, 10, '0c7de91766bfc5181b9ba5a0265e1447.png', 'Untitled.png', 110, 110, '2015-04-01 06:02:33', '2015-04-01 06:02:33'),
(3, 15, '9f0e8ae3853cd558fe43a722afbec7f8.png', 'Untitled.png', 110, 110, '2015-04-01 06:05:04', '2015-04-01 06:05:04'),
(4, 22, '6518a224aeff4202321765d5cbf33c56.xlsx', 'permissions.xlsx', 110, 110, '2015-04-14 07:09:20', '2015-04-14 07:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
`module_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `controller_name` varchar(60) NOT NULL,
  `action_name` varchar(60) NOT NULL,
  `permissions_required` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `name`, `description`, `controller_name`, `action_name`, `permissions_required`, `created_at`, `updated_at`) VALUES
(2, 'Assets', 'Assets management', 'assets', 'add', 2, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(3, 'Asset Types', 'Manage asset types', 'asset_types', 'index', 1, '2015-04-15 00:00:00', '2015-04-15 00:00:00'),
(4, 'Fuel', 'Fuel management', 'fuel', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(5, 'Maintenance', 'Maintenance management', 'maintenance', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(6, 'Exceptions', 'Exceptions management', 'exceptions', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(7, 'Locations', 'Location management', 'locations', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(8, 'Users', 'Users management', 'users', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(9, 'Roles and Permissions', 'Roles and Permissions management', 'roles', 'index', 1, '2015-03-18 00:00:00', '2015-03-18 00:00:00'),
(10, 'Reporting', 'Manage reporting', 'reporting', 'index', 1, '2015-05-22 00:00:00', '2015-05-22 00:00:00'),
(11, 'Accounting', 'Manage asset accounting', 'accounting', 'index', 1, '2015-06-03 00:00:00', '2015-06-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `module_roles`
--

CREATE TABLE IF NOT EXISTS `module_roles` (
`module_role_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `permissions` int(11) NOT NULL,
  `sub_permissions` varchar(4000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_roles`
--

INSERT INTO `module_roles` (`module_role_id`, `role_id`, `module_id`, `permissions`, `sub_permissions`, `created_at`, `updated_at`) VALUES
(85, 13, 2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 13, 4, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 13, 5, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 13, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 13, 7, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 13, 8, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 13, 9, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 14, 2, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 14, 4, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 14, 5, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 14, 6, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 14, 7, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 14, 8, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 14, 9, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 16, 2, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 16, 4, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 16, 5, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 16, 6, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 16, 7, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 16, 8, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 16, 9, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 17, 2, 2, '{"maintenance":"2","location":"2","exception":"2"}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 17, 3, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 17, 4, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 17, 5, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 17, 6, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 17, 7, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 17, 8, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 17, 9, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 18, 2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 18, 4, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 18, 5, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 18, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 18, 7, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 18, 8, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 18, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 19, 2, 1, '{"maintenance":"2","exception":"2","fuel":"2"}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 19, 4, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 19, 5, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 19, 6, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 19, 7, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 19, 8, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 19, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 20, 2, 1, '{"location":"2","exception":"2"}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 20, 4, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 20, 5, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 20, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 20, 7, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 20, 8, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 20, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 21, 2, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 21, 4, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 21, 5, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 21, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 21, 7, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 21, 8, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 21, 9, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 22, 2, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 22, 4, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 22, 5, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 22, 6, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 22, 7, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 22, 8, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 22, 9, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 16, 3, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 21, 3, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 23, 2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 23, 3, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 23, 4, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 23, 5, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 23, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 23, 7, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 23, 8, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 23, 9, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 25, 6, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 25, 5, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 25, 4, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 25, 3, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 25, 2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 25, 7, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 25, 8, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 25, 9, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 20, 3, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 17, 10, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 17, 11, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 25, 10, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 25, 11, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
`option_id` int(11) NOT NULL,
  `option` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`option_id`, `option`, `value`) VALUES
(1, 'cms_user_dashboard_notice', ' <p>Please contact us at service@xyrintech.com in order to begin using this software.&nbsp;<br></p><p>(This is dynamic text and can be managed from admin panel)</p> '),
(2, 'cms_user_dashboard_notice_title', ' Welcome to AirApp!<br> ');

-- --------------------------------------------------------

--
-- Table structure for table `remember_tokens`
--

CREATE TABLE IF NOT EXISTS `remember_tokens` (
`remember_token_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `token` varchar(400) NOT NULL,
  `created_on` datetime NOT NULL,
  `expire_on` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `remember_tokens`
--

INSERT INTO `remember_tokens` (`remember_token_id`, `user_id`, `token`, `created_on`, `expire_on`) VALUES
(1, 106, '152$2y$10$B61IPpoPZC5Gdi1CwKHngeiBCm3sefi0QzXORXzG6wRKY5bhYW9vK', '2015-05-02 11:45:02', '2015-05-16 11:45:02'),
(2, 106, '578$2y$10$Lo8Ipk32QSvYtd3nnn9hM.ui7RQVrdd1fLxcFoXSf1nZUlKDfBPFK', '2015-05-02 11:50:55', '2015-05-16 11:50:55'),
(3, 106, '758$2y$10$WBLaTBi40fMBEXQWejzQN.ZNYY5Xq1ps2EsLXVMTW.6l1beQVh8gW', '2015-05-02 12:41:26', '2015-05-16 12:41:26'),
(4, 106, '345$2y$10$tR8MFs0MaThK2.EAdWlDHuhDVClqhkYFUVpqQ3jMwf8sFA1IU22Ty', '2015-05-02 12:56:12', '2015-05-16 12:56:12'),
(5, 106, '603$2y$10$iVB1.IqSb3q6iPwUr10o.uL7gCdhTCmWuYMJrzswgRH7VRI5su5lu', '2015-05-02 13:12:40', '2015-05-16 13:12:40'),
(6, 106, '500$2y$10$ZdVuDQwfBoh6ZTkfyLFKFu5sSbJqx87L78VvnLLyDr3vBWTmpXXyy', '2015-05-02 14:05:04', '2015-05-16 14:05:04'),
(7, 106, '595$2y$10$P6akjcFhUGEezCA5dGumxefwMdGB38j5JA26DDcvbXLglGQg0TxQy', '2015-05-02 14:13:23', '2015-05-16 14:13:23'),
(8, 106, '667$2y$10$fUKeILRnXMjUrgqYPTIxxeBvaEbsRM.0f8pFj6lcfA6s4doljf0hG', '2015-05-02 14:52:45', '2015-05-16 14:52:45'),
(9, 106, '200$2y$10$r0rhk3ICHuSwsl3H/gwbVOy.W8MM4S35ws.iyLRjmFLc74.M1se72', '2015-05-02 15:03:20', '2015-05-16 15:03:20'),
(10, 106, '849$2y$10$9dVnGZtNS4zKwC05IwEln.k/sXWqzLsGcoqyaMafl0qUsSyO9bcR2', '2015-05-02 15:04:01', '2015-05-16 15:04:01'),
(11, 106, '672$2y$10$qfkyBUbf1uK1j/CBgYUFAu.qkiE0d/ACNI3vzbfCDFzQHM6LvPvlW', '2015-05-05 08:00:56', '2015-05-19 08:00:56'),
(12, 106, '866$2y$10$8JjPDJDU2.tqtqYYwDy58emium6qgjzkTOGiSdB4r7HgiR1vR.Tf2', '2015-05-05 09:43:51', '2015-05-19 09:43:51'),
(13, 106, '194$2y$10$HZppqZLVg9YjblNXFzB0d.uj.HDfp.k.Ox5Zu//U45pvKxcpH5tse', '2015-05-21 12:26:20', '2015-06-04 12:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`role_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `description`, `created_at`, `updated_at`, `company_id`) VALUES
(13, 'Super Admin', 'Super Admin', '2015-03-22 23:47:00', '2015-03-22 23:47:00', 14),
(14, 'View Only', 'View Only', '2015-03-23 00:17:30', '2015-03-23 00:17:30', 16),
(16, 'View Only', 'sd', '2015-03-23 11:27:36', '2015-04-15 10:56:26', 15),
(17, 'View Only', 'View Only', '2015-03-26 04:43:38', '2015-06-03 13:38:43', 17),
(18, 'Super Amin', 'Super Admin', '2015-03-27 00:06:12', '2015-03-27 00:06:12', 19),
(19, 'Sample Role', 'sdf dsf sdfsd ', '2015-04-03 05:09:02', '2015-04-03 05:09:21', 19),
(20, 'Exception Special Permission ', 'Exception Special Permission Testing', '2015-04-04 00:53:42', '2015-05-19 05:58:36', 17),
(21, 'Full access', 'Full access', '2015-04-07 12:37:27', '2015-04-27 15:34:14', 17),
(22, 'View Only Demo', 'View Only Demo', '2015-04-13 14:54:45', '2015-04-13 14:54:45', 17),
(23, 'Testing', 'Testing', '2015-04-30 09:38:57', '2015-04-30 09:45:22', 26),
(25, 'Jeremy Role', 'Jeremy Role', '2015-05-16 04:37:30', '2015-06-03 13:43:17', 17);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
`supplier_id` int(11) NOT NULL,
  `company_id` int(255) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `company_id`, `supplier_name`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 0, 'Birla', '2015-04-07 00:00:00', '2015-04-07 00:00:00', 0, 0),
(2, 17, 'Supplier Testing - 1', '2015-04-07 00:00:00', '2015-04-07 00:00:00', 0, 0),
(3, 17, 'Supplier Testing - 1', '2015-04-07 00:00:00', '2015-04-07 00:00:00', 0, 0),
(5, 17, 'demo', '2015-04-24 07:19:24', '2015-04-24 07:19:24', 110, 110),
(6, 17, 'df', '2015-04-24 11:52:10', '2015-04-24 11:52:10', 110, 110),
(7, 17, 'k', '2015-04-29 16:02:19', '2015-04-29 16:02:19', 110, 110);

-- --------------------------------------------------------

--
-- Table structure for table `su_modules`
--

CREATE TABLE IF NOT EXISTS `su_modules` (
`module_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `controller_name` varchar(60) NOT NULL,
  `action_name` varchar(60) NOT NULL,
  `permissions_required` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `su_modules`
--

INSERT INTO `su_modules` (`module_id`, `name`, `description`, `controller_name`, `action_name`, `permissions_required`, `created_at`, `updated_at`) VALUES
(1, 'Roles and Permissions', 'Module to manage roles and permissions', 'roles', 'index', 1, '2015-03-12 00:00:00', '2015-03-12 00:00:00'),
(2, 'Admin Users', 'Module to manage admin user accounts', 'users', 'index', 1, '2015-03-12 00:00:00', '2015-03-12 00:00:00'),
(3, 'Company Users', 'Module to manage company users', 'users', 'cusers', 1, '2015-03-14 00:00:00', '2015-03-14 00:00:00'),
(4, 'Companies', 'Module to manage companies', 'companies', 'index', 1, '2015-03-14 00:00:00', '2015-03-14 00:00:00'),
(5, 'User Dashboard CMS', 'Manage user dashboard cms', 'cms', 'index', 2, '2015-04-10 00:00:00', '2015-04-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `su_module_roles`
--

CREATE TABLE IF NOT EXISTS `su_module_roles` (
`module_role_id` int(11) NOT NULL,
  `permissions` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `su_module_roles`
--

INSERT INTO `su_module_roles` (`module_role_id`, `permissions`, `created_at`, `updated_at`, `role_id`, `module_id`) VALUES
(78, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, 1),
(79, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, 2),
(80, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, 3),
(81, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, 4),
(82, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, 1),
(83, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, 2),
(84, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, 3),
(85, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, 4),
(86, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 1),
(87, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 2),
(88, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 3),
(89, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 4),
(90, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 24, 5),
(91, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 1),
(92, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 2),
(93, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 3),
(94, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 4),
(95, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 25, 5);

-- --------------------------------------------------------

--
-- Table structure for table `su_roles`
--

CREATE TABLE IF NOT EXISTS `su_roles` (
`role_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `su_roles`
--

INSERT INTO `su_roles` (`role_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(22, 'View Only', 'View only', '2015-03-23 03:09:38', '2015-03-26 23:39:04'),
(23, 'Accounts', 'Accounts', '2015-03-26 23:57:40', '2015-04-02 21:56:16'),
(24, 'Super Admin', 'Super Admin', '2015-03-28 15:22:22', '2015-04-11 14:31:17'),
(25, 'View Only Demo', 'View Only Demo', '2015-04-13 14:52:47', '2015-04-13 14:52:47');

-- --------------------------------------------------------

--
-- Table structure for table `su_user_roles`
--

CREATE TABLE IF NOT EXISTS `su_user_roles` (
`user_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `su_user_roles`
--

INSERT INTO `su_user_roles` (`user_role_id`, `created_at`, `updated_at`, `user_id`, `role_id`) VALUES
(11, '2015-03-23 00:13:43', '2015-05-01 12:50:36', 109, 24),
(15, '2015-03-25 05:38:47', '2015-04-02 21:56:38', 117, 23),
(18, '2015-04-08 14:13:44', '2015-04-13 08:22:31', 129, 25);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
`tag_id` int(11) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `device_udid` varchar(255) NOT NULL,
  `units` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tag_bool_values`
--

CREATE TABLE IF NOT EXISTS `tag_bool_values` (
`tag_bool_value_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `value` tinyint(4) NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tag_float_values`
--

CREATE TABLE IF NOT EXISTS `tag_float_values` (
`tag_float_value_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `value` float NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `is_verified` tinyint(4) NOT NULL,
  `last_company` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `deleted_on` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `first_name`, `last_name`, `password`, `active`, `is_superuser`, `is_verified`, `last_company`, `created_on`, `updated_on`, `deleted_on`, `created_by`, `updated_by`, `deleted_by`, `address`, `city`, `country`) VALUES
(106, 'service@xyrintech.com', 'Xyrin', 'Technologies', '338d2b7719f8d57b3b4968b462d19ff9', 1, 2, 1, 26, '2015-03-22 23:44:08', '2015-04-30 09:38:42', '0000-00-00 00:00:00', 106, 106, 0, 'Address', 'City', 'IN'),
(107, 'nitinr@xyrintech.com', 'Nitin', 'Abrol', '0945fc9611f55fd0e183fb8b044f1afe', 0, 0, 1, 15, '2015-03-22 23:46:06', '2015-05-19 05:02:18', '2015-05-19 05:02:18', 106, 106, 106, 'Address', 'City', 'IN'),
(108, 'xyrinsadasddata@outlook.com', 'Xyrin', 'Data', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 0, 1, 17, '2015-03-22 23:48:44', '2015-04-08 19:33:25', '0000-00-00 00:00:00', 108, 108, 0, 'Address', 'City', 'IN'),
(109, 'admin@admin.com', 'Admin', 'User', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, 1, 1, 24, '2015-03-23 00:13:43', '2015-05-01 12:50:36', '0000-00-00 00:00:00', 106, 106, 0, 'Address', 'City', 'IN'),
(110, 'company@xyrintech.com', 'Company', 'Testing User', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 1, 17, '2015-03-23 00:15:31', '2015-06-03 14:42:02', '0000-00-00 00:00:00', 106, 106, 0, 'Address', 'City', 'IN'),
(111, 'abhijeetthakur26@gmail.com', 'abhijeet', 'thakur', '57701f4c796f3f81a49bb5f5f0e957b6', 1, 0, 1, 15, '2015-03-23 11:30:30', '2015-03-23 11:31:55', '0000-00-00 00:00:00', 111, 111, 0, 'new gulmandi h.no 5-23-47 aurangabad', 'Aurangabad', 'IN'),
(112, 'davinder17s@outlook.com', 'Davinder', 'Singh', '71e9fb45ef67a78425ba9339aee64a3c', 1, 0, 1, 0, '2015-03-24 00:28:45', '2015-03-24 00:28:45', '0000-00-00 00:00:00', 112, 112, 0, '', '', ''),
(113, 'demo@demo.com', 'Demo', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 1, 1, 0, '2015-03-24 08:21:04', '2015-03-24 14:30:14', '2015-03-24 14:30:14', 106, 106, 106, '', '', 'AS'),
(114, 'demo@xyrintech.com', 'demo', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, 1, 1, 0, '2015-03-24 08:21:55', '2015-03-24 14:30:21', '2015-03-24 14:30:21', 106, 106, 106, '', '', 'AS'),
(115, 'demo1@demo.com', 'demo', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, 1, 1, 0, '2015-03-24 08:22:32', '2015-03-24 14:30:25', '2015-03-24 14:30:25', 106, 106, 106, '', '', 'AS'),
(116, 'demo@xyrisntech.com', 'Demo', 'demo', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 0, 1, 0, '2015-03-24 23:24:39', '2015-04-02 09:12:11', '2015-04-02 09:12:11', 106, 106, 106, 'hjgg', 'jhghj', 'IN'),
(117, 'demo@d.com', 'Demo', 'Demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, 1, 1, 0, '2015-03-25 05:38:47', '2015-04-02 21:56:38', '0000-00-00 00:00:00', 106, 106, 0, 'demo', 'demo', 'AI'),
(118, 'demo@dd.com', 'demo', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, 1, 1, 0, '2015-03-25 05:39:14', '2015-03-25 07:49:52', '2015-03-25 07:49:52', 106, 106, 106, 'demo', 'demo', 'AD'),
(119, 'princekumar00923@gmail.com', 'prince', 'kumar', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, 1, 0, '2015-03-26 23:58:26', '2015-04-02 21:56:26', '2015-04-02 21:56:26', 106, 106, 106, '1127 konat place', 'ludhiana', 'IO'),
(120, 'prince6478@gmail.om', 'prince', 'kumar', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 1, 19, '2015-03-27 00:00:49', '2015-03-29 09:46:24', '0000-00-00 00:00:00', 120, 120, 0, '1127 konat place', 'ludhiana', 'AO'),
(121, 'prince_amit50@yahoo.co.in', 'prince', 'kumar', 'e10adc3949ba59abbe56e057f20f883e', 0, 0, 1, 0, '2015-03-27 03:32:28', '2015-04-02 09:12:44', '2015-04-02 09:12:44', 121, 121, 106, '1127 konat place', 'ludhiana', 'AD'),
(122, 'prince6478@gmail.com', 'prince', 'kumar', '2077e4a6bafa9b4e7b55e1fff16818af', 0, 0, 1, 0, '2015-03-28 02:10:46', '2015-04-02 09:13:14', '2015-04-02 09:13:14', 122, 122, 106, '123', 'ludhiana', 'DZ'),
(123, 'sameersoumil1988@outlook.com', 'Sameer', 'Soumil', '0318d040be0986bf5abdfdbf4e18cf18', 0, 0, 1, 0, '2015-03-28 09:23:52', '2015-04-28 11:53:34', '2015-04-28 11:53:34', 123, 123, 106, '', '', ''),
(124, 'sameersoumil19881@gmail.com', 'Sameer', 'Soumil', '285f05d6e052b6cf00a4847669326fb3', 0, 0, 1, 0, '2015-03-28 09:24:09', '2015-04-25 14:31:06', '2015-04-25 14:31:06', 124, 124, 106, '', '', ''),
(125, 'andrew.frahn@chsgroup.com.au', 'Andrew', 'Frahn', '34aac35c2f862b6e961d09772bcf4df7', 1, 0, 1, 21, '2015-03-31 16:03:51', '2015-04-15 06:11:27', '0000-00-00 00:00:00', 125, 125, 0, '', '', ''),
(126, 'development@xyrintech.com', 'Xyrin', 'Technologies', '91d84e8927e1124b2c8346ccae972ba5', 1, 0, 1, 0, '2015-04-02 02:03:51', '2015-04-02 02:03:51', '0000-00-00 00:00:00', 126, 126, 0, '', '', ''),
(127, 'deeee@demo.com', 'Demo', 'demo', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 0, 1, 0, '2015-04-04 12:49:58', '2015-04-28 14:03:29', '2015-04-28 14:03:29', 0, 0, 106, '', '', ''),
(128, 'xyrindatakjaHSjksh@outlook.com', 'Xyrin', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 0, 1, 17, '2015-04-08 11:02:20', '2015-04-10 11:02:38', '0000-00-00 00:00:00', 128, 128, 0, 'Address', 'City', 'IN'),
(129, 'demo@user.com', 'demouser', 'demo123', 'f759c1e4869f0dc87025f5884035884a', 1, 1, 1, 24, '2015-04-08 14:13:44', '2015-04-13 08:22:31', '0000-00-00 00:00:00', 106, 106, 0, '', '', ''),
(130, 'xyrindata@outlook.com', 'Xyrin', 'Dploy', '6365cc2b1f5743addde374698953d4d7', 1, 0, 1, 17, '2015-04-10 11:07:24', '2015-05-20 04:51:39', '0000-00-00 00:00:00', 130, 130, 0, '', '', ''),
(131, 'xyrin.dploy@gmail.com', 'Demo', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 0, 1, 0, '2015-04-10 11:13:02', '2015-04-11 06:38:18', '0000-00-00 00:00:00', 131, 131, 0, 'Address', '', ''),
(133, 'sameersasdasoumil1988@gmail.com', 'Sameer', 'Soumil', '049c0a2b823ffe0fbcdd91a204f942b4', 0, 0, 1, 0, '2015-04-11 11:50:13', '2015-04-28 11:53:12', '2015-04-28 11:53:12', 133, 133, 106, '', '', ''),
(134, 'jnzfzyg_qinsen_1428752358@tfbnw.net', 'Barbara', 'Qinsen', 'd59d014af41a1260c024af74466a36f6', 0, 0, 1, 0, '2015-04-11 11:55:39', '2015-04-28 11:53:17', '2015-04-28 11:53:17', 134, 134, 106, '', '', ''),
(135, 'xmlhcbm_bowersescu_1428754043@tfbnw.net', 'Dorothy', 'Bowersescu', '23660f5e6d674db3fb73b356a14f19fa', 0, 0, 1, 0, '2015-04-11 05:10:05', '2015-04-28 11:53:20', '2015-04-28 11:53:20', 135, 135, 106, '', '', ''),
(136, 'sameersouasdadamil1988@gmail.com', 'Sameer', 'Soumil', '620c4abd00098a0773531df17a1f023a', 0, 0, 0, 0, '2015-04-11 14:18:47', '2015-04-27 18:24:45', '2015-04-27 18:24:45', 136, 136, 106, '', '', ''),
(137, 'sameersoumil1988@gmail.com', 'Sameer', 'Soumil', '76c95e32c5aa9d077b881d9a714fc4e5', 0, 0, 1, 0, '2015-04-11 07:26:01', '2015-04-28 11:53:25', '2015-04-28 11:53:25', 137, 137, 106, '', '', ''),
(138, 'service@nerdforall.com', 'Nerd', 'All', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 0, 1, 17, '2015-04-16 13:48:55', '2015-04-16 13:49:02', '0000-00-00 00:00:00', 138, 138, 0, '', '', ''),
(139, 'davinder17s@gmail.com', 'Davinder', 'Singh', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 1, 0, '2015-04-24 14:04:09', '2015-05-02 12:42:04', '0000-00-00 00:00:00', 139, 139, 0, '', '', ''),
(140, 'dkjhkjhkh@demo.com', 'demo', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, 0, 0, 0, '2015-04-24 14:04:31', '2015-04-27 18:24:56', '2015-04-27 18:24:56', 140, 140, 106, 'kjh', '', ''),
(141, 'xyrintechnologie.s@gmail.com', 'fone', 'lone', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 1, 26, '2015-04-30 09:42:59', '2015-04-30 09:42:59', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(142, 'xyrin.technologies@gmail.com', 'ftwo', 'ltwo', 'c33367701511b4f6020ec61ded352059', 1, 0, 1, 26, '2015-04-30 09:42:59', '2015-04-30 09:42:59', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(143, 'xyrintechnologi.es@gmail.com', 'fone', 'lone', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 1, 26, '2015-04-30 09:57:35', '2015-04-30 09:57:35', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(144, 'xyri.ntechnologies@gmail.com', 'ftwo', 'ltwo', 'c33367701511b4f6020ec61ded352059', 1, 0, 1, 26, '2015-04-30 09:57:35', '2015-04-30 09:57:35', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(145, 'help@airapp.io', 'test', 'one', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 0, 1, 26, '2015-04-30 10:31:08', '2015-04-30 10:31:08', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(146, 'emmertextesttwo@gmail.com', 'test', 'two', 'd3345bf838db8ae167b7fa6784a001c0', 1, 0, 1, 26, '2015-04-30 10:31:09', '2015-04-30 10:31:09', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, NULL),
(147, 'bo@z-thru.com', 'Bo', 'Ekkelund', 'b5bb5ef1d4a113cee7e9ffe1161d4b9b', 0, 0, 1, 17, '2015-05-05 08:49:12', '2015-05-15 09:29:05', '2015-05-15 09:29:05', 147, 147, 106, 'Jonstrup', 'Jonstrup', 'DK'),
(148, 'tintu@tintu.com', 'Tintu', '', 'ed11644d4d2ff788b57b406b0f5087be', 0, 0, 0, 27, '2015-05-06 15:31:27', '2015-05-07 06:54:23', '2015-05-07 06:54:23', 106, 106, 106, '', '', ''),
(149, 'jeremy@amazingkidsparties.com.au', 'Jeremy', 'Cox', '05a671c66aefea124cc08b76ea6d30bb', 1, 0, 1, 17, '2015-05-16 05:04:28', '2015-05-16 05:04:43', '0000-00-00 00:00:00', 149, 149, 0, '', '', ''),
(150, 'testusercompany@xyrintech.com', 'test user company', '', '2693263661bd8632604b9c787965411e', 1, 0, 1, 0, '2015-05-19 04:59:39', '2015-05-19 04:59:39', '0000-00-00 00:00:00', 106, 106, 0, '', '', ''),
(151, 'kjhkjh@klhjk.com', 'ZXCzC', '', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, 0, 1, 0, '2015-05-19 05:01:47', '2015-05-19 05:01:47', '0000-00-00 00:00:00', 106, 106, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`user_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `created_at`, `updated_at`, `role_id`, `user_id`) VALUES
(8, '2015-03-22 23:48:47', '2015-03-22 23:48:47', 13, 108),
(33, '2015-05-04 07:46:02', '2015-05-04 07:46:02', 16, 107),
(10, '2015-03-23 11:31:39', '2015-03-23 11:31:39', 16, 111),
(27, '2015-05-02 11:56:16', '2015-05-02 11:56:16', 17, 110),
(18, '2015-04-09 15:07:09', '2015-04-09 15:07:09', 21, 110),
(28, '2015-05-02 11:58:44', '2015-05-02 11:58:44', 17, 110),
(20, '2015-04-10 11:13:20', '2015-04-10 11:13:20', 21, 131),
(23, '2015-04-30 09:57:35', '2015-04-30 09:57:35', 23, 143),
(22, '2015-04-29 15:13:20', '2015-04-29 15:13:20', 21, 110),
(24, '2015-04-30 09:57:35', '2015-04-30 09:57:35', 23, 144),
(25, '2015-04-30 10:31:08', '2015-04-30 10:31:08', 23, 145),
(26, '2015-04-30 10:31:09', '2015-04-30 10:31:09', 23, 146),
(29, '2015-05-02 12:00:24', '2015-05-02 12:00:24', 17, 110),
(30, '2015-05-02 13:11:07', '2015-05-02 13:11:07', 20, 110),
(32, '2015-05-04 02:53:55', '2015-05-04 02:53:55', 17, 110),
(39, '2015-05-16 05:04:28', '2015-05-16 05:04:28', 25, 149),
(35, '2015-05-04 07:56:37', '2015-05-04 07:56:37', 17, 138),
(37, '2015-05-05 06:46:13', '2015-05-05 06:46:13', 17, 130);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
 ADD PRIMARY KEY (`asset_id`), ADD KEY `fk_assets_companies1_idx` (`company_id`), ADD KEY `fk_assets_asset_types1_idx` (`asset_type_id`);

--
-- Indexes for table `asset_documents`
--
ALTER TABLE `asset_documents`
 ADD PRIMARY KEY (`asset_document_id`), ADD KEY `fk_asset_photos_assets1_idx` (`asset_id`), ADD KEY `fk_asset_photos_companies1_idx` (`company_id`);

--
-- Indexes for table `asset_expenses`
--
ALTER TABLE `asset_expenses`
 ADD PRIMARY KEY (`asset_expense_id`);

--
-- Indexes for table `asset_favourites`
--
ALTER TABLE `asset_favourites`
 ADD PRIMARY KEY (`asset_favourite_id`), ADD KEY `fk_asset_favourites_companies1_idx` (`company_id`), ADD KEY `fk_asset_favourites_assets1_idx` (`asset_id`);

--
-- Indexes for table `asset_managers`
--
ALTER TABLE `asset_managers`
 ADD PRIMARY KEY (`asset_manager_id`);

--
-- Indexes for table `asset_meta`
--
ALTER TABLE `asset_meta`
 ADD PRIMARY KEY (`asset_meta_id`);

--
-- Indexes for table `asset_photos`
--
ALTER TABLE `asset_photos`
 ADD PRIMARY KEY (`asset_photo_id`), ADD KEY `fk_asset_photos_assets1_idx` (`asset_id`), ADD KEY `fk_asset_photos_companies1_idx` (`company_id`);

--
-- Indexes for table `asset_types`
--
ALTER TABLE `asset_types`
 ADD PRIMARY KEY (`asset_type_id`);

--
-- Indexes for table `asset_valuations`
--
ALTER TABLE `asset_valuations`
 ADD PRIMARY KEY (`asset_valuation_id`);

--
-- Indexes for table `authentications`
--
ALTER TABLE `authentications`
 ADD PRIMARY KEY (`authentication_id`), ADD KEY `fk_authentications_users1_idx` (`user_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
 ADD PRIMARY KEY (`company_id`), ADD KEY `fk_companies_users_idx` (`user_id`);

--
-- Indexes for table `company_admins`
--
ALTER TABLE `company_admins`
 ADD PRIMARY KEY (`company_admin_id`);

--
-- Indexes for table `company_preferences`
--
ALTER TABLE `company_preferences`
 ADD PRIMARY KEY (`company_preference_id`), ADD KEY `fk_company_preferences_companies1_idx` (`company_id`);

--
-- Indexes for table `cron_emails`
--
ALTER TABLE `cron_emails`
 ADD PRIMARY KEY (`cron_email_id`);

--
-- Indexes for table `device_json_payload`
--
ALTER TABLE `device_json_payload`
 ADD PRIMARY KEY (`device_json_payload_id`);

--
-- Indexes for table `exceptions`
--
ALTER TABLE `exceptions`
 ADD PRIMARY KEY (`exception_id`), ADD KEY `fk_exceptions_users1_idx` (`created_user_id`), ADD KEY `fk_exceptions_users2_idx` (`closed_user_id`), ADD KEY `fk_exceptions_assets1_idx` (`asset_id`), ADD KEY `fk_exceptions_companies1_idx` (`company_id`);

--
-- Indexes for table `exception_notes`
--
ALTER TABLE `exception_notes`
 ADD PRIMARY KEY (`exception_note_id`), ADD KEY `fk_exception_notes_exceptions1_idx` (`exception_id`);

--
-- Indexes for table `exception_subscriptions`
--
ALTER TABLE `exception_subscriptions`
 ADD PRIMARY KEY (`exception_subscription_id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
 ADD PRIMARY KEY (`favourite_id`);

--
-- Indexes for table `fuel`
--
ALTER TABLE `fuel`
 ADD PRIMARY KEY (`fuel_id`), ADD KEY `fk_fuel_assets1_idx` (`asset_id`), ADD KEY `fk_fuel_companies1_idx` (`company_id`);

--
-- Indexes for table `invites`
--
ALTER TABLE `invites`
 ADD PRIMARY KEY (`invite_id`);

--
-- Indexes for table `locations_geo`
--
ALTER TABLE `locations_geo`
 ADD PRIMARY KEY (`location_geo_id`), ADD KEY `fk_locations_text_assets1_idx` (`asset_id`), ADD KEY `fk_locations_text_companies1_idx` (`company_id`);

--
-- Indexes for table `locations_text`
--
ALTER TABLE `locations_text`
 ADD PRIMARY KEY (`location_text_id`), ADD KEY `fk_locations_text_assets1_idx` (`asset_id`), ADD KEY `fk_locations_text_companies1_idx` (`company_id`);

--
-- Indexes for table `maintenance`
--
ALTER TABLE `maintenance`
 ADD PRIMARY KEY (`maintenance_id`), ADD KEY `fk_maintenance_companies1_idx` (`company_id`), ADD KEY `fk_maintenance_assets1_idx` (`asset_id`);

--
-- Indexes for table `maintenance_attachments`
--
ALTER TABLE `maintenance_attachments`
 ADD PRIMARY KEY (`maintenance_attachment_id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
 ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `module_roles`
--
ALTER TABLE `module_roles`
 ADD PRIMARY KEY (`module_role_id`), ADD KEY `fk_module_roles_roles1_idx` (`role_id`), ADD KEY `fk_module_roles_modules1_idx` (`module_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
 ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `remember_tokens`
--
ALTER TABLE `remember_tokens`
 ADD PRIMARY KEY (`remember_token_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`role_id`), ADD KEY `fk_roles_companies1_idx` (`company_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
 ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `su_modules`
--
ALTER TABLE `su_modules`
 ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `su_module_roles`
--
ALTER TABLE `su_module_roles`
 ADD PRIMARY KEY (`module_role_id`), ADD KEY `fk_su_module_roles_su_roles1_idx` (`role_id`), ADD KEY `fk_su_module_roles_su_modules1_idx` (`module_id`);

--
-- Indexes for table `su_roles`
--
ALTER TABLE `su_roles`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `su_user_roles`
--
ALTER TABLE `su_user_roles`
 ADD PRIMARY KEY (`user_role_id`), ADD KEY `fk_user_roles_users2_idx` (`user_id`), ADD KEY `fk_su_user_roles_su_roles1_idx` (`role_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
 ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `tag_bool_values`
--
ALTER TABLE `tag_bool_values`
 ADD PRIMARY KEY (`tag_bool_value_id`);

--
-- Indexes for table `tag_float_values`
--
ALTER TABLE `tag_float_values`
 ADD PRIMARY KEY (`tag_float_value_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`user_role_id`), ADD KEY `fk_user_roles_roles1_idx` (`role_id`), ADD KEY `fk_user_roles_users2_idx` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
MODIFY `asset_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `asset_documents`
--
ALTER TABLE `asset_documents`
MODIFY `asset_document_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `asset_expenses`
--
ALTER TABLE `asset_expenses`
MODIFY `asset_expense_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `asset_managers`
--
ALTER TABLE `asset_managers`
MODIFY `asset_manager_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `asset_meta`
--
ALTER TABLE `asset_meta`
MODIFY `asset_meta_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `asset_photos`
--
ALTER TABLE `asset_photos`
MODIFY `asset_photo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `asset_types`
--
ALTER TABLE `asset_types`
MODIFY `asset_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `asset_valuations`
--
ALTER TABLE `asset_valuations`
MODIFY `asset_valuation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `authentications`
--
ALTER TABLE `authentications`
MODIFY `authentication_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `company_admins`
--
ALTER TABLE `company_admins`
MODIFY `company_admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `company_preferences`
--
ALTER TABLE `company_preferences`
MODIFY `company_preference_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cron_emails`
--
ALTER TABLE `cron_emails`
MODIFY `cron_email_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `device_json_payload`
--
ALTER TABLE `device_json_payload`
MODIFY `device_json_payload_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `exceptions`
--
ALTER TABLE `exceptions`
MODIFY `exception_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `exception_notes`
--
ALTER TABLE `exception_notes`
MODIFY `exception_note_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `exception_subscriptions`
--
ALTER TABLE `exception_subscriptions`
MODIFY `exception_subscription_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
MODIFY `favourite_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `fuel`
--
ALTER TABLE `fuel`
MODIFY `fuel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `invites`
--
ALTER TABLE `invites`
MODIFY `invite_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `locations_geo`
--
ALTER TABLE `locations_geo`
MODIFY `location_geo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `locations_text`
--
ALTER TABLE `locations_text`
MODIFY `location_text_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `maintenance`
--
ALTER TABLE `maintenance`
MODIFY `maintenance_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `maintenance_attachments`
--
ALTER TABLE `maintenance_attachments`
MODIFY `maintenance_attachment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `module_roles`
--
ALTER TABLE `module_roles`
MODIFY `module_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=187;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `remember_tokens`
--
ALTER TABLE `remember_tokens`
MODIFY `remember_token_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `su_modules`
--
ALTER TABLE `su_modules`
MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `su_module_roles`
--
ALTER TABLE `su_module_roles`
MODIFY `module_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `su_roles`
--
ALTER TABLE `su_roles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `su_user_roles`
--
ALTER TABLE `su_user_roles`
MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag_bool_values`
--
ALTER TABLE `tag_bool_values`
MODIFY `tag_bool_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag_float_values`
--
ALTER TABLE `tag_float_values`
MODIFY `tag_float_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

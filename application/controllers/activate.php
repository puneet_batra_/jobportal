<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/9/2015
 * Time: 3:37 PM
 */
class Activate extends CI_Controller
{
    public function account($user_id, $secret)
    {
        $user = $this->db->where('user_id', $user_id)->get('users')->row();
        if ($user != false && $user_id == $user->user_id && md5($user->created_on) == $secret) {
            $this->db->where('user_id', $user_id);
            $this->db->update('users', array('is_verified' => 1));
            $this->session->set_flashdata('success', 'Account verified successfully.');
            if (!$this->auth->check()) {
                redirect('login');
            } else {
                redirect('bckcadmin/home');
            }
        } else {
            show_error('Invalid request code.', 200, 'Account verification');
        }
    }
}
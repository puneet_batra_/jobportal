<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Blog extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();       
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/blog');
        
    }

    public function get(){
        
            $this->datatables
            ->join('blog_category','blog.blog_category_id = blog_category.blog_category_id')
            ->select('blog.blog_id,blog_category.blog_category_name,blog.blog_title,blog.created_at,blog.active')
            ->from('blog');
        
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
    public function category(){
        $category = $this->db->where('active',1)->get("blog_category")->result();

        $option = "";
        foreach($category as $categories){
            $option .= "<option value='".$categories->blog_category_id."'>".ucfirst($categories->blog_category_name)."</option><br>";
        }

        $data = array(
                'status' => 'success',
                'options' => $option
            );
        
        echo json_encode($data);
    }
    public function add(){

        $this->form_validation->set_rules('blog_category','Blog category','trim|required');
        $this->form_validation->set_rules('blog_title','Blog Title','trim|required');
        $this->form_validation->set_rules('blog_description','Blog Description','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'blog_category_id' => $this->input->post('blog_category'),
                'blog_title' => $this->input->post('blog_title'),
                'blog_description' => $this->input->post('blog_description'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s'),
                'user_id' => $this->session->userdata('user_id')
            );
            $this->db->insert('blog',$mysql_data);
            $message = 'New Blog created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('blog_id',$id)->delete('blog');
        $data = array(
            'status' => 'success',
            'message' => 'Blog removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('blog_id',$id)->get('blog')->row();

        $category = $this->db->where('active',1)->get("blog_category")->result();

        $option = "";
        foreach($category as $categories){
            $option .= "<option value='".$categories->blog_category_id."'>".ucfirst($categories->blog_category_name)."</option><br>";
        }

        $data=array(
            'blog_id'=>$edit_content->blog_id,
            'blog_category'=>$edit_content->blog_category_id,
            'blog_title'=>$edit_content->blog_title,
            'blog_description'=>$edit_content->blog_description,
            'options'=>$option,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'blog_category_id'=>$this->input->post('blog_category'),
            'blog_title'=>$this->input->post('blog_title'),
            'blog_description'=>$this->input->post('blog_description'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $blog_id=$this->input->post('blog_id');
        $this->db->where('blog_id',$blog_id)->update('blog',$data);
        $message = 'Blog updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {   
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('blog_id', (int)$this->input->post('uid'));
            $this->db->update('blog', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Blog in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('blog_id', (int)$this->input->post('uid'));
            $this->db->update('blog', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Blog activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

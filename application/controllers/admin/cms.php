<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class CMS extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){

//
//        $this->db->where('parent_id', '0');
//        $data = $this->db->get('pages')->result();
//        $select = null;
//        $arr = array();
//        foreach ($data as $db)
//        {
//            $lvl=0;
//            if($db->parent_id == 0) {
//                $select .= '<tr class="odd gradeX">';
//            }
//
//            $select .= '<td>'.$db->page_title.'</td>';
//            $select .= '<td><a href="'.site_url('page/getEdit/').$db->page_id.'">Edit | </a><a href="'.site_url('page/getDelete/').''.$db->page_id.'">Delete</a></td>';
//            $select .= $this->sub_category($db->page_id, $lvl);
//
//            if($db->parent_id == 0) {
//                $select .= '</tr>';
//            }
////            $select .= '</tr>';
//        }
//        $a['category'] = $select;
//        $inner = array();
//        $inner['title'] = 'Page';
//        $inner['data'] = $a;
        $this->load->view('admin/cms');
    }

    public function get(){

        $this->datatables->select('page_id,page_title,active')
            ->from('pages');

        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function sub_category($id,$lvl)
    {

        $this->db->where('parent_id', $id);
        $data = $this->db->get('pages')->result();
        $select = null;
        $lvl++;
        foreach ($data as $db)
        {
            $space  = $this->level($lvl);
            $space  .= $db->page_title;

            if($db->parent_id != 0) {
                $select .= '</tr>';
                $select .= '<tr class="odd gradeX">';
                $select .= '<td>';
                $select .= $space.'</td>';
                $select .= '<td><a href="'.base_url().'page/getEdit/'.$db->page_id.'">Edit | </a><a href="'.base_url().'page/getDelete/'.$db->page_id.'">Delete</a></td>';
            }

            $select .= $this->sub_category($db->page_id,$lvl);
            //$select .= '</td>';

        }
        return $select;
    }

    public function getAdds(){
        $this->load->view('backend/addPage');
    }


    function getAdd()
    {
        $this->db->where('parent_id', '0');
        $data = $this->db->get('pages')->result();
        $select = null;
        foreach ($data as $db)
        {
            $lvl=0;
            $select .= "<option value=" . $db->page_id . ">" . $db->page_title. "</option>";
            $select .= $this->sub_cate($db->page_id, $lvl);
        }
        $a['category'] = $select;
        $inner['title'] = "Add Page";
        $inner['parent'] = $a;
        $this->load->view('backend/addPage',$inner,TRUE);
    }

    public function sub_cate($id,$lvl)
    {

        $this->db->where('parent_id', $id);
        $data = $this->db->get('pages')->result();
        $select = null;
        $lvl++;
        foreach ($data as $db)
        {
            $space= $this->level($lvl);
            $space.=$db->page_title;
            $select .= "<option value=" . $db->page_id . ">" . $space . "</option>";
            $select .= $this->sub_cate($db->page_id,$lvl);
        }
        return $select;
    }

    public function level($lvl)
    {
        switch ($lvl) {
            case 1:
                $text = " &#45;&#45;";
                // $text = " &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 2:
                $text = "     &#45;&#45;";
                //$text = "     &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 3:
                $text = "       &#45;&#45;";
                // $text = "       &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 4:
                $text = "         &#45;&#45;";
                //  $text = "         &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 5:
                $text = "              &#45;&#45;";
                //  $text = "              &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 6:
                $text = "                      &#45;&#45;";
                //  $text = "                      &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

            case 7:
                $text = "                                 &#45;&#45;";
                //  $text = "                                 &#8735;";
                $text = str_replace(' ', '&nbsp;', $text);
                return $text;
                break;

        }

    }

    public function add(){

        $this->form_validation->set_rules('page_title','Page Title','trim|required');
        $this->form_validation->set_rules('page_content','Page Content','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'page_label' => $this->input->post('page_title'),
                'page_title' => ucfirst($this->input->post('page_title')),
                'page_content' => $this->input->post('page_content'),
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('pages',$mysql_data);
            $message = 'New Page created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('page_id',$id)->delete('pages');
        $data = array(
            'status' => 'success',
            'message' => 'Page removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('page_id',$id)->get('pages')->row();

        $data=array(
            'page_title'=>$edit_content->page_title,
            'page_content'=>$edit_content->page_content,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'page_title'=>$this->input->post('page_title'),
            'page_content'=>$this->input->post('page_content')
        );

        $page_id=$this->input->post('page_id');
        $this->db->where('page_id',$page_id)->update('pages',$data);
        $message = 'Page updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);

    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('page_id', (int)$this->input->post('uid'));
            $this->db->update('pages', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Page in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('page_id', (int)$this->input->post('uid'));
            $this->db->update('pages', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Page activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

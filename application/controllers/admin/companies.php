<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/13/2015
 * Time: 10:48 AM
 */
class Companies extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('admin/companies');
    }

    public function single()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read companies.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $uid = (int)$this->input->post('cid');
        $company = $this->db->where('company_id', $uid)->get('companies')->row();

        if (!$company) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_user' => 'No user found, it may be already removed.'
                )
            );
        } else {
            $user = $this->db->where('user_id', $company->user_id)->get('users')->row();
            $company->user = $user;
            $data = array(
                'status' => 'success',
                'message' => 'Company found',
                'errors' => '',
                'record' => $company
            );
        }
        echo json_encode($data);
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read companies.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->datatables->select('company_id, concat_ws(" " , `first_name`, `last_name`) as user, companies.email, companies.active, companies.name', false)
            ->join('users', 'companies.user_id = users.user_id')
            ->where('companies.deleted_on', '0000-00-00 00:00:00')
            ->from('companies');

        $actions = '<a onclick="login_as(this)" title="Login as">
                        <i class="fa fa-play"></i>
                    </a> |
                    <a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';
        $this->datatables->add_column('actions', $actions, 'company_id');
        echo $this->datatables->generate('json', 'UTF-8');
    }

    public function getusers()
    {
        $users = $this->db
            ->or_like('first_name', $this->input->post('q'))
            ->or_like('email', $this->input->post('q'))
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->limit($this->input->post('page_limit'))
            ->get('users')
            ->result();

        echo json_encode(
            array(
                'users' => $users
            )
        );
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name','Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[companies.email]');
        $this->form_validation->set_rules('user_id', 'User', 'required|numeric');
        $this->form_validation->set_rules('active', 'Active', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add companies.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $mysql_data = array(
                'user_id' => $this->input->post('user_id'),
                'email' => $this->input->post('email'),
                'name' => $this->input->post('name'),
                'active' => $this->input->post('active'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->company_model->create($mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Company created successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cid','Company', 'required|numeric');
        $this->form_validation->set_rules('name','Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('user_id', 'User', 'required|numeric');
        $this->form_validation->set_rules('active', 'Active', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit companies.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $mysql_data = array(
                'user_id' => $this->input->post('user_id'),
                'email' => $this->input->post('email'),
                'name' => $this->input->post('name'),
                'active' => $this->input->post('active'),
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $this->company_model->update($mysql_data, $this->input->post('cid'));

            $data = array(
                'status' => 'success',
                'message' => 'Company updated successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);


    }

    public function delete()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cid','Company ID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2, $this->input->get('a'))) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to delete companies.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $mysql_data = array(
                'deleted_on' => date('Y-m-d H:i:s'),
                'deleted_by' =>$this->auth->id()
            );
            $this->company_model->update($mysql_data, $this->input->post('cid'));

            $data = array(
                'status' => 'success',
                'message' => 'Company removed successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;

        }
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit companies.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cid','Company', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0,
                'updated_by' =>$this->auth->id()
            );
            $this->company_model->update($mysql_data, $this->input->post('cid'));
            $data = array(
                'status' => 'success',
                'message' => 'Company in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit companies.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cid','Company', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            $this->company_model->update($mysql_data, $this->input->post('cid'));
            $data = array(
                'status' => 'success',
                'message' => 'Company activated successfully',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function loginas()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to login as this company user.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cid','CID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $user = $this->db->where('company_id', $this->input->post('cid'))->get('companies')->row();
            $this->auth->loginas($user->user_id);
            $data = array(
                'status' => 'success',
                'message' => 'Logged in successfully, Redirecting...',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Contact extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();       
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/contact');
        
    }

    public function get(){
        
        $query = $this->datatables            
                ->select('contact_id,username,email,message,created_at')
                ->from('email_contact');
        $reply='<a onclick="show_edit(this)" class="label label-sm  label-success">Reply</a>';
        $this->datatables->add_column('reply', $reply);

        $actions = '<a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
        
    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('contact_id',$id)->delete('email_contact');
        $data = array(
            'status' => 'success',
            'message' => 'Message removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('contact_id',$id)->get('email_contact')->row();
        
        $data=array(
            'contact_id'=>$edit_content->contact_id,
            'username'=>$edit_content->username,
            'email'=>$edit_content->email,
            'message'=>$edit_content->message,            
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {          
        $this->form_validation->set_rules('message','Message', 'required');
         if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {

        $config = $this->config->item('email_config');         
        $this->load->library('email', $config);
        $this->email->initialize($config);
          //  print_r($this->email);
    
        $this->email->from($this->config->item('email_from_address'),
                             $this->config->item('email_from_name'));
         $this->email->to($this->input->post('email'));
        $this->email->subject('Contact Us - Reply');
        $this->email->message($this->input->post('message'));
        // exit;
       // $this->email->send();
        if($this->email->send()){
            $message = 'Reply sent successfully';
            $status=array(
                'status'=>'success',
                'message' => $message,
                'errors' => ''
            );
        echo json_encode($status);
        }
        else{
            $message = 'Error while sending email';
            $status=array(
                'status'=>'error',
                'message' => $message,
                'errors' => ''
            );
        echo json_encode($status);
        }        
    }    
}

}

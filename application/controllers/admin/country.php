<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Country extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();       
        $this->load->library('datatables');
    }

    public function index($id){
        $data['id']= $id;
        $this->load->view('admin/country',$data);
        
    }

    public function get($regionid){
            //
        $this->datatables
            ->join('region','country.region_id = region.region_id')
            ->where('country.region_id',$regionid)
            ->select('country.country_id,region.region_name,country.country_name,country.active')            
            ->from('country');

        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
    
    public function add(){

        $this->form_validation->set_rules('country_name','Country Name','trim|required');        
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(                
                'country_name' => $this->input->post('country_name'),
                'region_id' => $this->input->post('region_id'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s'),
                'updated_at' => date('Y:m:d h:i:s')
               
            );
            $this->db->insert('country',$mysql_data);
            $message = 'New Country created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('country_id',$id)->delete('country');
        $data = array(
            'status' => 'success',
            'message' => 'Country removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('country_id',$id)->get('country')->row();
        $data=array(
            'country_id'=>$edit_content->country_id,            
            'country_name'=>$edit_content->country_name,                        
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'country_id'=>$this->input->post('country_id'),
            'country_name'=>$this->input->post('country_name'),            
            'updated_at' => date('Y:m:d h:i:s')
        );

        $country_id=$this->input->post('country_id');
        $this->db->where('country_id',$country_id)->update('country',$data);
        $message = 'Country updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {   
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('country_id', (int)$this->input->post('uid'));
            $this->db->update('country', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Country in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('country_id', (int)$this->input->post('uid'));
            $this->db->update('country', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Country activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}


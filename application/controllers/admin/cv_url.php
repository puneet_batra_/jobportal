<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Cv_url extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){
        $user = $this->db->get('users')->result();
        $data = array(
            'users' => $user
        );
        $this->load->view('admin/cv_url',$data);
    }

    public function get(){

        $this->datatables
            ->join('users','users.user_id = cv_url.user_id')
            ->select('cv_url.cv_id,users.username,cv_url.active')
            ->from('cv_url');

        $cv_url = '<a href="javascript:;" title="CV Details">
                       CV Link
                    </a>';

        $actions = '<a data-toggle="modal" data-target="#cvDetailModal" onclick="customFun(this)" href="javascript:;" title="CV Details">
                        <i class="fa fa-file-text"></i>
                    </a>|<a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('cv_url', $cv_url);
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('cv_id',$id)->delete('cv_url');
        $this->db->where('cv_id',$id)->delete('cv_detail');
        $data = array(
            'status' => 'success',
            'message' => 'CV Url removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('cv_id', (int)$this->input->post('uid'));
            $this->db->update('cv_url', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'CV Url in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('cv_id', (int)$this->input->post('uid'));
            $this->db->update('cv_url', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'CV Url activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function getCvDetail(){
        $id = $this->input->post('custom_id');
        $this->datatables
            ->select('cv_detail.cv_detail_id,cv_url.cv_id,job_position.position_name,job_industry.industry_type,job_project_type.project_type,cv_detail.active',false)
            ->from('cv_detail')
            ->join('job_industry','job_industry.industry_id = cv_detail.industry_id')
            ->join('job_position','job_position.position_id = cv_detail.position_id')
            ->join('job_project_type','job_project_type.project_type_id = cv_detail.project_type_id')
            ->join('cv_url','cv_url.cv_id = cv_detail.cv_id')
            ->where('cv_url.cv_id',$id);

        $actions = '<a onclick="cv_detail_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function cvDetailDelete() {
        $id=$this->input->post('id');
        $this->db->where('cv_detail_id',$id)->delete('cv_detail');
        $data = array(
            'status' => 'success',
            'message' => 'CV Detail removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function cv_detail_disapprove()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('cv_detail_id', (int)$this->input->post('uid'));
            $this->db->update('cv_detail', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'CV Detail disapprove successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function cv_detail_approve()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {

            $mysql_data = array(
                'active' => 1,
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('cv_detail_id', (int)$this->input->post('uid'));
            $this->db->update('cv_detail',$mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'CV Detail approved successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }




}

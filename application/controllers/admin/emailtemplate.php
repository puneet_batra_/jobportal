<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Emailtemplate extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/emailtemplate');
    }

    public function get(){

        $this->datatables->select('temp_id,temp_title,temp_content')
            ->from('email_template');

        $actions = '<a onclick="editTemplate(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a>';


        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function add(){

        $this->form_validation->set_rules('temp_title','Template Title','trim|required');
        $this->form_validation->set_rules('temp_content','Template Content','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'temp_title' => ucfirst($this->input->post('temp_title')),
                'temp_content' => $this->input->post('temp_content'),
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('email_template',$mysql_data);
            $message = 'New Template created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('page_id',$id)->delete('pages');
        $data = array(
            'status' => 'success',
            'message' => 'Page removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('temp_id',$id)->get('email_template')->row();

        $data=array(
            'temp_title'=>$edit_content->temp_title,
            'temp_content'=>$edit_content->temp_content,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'temp_title'=>$this->input->post('temp_title'),
            'temp_content'=>$this->input->post('temp_content')
        );

        $temp_id=$this->input->post('temp_id');
        $this->db->where('temp_id',$temp_id)->update('email_template',$data);
        $message = 'Template updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);

    }

    public function template($id){
        //$id=$this->input->post('id');
        $edit_content=$this->db->where('temp_id',$id)->get('email_template')->row();
        $data = array(
            'edit_content'=>$edit_content
        );
        $this->load->view('admin/editemailtemplate',$data);
    }
}

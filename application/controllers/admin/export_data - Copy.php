<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Export_data extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){
        $user = $this->db->get('users')->result();
        $ind = $this->db->get('job_industry')->result();
        $pos = $this->db->get('job_position')->result();
        $project = $this->db->get('job_project_type')->result();
        $type = $this->db->get('user_type')->result();
        $data = array(
            'users' => $user,
            'industry' => $ind,
            'position' => $pos,
            'project' => $project,
            'user_type' => $type
        );
        $this->load->view('admin/export_data',$data);
    }

    public function get(){

//$type = $this->input->post('user_type');


        $this->datatables
            ->join('education','users.user_id = education.user_id','left outer')
            ->join('experience','users.user_id = experience.user_id','left outer')
            ->join('hobbies','users.user_id = hobbies.user_id','left outer')
            ->join('user_type','user_type.user_type_id = users.user_type','left outer')
            ->join('skills','users.user_id = skills.user_id','left outer')
            ->group_by('users.user_id')
            ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,users.phone,
            users.dob,users.email,users.address,users.city,users.country,users.nationality,users.age,users.height,users.weight,users.relegion,
            users.martial_status,users.passport_num,users.mobile_num,users.telephone_num,users.skype_id,users.job_seeker_status,users.languages,
            users.job_position,users.job_industry,users.project_type, GROUP_CONCAT(hobbies.hobbies_name) as hobbies_name,
            GROUP_CONCAT(education.edu_level) as edu_level,GROUP_CONCAT(education.edu_institute_name) as edu_institute_name,GROUP_CONCAT(education.edu_city) as edu_city,
            GROUP_CONCAT(education.edu_country) as edu_country,GROUP_CONCAT(education.edu_year) as edu_year,GROUP_CONCAT(education.edu_degree) as edu_degree,
            GROUP_CONCAT(education.edu_grade) as edu_grade, GROUP_CONCAT(experience.exp_company_name) as exp_company_name,GROUP_CONCAT(experience.exp_city) as exp_city,
            GROUP_CONCAT(experience.exp_country) as exp_country,GROUP_CONCAT(experience.exp_years) as exp_years,GROUP_CONCAT(experience.exp_job_position) as exp_job_position,
            GROUP_CONCAT(experience.exp_job_description) as exp_job_description,GROUP_CONCAT(experience.company_reference) as company_reference,
            GROUP_CONCAT(skills.soft_skills) as soft_skills,GROUP_CONCAT(skills.hard_skills) as hard_skills',false)
            ->from('users');


        $actions = '<a data-toggle="modal" data-target="#cvDetailModal" href="javascript:;" title="Print Details">
                        <i class="fa fa-print"></i>
                    </a>';
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }







}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Export_data extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('export_data_model');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->library('datatables');
    }

    public function index(){
        $user = $this->db->get('users')->result();
        $ind = $this->db->get('job_industry')->result();
        $pos = $this->db->get('job_position')->result();
        $project = $this->db->get('job_project_type')->result();
        $type = $this->db->get('user_type')->result();
        $data = array(
            'users' => $user,
            'industry' => $ind,
            'position' => $pos,
            'project' => $project,
            'user_type' => $type
        );
        $this->load->view('admin/export_data',$data);
    }

    public function get(){

//$type = $this->input->post('user_type');

        $this->datatables
            ->select('user_type.user_type_id,user_type.user_type_name')
            ->from('user_type');

        $actions = '<a onclick="customFun(this)" href="javascript:;" title="Export CSV">
                        <i class="fa fa-file-excel-o"></i>
                    </a>';
//                    |<a onclick="customPrint(this)" href="javascript:;" title="Print Details">
//                        <i class="fa fa-print"></i>
//                    </a>';
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function getReport()
    {
        $filename = sha1(time())."csv_file.csv";
        $type_id = $this->input->post('user_type_id');

        if($type_id == 1) {
           $report = $this->export_data_model->job_seeker();
           $new_report = $this->dbutil->csv_from_result($report);
           file_put_contents("uploads/assets/documents/job_seeker/$filename", $new_report);
            $data = array(
                'status' => 'success',
                'message' => 'Job Seeker Details Exported successfully.',
                'csv_link' => site_url("uploads/assets/documents/job_seeker/$filename"),
                'errors' => ''
            );
            echo json_encode($data);
        }
       else if($type_id == 2) {

            $report = $this->export_data_model->employer();
            $new_report = $this->dbutil->csv_from_result($report);
            write_file(str_replace('\\', '/', realpath(BASEPATH . '../')) . "/uploads/assets/documents/employer/$filename", $new_report);
            $data = array(
                'status' => 'success',
                'message' => 'Employer Detail Exported successfully.',
                'csv_link' => site_url("uploads/assets/documents/employer/$filename"),
                'errors' => ''
            );
            echo json_encode($data);
        }
       else if($type_id == 3) {
            $report = $this->export_data_model->recruiter();
            $new_report = $this->dbutil->csv_from_result($report);
            write_file(str_replace('\\', '/', realpath(BASEPATH . '../')) . "/uploads/assets/documents/recruiter/$filename", $new_report);
            $data = array(
                'status' => 'success',
                'message' => 'Recruiter Detail Exported successfully.',
                'csv_link' => site_url("uploads/assets/documents/recruiter/$filename"),
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function getPrint($type_id){
        //$type_id = $this->input->post('user_type_id');
        if($type_id == 1) {
            $report_data = $this->export_data_model->job_seeker_print();

                 $data = array(
                'report_data' => $report_data,
                'message' => 'Job Seeker Details Exported successfully.',
                'errors' => ''
            );
//            echo json_encode($data);
            $this->load->view('admin/print_report',$data);
        }
        else if($type_id == 2) {

            $report = $this->export_data_model->employer_print();
            $new_report = $this->dbutil->csv_from_result($report);
            write_file(str_replace('\\', '/', realpath(BASEPATH . '../')) . "/uploads/assets/documents/employer/$filename", $new_report);
            $data = array(
                'status' => 'success',
                'message' => 'Employer Detail Exported successfully.',
                'csv_link' => site_url("uploads/assets/documents/employer/$filename"),
                'errors' => ''
            );
            echo json_encode($data);
        }
        else if($type_id == 3) {
            $report = $this->export_data_model->recruiter_print();
            $new_report = $this->dbutil->csv_from_result($report);
            write_file(str_replace('\\', '/', realpath(BASEPATH . '../')) . "/uploads/assets/documents/recruiter/$filename", $new_report);
            $data = array(
                'status' => 'success',
                'message' => 'Recruiter Detail Exported successfully.',
                'csv_link' => site_url("uploads/assets/documents/recruiter/$filename"),
                'errors' => ''
            );
            echo json_encode($data);
        }



    }



}

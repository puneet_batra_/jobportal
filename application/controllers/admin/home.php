<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct()
	{
		parent::__construct(false, array('*'));
	}

	public function index()
	{

		$users = $this->db->where('deleted_on', '0000-00-00 00:00:00')
			->where('user_id <>', $this->auth->id())
			->count_all_results('users');
		$companies = $this->db->where('deleted_on', '0000-00-00 00:00:00')
			->count_all_results('companies');

		$data = array(
			'count_users' => $users,

			'count_companies' => $companies
		);

		$this->load->view('admin/home', $data);
	}

	public function getsize()
	{
		$directory = realpath('./uploads/');
		$f = $directory;
			/* $obj = new COM ( 'scripting.filesystemobject' );   This is WINDOWS code ... no one uses windows unless they have no choice :P
		if ( is_object ( $obj ) )
		{
			$ref = $obj->getfolder ( $f );
			$size = $ref->size;
			$unit = 'B';
			if ($size > 1024) {
				$size = $size / 1024;
				$unit = 'KB';
				if ($size > 1024) {
					$size = $size / 1024;
					$unit = 'MB';
					if ($size > 1024) {
						$size = $size / 1024;
						$unit = 'GB';
					}
				}
			}
			$data = array(
				'size' => round($size, 2),
				'unit' => $unit
			);
			echo json_encode($data);
		}
		else
		{
			$data = array(
				'size' => '-',
				'unit' => ''
			);
			echo json_encode($data);
		}
		
		*/
		
		//This is for a real operating system
		
		$io = popen ( '/usr/bin/du -s --block-size 1 ' . $f, 'r' ); // This will return a line such as '1017876 /home/chs/'   -sk = summarise in 1 line, k = use 1K block  (--block-size 1 will result in same as what windows returns, instead of k.  I have done this for compatibilty)
		$size = fgets ( $io, 4096);  																// Retrieve that above line
		$size = substr ( $size, 0, strpos ( $size, "\t" ) );  			// Get the fitst substring, the tab is what seperates the size and the folder
		pclose ($io);
		// Now we use your code for formatting the result. Because I used --block-size 1, we have 1 Byte Blocks, as windows returns.
		//  -k would use --blobk-size 1024, -m is Megabytes.  they can be used, to simplify.
		
		$unit = 'B';
		if ($size > 1024) {
			$size = $size / 1024;
			$unit = 'KB';
			if ($size > 1024) {
				$size = $size / 1024;
				$unit = 'MB';
				if ($size > 1024) {
					$size = $size / 1024;
					$unit = 'GB';
				}
			}
		}
		$data = array(
			'size' => round($size, 2),
			'unit' => $unit
		);
		echo json_encode($data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
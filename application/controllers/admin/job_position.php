<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Job_position extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/job_position');
    }

    public function get(){

        $this->datatables->select('position_id,position_name,active')
            ->from('job_position');

        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function add(){

        $this->form_validation->set_rules('pos_name','Job Position Required','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'position_name' => $this->input->post('pos_name'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('job_position',$mysql_data);
            $message = 'New Job Position added';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('position_id',$id)->delete('job_position');
        $data = array(
            'status' => 'success',
            'message' => 'Job position removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('position_id',$id)->get('job_position')->row();
        $data=array(
            'pos_id'=>$edit_content->position_id,
            'pos_name'=>$edit_content->position_name,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'position_name'=>$this->input->post('pos_name'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $pos_id=$this->input->post('pos_id');
        $this->db->where('position_id',$pos_id)->update('job_position',$data);
        $message = 'Job Position updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('position_id', (int)$this->input->post('uid'));
            $this->db->update('job_position', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Job Position in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('position_id', (int)$this->input->post('uid'));
            $this->db->update('job_position', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Job Position activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function importCSV()
    {
        $custom_path = str_replace('\\', '/', realpath(BASEPATH . '../')) . '/uploads/assets/documents/';
        $filename = $_FILES['csv_file']['name'];
        $config['upload_path'] = $custom_path;
        $config['allowed_types'] = 'csv';
        $config['max_size']	= '1000';
        $config['max_width']  = '102400';
        $config['max_height']  = '7680';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('csv_file'))
        {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'upload_fail' => $this->upload->display_errors()
                ));
            echo json_encode($data);
            return false;
        } else {
            $item = array('upload_data' => $this->upload->data('csv_file'));
            $csv_file = $custom_path . $item['upload_data']['file_name']; // Name of your CSV file
            $lines = file($csv_file, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            $i=0;
            $this->db->trans_start();
            foreach ($lines as $line) {
                $csv = str_getcsv($line);
                $csv = array_filter($csv);
                $i++;
                if($i==1)
                    continue;

                if (!empty($csv[0]) && trim($csv[0]) != '' ) {
                    $insert_csv['position_name'] = $csv[0];
                    $insert_csv['active'] = 1;
                    $insert_csv['created_at'] = date('Y-m-d H:i:s');
                    $result = $this->db->get_where('job_position', array('position_name' => $csv[0]));
                    $inDatabase = (bool)$result->num_rows();
                    if (!$inDatabase)
                    {
                        $this->db->insert('job_position', $insert_csv);
                    }
                }
            }
            $this->db->trans_complete();
            $data = array(
                'status' => 'success',
                'message' => 'CSV imported successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function getSuggestion(){

        $this->datatables->select('position_sug_id,user_type,position_name,active')
            ->from('user_position_suggestion');

        $actions = '<a onclick="sug_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function sugDelete() {
        $id=$this->input->post('id');
        $this->db->where('position_sug_id',$id)->delete('user_position_suggestion');
        $data = array(
            'status' => 'success',
            'message' => 'Suggestion removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function sug_disapprove()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'ind_active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('position_sug_id', (int)$this->input->post('uid'));
            $this->db->update('user_position_suggestion', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Suggestion disapprove successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function sug_approve()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {

            $mysql_data = array(
                'position_name' => $this->input->post('pos_name'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('job_position',$mysql_data);


            $this->db->where('position_sug_id', (int)$this->input->post('uid'));
            $this->db->delete('user_position_suggestion');

            $data = array(
                'status' => 'success',
                'message' => 'Suggestion approved successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }




}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Job_post extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){
        $ind = $this->db->get('job_industry')->result();
        $pos = $this->db->get('job_position')->result();
        $project = $this->db->get('job_project_type')->result();
        $type = $this->db->get('user_type')->result();
        $data = array(
            'industry' => $ind,
            'position' => $pos,
            'project' => $project,
            'user_type' => $type
        );

        $this->load->view('admin/job_post',$data);
    }

    public function get(){

        $this->datatables
            ->join('user_type','user_type.user_type_id = job_post.user_type')
            ->join('users','users.user_id = job_post.user_id')
            ->select('job_post.job_id,user_type.user_type_name,user_type.user_type_id,users.username,job_post.active')
            ->from('job_post');

        $actions = '<a onclick="show_job_posted(this)" title="Job Posted" class="label label-sm label-success">
                       Job Posted
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }


    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('job_id',$id)->delete('job_post');
        $data = array(
            'status' => 'success',
            'message' => 'Posted Job removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $this->db
            ->join('user_type','user_type.user_type_id = job_post.user_type')
            ->join('users','users.user_id = job_post.user_id')
            ->select('job_post.job_id,user_type.user_type_name,user_type.user_type_id,
             users.username,job_post.active')
            ->from('job_post');
        $edit_content=$this->db->where('job_post.job_id',$id)->get()->row();

        $data=array(
            'job_id'=>$edit_content->job_id,
            'user_type_name'=>$edit_content->user_type_name,
            'user_type_id'=>$edit_content->user_type_id,
            'username'=>$edit_content->username,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'position_id'=>$this->input->post('job_pos'),
            'user_type'=>$this->input->post('user_type'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $job_id=$this->input->post('job_id');
        $this->db->where('job_id',$job_id)->update('job_post',$data);
        $message = 'Posted Job updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('job_id', (int)$this->input->post('uid'));
            $this->db->update('job_post', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Posted Job in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('job_id', (int)$this->input->post('uid'));
            $this->db->update('job_post', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Posted Job activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }
}

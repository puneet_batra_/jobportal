<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Job_posted extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index($job_id){
        $ind = $this->db->get('job_industry')->result();
        $pos = $this->db->get('job_position')->result();
        $project = $this->db->get('job_project_type')->result();
        $type = $this->db->get('user_type')->result();
        $data = array(
            'industry' => $ind,
            'posted_job_id' => $job_id,
            'position' => $pos,
            'project' => $project,
            'user_type' => $type
        );

        $this->load->view('admin/job_posted',$data);
    }

    public function get($job_id){
    $this->datatables
        ->join('job_industry','job_industry.industry_id = job_posted.industry_id')
        ->join('job_post','job_post.job_id = job_posted.job_id')
        ->join('job_position','job_position.position_id = job_posted.position_id')
        ->join('job_project_type','job_project_type.project_type_id = job_posted.project_type_id')
        ->where('job_posted.job_id',$job_id)
        ->select('job_posted.job_posted_id,job_post.job_id,job_position.position_name,job_industry.industry_type,job_project_type.project_type,
        job_posted.job_salary,job_posted.job_applications,job_posted.total_positions,job_posted.filled_positions,job_posted.available_positions,job_posted.active')
        ->from('job_posted');
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');
    }


    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('job_posted_id',$id)->delete('job_posted');
        $data = array(
            'status' => 'success',
            'message' => 'Posted Job removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $this->db
        ->join('job_industry','job_industry.industry_id = job_posted.industry_id')
            ->join('job_position','job_position.position_id = job_posted.position_id')
            ->join('user_type','user_type.user_type_id = job_posted.user_type')
            ->join('job_project_type','job_project_type.project_type_id = job_posted.project_type_id')
            ->select('job_posted.job_posted_id,job_position.position_name,job_position.position_id,
             job_posted.total_positions,job_posted.filled_positions,job_posted.available_positions,
             job_industry.industry_type,job_industry.industry_id,job_project_type.project_type,job_project_type.project_type_id,
             job_posted.job_salary,job_posted.job_applications,job_posted.active')
            ->from('job_posted');
        $edit_content=$this->db->where('job_posted.job_posted_id',$id)->get()->row();

        $data=array(
            'job_posted_id'=>$edit_content->job_posted_id,
            'position_id'=>$edit_content->position_id,
            'position_name'=>$edit_content->position_name,
            'industry_id'=>$edit_content->industry_id,
            'industry_type'=>$edit_content->industry_type,
            'project_type_id'=>$edit_content->project_type_id,
            'project_type'=>$edit_content->project_type,
            'job_salary'=>$edit_content->job_salary,
            'job_applications'=>$edit_content->job_applications,
            'total_positions'=>$edit_content->total_positions,
            'filled_positions'=>$edit_content->filled_positions,
            'available_positions'=>$edit_content->available_positions,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'position_id'=>$this->input->post('job_pos'),
            'industry_id'=>$this->input->post('ind_type'),
            'project_type_id'=>$this->input->post('project_type'),
            'job_salary'=>$this->input->post('job_salary'),
            'job_applications'=>$this->input->post('job_app'),
            'total_positions'=>$this->input->post('tot_pos'),
            'filled_positions'=>$this->input->post('fill_pos'),
            'available_positions'=>$this->input->post('avail_pos'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $job_posted_id=$this->input->post('job_id');
        $this->db->where('job_posted_id',$job_posted_id)->update('job_posted',$data);
        $message = 'Posted Job updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('job_posted_id', (int)$this->input->post('uid'));
            $this->db->update('job_posted', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Posted Job in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('job_posted_id', (int)$this->input->post('uid'));
            $this->db->update('job_posted', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Posted Job activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Newsletter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter_model');
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/newsletter');
    }

    public function get(){

        $this->datatables->select('news_id,news_title,news_description,active')
            ->from('newsletter');

        $sub_user = '<a data-toggle="modal" data-target="#subModel" href="javascript:;" onclick="customFun(this)" class="btn btn-sm btn-default" title="Subscribed User">
                         Users
                    </a>';

        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('sub_user', $sub_user);
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function subscriberList(){
        $this->datatables->select('sub_name,sub_email')
        ->where('news_id',$this->input->post('custom_id'))
        ->from('subscriber');
        echo $this->datatables->generate('json', 'UTF-8');
    }



    public function add(){

        $this->form_validation->set_rules('news_title','News Title','trim|required');
        $this->form_validation->set_rules('news_content','News Description','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'news_title' => $this->input->post('news_title'),
                'news_description' => $this->input->post('news_content'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('newsletter',$mysql_data);
            $message = 'New Newsletter created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('news_id',$id)->delete('newsletter');
        $data = array(
            'status' => 'success',
            'message' => 'Newsletter removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('news_id',$id)->get('newsletter')->row();

        $data=array(
            'news_id'=>$edit_content->news_id,
            'news_title'=>$edit_content->news_title,
            'news_content'=>$edit_content->news_description,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'news_title'=>$this->input->post('news_title'),
            'news_description'=>$this->input->post('news_content'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $news_id=$this->input->post('news_id');
        $this->db->where('news_id',$news_id)->update('newsletter',$data);
        $message = 'Newsletter updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $this->newsletter_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'Newletter in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            $this->newsletter_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'Newletter activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

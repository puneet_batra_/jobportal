<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 11/7/15
 * Time: 3:56 PM
 */
class Packages extends MY_Controller{

    public function __construct(){
        parent::__construct();
      $this->load->library('datatables');

    }
    public function index(){
        $this->load->view('admin/packages');
        
    
    }
    public function get(){
        
        $query = $this->datatables
            ->join('user_type','packages.user_type = user_type.user_type_id')
            ->join('currency','packages.package_currency = currency.currency_id')
            ->select('packages.package_id,packages.package_name,user_type.user_type_name,packages.package_price_in,currency.currency_symbol,packages.package_validity,packages.total_credits,packages.credit_per_user_activity,packages.active')
            ->from('packages');
        
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
    public function get_user_type(){
        
        //users
        $user_type = $this->db->where('active',1)->get("user_type")->result();
        $option = "<option value=''>Select User Type</option>";
        foreach($user_type as $user_types){
            $option .= "<option value='".$user_types->user_type_id."'>".ucfirst($user_types->user_type_name)."</option><br>";
        }

        //currencies
        $currency = $this->db->where('active',1)->get("currency")->result();
        $option1 = "<option value=''>Package Currency</option>";
        foreach($currency as $currencies){
            $option1 .= "<option value='".$currencies->currency_id."'>".ucfirst($currencies->currency_code)."</option><br>";
        }

        $data = array(
                'status' => 'success',
                'options' => $option,
                'options1' =>$option1
            );
        
        echo json_encode($data);
    }
    public function add(){

        $this->form_validation->set_rules('package_name','Package Name','trim|required');        
        $this->form_validation->set_rules('user_type','User Type','trim|required');
        $this->form_validation->set_rules('package_price_in','Package Price','trim|required|numeric');
        $this->form_validation->set_rules('package_currency','Package Currency','trim|required');        
        $this->form_validation->set_rules('total_credits','Total Credits','trim|numeric');        
        $this->form_validation->set_rules('credit_per_user_activity','Credits per user activity','trim|numeric');        
        
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'package_name' => $this->input->post('package_name'),
                'package_facilities' => $this->input->post('package_facilities'),
                'user_type' => $this->input->post('user_type'),
                'package_price_in' => $this->input->post('package_price_in'),
                'package_currency' => $this->input->post('package_currency'),
                'package_validity' => $this->input->post('package_validity'),
                'total_credits' => $this->input->post('total_credits'),
                'credit_per_user_activity' => $this->input->post('credit_per_user_activity'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s'),
                'updated_at' => date('Y:m:d h:i:s')
            );          
            $this->db->insert('packages',$mysql_data);
            $message = 'New Package created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('package_id',$id)->delete('packages');
        $data = array(
            'status' => 'success',
            'message' => 'Package removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        //exit;
        $edit_content=$this->db->where('package_id',$id)->get('packages')->row();

        $user_type = $this->db->where('active',1)->get("user_type")->result();

        $option = "";
        foreach($user_type as $user_types){
            $option .= "<option value='".$user_types->user_type_id."'>".ucfirst($user_types->user_type_name)."</option><br>";
        }

        $currency = $this->db->where('active',1)->get("currency")->result();
        $option1 = "";

        foreach($currency as $currencies){
            $option1 .= "<option value='".$currencies->currency_id."'>".ucfirst($currencies->currency_code)."</option><br>";
        }

        $data=array(
            'package_id'=>$edit_content->package_id,
            'package_name'=>$edit_content->package_name,
            'package_facilities'=>$edit_content->package_facilities,
            'user_type'=>$edit_content->user_type,
            'package_price_in'=>$edit_content->package_price_in,
            'package_currency'=>$edit_content->package_currency,
            'package_validity'=>$edit_content->package_validity,
            'total_credits'=>$edit_content->total_credits,
            'credit_per_user_activity'=>$edit_content->credit_per_user_activity,
            'options'=>$option,
            'options1'=>$option1,
           
            'active'=>$edit_content->active,
            'status'=>'success'
        );       
        echo json_encode($data);
    }

    public function postUpdate()
    {   
        $package_id=$this->input->post('package_id');
       //exit;
        $data=array(
            'package_id'=>$this->input->post('package_id'),
            'package_name'=>$this->input->post('package_name'),
            'package_facilities'=>$this->input->post('package_facilities'),
            'user_type'=>$this->input->post('user_type'),
            'package_price_in'=>$this->input->post('package_price_in'),
            'package_currency'=>$this->input->post('package_currency'),
            'package_validity'=>$this->input->post('package_validity'),
            'total_credits'=>$this->input->post('total_credits'),
            'credit_per_user_activity'=>$this->input->post('credit_per_user_activity'),            
            'updated_at' => date('Y:m:d h:i:s')
        );


        $this->db->where('package_id',$package_id)->update('packages',$data);
        $message = 'Package updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );

        echo json_encode($status);
    }

    public function inactivate()
    {   
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('package_id', (int)$this->input->post('uid'));
            $this->db->update('packages', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Package in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('package_id', (int)$this->input->post('uid'));
            $this->db->update('packages', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Package activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

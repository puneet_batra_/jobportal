<?php
/**
 * Created by PhpStorm.
 * User: step
 * Date: 11/7/15
 * Time: 3:56 PM
 */
class Packages_order extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library('datatables');

    }
    public function index(){
        $this->load->view('admin/packages_order');
    }
    public function get(){
        
        $query = $this->datatables
            ->join('user_type','packages_order.user_type_id = user_type.user_type_id')
            ->join('users','packages_order.user_id = users.user_id')
            ->join('packages','packages_order.package_id = packages.package_id')
            ->select('packages_order.package_order_id,user_type.user_type_name,users.username,packages.package_name,packages.package_validity,packages_order.purchased_at,packages_order.expired_at,packages_order.amount')
            ->from('packages_order');
        
        $actions = '<a onclick="view_invoice(this)" title="Generate Invoice">
                        <i class="fa fa-file-pdf-o"></i>
                    </a> 
                    ';
/* | <a onclick="do_delete(this)" title="Delete"><i class="fa fa-trash"></i></a>*/
        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
   
    public function add(){

        $this->form_validation->set_rules('package_name','Package Name','trim|required');        
        $this->form_validation->set_rules('user_type','User Type','trim|required');
        $this->form_validation->set_rules('package_price_in','Package Price','trim|required');
        $this->form_validation->set_rules('package_validity','Package Validity','trim|required');        
        
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'package_name' => $this->input->post('package_name'),
                'package_facilities' => $this->input->post('package_facilities'),
                'user_type' => $this->input->post('user_type'),
                'package_price_in' => $this->input->post('package_price_in'),
                'package_validity' => $this->input->post('package_validity'),
                'total_credits' => $this->input->post('total_credits'),
                'credit_per_user_activity' => $this->input->post('credit_per_user_activity'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s'),
                'updated_at' => date('Y:m:d h:i:s')
            );          
            $this->db->insert('packages',$mysql_data);
            $message = 'New Package created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('package_id',$id)->delete('packages');
        $data = array(
            'status' => 'success',
            'message' => 'Package removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getInvoice(){
        $id=$this->input->post('id');
        //exit;
        $edit_content=$this->db->where('package_order_id',$id)->get('packages_order')->row();

        $user_type = $this->db->where('active',1)->where('user_type_id',$edit_content->user_type_id)->get("user_type")->row();
        $user_name=$this->db->where('user_id',$edit_content->user_id)->get("users")->row();
        $package_name=$this->db->where('package_id',$edit_content->package_id)->get("packages")->row();

        $data=array(
            'package_order_id'=>$edit_content->package_order_id,
            'user_type'=>$user_type->user_type_name,
            'user_name'=>$user_name->username,
            'package_name'=>$package_name->package_name,
            'package_validity'=>$edit_content->package_validity,
            'purchased_at'=>$edit_content->purchased_at,
            'expired_at'=>$edit_content->expired_at,
            'amount'=>$edit_content->amount,            
            'status'=>'success'
        );   
       // print_r($data);    
       // exit;
        echo json_encode($data);
    }

    public function postUpdate()
    {   
        $package_id=$this->input->post('package_id');
       //exit;
        $data=array(
            'package_id'=>$this->input->post('package_id'),
            'package_name'=>$this->input->post('package_name'),
            'package_facilities'=>$this->input->post('package_facilities'),
            'user_type'=>$this->input->post('user_type'),
            'package_price_in'=>$this->input->post('package_price_in'),
            'package_validity'=>$this->input->post('package_validity'),
            'total_credits'=>$this->input->post('total_credits'),
            'credit_per_user_activity'=>$this->input->post('credit_per_user_activity'),            
            'updated_at' => date('Y:m:d h:i:s')
        );


        $this->db->where('package_id',$package_id)->update('packages',$data);
        $message = 'Package updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );

        echo json_encode($status);
    }

    public function inactivate()
    {   
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('package_id', (int)$this->input->post('uid'));
            $this->db->update('packages', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Package in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('package_id', (int)$this->input->post('uid'));
            $this->db->update('packages', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Package activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/10/2015
 * Time: 12:18 PM
 */
class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('*'));
    }
    public function index()
    {
        $user = $this->auth->get();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name' ,'First Name', 'required|alpha');
        $this->form_validation->set_rules('last_name' ,'Last Name', 'alpha');
        $this->form_validation->set_rules('email' ,'Email', 'required|valid_email|callback_email_change');
        $this->form_validation->set_rules('address' ,'Address', '');
        $this->form_validation->set_rules('city' ,'City', 'alpha');
        $this->form_validation->set_rules('country' ,'Country', '');
        $this->form_validation->set_rules('password', 'Password', 'min_length[4]');
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('rpassword', 'Repeat Password', 'required|matches[password]');
        }

        if ($this->form_validation->run() == false) {
            $connected_google = false;
            $connected_live = false;
            $connected_facebook = false;
            foreach ($user->authentications as $authentication) {
                if ($authentication->account_type == 'Google') {
                    $connected_google = true;
                }
                if ($authentication->account_type == 'Live') {
                    $connected_live = true;
                }
                if ($authentication->account_type == 'Facebook') {
                    $connected_facebook = true;
                }
            }
            $data = array(
                'user' => $user,
                'connected_google' => $connected_google,
                'connected_live' => $connected_live,
                'connected_facebook' => $connected_facebook
            );
            $this->load->view('admin/profile.php', $data);
        } else {
            $mysql_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country'),
            );
            // update new password if entered
            if ($this->input->post('password')) {
                $mysql_data['password'] = md5($this->input->post('password'));
            }

            $this->db->where('user_id', $this->auth->id());
            $this->db->update('users', $mysql_data);

            $this->session->set_flashdata('success', 'Your settings saved successfully.');
            if ($user->email != $mysql_data['email']) {
                // detected email change.
                $this->db->where('user_id', $this->auth->id())->update('users', array('is_verified'=> 0));
                $this->user_model->send_email_verification($this->auth->id());
                $this->session->set_flashdata('success', 'Your settings saved and email has been sent to your new email for verification.');
            }

            redirect('admin/profile');
        }

    }

    public function email_change($email)
    {
        $exists = $this->db->where('user_id <>', $this->auth->id())->where('email', $email)->get('users')->row();
        if ($exists) {
            $this->form_validation->set_message('email_change', 'This email is already in use by another account.');
            return false;
        } else {
            return true;
        }
    }

    public function reverify()
    {
        $user = $this->auth->get();
        $this->user_model->send_email_verification($user->user_id);
        $this->session->set_flashdata('success', 'Email sent to '.$user->email.' successfully, please check your email.');
        redirect('admin/home');
    }

    public function unlink($provider)
    {
        $this->db->where('user_id', $this->auth->id())->where('account_type', $provider)->delete('authentications');
        $this->session->set_flashdata('success', 'Your ' . $provider . ' account unlinked successfully.');
        redirect('admin/profile');
    }
}
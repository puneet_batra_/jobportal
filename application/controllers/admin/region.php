<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Region extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library('datatables');

    }       

    public function index(){

        $this->load->view('admin/region');
    }

    public function get(){

        $this->datatables->select('region_id,region_name,active')
            ->from('region');

        $countries='<a onclick="get_countries(this)" class="label label-sm label-success">Countries</a>';
        $this->datatables->add_column('countries', $countries);

        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function add(){

        $this->form_validation->set_rules('region_name','Region Name','trim|required');
       
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'region_name' =>$this->input->post('region_name'),                
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('region',$mysql_data);
            $message = 'New Region created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('region_id',$id)->delete('region');
        $this->db->where('region_id',$id)->delete('country');
        $data = array(
            'status' => 'success',
            'message' => 'Region removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
       // echo $id;
        //exit;
        $edit_content=$this->db->where('region_id',$id)->get('region')->row();

        $data=array(
            'region_id'=>$edit_content->region_id,
            'region_name'=>$edit_content->region_name,            
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'region_name'=>$this->input->post('region_name'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $region_id=$this->input->post('region_id');
        $this->db->where('region_id',$region_id)->update('region',$data);
        $message = 'Region updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('region_id', (int)$this->input->post('uid'));
            $this->db->update('region', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Region in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('region_id', (int)$this->input->post('uid'));
            $this->db->update('region', $mysql_data);
            
            $data = array(
                'status' => 'success',
                'message' => 'Region activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function importCSV()
    {

        $custom_path = str_replace('\\', '/', realpath(BASEPATH . '../')) . '/uploads/assets/documents/';
        $filename = $_FILES['csv_file']['name'];
        $config['upload_path'] = $custom_path;
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
        $config['max_width']  = '102400';
        $config['max_height']  = '7680';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('csv_file'))
        {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'upload_fail' => $this->upload->display_errors()
                ));
            echo json_encode($data);
            return false;
        } else {
            $item = array('upload_data' => $this->upload->data('csv_file'));
            $csv_file = $custom_path . $item['upload_data']['file_name']; // Name of your CSV file
            $lines = file($csv_file, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            $num_rows = count($lines);
            $i=0;
            $this->db->trans_start();
            foreach ($lines as $line) {
                $csv = str_getcsv($line);
                $csv = array_filter($csv);
                $i++;
                if($i==1)
                    continue;


                if (!empty($csv[1]) && trim($csv[1]) != '' ) {
                     
                    $insert_csv1['region_name'] = $csv[0];
                    $insert_csv1['active'] = 1;
                    $insert_csv1['created_at'] = date('Y-m-d H:i:s');    
                    $insert_csv1['updated_at'] = date('Y-m-d H:i:s');                  
                    $result1 = $this->db->get_where('region', array('region_name' => $csv[0]));
                    $inDatabase1 = (bool)$result1->num_rows();
                    if (!$inDatabase1)
                    {
                        $this->db->insert('region', $insert_csv1);
                    }

                    $insert_csv['country_name'] = $csv[1];
                    $insert_csv['active'] = 1;
                    $insert_csv['created_at'] = date('Y-m-d H:i:s');
                    $insert_csv['updated_at'] = date('Y-m-d H:i:s');  

                    $this->db->where('region_name',$csv[0]);
                    $this->db->select('region_id');
                    $res = $this->db->get('region')->result();
                    foreach($res as $r){
                        
                        $insert_csv['region_id'] =  $r->region_id;
                        $result = $this->db->get_where('country', array('country_name' => $csv[1]));
                        $inDatabase = (bool)$result->num_rows();
                        if (!$inDatabase)
                        {
                            $this->db->insert('country', $insert_csv);
                        }
                    }
                }
            }
            $this->db->trans_complete();
            $data = array(
                'status' => 'success',
                'message' => 'CSV imported successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Resource extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();       
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/resource');
        
    }

    public function get(){
        
            $this->datatables
            ->join('resource_category','resource.resource_category_id = resource_category.resource_category_id')
            ->select('resource.resource_id,resource_category.resource_category_name,resource.resource_name,resource.created_at,resource.active')
            ->from('resource');
        
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }
    public function category(){
        $category = $this->db->where('active',1)->get("resource_category")->result();

        $option = "<option value=''>Select Resource Category</option>";
        foreach($category as $categories){
            $option .= "<option value='".$categories->resource_category_id."'>".ucfirst($categories->resource_category_name)."</option><br>";
        }

        $data = array(
                'status' => 'success',
                'options' => $option
            );
        
        echo json_encode($data);
    }
    public function add(){

        $this->form_validation->set_rules('resource_category_id','Resource category','trim|required');
         $this->form_validation->set_rules('resource_name','Resource Name','trim|required');
        $this->form_validation->set_rules('resource_description','Resource Description','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'resource_category_id' => $this->input->post('resource_category_id'),
                'resource_name' => $this->input->post('resource_name'),
                'resource_description' => $this->input->post('resource_description'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
               
            );
            $this->db->insert('resource',$mysql_data);
            $message = 'New Resource created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('resource_id',$id)->delete('resource');
        $data = array(
            'status' => 'success',
            'message' => 'Resource removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('resource_id',$id)->get('resource')->row();

        $category = $this->db->where('active',1)->get("resource_category")->result();

        $option = "";
        foreach($category as $categories){
            $option .= "<option value='".$categories->resource_category_id."'>".ucfirst($categories->resource_category_name)."</option><br>";
        }

        $data=array(
            'resource_id'=>$edit_content->resource_id,
            'resource_category_id'=>$edit_content->resource_category_id,
            'resource_name'=>$edit_content->resource_name,
            'resource_description'=>$edit_content->resource_description,
            'options'=>$option,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'resource_category_id'=>$this->input->post('resource_category_id'),
            'resource_name'=>$this->input->post('resource_name'),
            'resource_description'=>$this->input->post('resource_description'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $resource_id=$this->input->post('resource_id');
        $this->db->where('resource_id',$resource_id)->update('resource',$data);
        $message = 'Resource updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {   
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );

            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('resource_id', (int)$this->input->post('uid'));
            $this->db->update('resource', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Resource in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('resource_id', (int)$this->input->post('uid'));
            $this->db->update('resource', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Resource activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}


<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/12/2015
 * Time: 12:38 PM
 */
class Roles extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }
    public function index()
    {

        $roles = $this->db->get('su_roles')->result();
        $modules = $this->db->get('su_modules')->result();
        $data = array(
            'roles' => $roles,
            'modules' => $modules
        );
        $this->load->view('admin/roles', $data);
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            show_error('You do not have permission to get roles', '403', 'Permission Required');
            exit;
        }
        $role_id = (int)$this->input->post('role_id');

        $sql = "SELECT m.module_id,
                       mr.role_id,
                       m.name,
                       m.description,
                       mr.permissions,
                       m.controller_name,
                       m.action_name
                FROM   su_modules AS m
                       JOIN su_module_roles AS mr
                         ON m.module_id = mr.module_id
                WHERE  mr.role_id IN (SELECT r.role_id
                                      FROM   su_roles AS r
                                      )
                      and mr.role_id = $role_id";

        $modules = $this->db->query($sql)->result();
        $role = $this->db->where('role_id', $role_id)->get('su_roles')->row();
        $data = array(
            'modules' => $modules,
            'role' => $role
        );
        echo json_encode($data);
    }

    public function get_roles()
    {
        $roles = $this->db->get('su_roles')->result();
        echo json_encode($roles);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_name','Role name', 'required|alpha');
        $this->form_validation->set_rules('role_description', 'Role description', 'required');
        $this->form_validation->set_rules('permission', 'Permissions', 'required|callback_check_permission');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add roles.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $mysql_data = array(
                'name' => $this->input->post('role_name'),
                'description' => $this->input->post('role_description'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('su_roles', $mysql_data);
            $role_id = $this->db->insert_id();

            foreach ($this->input->post('permission') as $module_id => $permission) {
                $mysql_data = array(
                    'permissions' => $permission,
                    'role_id' => $role_id,
                    'module_id' => $module_id
                );
                $this->db->insert('su_module_roles', $mysql_data);
            }

            $data = array(
                'status' => 'success',
                'message' => 'Role and permissions added successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_name','Role name', 'required|alpha');
        $this->form_validation->set_rules('role_description', 'Role description', 'required');
        $this->form_validation->set_rules('permission', 'Permissions', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to update roles.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $mysql_data = array(
                'name' => $this->input->post('role_name'),
                'description' => $this->input->post('role_description'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->db->where('role_id', $this->input->post('role_id'));
            $this->db->update('su_roles', $mysql_data);
            $role_id = $this->input->post('role_id');

            foreach ($this->input->post('permission') as $module_id => $permission) {
                // check if module is alredy existing
                $module_exists = $this->db->where('role_id', $role_id)->where('module_id', $module_id)->get('su_module_roles')->row();
                if ($module_exists) {
                    $mysql_data = array(
                        'permissions' => $permission,
                    );
                    $this->db->where('role_id', $role_id)
                        ->where('module_id', $module_id);
                    $this->db->update('su_module_roles', $mysql_data);
                } else {
                    // otherwise create it
                    $mysql_data = array(
                        'permissions' => $permission,
                        'role_id' => $role_id,
                        'module_id' => $module_id
                    );
                    $this->db->insert('su_module_roles', $mysql_data);
                }

            }

            $data = array(
                'status' => 'success',
                'message' => 'Role and permissions udpated successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function delete()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_id','Role', 'required|callback_validate_user_exist');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to delete roles.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $this->db->where('role_id', $this->input->post('role_id'))->delete('su_module_roles');
            $this->db->where('role_id', $this->input->post('role_id'))->delete('su_roles');

            $data = array(
                'status' => 'success',
                'message' => 'Role removed successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function validate_user_exist($role_id)
    {
        $user_roles_exists = $this->db->where('role_id', $role_id)
            ->get('su_user_roles')->result();
        if (count($user_roles_exists) > 0) {
            if (count($user_roles_exists) > 1) {
                $users_count = count($user_roles_exists) . ' existing users.';
            } else {
                $users_count = '1 existing user.';
            }
            $this->form_validation->set_message('validate_user_exist', 'This role is assigned to ' . $users_count);
            return false;
        } else {
            return true;
        }
    }
}
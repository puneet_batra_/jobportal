<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Statistics extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library('datatables');
    }

    public function index(){
        $this->db->where('user_type','1');
        $job_seeker = $this->db->get('users')->num_rows();

        $this->db->where('user_type','2');
        $employer = $this->db->get('users')->num_rows();

        $this->db->where('user_type','3');
        $recruiter = $this->db->get('users')->num_rows();

        $industry = $this->db->get('job_industry')->num_rows();

        $project_type = $this->db->get('job_project_type')->num_rows();

        $position = $this->db->get('job_position')->num_rows();


        $this->db->select('sum(available_positions) as total_pos');
        $query = $this->db->get('job_posted')->row_array();
        $total_jobs  = $query['total_pos'];

        $data = array(
            'job_seeker' => $job_seeker,
            'employer' => $employer,
            'recruiter' => $recruiter,
            'industry' => $industry,
            'project_type' => $project_type,
            'position' => $position,
            'total_jobs' => $total_jobs
        );
        $this->load->view('admin/statistics',$data);
    }

    public function get(){
        $this->datatables
            ->join('job_industry','job_industry.industry_id = users.user_id')
            ->join('job_post','job_post.job_id = users.user_id')
            ->join('job_position','job_position.position_id = users.user_id')
            ->join('job_project_type','job_project_type.project_type_id = users.user_id')
            ->select('job_posted.job_posted_id,job_post.job_id,job_position.position_name,job_industry.industry_type,job_project_type.project_type,
        job_posted.job_salary,job_posted.job_applications,job_posted.total_positions,job_posted.filled_positions,job_posted.available_positions,job_posted.active')
            ->from('job_posted');
        echo $this->datatables->generate('json', 'UTF-8');
    }
}

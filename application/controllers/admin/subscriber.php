<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class Subscriber extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter_model');
        $this->load->library('datatables');
    }

    public function index(){

        $this->load->view('admin/subscriber');
    }

    public function get(){

        $this->datatables->select('sub_id,sub_user_type,sub_name,sub_email,active')
            ->from('subscriber');

        $actions = '<a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';

        $this->datatables->add_column('actions', $actions);
        echo $this->datatables->generate('json', 'UTF-8');

    }

    public function add(){

        $this->form_validation->set_rules('news_title','News Title','trim|required');
        $this->form_validation->set_rules('news_content','News Description','trim|required');
        if($this->form_validation->run() == FALSE ){
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        }
        else{
            $mysql_data = array(
                'news_title' => $this->input->post('news_title'),
                'news_description' => $this->input->post('news_content'),
                'active' => 1,
                'created_at' => date('Y:m:d h:i:s')
            );
            $this->db->insert('newsletter',$mysql_data);
            $message = 'New Newsletter created';
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }
        echo json_encode($data);
    }

    public function getDelete() {
        $id=$this->input->post('id');
        $this->db->where('sub_id',$id)->delete('subscriber');
        $data = array(
            'status' => 'success',
            'message' => 'Subscriber removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getEdit(){
        $id=$this->input->post('id');
        $edit_content=$this->db->where('news_id',$id)->get('newsletter')->row();

        $data=array(
            'news_id'=>$edit_content->news_id,
            'news_title'=>$edit_content->news_title,
            'news_content'=>$edit_content->news_description,
            'active'=>$edit_content->active,
            'status'=>'success'
        );
        echo json_encode($data);
    }

    public function postUpdate()
    {
        $data=array(
            'news_title'=>$this->input->post('news_title'),
            'news_description'=>$this->input->post('news_content'),
            'updated_at' => date('Y:m:d h:i:s')
        );

        $news_id=$this->input->post('news_id');
        $this->db->where('news_id',$news_id)->update('newsletter',$data);
        $message = 'Newsletter updated successfully';
        $status=array(
            'status'=>'success',
            'message' => $message,
            'errors' => ''
        );
        echo json_encode($status);
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to activate it.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('sub_id', (int)$this->input->post('uid'));
            $this->db->update('subscriber', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Subscriber in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            $mysql_data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('sub_id', (int)$this->input->post('uid'));
            $this->db->update('subscriber', $mysql_data);
            $data = array(
                'status' => 'success',
                'message' => 'Subscriber activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/13/2015
 * Time: 10:48 AM
 */
class Users extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
        $this->load->library('datatables');
    }

    public function index()
    {
        $roles = $this->db->get('su_roles')->result();
        $data = array(
            'roles' => $roles,
        );
        $this->load->view('admin/users', $data);
    }

    public function cusers()
    {
        $roles = $this->db->get('su_roles')->result();
        $user_types = $this->db->get('user_type')->result();
        $data = array(
            'roles' => $roles,
            'user_type' => $user_types,
        );
        $this->load->view('admin/users.cusers.php', $data);
    }

    public function single()
    {
        if (!$this->has_permission(1, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $uid = (int)$this->input->post('uid');

        $user = $this->db->select('users.*,user_type_id,user_type_name')
            ->join('user_type', 'user_type.user_type_id = users.user_type')
            ->where('users.user_id', $uid)
            ->get('users')->row();

        //$user = $this->db->where('user_id', $uid)->get('users')->row();
        if (!$user) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_user' => 'No user found, it may be already removed.'
                )
            );
        } else {
            $role = $this->db->select('su_roles.role_id, su_roles.name')
                ->join('su_roles', 'su_user_roles.role_id = su_roles.role_id')
                ->where('su_user_roles.user_id', $user->user_id)
                ->get('su_user_roles')->row();



            $user->role = $role;
            $data = array(
                'status' => 'success',
                'message' => 'User found',
                'errors' => '',
                'record' => $user
            );
        }
        echo json_encode($data);
    }

    public function get()
    {
        if (!$this->has_permission(1, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->datatables->select('user_id, username, email, active, is_verified')
            ->where('is_superuser', 1)
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->from('users');
//        $actions = '<a onclick="login_as(this)" title="Login as">
//                        <i class="fa fa-play"></i>
//                    </a> |
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';
        $this->datatables->add_column('actions', $actions, 'user_id');
        echo $this->datatables->generate('json', 'UTF-8');
    }

    public function getcusers()
    {
        if (!$this->has_permission(1, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->datatables->select('user_id, username, email, active, is_verified')
            ->where('is_superuser', 0)
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->from('users');
//        $actions = '<a onclick="login_as(this)" title="Login as">
//                        <i class="fa fa-play"></i>
//                    </a> |
        $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit"></i>
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>';
        $this->datatables->add_column('actions', $actions, 'user_id');
        echo $this->datatables->generate('json', 'UTF-8');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name','First Name', 'required|alpha');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('role_id', 'Role', 'numeric');
        $this->form_validation->set_rules('active', 'Active', 'required');
        $this->form_validation->set_rules('city', 'Address', '');
        $this->form_validation->set_rules('country', 'Country', '');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2, $this->input->get('a'))) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add users.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            if ($this->input->post('password')) {
                $new_pass = $this->input->post('password');
            } else {
                $new_pass = rand(99999, 999999);
            }


            $mysql_data = array(
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'password' => md5($new_pass),
                'active' => $this->input->post('active'),
                'is_superuser' => ($this->input->post('role_id') > 0)?1:0,
                'is_verified' => 1,
                'updated_on' => date('Y-m-d H:i:s'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'country' => $this->input->post('country')
            );
            if($this->input->post('user_type')){
                $mysql_data['user_type'] = $this->input->post('user_type');
            }
            $user_id = $this->user_model->create($mysql_data, false);
            $mysql_data = array(
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->user_model->update($mysql_data, $user_id);

            if ($this->input->post('role_id') > 0) {
                $mysql_data = array(
                    'user_id' => $user_id,
                    'role_id' => $this->input->post('role_id'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('su_user_roles', $mysql_data);
            }

            if ($this->input->post('password')) {
                $message = 'New user created with provided password';
            } else {
                $message = 'New user created with password: ' . $new_pass;
            }
            $data = array(
                'status' => 'success',
                'message' => $message,
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name','First Name', 'required|alpha');
        //$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('role_id', 'Role', 'numeric');
        $this->form_validation->set_rules('active', 'Active', 'required');
        $this->form_validation->set_rules('city', 'Address', '');
        $this->form_validation->set_rules('country', 'Country', '');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2, $this->input->get('a'))) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit users.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $user = $this->db->where('user_id', $this->input->post('uid'))->get('users')->row();
            if ($user) {
                // if tried to change email

                if ($user->email != $this->input->post('email')) {
                    $already_exists = $this->db->where('email', $this->input->post('email'))->get('users')->row();
                    if ($already_exists) {
                        $data = array(
                            'status' => 'fail',
                            'message' => 'There were errors',
                            'errors' => array(
                                'no_user' => 'No user found, it may be already removed'
                            )
                        );
                        echo json_encode($data);
                        return false;
                    }
                }

                $mysql_data = array(
                    'email' => $this->input->post('email'),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'active' => $this->input->post('active'),
                    'is_superuser' => ($this->input->post('role_id') > 0)?1:0,
                    'updated_on' => date('Y-m-d H:i:s'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'country' => $this->input->post('country')
                );
                if($this->input->post('user_type')){
                    $mysql_data['user_type'] = $this->input->post('user_type');
                }
                // if password changed
                if ($this->input->post('password')) {
                    $mysql_data['password'] = md5($this->input->post('password'));
                }

                $this->user_model->update($mysql_data, $user->user_id);
                //$this->user_model->send_email_signup_password($user->user_id, 'Manual', $mysql_data, $this->input->post('password'));

                if ($this->input->post('role_id') > 0) {
                    $existing_role = $this->db->where('user_id', $user->user_id)->get('su_user_roles')->row();
                    if ($existing_role) {
                        $mysql_data = array(
                            'role_id' => $this->input->post('role_id'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->update('su_user_roles', $mysql_data, array('user_id' => $user->user_id));
                    } else {
                        $mysql_data = array(
                            'user_id' => $user->user_id,
                            'role_id' => $this->input->post('role_id'),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('su_user_roles', $mysql_data);
                    }
                } else {
                    $this->db->where('user_id', $user->user_id)->delete('su_user_roles');
                }

                if (!$this->input->post('password')) {
                    $message = 'User updated successfully.';
                } else {
                    $message = 'User updated successfully with password: ' . $this->input->post('password');
                }
                $data = array(
                    'status' => 'success',
                    'message' => $message,
                    'errors' => ''
                );
                echo json_encode($data);
                return false;


            } else {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_user' => 'No user found, it may be already removed'
                    )
                );
                echo json_encode($data);
                return false;
            }

        }


    }

    public function delete()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2, $this->input->get('a'))) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to delete users.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            if ($this->input->post('uid') == $this->auth->id()) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'not_allowed' => 'You cannot delete yourself.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            // check if it is a company admin
            $company = $this->db->where('user_id', $this->input->post('uid'))
                ->where('deleted_on', '0000-00-00 00:00:00')
                ->get('companies')->row();
            if ($company) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'not_allowed' => 'Cannot remove, user is admin of '. $company->name .', ID:'.$company->company_id
                    )
                );
                echo json_encode($data);
                return false;
            }


            $mysql_data = array(
                'active' => 0,
                'deleted_on' => date('Y-m-d H:i:s'),
                'deleted_by' => $this->auth->id()
            );
            $this->user_model->update($mysql_data, $this->input->post('uid'));
            $this->db->where('user_id', $this->input->post('uid'))->delete('authentications');
            $this->db->where('user_id', $this->input->post('uid'))->delete('su_user_roles');
            $data = array(
                'status' => 'success',
                'message' => 'User removed successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;

        }
    }

    public function inactivate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 0
            );
            $this->user_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'User in-activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function activate()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'active' => 1
            );
            $this->user_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'User activated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function unverify()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'is_verified' => 0
            );
            $this->user_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'User made non verified successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function verify()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $mysql_data = array(
                'is_verified' => 1
            );
            $this->user_model->update($mysql_data, $this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'User made verified successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }

    public function loginas()
    {
        if (!$this->has_permission(2, $this->input->get('a'))) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to login as this user.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            $this->auth->loginas($this->input->post('uid'));
            $data = array(
                'status' => 'success',
                'message' => 'Logged in successfully, Redirecting...',
                'errors' => ''
            );
            echo json_encode($data);
            return true;
        }
    }
}
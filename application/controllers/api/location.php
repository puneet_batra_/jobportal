<?php

class Location extends CI_Controller
{
    /**
     * location/add
     *
     * Request format:
     * {"_type": "location", "lat": "-36.7277507", "lon": "142.1988267", "tst": "1427854340", "acc": "20.0", "batt": "67", "device_udid":44548}
     */
    public function add()
    {
        $this->load->library('api');
        $this->api->log();

        // Validation
        $device_udid = $this->api->get('device_udid');
        $asset = $this->db->where('device_udid', $device_udid)
            ->get('assets')
            ->row();

				$lastloc = $this->db->where('device_udid', $device_udid)
						->order_by('location_geo_id', 'desc')
						->get('locations_geo')
						->row();
						
						
        if (!$asset || $device_udid == '') {
            $data = array(
                'status' => 'fail',
                'message' => 'No asset found with this device uuid.'
            );
            $this->api->sendJson($data);
            return false;
        }

        if($this->api->get('_type') != 'location') {
            $data = array(
                'status' => 'fail',
                'message' => 'Only location accepted'
            );
            $this->api->sendJson($data);
            return false;
        }
        

        if(is_numeric($this->api->get('lat')) == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'Invalid latitude value'
            );
            $this->api->sendJson($data);
            return false;
        }

        if(is_numeric($this->api->get('lon')) == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'Invalid longitude value'
            );
            $this->api->sendJson($data);
            return false;
        }
        
        
        //Approximately +/- 70m is needed to accept a new location
        if(!( ($lastloc->latitude + 0.0006 < $this->api->get('lat')) || ($lastloc->latitude - 0.0006 > $this->api->get('lat'))  ||  ($lastloc->longitude + 0.0006 < $this->api->get('lon')) || ($lastloc->longitude - 0.0006 > $this->api->get('lon')) )){
        	  $data = array(
                'status' => 'fail',
                'message' => 'Duplicate Entry Detected, rejected',
                'lastlat' => round($lastloc->latitude,4),
                'thislat' => round($this->api->get('lat'),4),
                'lastlon' => round($lastloc->longitude,4),
                'thislon' => round($this->api->get('lon'),4)
            );
            $this->api->sendJson($data);
            
            //Need to also update the location, so that we know the device is working, and not dead.
            $mysql_data = array(
            		'updated_on' => date('Y-m-d H:i:s'),
            		'hdop' => $this->api->get('acc')
            );
            $this->db->where('location_geo_id', $lastloc->location_geo_id);
            $this->db->update('locations_geo', $mysql_data);
            return false;
        }
        // End validation

        // Save data
        $mysql_data = array(
            'device_udid' => $this->api->get('device_udid'),
            'longitude' => $this->api->get('lon'),
            'latitude' => $this->api->get('lat'),
            'hdop' => $this->api->get('acc'),
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s'),
            'asset_id' => $asset->asset_id,
            'company_id' => $asset->company_id
        );

        $this->db->insert('locations_geo', $mysql_data);

        $data = array(
            'status' => 'success',
            'message' => 'Location submitted successfully.'
        );
        $this->api->sendJson($data);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: 007
 * Date: 6/2/2015
 * Time: 3:47 PM
 */
class Accounting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('*'));
    }

    public function index()
    {
        $this->load->view('bckcadmin/accounting.php');
    }

    public function add()
    {
        $this->load->view('bckcadmin/accounting.add.php');
    }

    public function edit($meta_id = 0)
    {
        $asset_meta = $this->db->where('asset_meta_id', $meta_id)
            ->where('company_id', $this->auth->company_id())
            ->where('deleted-at', '0000-00-00 00:00:00')
            ->get('asset_meta')->row();
        $asset = $this->db->where('asset_id', $asset_meta->asset_id)
            ->get('assets')
            ->row();
        if ($asset_meta) {
            $data = array(
                'asset' => $asset,
                'asset_meta' => $asset_meta
            );
            $this->load->view('bckcadmin/accounting.edit.php', $data);
        } else {
            show_404();
        }
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read assets.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('datatables');
        $this->datatables->select('
            asset_meta.asset_meta_id,
            assets.friendly_name,
            assets.asset_id,
            asset_meta.purchase_date,
            asset_meta.purchase_value,
            asset_meta.effective_life,
            asset_meta.depreciation_method,
            asset_meta.created_at
        ');
        $this->datatables->join('assets', 'assets.asset_id = asset_meta.asset_id');
        $this->datatables->where('asset_meta.company_id', $this->auth->company_id());
        $this->datatables->from('asset_meta');
        if ($this->has_permission(2)) {
            $actions = ' <a onclick="edit_fuel(this)"><i class="fa fa-edit" id="datatable-item-$1"></i> Edit</a> |
                    <a onclick="delete_record(this)"><i class="fa fa-trash" id="datatable-item-$1"></i> Delete</a>';
        } else {
            $actions = '<span class="label label-sm label-danger">Not Authorized</span>';
        }
        $this->datatables->add_column('actions', $actions, 'asset_meta_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function single()
    {
        $asset_meta_id = (int)$this->input->post('rid');
        $asset_meta = $this->db->where('asset_meta_id', $asset_meta_id)->where('company_id', $this->auth->company_id())
            ->get('asset_meta')
            ->row();
        if (!$asset_meta) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Asset record not found.'
                )
            );
            echo json_encode($data);
            return false;
        } else {
            $data = array(
                'status' => 'success',
                'message' => 'Asset meta read successfully.',
                'errors' => '',
                'asset_meta' => $asset_meta
            );
            echo json_encode($data);
            return false;
        }
    }

    public function create()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric');
        $this->form_validation->set_rules('purchase_date', 'Purchase Date', 'required');
        $this->form_validation->set_rules('purchase_value', 'Purchase Value', 'required|numeric');
        $this->form_validation->set_rules('effective_life', 'Effective Life', 'required|numeric');
        $this->form_validation->set_rules('depreciation_method', 'Depreciation Method', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // insert data
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to create.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $mysql_data = array(
                'asset_id' => $this->input->post('asset_id'),
                'purchase_date' => date('Y-m-d H:i:s', strtotime($this->input->post('purchase_date'))),
                'purchase_value' => $this->input->post('purchase_value'),
                'effective_life' => $this->input->post('effective_life'),
                'depreciation_method' => $this->input->post('depreciation_method'),
                'company_id' => $this->auth->company_id(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('asset_meta', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Asset meta added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function update($asset_meta_id = 0)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric');
        $this->form_validation->set_rules('purchase_date', 'Purchase Date', 'required');
        $this->form_validation->set_rules('purchase_value', 'Purchase Value', 'required|numeric');
        $this->form_validation->set_rules('effective_life', 'Effective Life', 'required|numeric');
        $this->form_validation->set_rules('depreciation_method', 'Depreciation Method', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to update.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            // insert data
            $mysql_data = array(
                'asset_id' => $this->input->post('asset_id'),
                'purchase_date' => date('Y-m-d H:i:s', strtotime($this->input->post('purchase_date'))),
                'purchase_value' => $this->input->post('purchase_value'),
                'effective_life' => $this->input->post('effective_life'),
                'depreciation_method' => $this->input->post('depreciation_method'),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $this->db->where('asset_meta_id', $asset_meta_id)->update('asset_meta', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Asset meta updated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function delete()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to delete asset account entry.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $asset_meta_id = (int)$this->input->post('rid');

        $this->db->where('asset_meta_id', $asset_meta_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('asset_meta');
        $data = array(
            'status' => 'success',
            'message' => 'Asset entry removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }
}
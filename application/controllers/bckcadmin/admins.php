<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/15/2015
 * Time: 12:06 PM
 */
class Admins extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function index()
    {
        $this->load->view('bckcadmin/admins.php');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_id', 'User', 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add company admins.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'user_id' => $this->input->post('user_id'),
                'company_id' => $this->auth->company_id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('company_admins', $mysql_data);
            $user_id = $this->db->insert_id();

            $data = array(
                'status' => 'success',
                'message' => 'Company user added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read company admins.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('datatables');
        $this->datatables->select('company_admin_id, concat_ws(" ", first_name, last_name) as name, users.email',false);
        $this->datatables->join('users', 'company_admins.user_id = users.user_id');
        $this->datatables->where('company_id', $this->auth->company_id());
        $this->datatables->from('company_admins');
        if ($this->has_permission(2)) {
            $actions = '
                    <a onclick="delete_record(this)"><i class="fa fa-trash" id="datatable-item-$1"></i> Delete</a>';
        } else {
            $actions = '<span class="label label-sm label-danger">Not Authorized</span>';
        }
        $this->datatables->add_column('actions', $actions, 'company_admin_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function getlist()
    {
        $company_users = $this->company_model->get_company_users('existing');
        $user_ids = array(0);
        foreach ($company_users as $user) {
            $user_ids[] = $user->user_id;
        }

        $company_admins = $this->db->where_in('user_id', $user_ids)
            ->where('company_id', $this->auth->company_id())
            ->get('company_admins')
            ->result();

        $company_admin_ids = array(0);
        foreach ($company_admins as $company_admin) {
            $company_admin_ids[] = $company_admin->user_id;
        }

        // hide already admin users
        foreach ($user_ids as $key => $user_id) {
            if (in_array($user_id, $company_admin_ids)) {
                unset($user_ids[$key]);
            }
        }

        $user_ids[] = 0;

        // list of users that are not admin currently
        $users = $this->db->where_in('user_id', $user_ids)
            ->like('first_name', $this->input->post('q'))
            ->get('users')
            ->result();

        $data = array(
            'status' => 'success',
            'message' => 'Fetch successful',
            'users' => $users
        );
        echo json_encode($data);
    }

    public function delete()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to delete company admin.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $user_id = (int)$this->input->post('rid');

        $this->db->where('company_admin_id', $user_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('company_admins');
        $data = array(
            'status' => 'success',
            'message' => 'Company admin removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }
}
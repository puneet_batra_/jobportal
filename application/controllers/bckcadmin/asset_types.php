<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/15/2015
 * Time: 12:06 PM
 */
class Asset_types extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function index()
    {
        $this->load->view('bckcadmin/asset_types.php');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('feature_fuel', 'Fuel', 'required');
        $this->form_validation->set_rules('feature_maintenance', 'Maintenance', 'required');
        $this->form_validation->set_rules('feature_exception', 'Exception', 'required');
        $this->form_validation->set_rules('feature_location', 'Location', 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add asset type.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'name' => $this->input->post('name'),
                'feature_fuel' => $this->input->post('feature_fuel'),
                'feature_maintenance' => $this->input->post('feature_maintenance'),
                'feature_exception' => $this->input->post('feature_exception'),
                'feature_location' => $this->input->post('feature_location'),
                'company_id' => $this->auth->company_id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('asset_types', $mysql_data);
            $asset_type_id = $this->db->insert_id();

            $managers = (array)explode(',', $this->input->post('managers'));
            foreach ($managers as $user_id) {
                $user_id = (int)$user_id;
                if ($user_id > 0) {
                    $mysql_data = array(
                        'asset_type_id' => $asset_type_id,
                        'company_id' => $this->auth->company_id(),
                        'user_id' => $user_id,
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id(),
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('asset_managers', $mysql_data);
                }
            }

            $data = array(
                'status' => 'success',
                'message' => 'Asset type added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('feature_fuel', 'Fuel', 'required');
        $this->form_validation->set_rules('feature_maintenance', 'Maintenance', 'required');
        $this->form_validation->set_rules('feature_exception', 'Exception', 'required');
        $this->form_validation->set_rules('feature_location', 'Location', 'required');
        $this->form_validation->set_rules('asset_type_id', 'Asset Type', 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit asset type.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'name' => $this->input->post('name'),
                'feature_fuel' => $this->input->post('feature_fuel'),
                'feature_maintenance' => $this->input->post('feature_maintenance'),
                'feature_exception' => $this->input->post('feature_exception'),
                'feature_location' => $this->input->post('feature_location'),
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $asset_type_id = (int)$this->input->post('asset_type_id');
            $this->db->where('asset_type_id', $asset_type_id)
                ->where('company_id', $this->auth->company_id())
                ->update('asset_types', $mysql_data);

            $this->db->where('asset_type_id', $asset_type_id)
                ->where('company_id', $this->auth->company_id())
                ->delete('asset_managers');
            $managers = (array)explode(',', $this->input->post('managers'));
            foreach ($managers as $user_id) {
                $user_id = (int)$user_id;
                if ($user_id > 0) {
                    $mysql_data = array(
                        'asset_type_id' => $asset_type_id,
                        'company_id' => $this->auth->company_id(),
                        'user_id' => $user_id,
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id(),
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('asset_managers', $mysql_data);
                }
            }

            $data = array(
                'status' => 'success',
                'message' => 'Asset type updated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read asset types.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('datatables');
        $this->datatables->select('asset_type_id, name, feature_fuel, feature_maintenance, feature_exception, feature_location');
        $this->datatables->where('company_id', $this->auth->company_id());
        $this->datatables->from('asset_types');
        if ($this->has_permission(2)) {
            $actions = ' <a onclick="edit_fuel(this)"><i class="fa fa-edit" id="datatable-item-$1"></i> Edit</a> |
                    <a onclick="delete_record(this)"><i class="fa fa-trash" id="datatable-item-$1"></i> Delete</a>';
        } else {
            $actions = '<span class="label label-sm label-danger">Not Authorized</span>';
        }
        $this->datatables->add_column('actions', $actions, 'asset_type_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function delete()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to delete asset types.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $asset_type_id = (int)$this->input->post('rid');

        $assets = $this->db->where('asset_type_id', $asset_type_id)
            ->get('assets')->result();
        if (count($assets) > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Cannot delete: It is in use by ' . count($assets) . ' ' . ((count($assets)>1)?'assets':'asset') .'.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $this->db->where('asset_type_id', $asset_type_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('asset_types');
        $data = array(
            'status' => 'success',
            'message' => 'Asset type removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function single()
    {
        $asset_type_id = (int)$this->input->post('rid');
        $asset_type = $this->db->where('asset_type_id', $asset_type_id)->where('company_id', $this->auth->company_id())
            ->get('asset_types')
            ->row();
        if (!$asset_type) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Asset type not found.'
                )
            );
            echo json_encode($data);
            return false;
        } else {
            $managers = $this->db
                ->select('users.*', false)
                ->join('asset_managers', 'asset_managers.user_id = users.user_id')
                ->where('asset_managers.asset_type_id', $asset_type_id)
                ->where('asset_managers.company_id', $this->auth->company_id())
                ->get('users')
                ->result();
            $asset_type->managers = $managers;

            $data = array(
                'status' => 'success',
                'message' => 'Asset type read successfully.',
                'errors' => '',
                'asset_type' => $asset_type
            );
            echo json_encode($data);
            return false;
        }
    }


}
<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/19/2015
 * Time: 12:26 PM
 */
class Assets extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('view', 'search', 'getdocuments'));
    }

    /**
     * Default asset listing page (currently not used)
     */
    public function index()
    {
        redirect('bckcadmin/assets/add');
    }

    /**
     * Show form for adding new asset
     */
    public function add()
    {
        $this->load->model('asset_types_model');

        $asset_types = $this->asset_types_model->getList();
        $data = array(
            'asset_types' => $asset_types
        );
        $this->load->view('bckcadmin/assets.add.php', $data);
    }

    /**
     * Add asset ajax submit
     */
    public function assetadd()
    {
        $this->load->model( array(
                'files_model',
                'assets_model'
            ));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('friendly_name', 'Friendly Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('nfc_uuid', 'NFC UUID', '');
        $this->form_validation->set_rules('asset_type_id', 'Asset Type', 'required');
        $this->form_validation->set_rules('features', 'Features', '');
        $this->form_validation->set_rules('enabled', 'Enabled', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->response->error($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add assets.'
                    )
                );
                return $this->response->error($data);
            }
            // validate photos before uploading
            $extensions = array('jpg', 'jpeg', 'png', 'gif');
            $valid_type = $this->files_model->validExtension('files', $extensions);

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only pictures allowed'
                    )
                );
                return $this->response->error($data);
            }

            // insert data
            $mysql_data = array(
                'company_id' => $this->auth->company_id(),
                'friendly_name' => $this->input->post('friendly_name'),
                'description' => $this->input->post('description'),
                'nfc_uuid' => $this->input->post('nfc_uuid'),
                'asset_key' => hash('sha256',microtime(true)),
                'asset_type_id' => $this->input->post('asset_type_id'),
                'enabled' => $this->input->post('enabled'),
                'feature_fuel' => (in_array('feature_fuel', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_maintenance' => (in_array('feature_maintenance', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_exception' => (in_array('feature_exception', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_location' => (in_array('feature_location', (array)$this->input->post('features'))) ? 1 : 0,
            );
            $asset_id = $this->assets_model->add($mysql_data);

            // upload the photos selected if submitted
            $save_path = './uploads/' . $this->auth->company_id() . '/assets/photos/';
            $uploaded_files = $this->files_model->bulkSave('files', $save_path);

            foreach ($uploaded_files as $uploaded_file) {
                $mysql_data = array(
                    'filename' => $uploaded_file['uploaded_name'],
                    'asset_id' => $asset_id,
                    'company_id' => $this->auth->company_id(),
                );
                $this->assets_model->addAttachment($mysql_data);
            }

            $data = array(
                'message' => 'Asset added successfully.',
                'redirect_url' => site_url('bckcadmin/assets/view/' . $asset_id),
            );
            return $this->response->success($data);
        }

    }

    public function addphotos($asset_id)
    {
        $asset_id = (int)$asset_id;
        $asset = $this->db->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->get('assets')
            ->row();
        if ($asset) {
            $this->load->model('files_model');
            $this->load->model('assets_model');
            $extensions = array('jpg', 'jpeg', 'png', 'gif');
            $valid_type = $this->files_model->validExtensionSingle('photos', $extensions);

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only pictures allowed'
                    )
                );
                return $this->response->error($data);
            }
            $save_path = './uploads/' . $this->auth->company_id() . '/assets/photos/';
            $uploaded_file = $this->files_model->save('photos', $save_path);
            $mysql_data = array(
                'filename' => $uploaded_file[0]['uploaded_name'],
                'asset_id' => $asset_id,
                'company_id' => $this->auth->company_id(),
            );
            $this->assets_model->addAttachment($mysql_data);
            $data = array(
                'message' => 'File uploaded successfully.'
            );
            return $this->response->success($data);
        }
    }

    public function addDocuments($asset_id)
    {
        $asset_id = (int)$asset_id;
        $asset = $this->db->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->get('assets')
            ->row();
        if ($asset) {
            $this->load->model('files_model');
            $this->load->model('assets_model');
            $extensions = array('php', 'php5', 'php54', 'exe');
            $valid_type = $this->files_model->validExtensionSingle('documents', $extensions, true);

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only documents allowed'
                    )
                );
                return $this->response->error($data);
            }
            $save_path = './uploads/' . $this->auth->company_id() . '/assets/documents/';
            $uploaded_file = $this->files_model->save('documents', $save_path);
            $mysql_data = array(
                'title' => $uploaded_file[0]['original_name'],
                'filename' => $uploaded_file[0]['uploaded_name'],
                'asset_id' => $asset_id,
                'company_id' => $this->auth->company_id(),
            );
            $this->assets_model->addDocument($mysql_data);
            $data = array(
                'message' => 'File uploaded successfully.'
            );
            return $this->response->success($data);
        }
    }

    /**
     * Dynamically get features of selected asset type
     */
    public function getAssetFeatures()
    {
        $this->load->model('asset_types_model');
        $asset_id = (int)$this->input->post('aid');
        $asset = $this->asset_types_model->get($asset_id);
        $data = array(
            'features' => array(
                'feature_fuel' => 0,
                'feature_maintenance' => 0,
                'feature_exception' => 0,
                'feature_location' => 0
            )
        );
        if ($asset) {
            $data = array(
                'features' => array(
                    'feature_fuel' => $asset->feature_fuel,
                    'feature_maintenance' => $asset->feature_maintenance,
                    'feature_exception' => $asset->feature_exception,
                    'feature_location' => $asset->feature_location
                )
            );
        }

        return $this->response->success($data);
    }

    /**
     * Show asset detailed information
     * @param int $asset_id
     * @return bool
     */
    public function view($asset_id = 0)
    {
        // check if fuel sub module, has write access for assets module ?
        // Sub module permissions system ex: is_allowed(2, 'assets', '', 'fuel');
        $this->load->model(array(
            'assets_model',
            'asset_types_model',
            'suppliers_model'
        ));

        $asset_id = (int)$asset_id;
        $asset = $this->assets_model->get($asset_id);
        if ($asset) {
            // check if asset exists for the current user?
            $asset_found = false;
            foreach ($this->auth->get()->companies as $joined_company) {
                if ($joined_company->company_id == $asset->company_id) {
                    $asset_found = true;
                    break;
                }
            }
            if ($asset_found == false) {
                show_error('You do not have access to this asset.', '404', 'Not allowed');
                return false;
            } else {
                // asset found if this is not in current company, login him to that company
                if ($this->auth->company_id() != $asset->company_id) {
                    $login_url = 'company/login/' . $asset->company_id . '?ret=assets&id=' . $asset->asset_id;
                    redirect($login_url);
                }
            }

            // permissions enforcement
            if (!$this->has_permission(1)) {
                show_error('You do not have access to view assets.', '403', 'Not allowed');
                return false;
            }
            if ($this->has_permission(2)) {
                $has_edit_permissions = true;
            } else {
                $has_edit_permissions = false;
            }

            // Now we can safely proceed with asset displaying
            $photos = $this->assets_model->getPhotos($asset->asset_id);
            $documents = $this->assets_model->getDocuments($asset->asset_id);
            $asset_types = $this->asset_types_model->getList();
            $company = $this->auth->company();
            $suppliers = $this->suppliers_model->getList();

            $asset->photos = $photos;
            $asset->documents = $documents;
            $asset->company = $company;

            $data = array(
                'suppliers' => $suppliers,
                'has_edit_permissions' => $has_edit_permissions,
                'asset' => $asset,
                'asset_types' => $asset_types
            );

            if ($this->input->get('partial')) {
                $partials = array(
                    'exception' => 'assets.view.partials.exception.php',
                    'fuel' => 'assets.view.partials.fuel.php',
                    'location' => 'assets.view.partials.location.php',
                    'maintenance' => 'assets.view.partials.maintenance.php'
                );
                if (in_array($this->input->get('partial'), array_keys($partials))) {
                    $this->load->view('bckcadmin/' . $partials[$this->input->get('partial')], $data);
                }
            } else {
                $this->load->view('bckcadmin/assets.view.php', $data);
            }
        } else {
            show_error('Asset you are looking for is not found.', '404', 'Not found');
        }
    }

    /**
     * Update asset information with ajax submit
     * @param $asset_id
     * @return bool
     */
    public function update($asset_id)
    {
        $this->load->model(array(
            'assets_model',
            'files_model'
        ));

        $asset_id = (int)$asset_id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('asset_type_id', 'Asset Type', 'required');
        $this->form_validation->set_rules('features', 'Features', '');
        $this->form_validation->set_rules('enabled', 'Enabled', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->response->error($data);
        } else {
            // check permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit assets.'
                    )
                );
                return $this->response->error($data);
            }
            $asset = $this->assets_model->get($asset_id);
            if (!$asset || $asset->company_id != $this->auth->company_id()) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'This asset does not belongs to you.'
                    )
                );
                return $this->response->error($data);
            }

            // check for valid photos
            $extensions = array('jpg', 'jpeg', 'png', 'gif');
            $valid_type = $this->files_model->validExtension('photos_upload', $extensions);
            // show error if files are not valid
            if ($valid_type == false) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only pictures allowed in photos.'
                    )
                );
                return $this->response->error($data);
            }

            // documents check
            $extensions = array('php', 'php5', 'php54', 'exe');
            $valid_type = $this->files_model->validExtension('documents_upload', $extensions, true);
            // show error if document files are not valid
            if ($valid_type == false) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, documents only allowed in documents.'
                    )
                );
                return $this->response->error($data);
            }

            $can_be_deleted = true;
            $feature = '';

            if ($asset->feature_maintenance == 1 && !in_array('feature_maintenance', (array)$this->input->post('features'))) {
                $count = $this->db->where('asset_id', $asset_id)
                    ->count_all_results('maintenance');
                if ($count > 0) {
                    $can_be_deleted = false;
                    $feature = 'Maintenance';
                }
            }
            if ($asset->feature_fuel == 1 && !in_array('feature_fuel', (array)$this->input->post('features'))) {
                $count = $this->db->where('asset_id', $asset_id)
                    ->count_all_results('fuel');
                if ($count > 0) {
                    $can_be_deleted = false;
                    $feature = 'Fuel';
                }
            }
            if ($asset->feature_exception == 1 && !in_array('feature_exception', (array)$this->input->post('features'))) {
                $count = $this->db->where('asset_id', $asset_id)
                    ->count_all_results('exceptions');
                if ($count > 0) {
                    $can_be_deleted = false;
                    $feature = 'Exception';
                }
            }
            if ($asset->feature_location == 1 && !in_array('feature_location', (array)$this->input->post('features'))) {
                $count = $this->db->where('asset_id', $asset_id)
                    ->count_all_results('locations_geo');
                if ($count > 0) {
                    $can_be_deleted = false;
                    $feature = 'Location';
                }
            }
            if ($asset->feature_location == 1 && !in_array('feature_location', (array)$this->input->post('features'))) {
                $count = $this->db->where('asset_id', $asset_id)
                    ->count_all_results('locations_text');
                if ($count > 0) {
                    $can_be_deleted = false;
                    $feature = 'Location';
                }
            }

            if($can_be_deleted == false)
            {
                $data = array(
                    'errors' => array(
                        'no_permission' => $feature .' has data and cannot be disabled.'
                    )
                );
                return $this->response->error($data);
            }

            // update the information about asset
            $mysql_data = array(
                'asset_type_id' => $this->input->post('asset_type_id'),
                'enabled' => $this->input->post('enabled'),
                'feature_fuel' => (in_array('feature_fuel', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_maintenance' => (in_array('feature_maintenance', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_exception' => (in_array('feature_exception', (array)$this->input->post('features'))) ? 1 : 0,
                'feature_location' => (in_array('feature_location', (array)$this->input->post('features'))) ? 1 : 0,
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $this->assets_model->update($mysql_data, $asset_id);

            // upload photos if selected
            // upload the photos selected if submitted
            $save_path = './uploads/' . $this->auth->company_id() . '/assets/photos/';
            $uploaded_files = $this->files_model->bulkSave('photos_upload', $save_path);

            foreach ($uploaded_files as $uploaded_file) {
                $mysql_data = array(
                    'filename' => $uploaded_file['uploaded_name'],
                    'asset_id' => $asset_id,
                    'company_id' => $this->auth->company_id(),
                );
                $this->assets_model->addAttachment($mysql_data);
            }


            // upload documents if valid
            // upload the photos selected if submitted
            $save_path = './uploads/' . $this->auth->company_id() . '/assets/documents/';
            $uploaded_files = $this->files_model->bulkSave('documents_upload', $save_path);

            foreach ($uploaded_files as $uploaded_file) {
                $mysql_data = array(
                    'title' => $uploaded_file['original_name'],
                    'filename' => $uploaded_file['uploaded_name'],
                    'asset_id' => $asset_id,
                    'company_id' => $this->auth->company_id(),
                );
                $this->assets_model->addDocument($mysql_data);
            }

            $data = array(
                'message' => 'Asset updated successfully.',
            );
            return $this->response->success($data);
        }
    }

    /**
     * Inline asset edit function for ajax
     * @return bool
     */
    public function updatesingle()
    {
        $this->load->model(array(
            'assets_model'
        ));
        // check permissions
        if (!$this->has_permission(2)) {
            $data = array(
                'message' => 'You do not have permission to edit assets.',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit assets.'
                )
            );
            return $this->response->error($data);
        }
        // edit your own assets only
        $asset = $this->assets_model->get((int)$this->input->post('pk'));
        if (!$asset) {
            $data = array(
                'message' => 'This asset does not belong to you.',
                'errors' => array(
                    'no_permission' => 'This asset does not belong to you.'
                )
            );
            return $this->response->error($data);
        }

        // apply rules
        $rules = array(
            'friendly_name' => 'required',
            'description' => 'required',
            'nfc_uuid' => '',
            'device_udid' => 'callback_validate_device_udid'
        );
        // define user friendly names
        $names = array(
            'friendly_name' => 'Name',
            'description' => 'Description',
            'nfc_uuid' => 'Barcode',
            'device_udid' => 'LoRa ID'
        );

        $key = $this->input->post('name'); // name of field
        $pk = (int)$this->input->post('pk'); // primary key of record

        // if field not found in given set of rules
        if (!isset($names[$key])) {
            $data = array(
                'errors' => 'Field editable not supported.',
                'message' => 'There were errors'
            );
            return $this->response->error($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('value', $names[$key], $rules[$key]);
        if ($this->form_validation->run() == false) {
            $data = array(
                'errors' => $this->form_validation->get_message('value'),
                'message' => 'There were errors'
            );
            return $this->response->error($data);
        } else {
            // update the record
            $mysql_data = array(
                $key => trim(str_replace('&nbsp;', ' ', $this->input->post('value')))
            );
            $this->db->where('asset_id', $pk)->update('assets', $mysql_data);

            $data = array(
                'errors' => '',
                'message' => $names[$key] . ' updated successfully.'
            );
            return $this->response->success($data);
        }
    }

    public function validate_device_udid($value)
    {
        $device_udid = $value;
        $pk = (int)$this->input->post('pk');
        $asset = $this->db->where('asset_id <>', $pk)
            ->where('device_udid', $device_udid)
            ->get('assets')
            ->row();
        if ($asset) {
            $this->form_validation->set_message('validate_device_udid', 'This field should be unique.');
            return false;
        } else {
            return true;
        }
    }

    /**
     * Remove photo from asset with ajax call
     * @return bool
     */
    public function removephoto()
    {
        $this->load->model(array(
            'assets_model'
        ));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rid', 'Photo', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->response->error($data);
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit assets.'
                    )
                );
                return $this->response->error($data);
            }
            // delete your own photos only
            $asset_photo_id = (int)$this->input->post("rid");
            $asset_photo = $this->assets_model->getPhoto($asset_photo_id);
            if (!$asset_photo || $asset_photo->company_id != $this->auth->company_id()) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'This asset does not belongs to you.'
                    )
                );
                return $this->response->error($data);
            }

            $this->assets_model->deletePhoto($asset_photo_id);
            $data = array(
                'message' => 'Photo removed successfully.',
            );
            return $this->response->success($data);
        }
    }

    public function removedocument()
    {
        $this->load->model(array(
            'assets_model'
        ));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('did', 'Document', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->response->error($data);
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit assets.'
                    )
                );
                return $this->response->error($data);
            }
            // delete your own photos only
            $asset_document_id = (int)$this->input->post("did");
            $asset = $this->assets_model->getDocument($asset_document_id);
            if (!$asset) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'This asset does not belongs to you.'
                    )
                );
                return $this->response->error($data);
            }

            $this->assets_model->deleteDocument($asset_document_id);
            $data = array(
                'message' => 'Document removed successfully.',
                'errors' => ''
            );
            return $this->response->success($data);
        }
    }

    public function getDocuments()
    {
        $this->load->model(array(
            'assets_model'
        ));

        if (!$this->has_permission(1)) {
            $data = array(
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read assets.'
                )
            );
            return $this->response->error($data);
        }

        $asset_id = (int)$this->input->post('rid');
        $records = $this->assets_model->getDocuments($asset_id);
        $data = array(
            'status' => 'success',
            'message' => 'success.',
            'errors' => '',
            'records' => $records
        );
        return $this->response->success($data);
    }

    public function getPhotos()
    {
        $this->load->model(array(
            'assets_model'
        ));
        if (!$this->has_permission(1)) {
            $data = array(
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read assets.'
                )
            );
            return $this->response->error($data);
        }

        $asset_id = (int)$this->input->post('rid');
        $records = $this->assets_model->getPhotos($asset_id);
        foreach ($records as $id => $record) {
            if (!file_exists('uploads/' . $this->auth->company_id() . '/assets/photos/' . $record->filename)) {
                unset($records[$id]);
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'success.',
            'errors' => '',
            'records' => $records
        );
        return $this->response->success($data);
    }

    public function search()
    {
        $query = $this->input->post('q');
        $is_search_page = $this->input->post('is_search_page');
        if ($is_search_page) {
            // searched through search results page
            $offset = (int)$this->input->post('offset');
            $limit = (int)$this->input->post('limit');
        } else {
            // searched through top search bar
            $offset = 0;
            $limit = 25;
        }


        $filtered_query = $this->db->escape_like_str($query);

        // prepare for the best results
        $keywords = explode(' ', $filtered_query);
        $search1_array = array();
        $search2_array = array();
        $search3_array = array();
        foreach ($keywords as $keyword) {
            $keyword = preg_replace('/([^A-Za-z0-9\s]+)/i', '', $keyword);
            if (trim($keyword) != '') {
                $search1_array[] = "(
                            `friendly_name` LIKE '%$keyword%'
                            )";
                $search2_array[] = "(
                						`friendly_name` SOUNDS LIKE '%$keyword%'
                						OR `description`  LIKE '%{$keyword}%'
                						OR `friendly_name`  LIKE '%{$keyword}%'
                     				
                )";
                $search3_array[] = "(
                            `nfc_uuid` = '$query'
                )";
            }
        }
        $search1_str = implode(' AND ', $search1_array);
        $search2_str = implode(' AND ', $search2_array);
        $search3_str = implode(' AND ', $search3_array);

        $company_id = (int)$this->auth->company_id();

        $where = array();
        $where_str = '';
        if ($this->input->post('hide_deleted') == true) {
            $where[] = 'assets.deleted_on = "0000-00-00 00-00-00"';
        }
        if (strpos($query, '&') === 0) {
            // show inactive assets
            // remove & sign
            $query = substr($query, 1);
        } else {
            $where[] = 'assets.enabled = 1';
        }

        if ($this->input->post('has_feature')) {
            $where[] = 'assets.feature_' . $this->input->post('has_feature') . ' = 1';
        }

        if (count($where) > 0) {
            $where_str = 'AND ' . implode(' AND ', $where);
        }

        $max = rand(15, 25);
        $results = array();
        $search_sql_1 = "SELECT * FROM (
												SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                        FROM (`assets`)
                        JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                        left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                        WHERE
                        `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                        AND `assets`.`company_id` =  {$company_id}
                        AND
                        (
                            $search1_str
                        )
                        $where_str

                        group by assets.asset_id
                        )
                        as filtered_tbl
                        ORDER BY friendly_name
                        LIMIT $offset,$limit";

        $search_sql_2 = "SELECT * FROM (
    										SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                        FROM (`assets`)
                        JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                        left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                        WHERE
                        `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                        AND `assets`.`company_id` =  {$company_id}
                        AND
                        (
                            $search2_str
                        )

                        $where_str
                        
                        
                        )
                        as filtered_tbl
                        GROUP BY asset_id
                        ORDER BY friendly_name
                        LIMIT $offset,$limit";

        $search_sql_barcode = "SELECT * FROM (
    										SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                        FROM (`assets`)
                        JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                        left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                        WHERE
                        `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                        AND `assets`.`company_id` =  {$company_id}
                        AND
                        (
                            $search3_str
                        )
                        $where_str
                        )
                        as filtered_tbl
                        GROUP BY asset_id
                        ORDER BY friendly_name
                        LIMIT $offset,$limit";
        /*
                $search_sql = "
                                                        SELECT * FROM (
                                                        SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                                FROM (`assets`)
                                JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                                left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                                WHERE
                                `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                                AND `assets`.`company_id` =  {$company_id}
                                AND
                                (
                                    $search1_str
                                )
                                $where_str

                                UNION

                                SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                                FROM (`assets`)
                                JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                                left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                                WHERE
                                `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                                AND `assets`.`company_id` =  {$company_id}
                                AND
                                (
                                    $search2_str
                                )
                                $where_str

                                UNION

                                SELECT assets.*, asset_types.name as asset_type, asset_photos.filename as picture
                                FROM (`assets`)
                                JOIN `asset_types` ON `asset_types`.`asset_type_id` = `assets`.`asset_type_id`
                                left outer JOIN asset_photos on assets.asset_id = asset_photos.asset_id
                                WHERE
                                `assets`.`deleted_on` =  '0000-00-00 00:00:00'
                                AND `assets`.`company_id` =  {$company_id}
                                AND
                                (
                                    $search3_str
                                )
                                $where_str
                                ) as filtered_tbl
                                group by asset_id
                                LIMIT $offset,$limit";
                                */

        if (trim($search1_str) != '') {
            $count_asset_sbc = $this->db->query("select count(*) as numrows from ($search_sql_barcode) as barcode_table")->row()->numrows;
            if ($count_asset_sbc == 0) {
                $assets = $this->db
                    ->query($search_sql_1)
                    ->result();
                $count_assets = $this->db->query("select count(*) as numrows from ($search_sql_1) as sql1")->row()->numrows;

                if ($count_assets < $limit) {
                    $limit = $limit - $count_assets;
                    $assets = $this->db
                        ->query($search_sql_2)
                        ->result();
                }
            } else {
                $assetsbc = $this->db
                    ->query($search_sql_barcode)
                    ->result();
                $assets = $assetsbc;
            }
        } else {
            $assets = array();
        }


        if (count($assets) > 0) {
            for ($i = 0; $i < count($assets); $i++) {
                $has_exception = $this->db->where('asset_id', $assets[$i]->asset_id)->count_all_results('exceptions');
                if ($has_exception > 0) {
                    $exception_class = 'has-exception';
                } else {
                    $exception_class = '';
                }
                $image = 'public/img/default_product.png';
                if (!$assets[$i]->picture == '') {
                    $image = 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $assets[$i]->picture;
                }
                //
                //
                //  Round has been ommitted due to an issue with gd on the server
                //$image = site_url('image/' . $image) . '?s=50x50&method=crop&round=2';

                //
                //
                if ($is_search_page) {
                    $desc = $assets[$i]->description;
                    $desc = str_replace('<h1>', '<div>', $desc);
                    $desc = str_replace('</h1>', '</div>', $desc);
                    $desc = str_replace('<h2>', '<div>', $desc);
                    $desc = str_replace('</h2>', '</div>', $desc);
                    $desc = str_replace('<h3>', '<div>', $desc);
                    $desc = str_replace('</h3>', '</div>', $desc);
                    $desc = str_replace('<h4>', '<div>', $desc);
                    $desc = str_replace('</h4>', '</div>', $desc);
                    $image = site_url('image/' . $image) . '?s=150x150&method=crop';
                } else {
                    // Prevention: add spaces between tags because stripped text would join them together
                    $desc = preg_replace('/<([^>]+)>([^<]+)<([^>]+)>/i', '<$1>$2 <$3>', $assets[$i]->description);
                    $desc = str_replace('&nbsp;', ' ', strip_tags($desc, '<br>'));
                    $image = site_url('image/' . $image) . '?s=50x50&method=crop';
                }

                $results[] = array(
                    "id" => $assets[$i]->asset_id,
                    "value" => str_replace('&nbsp;', ' ', $assets[$i]->friendly_name),
                    "desc" => $desc,
                    "link" => site_url('bckcadmin/assets/view/' . $assets[$i]->asset_id),
                    "class" => $exception_class,
                    "img" => $image,
                    "tokens" => array($query, $query . rand(1, 10))
                );

            }
        }

        $this->output->set_content_type('application/json')
            ->set_output(json_encode($results));

    }

    public function delete()
    {
        $this->load->model(array(
            'assets_model'
        ));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('aid', 'Asset', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->response->error($data);
        } else {
            // check permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit/remove assets.'
                    )
                );
                return $this->response->error($data);
            }

            $asset_id = (int)$this->input->post('aid');
            $asset = $this->assets_model->get($asset_id);
            if (!$asset) {
                $data = array(
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You can delete your own assets only.'
                    )
                );
                return $this->response->error($data);
            } else {
                $this->assets_model->delete($asset_id);
                $data = array(
                    'message' => 'Asset removed successfully.',
                    'errors' => ''
                );
                return $this->response->success($data);
            }
        }
    }

    public function checkfeature()
    {
        $this->load->model(array(
            'assets_model'
        ));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('aid', 'Asset', 'required|numeric');
        $this->form_validation->set_rules('item', 'Item', 'required');
        if ($this->form_validation->run() == false) {
            $data = array(
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            return $this->respnose->error($data);
        } else {
            // check permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit/remove assets.'
                    )
                );
                return $this->response->error($data);
            }

            $asset = $this->assets_model->get($this->input->post('aid'));
            if (!$asset) {
                $data = array(
                    'errors' => array(
                        'no_permission' => 'You can edit your own assets only.'
                    )
                );
                return $this->response->error($data);
            } else {
                $item = $this->input->post('item');
                $can_be_deleted = true;
                switch ($item) {
                    case 'feature_maintenance':
                        if ($asset->feature_maintenance == 1) {
                            $count = $this->db->where('asset_id', $this->input->post('aid'))
                                ->count_all_results('maintenance');
                            if ($count > 0) {
                                $can_be_deleted = false;
                            }
                        }
                        break;
                    case 'feature_fuel':
                        if ($asset->feature_fuel == 1) {
                            $count = $this->db->where('asset_id', $this->input->post('aid'))
                                ->count_all_results('fuel');
                            if ($count > 0) {
                                $can_be_deleted = false;
                            }
                        }
                        break;
                    case 'feature_exception':
                        if ($asset->feature_exception == 1) {
                            $count = $this->db->where('asset_id', $this->input->post('aid'))
                                ->count_all_results('exceptions');
                            if ($count > 0) {
                                $can_be_deleted = false;
                            }
                        }
                        break;
                    case 'feature_location':
                        if ($asset->feature_location == 1) {
                            $count = $this->db->where('asset_id', $this->input->post('aid'))
                                ->count_all_results('locations_geo');
                            if ($count > 0) {
                                $can_be_deleted = false;
                            }
                        }
                        if ($asset->feature_location == 1) {
                            $count = $this->db->where('asset_id', $this->input->post('aid'))
                                ->count_all_results('locations_text');
                            if ($count > 0) {
                                $can_be_deleted = false;
                            }
                        }
                        break;
                }
                if ($can_be_deleted == true) {
                    $data = array(
                        'message' => 'Feature can be turned off',
                        'errors' => ''
                    );
                    return $this->response->success($data);
                } else {
                    $data = array(
                        'errors' => array(
                            'no_permission' => 'This feature has data and cannot be turned off.'
                        )
                    );
                    return $this->response->error($data);
                }
            }
        }
    }

    public function make_favourite()
    {
        $this->load->model(array(
            'assets_model'
        ));

        $asset_id = (int)($this->input->post('aid'));
        $user_id = $this->auth->id();
        $company_id = $this->auth->company_id();
        $this->assets_model->favourite($user_id, $asset_id, $company_id);

        $data = array(
            'message' => 'This asset is now your favourite.',
            'errors' => ''
        );
        return $this->response->success($data);
    }

    public function make_unfavourite()
    {
        $this->load->model(array(
            'assets_model'
        ));

        $asset_id = (int)($this->input->post('aid'));
        $user_id = $this->auth->id();
        $company_id = $this->auth->company_id();
        $this->assets_model->unfavourite($user_id, $asset_id, $company_id);

        $data = array(
            'message' => 'Successfully removed from favourites.',
            'errors' => ''
        );
        return $this->response->success($data);
    }

    public function get_favourite()
    {
        $this->load->model(array(
            'assets_model'
        ));

        $asset_id = (int)($this->input->post('aid'));
        $favourite = $this->assets_model->getFavourite($this->auth->id(), $asset_id, $this->auth->company_id());

        if ($favourite == true) {
            $data = array(
                'message' => 'Has Favourite',
                'errors' => ''
            );
            return $this->response->success($data);
        } else {
            $data = array(
                'errors' => array(
                    'no_permission' => 'No Favourite.'
                )
            );
            return $this->response->error($data);
        }
    }

    public function updatedocument()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit/remove assets.'
                )
            );
            return $this->response->error($data);
        }
        $pk = (int)$this->input->post('pk');
        $friendly_name = $this->input->post('value');
        $mysql_data = array(
            'friendly_name' => $friendly_name
        );
        $this->db->where('asset_document_id', $pk)
            ->update('asset_documents', $mysql_data);

        $data = array(
            'message' => 'Friendly name updated successfully.',
        );
        return $this->response->success($data);
    }

    public function getSingle()
    {
        $aid = (int)$this->input->post('asset_id');
        if($aid > 0){
            $asset = $this->db->where('company_id', $this->auth->company_id())
                ->where('asset_id', $aid)
                ->get('assets')
                ->row();
            if($asset){
                $data = array(
                    'result' => $asset
                );
                return $this->response->success($data);
            }
        }
        $data = array(
            'status' => 'fail',
            'result' => false
        );
        return $this->response->error($data);
    }

    public function getEnabledPortlets()
    {
        $asset_id = (int)$this->input->post('aid');

        $asset = $this->db->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->get('assets')
            ->row();

        $features_enabled = array(
            'maintenance' => false,
            'exception' => false,
            'fuel' => false,
            'location' => false
        );

        $features_hasdata = array(
            'maintenance' => false,
            'exception' => false,
            'fuel' => false,
            'location' => false
        );

        $features_add = array(
            'maintenance' => false,
            'exception' => false,
            'fuel' => false,
            'location' => false
        );

        $features_view = array(
            'maintenance' => false,
            'exception' => false,
            'fuel' => false,
            'location' => false
        );

        // enabled features override
        if ($asset) {
            if ($asset->feature_fuel == 1) {
                $features_enabled['fuel'] = true;
            }
            if ($asset->feature_maintenance == 1) {
                $features_enabled['maintenance'] = true;
            }
            if ($asset->feature_exception == 1) {
                $features_enabled['exception'] = true;
            }
            if ($asset->feature_location == 1) {
                $features_enabled['location'] = true;
            }
        }


        // has data override
        $fuel = $this->db->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->count_all_results('fuel');

        $maintenance = $this->db->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->count_all_results('maintenance');

        $exception = $this->db->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->count_all_results('exceptions');

        $location_text = $this->db->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->count_all_results('locations_text');

        $location_geo = $this->db->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->count_all_results('locations_geo');


        if ($fuel > 0) {
            $features_hasdata['fuel'] = true;
        }
        if ($maintenance > 0) {
            $features_hasdata['maintenance'] = true;
        }
        if ($exception > 0) {
            $features_hasdata['exception'] = true;
        }
        if ($location_geo > 0 || $location_text > 0) {
            $features_hasdata['location'] = true;
        }

        // add buttons override
        if (is_allowed(2, 'assets', '', 'fuel') || is_allowed(2, 'fuel')) {
            $features_add['fuel'] = true;
        }
        if (is_allowed(2, 'assets', '', 'maintenance') || is_allowed(2, 'maintenance')) {
            $features_add['maintenance'] = true;
        }
        if (is_allowed(2, 'assets', '', 'exception') || is_allowed(2, 'exceptions')) {
            $features_add['exception'] = true;
        }
        if (is_allowed(2, 'assets', '', 'location') || is_allowed(2, 'locations')) {
            $features_add['location'] = true;
        }

        // view access override
        if (is_allowed(1, 'fuel')) {
            $features_view['fuel'] = true;
        }
        if (is_allowed(1, 'maintenance')) {
            $features_view['maintenance'] = true;
        }
        if (is_allowed(1, 'exceptions')) {
            $features_view['exception'] = true;
        }
        if (is_allowed(1, 'locations')) {
            $features_view['location'] = true;
        }

        $data = array(
            'features_enabled' => $features_enabled,
            'features_hasdata' => $features_hasdata,
            'features_add' => $features_add,
            'features_view' => $features_view
        );

        return $this->response->success($data);
    }
}
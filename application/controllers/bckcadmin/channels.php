<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/24/2015
 * Time: 12:22 PM
 */
class Channels extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function asset($asset_id=0)
    {
        $asset_id = (int)$asset_id;
        $asset = $this->db//->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->get('assets')
            ->row();
        if ($asset) {
            $data = array(
                'asset' => $asset
            );
            $this->load->view('bckcadmin/channels.asset.php', $data);
        } else {
            show_error('This asset does not belongs to you.', '403', 'Not allowed');
            exit;
        }
    }
}
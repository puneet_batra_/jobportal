<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 12:27 PM
 */
class CMS extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function index()
    {
        $company = $this->db->where('company_id', $this->auth->company_id())->get('companies')->row();
        $data = array(
            'company' => $company
        );
        $this->load->view('bckcadmin/cms.php', $data);
    }

    public function update_dashboard()
    {
        $content = $this->input->post('html');
        $title = $this->input->post('title');

        // update
        $mysql_data = array(
            'cms_dashboard_notice_title' => $title,
            'cms_dashboard_notice' => $content
        );
        $this->db->where('company_id', $this->auth->company_id())
            ->update('companies', $mysql_data);

        $data = array(
            'status' => 'success',
            'message' => 'Dashboard content updated successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }
}
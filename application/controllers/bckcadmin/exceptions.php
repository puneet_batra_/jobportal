<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/31/2015
 * Time: 11:36 AM
 */
class Exceptions extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('bckcadmin/exceptions.php');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('comments', 'Comments', '');
        $this->form_validation->set_rules('status', 'Status', '');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2) && !is_allowed(2, 'assets', '', 'exception')) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add and comment exceptions.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            // validate file before uploading
            $valid_type = true;
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $ext = end(explode('.', $_FILES['files']['name']));
                if (in_array($ext, array('php', 'html', 'js', 'exe', 'deb'))) {
                    $valid_type = false;
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, This file type is not allowed for security reasons.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'company_id' => $this->auth->company_id(),
                'title' => $this->input->post('title'),
                'status' => 'new',
                'asset_id' => $this->input->post('asset_id'),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id(),
                'created_user_id' => $this->auth->id(),
                'closed_user_id' => $this->auth->id()
            );
            $this->db->insert('exceptions', $mysql_data);
            $exception_id = $this->db->insert_id();

            // upload the photos selected if submitted
            $random_name = '';
            $attachment_real_name = '';
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $attachment_real_name = $_FILES['files']['name'];
                $tmp_name = $_FILES['files']['tmp_name'];
                $ext = end(explode('.', $_FILES['files']['name']));
                $random_name = md5(microtime(true)) . '.' . $ext;
                $save_path = './uploads/'. $this->auth->company_id() .'/exceptions/attachments/';
                if (!is_dir($save_path)) {
                    mkdir($save_path, 0777, true);
                }
                $save_path .= $random_name;
                move_uploaded_file($tmp_name, $save_path);
            }
            // add to database
            $exception_comments = $this->input->post('comments');
            if ($exception_comments == '') {
                $exception_comments = $this->input->post('title');
            }
            $mysql_data = array(
                'comments' => $exception_comments,
                'attachment' => $random_name,
                'attachment_name' => $attachment_real_name,
                'exception_id' => $exception_id,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('exception_notes', $mysql_data);
            $exception_note_id = $this->db->insert_id();

            // Send email to subscribers
            $this->user_model->send_new_exception_note_email($exception_id, $exception_note_id);

            // subscribe for exception
            $mysql_data = array(
                'user_id' => $this->auth->id(),
                'company_id' => $this->auth->company_id(),
                'exception_id' => $exception_id,
                'created_on' => date('Y-m-d H:i:s')
            );
            $this->db->insert('exception_subscriptions', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Exception added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function getlist()
    {
        if (!$this->has_permission(1) && !is_allowed(1, 'assets', '', 'exception')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read exceptions.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->datatables
            ->select('exceptions.title, exceptions.exception_id, exceptions.status,
            unix_timestamp('. $this->timezone->convertSql('created_on') .') as created_on_formatted', false)
            ->where('asset_id', (int)$this->input->post('asset_id'))
            ->where('company_id', $this->auth->company_id());

        if (!$this->has_permission(2)) {
            $this->datatables->where('status <>', 'closed');
        }
        $this->datatables->from('exceptions');
        $this->datatables->add_column('title_formatted', '<a onclick="showException(this)" id="datatable-item-$1">$2</a>', 'exception_id, title');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function getcomments()
    {
        if (!$this->has_permission(1) && !is_allowed(1, 'assets', '', 'exception')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read exceptions.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $exception_id = (int)$this->input->post('rid');
        $default_pic = base_url('public/img/avatar.png');

        $sql = "SELECT
        en.*,
        if(en.created_by = {$this->auth->id()}, 'out', 'in') as class,
        if(u.profile_pic <> '', u.profile_pic, '{$default_pic}') as profile_pic,
        concat_ws(' ', u.first_name, u.last_name) as username,
        date_format({$this->timezone->convertSql('en.created_on')}, '%M %e, %Y at %l:%i %p') as created_on_formatted
        FROM exception_notes as en
        JOIN (
          select u.*, a.profile_pic from users as u
          left outer join authentications as a on u.user_id = a.user_id
          group by u.user_id
        ) as u on u.user_id = en.created_by
        WHERE en.exception_id IN (SELECT exception_id FROM exceptions WHERE company_id = {$this->auth->company_id()})
        AND en.exception_id = {$exception_id}
        ORDER BY en.created_on asc
        ";
        $comments = $this->db->query($sql)->result();
        $data = array(
            'comments' => $comments,
            'exception' => $this->db->where('exception_id', $exception_id)->get('exceptions')->row()
        );
        echo json_encode($data);
    }

    public function update()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to update exceptions.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('exception_id', 'Asset', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            $exception = $this->db->where('company_id', $this->auth->company_id())
                ->where('exception_id', $this->input->post('exception_id'))
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select an exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // status change conditions
            $status_codes = array(
                'new' => 0,
                'in_progress' => 1,
                'closed' => 2
            );
            // detect backward status, eg: in progress to new || closed to new || closed to in progress
            if ($status_codes[$this->input->post('status')] < $status_codes[$exception->status]) {
                // no body can move from any to new
                if ($this->input->post('status') == 'new') {
                    $data = array(
                        'status' => 'fail',
                        'message' => 'There were errors',
                        'errors' => array(
                            'no_permission' => 'You cannot change status to new again.'
                        )
                    );
                    echo json_encode($data);
                    return false;
                }

                // only company admin can do the closed to in progress
                if (!$this->has_permission(2)
                    && $this->input->post('status') == 'in_progress'
                    && $exception->status == 'closed'
                ) {
                    $data = array(
                        'status' => 'fail',
                        'message' => 'There were errors',
                        'errors' => array(
                            'no_permission' => 'You cannot change status to In progress once closed.'
                        )
                    );
                    echo json_encode($data);
                    return false;
                }
            }

            $mysql_data = array(
                'status' => $this->input->post('status'),
                'title' => $this->input->post('title')
            );
            $this->db->where('exception_id', $this->input->post('exception_id'))
                ->update('exceptions', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Exception updated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function do_comment()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('comments', 'Comments', 'required');
        $this->form_validation->set_rules('exception_id', 'Asset', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2) && !is_allowed(2, 'assets', '', 'exception')) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add and comment exceptions.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // validate file before uploading
            $valid_type = true;
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $ext = end(explode('.', $_FILES['files']['name']));
                if (in_array($ext, array('php', 'html', 'js', 'exe', 'deb'))) {
                    $valid_type = false;
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, This file type is not allowed for security reasons.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $exception = $this->db->where('exception_id', $this->input->post('exception_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            if (strtolower($exception->status) == 'closed' && !$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'This exception is closed.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $exception_id = $exception->exception_id;

            // upload the photos selected if submitted
            $random_name = '';
            $attachment_real_name = '';
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $attachment_real_name = $_FILES['files']['name'];
                $tmp_name = $_FILES['files']['tmp_name'];
                $ext = end(explode('.', $_FILES['files']['name']));
                $random_name = md5(microtime(true)) . '.' . $ext;
                $save_path = './uploads/' . $this->auth->company_id() . '/exceptions/attachments/';
                if (!is_dir($save_path)) {
                    mkdir($save_path, 0777, true);
                }
                $save_path .= $random_name;
                move_uploaded_file($tmp_name, $save_path);
            }
            // add to database
            $mysql_data = array(
                'comments' => $this->input->post('comments'),
                'attachment' => $random_name,
                'attachment_name' => $attachment_real_name,
                'exception_id' => $exception_id,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('exception_notes', $mysql_data);
            $exception_note_id = $this->db->insert_id();

            // Send email to subscribers
            $this->load->model('cron_model');
            $this->cron_model->createExceptionCommentEmail($exception_id, $exception_note_id, $this->auth->id());

            $data = array(
                'status' => 'success',
                'message' => 'Exception note created successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function remove_exception()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('exception_id', 'Exception', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->auth->is_companyadmin()) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to remove exceptions.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $exception = $this->db->where('exception_id', $this->input->post('exception_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $exception_id = $exception->exception_id;

            $exception_notes_with_attachments = $this->db->where('exception_id', $exception_id)
                ->where('attachment <>', '')
                ->get('exception_notes')
                ->result();

            $this->db->where('exception_id', $exception_id)->delete('exception_notes');
            $this->db->where('exception_id', $exception_id)->delete('exceptions');

            foreach ($exception_notes_with_attachments as $attachment) {
                if ($attachment) {
                    $filename = $attachment->attachment_name;
                    $path = './uploads/' . $this->auth->company_id() . '/exceptions/attachments/' . $filename;
                    if (file_exists($path)) {
                        @unlink($path);
                    }
                }
            }


            $data = array(
                'status' => 'success',
                'message' => 'Exception removed successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    function check_subscription()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('exception_id', 'Exception', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {

            $exception = $this->db->where('exception_id', $this->input->post('exception_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $exception_id = $exception->exception_id;

            $subscribed = 0;
            $exception_subscribed = $this->db->where('user_id', $this->auth->id())
                ->where('company_id', $this->auth->company_id())
                ->where('exception_id', $exception_id)
                ->get('exception_subscriptions')
                ->row();
            if ($exception_subscribed) {
                $subscribed = 1;
            }

            $data = array(
                'status' => 'success',
                'message' => 'Exception subscription determined successfully.',
                'errors' => '',
                'subscribed' => $subscribed
            );
            echo json_encode($data);
        }
    }

    function subscribe()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('exception_id', 'Exception', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {

            $exception = $this->db->where('exception_id', $this->input->post('exception_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $exception_id = $exception->exception_id;

            $subscribed = 0;
            $exception_subscribed = $this->db->where('user_id', $this->auth->id())
                ->where('company_id', $this->auth->company_id())
                ->where('exception_id', $exception_id)
                ->get('exception_subscriptions')
                ->row();
            if ($exception_subscribed) {
                $subscribed = 1;
            } else {
                $mysql_data = array(
                    'user_id' => $this->auth->id(),
                    'company_id' => $this->auth->company_id(),
                    'exception_id' => $exception_id,
                    'created_on' => date('Y-m-d H:i:s')
                );
                $this->db->insert('exception_subscriptions', $mysql_data);
                $subscribed = 1;
            }

            $data = array(
                'status' => 'success',
                'message' => 'Exception subscribed successfully.',
                'errors' => '',
                'subscribed' => $subscribed
            );
            echo json_encode($data);
        }
    }

    function unsubscribe()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('exception_id', 'Exception', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {

            $exception = $this->db->where('exception_id', $this->input->post('exception_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('exceptions')
                ->row();
            if (!$exception) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Please select exception first.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            $exception_id = $exception->exception_id;

            $subscribed = 0;
            $exception_subscribed = $this->db->where('user_id', $this->auth->id())
                ->where('company_id', $this->auth->company_id())
                ->where('exception_id', $exception_id)
                ->get('exception_subscriptions')
                ->row();
            if ($exception_subscribed) {
                $this->db->where('exception_subscription_id', $exception_subscribed->exception_subscription_id)
                    ->delete('exception_subscriptions');
                $subscribed = 0;
            } else {
                $subscribed = 0;
            }

            $data = array(
                'status' => 'success',
                'message' => 'Exception unsubscribed successfully.',
                'errors' => '',
                'subscribed' => $subscribed
            );
            echo json_encode($data);
        }
    }
}

;
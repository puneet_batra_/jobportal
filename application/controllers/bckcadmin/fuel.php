<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/3/2015
 * Time: 12:20 PM
 */
class Fuel extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('edit', 'getchartdata'));
    }

    public function index()
    {
        $this->load->view('bckcadmin/fuel');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('odometer', 'Odo Meter', 'required|numeric|max_length[11]|callback_validate_odometer');
        $this->form_validation->set_rules('fueled_date', 'Fueled Date', 'required');
        $this->form_validation->set_rules('towing', 'Towing', 'required|numeric');
        $this->form_validation->set_rules('volume', 'Litres', 'required|max_length[11]|numeric');
        //$this->form_validation->set_rules('pre_purchased', 'Pre Purchased', 'required|numeric');
        $this->form_validation->set_rules('cost', 'Cost', 'required|numeric');
        $this->form_validation->set_rules('supplier', 'Supplier', '');
        $this->form_validation->set_rules('has_exception', 'Has Exception', '');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            if($this->input->post('has_exception') == 1 && $this->input->post('exception_title') == '')
            {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'invalid' => 'Exception title required'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // check for permissions
            if (!$this->has_permission(2) && !is_allowed(2, 'assets', '', 'fuel')) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add fuel.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            // validate photos before uploading
            $valid_type = true;
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $ext = end(explode('.', $_FILES['files']['name']));
                if (!in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'))) {
                    $valid_type = false;
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only pictures allowed'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $pre_purchased = 0;
            if($this->input->post('cost') == 0 || $this->input->post('cost') == '') {
                $pre_purchased = 1;
            }
            // insert data
            $fueled_date = $this->timezone->convertDate($this->input->post('fueled_date'), 'Y-m-d H:i:s', 'user-to-server');
            $mysql_data = array(
                'odometer' => $this->input->post('odometer'),
                'supplier_id' => $this->input->post('supplier'),
                'fueled_date' => $fueled_date,
                'towing' => $this->input->post('towing'),
                'volume' => $this->input->post('volume'),
                'pre_purchased' => $pre_purchased,
                'cost' => $this->input->post('cost'),
                'asset_id' => $this->input->post('asset_id'),
                'company_id' => $this->auth->company_id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('fuel', $mysql_data);
            $fuel_id = $this->db->insert_id();

            $asset = $this->db->where('asset_id', $this->input->post('asset_id'))->get('assets')->row();
            $fuel_data = array(
                'Odometer' => $mysql_data['odometer'],
                'Fueled Date' => date('M d, Y', strtotime($this->input->post('fueled_date'))),
                'Volume' => $mysql_data['volume'],
                'Cost' => $mysql_data['cost'],
                'Asset' => '<a target="_blank" href="'.site_url('bckcadmin/assets/view/' . $mysql_data['asset_id']).'">'. $asset->friendly_name .'</a>'
            );

            $this->load->model('files_model');
            $save_path = './uploads/' . $this->auth->company_id() . '/fuel/photos/';
            $uploaded_files = $this->files_model->save('files', $save_path);
            if(count($uploaded_files) > 0) {
                // add to database
                $mysql_data = array(
                    'photo' => $uploaded_files[0]['uploaded_name'],
                );
                $this->db->where('fuel_id', $fuel_id);
                $this->db->update('fuel', $mysql_data);
            }

            $this->load->model('maintenance_model');
            $this->load->model('cron_model');
            $maintenance = $this->db
                ->select('*, max('. $this->maintenance_model->get_column(2) .') as odo_max', false)
                ->where('maintenance_type' , 2)
                ->where('asset_id', $this->input->post('asset_id'))
                ->group_by($this->maintenance_model->get_column(2))
                ->get('maintenance')
                ->row();

            $create_exception = false;
            if($maintenance) {
                if ($maintenance->exception_created == 0 && $this->input->post('odometer') > $maintenance->odo_max) {
                    $create_exception = true;
                }
            }

            // create exception if has_exception selected
            if ($this->input->post('has_exception') == 1 || $create_exception) {
                $defaults = array(
                    'title' => $this->input->post('exception_title')//'Exception for Fuel Entry #'.$fuel_id
                );
                if($defaults['title'] == '' && $maintenance) {
                    $defaults['title'] = 'Exception for Maintenance entry #' . $maintenance->maintenance_id;
                }
                // insert data
                $mysql_data = array(
                    'company_id' => $this->auth->company_id(),
                    'title' => $defaults['title'],
                    'status' => 'new',
                    'asset_id' => $this->input->post('asset_id'),
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $this->auth->id(),
                    'updated_by' => $this->auth->id(),
                    'created_user_id' => $this->auth->id(),
                    'closed_user_id' => $this->auth->id()
                );
                $this->db->insert('exceptions', $mysql_data);
                $exception_id = $this->db->insert_id();

                $comments = '';
                foreach($fuel_data as $key => $value)
                {
                    $comments .= $key . ' : ' . $value . ' <br>'."\r\n";
                }
                // add to database
                $mysql_data = array(
                    'comments' => $comments,
                    'exception_id' => $exception_id,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $this->auth->id(),
                    'updated_by' => $this->auth->id()
                );
                $this->db->insert('exception_notes', $mysql_data);
                $exception_note_id = $this->db->insert_id();

                $this->cron_model->createExceptionCommentEmail($exception_id, $exception_note_id, $this->auth->id());
            }


            $data = array(
                'status' => 'success',
                'message' => 'Fuel entry added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function validate_odometer()
    {
        $odometer = (int)$this->input->post('odometer');
        $asset_id = (int)$this->input->post('asset_id');
        $company_id = (int)$this->auth->company_id();

        $last_entry = $this->db->where('company_id', $company_id)
            ->where('asset_id', $asset_id)
            ->order_by('odometer', 'desc')
            ->limit(1)
            ->get('fuel')
            ->row();
        if ($last_entry) {
            if ($odometer > $last_entry->odometer) {
                return true;
            } else {
                $this->form_validation->set_message('validate_odometer', 'Odometer value cannot be less than ' . $last_entry->odometer);
                return true;
            }
        } else {
            // no previous entry exists, no need to validate
            return true;
        }
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('odometer', 'Odo Meter', 'required|numeric|max_length[11]|callback_validate_odometer_edit');
        $this->form_validation->set_rules('fueled_date', 'Fueled Date', 'required');
        $this->form_validation->set_rules('towing', 'Towing', 'required|numeric');
        $this->form_validation->set_rules('volume', 'Litres', 'required|max_length[11]|numeric');
        $this->form_validation->set_rules('pre_purchased', 'Pre Purchased', 'required|numeric');
        $this->form_validation->set_rules('cost', 'Cost', 'required|numeric');
        $this->form_validation->set_rules('supplier', 'Supplier', '');
        $this->form_validation->set_rules('fuel_id', 'Fuel', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit fuel.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            // validate photos before uploading
            $valid_type = true;
            if (isset($_FILES['files']) && $_FILES['files']['name'] != '') {
                $ext = end(explode('.', $_FILES['files']['name']));
                if (!in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'JPG','JPEG', 'PNG', 'GIF'))) {
                    $valid_type = false;
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, only pictures allowed'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $fuel_id = $this->input->post('fuel_id');
            $fuel_entry = $this->db->where('fuel_id', $fuel_id)->get('fuel')->row();
            // insert data
            $mysql_data = array(
                'odometer' => $this->input->post('odometer'),
                'supplier_id' => $this->input->post('supplier'),
                'fueled_date' => date('Y-m-d', strtotime($this->input->post('fueled_date'))) . ' ' . date('H:i:s', strtotime($fuel_entry->fueled_date)),
                'towing' => $this->input->post('towing'),
                'volume' => $this->input->post('volume'),
                'pre_purchased' => $this->input->post('pre_purchased'),
                'cost' => $this->input->post('cost'),
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $this->db->where('fuel_id', $this->input->post('fuel_id'));
            $this->db->update('fuel', $mysql_data);


            // upload the photos selected if submitted
            $this->load->model('files_model');
            $save_path = './uploads/' . $this->auth->company_id() . '/fuel/photos/';
            $uploaded_files = $this->files_model->save('files', $save_path);
            if(count($uploaded_files) > 0) {
                // add to database
                $mysql_data = array(
                    'photo' => $uploaded_files[0]['uploaded_name'],
                );
                $this->db->where('fuel_id', $fuel_id);
                $this->db->update('fuel', $mysql_data);
            }

            $data = array(
                'status' => 'success',
                'message' => 'Fuel entry updated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read fuel.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $asset_id = (int)$this->input->post('asset_id');
        $this->load->library('datatables');
        $this->datatables->select('
            fuel.*,
            fuel.odometer,
            fuel.volume,
            fuel.photo,
            fuel.fuel_id as data_fuel_id,
            unix_timestamp('. $this->timezone->convertSql('fuel.fueled_date') .') as fueled_date_formatted', false);
        $this->datatables->where('asset_id', $asset_id);
        $this->datatables->where('company_id', $this->auth->company_id());
        $this->datatables->from('fuel');
        if ($this->has_permission(2)) {
            $actions = ' <a onclick="edit_fuel(this)"><i class="fa fa-edit" id="datatable-item-$1"></i> Edit</a> |
                    <a onclick="delete_record(this)"><i class="fa fa-trash" id="datatable-item-$1"></i> Delete</a>';
        } else {
            $actions = '<span class="label label-sm label-danger">Not Authorized</span>';
        }
        $this->datatables->add_column('actions', $actions, 'data_fuel_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function delete()
    {
        if (!$this->has_permission(2) && !is_allowed(1, 'assets', '', 'fuel')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to delete fuel.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $fuel_id = (int)$this->input->post('rid');
        $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('fuel');

        $fuel = $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $this->auth->company_id())
            ->get('fuel')
            ->row();

        if ($fuel) {
            $filename = $fuel->photo;
            $path = './uploads/' . $this->auth->company_id() . '/fuel/photos/' . $filename;
            if (file_exists($path)) {
                @unlink($path);
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'Fuel entry removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function edit($id)
    {
        $fuel_id = (int)$id;
        $fuel = $this->db->where('fuel_id', $fuel_id)->where('company_id', $this->auth->company_id())
            ->get('fuel')
            ->row();
        if (!$fuel) {
            show_error('Fuel does not exists', '404', 'Not Found');
        } else {
            if ($this->has_permission(2)) {
                $user = $this->db->where('user_id', $fuel->created_by)->get('users')->row();
                $asset = $this->db->where('asset_id', $fuel->asset_id)
                    ->get('assets')->row();
                $fuel->user = $user;
                $fuel->asset = $asset;

                $suppliers = $this->db->where('company_id', $this->auth->company_id())->get('suppliers')->result();

                $data = array(
                    'fuel' => $fuel,
                    'suppliers' => $suppliers
                );
                $this->load->view('bckcadmin/fuel.edit.php', $data);
            } else {
                show_error('You are not authorized to edit fuel entries.', '403', 'No permission');
            }
        }
    }

    public function deletephoto()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit fuel.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $fuel_id = (int)$this->input->post('fid');

        $fuel = $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $this->auth->company_id())
            ->get('fuel')
            ->row();

        $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $this->auth->company_id())
            ->update('fuel', array('photo' => ''));

        //
        if ($fuel) {
            $filename = $fuel->photo;
            $path = './uploads/' . $this->auth->company_id() . '/fuel/photos/' . $filename;
            if (file_exists($path)) {
                @unlink($path);
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'Photo removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getchartdata()
    {
        /* Get chart data in this format
					{date: "2013-03-12", lphk: 10.044145873321}
					{date: "2013-03-18", lphk: 10.603206412826}
					{date: "2013-03-22", lphk: 10.472837022133}
					{date: "2013-03-26", lphk: 10.591093117409}
         * */

        $asset_id = (int)$this->input->post('asset_id');
        $fuel_data = array();
        $fuel_items = $this->db
            ->select('date_format(' . $this->timezone->convertSql('fueled_date') . ', "%Y-%m-%d") as fueled_on_date, odometer as odometer, volume as volume', false)
            ->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->order_by('odometer', 'asc')
            ->get('fuel')
            ->result();

        if (count($fuel_items) > 0) {

          // Andrew Frahn
          $lastOdo = (double)0;
          $count = (int)0;
          $count2 = (int)0;
          $fuel_average = (float)0.0;
          $rejected = (int)0;
          $total = (int)0;
          $indexed = array();
          foreach($fuel_items as $fuel_item) {
          	$deltaOdo = $fuel_item->odometer - $lastOdo;
          	$lastOdo = $fuel_item->odometer;
          	if($deltaOdo != 0){
          		
	          	$lphk = $fuel_item->volume / ($deltaOdo / 100);
	          	if($lphk > 5.0 && $lphk < 30.0){
		          	$fuel_average += $lphk;
		          	$formatted_date = date('Y-m-d', strtotime($fuel_item->fueled_on_date));
		          	$fuel_data[$count] = array(
		          		'date' => $formatted_date,
		          		'lphk' => $lphk
		          		);
		          	$count++;
          		}
          		$total++;

            }
            
            	
          	
          }
          $fuel_average = $fuel_average / $count;
		  while($count2<$count) {
	      	if($fuel_data[$count2]["lphk"] > $fuel_average * 1.3 || $fuel_data[$count2]["lphk"] < $fuel_average * 0.7){
			    unset($fuel_data[$count2]);
			    $rejected++;
			}
			$count2++;
		  }
		  $fuel_data = array_values($fuel_data);
          
        }

        // last fueled info
        $last_fueled = $this->db
            ->select('fuel.*,
            concat_ws(" ", users.first_name, users.last_name) as username,
            date_format('. $this->timezone->convertSql('fuel.fueled_date') .', "%M %e, %Y") as fueled_on_date,
            date_format('. $this->timezone->convertSql('fuel.fueled_date') .', "%h:%i %p") as fueled_on_time
            ', false)
            ->join('users', 'users.user_id = fuel.created_by')
            ->where('company_id', $this->auth->company_id())
            ->where('asset_id', $asset_id)
            ->order_by('fueled_date', 'desc')
            ->limit(1)
            ->get('fuel')
            ->row();


        $data = array(
            'records' => $fuel_data,
            'last_fueled' => $last_fueled,
            'rejected' => $rejected,
            'total' => $total
        );
        echo json_encode($data);
    }

    public function updateSingle()
    {
        header('Content-Type: application/json');
        // check permissions
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'You do not have permission to edit fuel.',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit fuel.'
                )
            );
            echo json_encode($data);
            return false;
        }
        // edit your own assets only
        $asset = $this->db->where('fuel_id', (int)$this->input->post('pk'))
            ->where('company_id', $this->auth->company_id())
            ->get('fuel')->row();
        if (!$asset) {
            $data = array(
                'status' => 'fail',
                'message' => 'This asset does not belong to you.',
                'errors' => array(
                    'no_permission' => 'This fuel entry does not belong to you.'
                )
            );
            echo json_encode($data);
            return false;
        }

        // apply rules
        $rules = array(
            'odometer' => 'required|numeric|callback_validate_odometer_inline',
            'volume' => 'required|numeric',
        );
        // define user friendly names
        $names = array(
            'odometer' => 'Odometer',
            'volume' => 'Volume',
        );

        $key = $this->input->post('name'); // name of field
        $pk = (int)$this->input->post('pk'); // primary key of record

        // if field not found in given set of rules
        if (!isset($names[$key])) {
            $data = array(
                'status' => 'fail',
                'errors' => 'Field editable not supported.',
                'message' => 'There were errors'
            );
            echo json_encode($data);
            return false;
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('value', $names[$key], $rules[$key]);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'errors' => $this->form_validation->get_message('value'),
                'message' => 'There were errors'
            );
            echo json_encode($data);
            return false;
        } else {
            // update the record
            $mysql_data = array(
                $key => trim(str_replace('&nbsp;', ' ', $this->input->post('value')))
            );
            $this->db->where('fuel_id', $pk)->update('fuel', $mysql_data);


            $data = array(
                'status' => 'success',
                'errors' => '',
                'message' => $names[$key] . ' updated successfully.'
            );
            echo json_encode($data);
            return true;
        }
    }

    public function validate_odometer_inline()
    {
        $odometer = (int)$this->input->post('value');
        $fuel_id = (int)$this->input->post('pk');
        $company_id = (int)$this->auth->company_id();

        $fuel = $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $company_id)
            ->get('fuel')
            ->row();

        if ($fuel) {
            $last_entry = $this->db->where('company_id', $company_id)
                ->where('asset_id', $fuel->asset_id)
                ->where('fueled_date <', $fuel->fueled_date)
                ->order_by('fueled_date', 'desc')
                ->limit(1)
                ->get('fuel')
                ->row();
            $next_entry = $this->db->where('company_id', $company_id)
                ->where('asset_id', $fuel->asset_id)
                ->where('fueled_date >', $fuel->fueled_date)
                ->order_by('fueled_date', 'asc')
                ->limit(1)
                ->get('fuel')
                ->row();
            /* if ($last_entry || $next_entry) {
                if ($last_entry && $next_entry) {
                    // last and next both entries exists
                    if ($odometer > $last_entry->odometer && $odometer < $next_entry->odometer) {
                        return true;
                    } else {
                        $message = 'Odometer value should be between ' . $last_entry->odometer . ' and '. $next_entry->odometer;
                        $this->form_validation->set_message('validate_odometer_inline', $message);
                    //    return false;
                        // This is generating false alerts.  There is also times that both Date and Odometer need to be changed at the same time
                        return true;
                    }
                } else if ($last_entry && !$next_entry) {
                    // only last entry exists
                    if ($odometer > $last_entry->odometer) {
                        return true;
                    } else {
                        $message = 'Odometer value should be greater than ' . $last_entry->odometer;
                        $this->form_validation->set_message('validate_odometer_inline', $message);
                        return false;
                    }
                } else if (!$last_entry && $next_entry) {
                    // only last entry exists
                    if ($odometer < $next_entry->odometer) {
                        return true;
                    } else {
                        $message = 'Odometer value should be less than ' . $next_entry->odometer;
                        $this->form_validation->set_message('validate_odometer_inline', $message);
                        return false;
                    }
                }
            } else {
                // no previous, next entry exists, no need to validate
                */
                return true;
            //}
        } else {
            $this->form_validation->set_message('validate_odometer_inline', 'Fuel entry doesnt exists.');
            return false;
        }


    }

    public function validate_odometer_edit()
    {
        $odometer = (int)$this->input->post('odometer');
        $fuel_id = (int)$this->input->post('fuel_id');
        $company_id = (int)$this->auth->company_id();

        $fuel = $this->db->where('fuel_id', $fuel_id)
            ->where('company_id', $company_id)
            ->get('fuel')
            ->row();

        if ($fuel) {
            $last_entry = $this->db->where('company_id', $company_id)
                ->where('asset_id', $fuel->asset_id)
                ->where('fueled_date <', $fuel->fueled_date)
                ->order_by('fueled_date', 'desc')
                ->limit(1)
                ->get('fuel')
                ->row();
            $next_entry = $this->db->where('company_id', $company_id)
                ->where('asset_id', $fuel->asset_id)
                ->where('fueled_date >', $fuel->fueled_date)
                ->order_by('fueled_date', 'asc')
                ->limit(1)
                ->get('fuel')
                ->row();
                /*
            if ($last_entry || $next_entry) {
                if ($last_entry && $next_entry) {
                    // last and next both entries exists
                    if ($odometer > $last_entry->odometer && $odometer < $next_entry->odometer) {
                        return true;
                    } else {
                        //$message = 'Odometer value should be between ' . $last_entry->odometer . ' and '. $next_entry->odometer;
                        //$this->form_validation->set_message('validate_odometer_edit', $message);
                        //return false;
			return true;
                    }
                } else if ($last_entry && !$next_entry) {
                    // only last entry exists
                    if ($odometer > $last_entry->odometer) {
                        return true;
                    } else {
                        $message = 'Odometer value should be greater than ' . $last_entry->odometer;
                        $this->form_validation->set_message('validate_odometer_edit', $message);
                        return false;
                    }
                } else if (!$last_entry && $next_entry) {
                    // only last entry exists
                    if ($odometer < $next_entry->odometer) {
                        return true;
                    } else {
                        $message = 'Odometer value should be less than ' . $next_entry->odometer;
                        $this->form_validation->set_message('validate_odometer_edit', $message);
                        return false;
                    }
                }
            } else {
            */
                // no previous, next entry exists, no need to validate
                return true;
            //}
        } else {
            $this->form_validation->set_message('validate_odometer_edit', 'Fuel entry doesnt exists.');
            return false;
        }


    }
}

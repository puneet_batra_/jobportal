<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct()
	{
		parent::__construct(false, array('*'));
	}

	public function welcome()
	{
		$this->load->helper('options_helper');
		$this->load->view('bckcadmin/welcome');
	}

	public function index()
	{
		if ($this->auth->company_id() == false) {
			redirect('bckcadmin/home/welcome');
		}

		$this->load->model(array(
			'assets_model'
		));

		$favourites = $this->company_model->getMyFavourites();
		$assets = $this->assets_model->getCount();
		$company = $this->auth->company();
		$users = $this->company_model->getCompanyUsersCount('accepted');

		$data = array(
			'favourites' => $favourites,
			'company' => $company,
			'count_users' => $users,
			'count_assets' => $assets,
		);
		$this->load->view('bckcadmin/home', $data);
	}

	public function getsize()
	{
		$this->load->model('files_model');
		$directory = realpath('./uploads/' . $this->auth->company_id(). '/');
		$data = $this->files_model->getSize($directory);

		$this->output->set_content_type('application/json');
		echo json_encode($data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/26/2015
 * Time: 6:37 PM
 */
class Locations extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('bckcadmin/locations');
    }

    public function add()
    {
        /*
         * Data received as
         * Array
            (
                [type] => geolocation
                [latlng] => 30.852234099999997,75.8591939
                [geolocation] => Shahid Jasdev Singh Nagar, Ludhiana, Punjab, India
                [status] => taken
                [manual] => sdgdfgdfgsdf gsdfg dsfg fdsg dfsg dsfgfd
                [asset_id] => 2
            )
         * */
        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric|callback_validate_location_form');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            if (!$this->has_permission(2, 'index') && !is_allowed(2, 'assets', '', 'location')) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_asset' => 'You do not have permission to add locations'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $asset = $this->db->where('asset_id', $this->input->post('asset_id'))
                ->where('company_id', $this->auth->company_id())
                ->get('assets')->row();
            if (!$asset) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_asset' => 'Asset not found'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // type is geolocation
            if ($this->input->post('type') == 'geolocation') {
                // location coords were automatically calculated, store them
                list($lat, $lng) = explode(',', $this->input->post('latlng'));
                $location = $this->input->post('geolocation');
                $mysql_data = array(
                    'location_geo' => $location,
                    'latitude' => $lat,
                    'longitude' => $lng,
                    'asset_id' => $asset->asset_id,
                    'company_id' => $this->auth->company_id(),
                    'hdop' => 0,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $this->auth->id(),
                    'updated_by' => $this->auth->id()
                );
                $this->db->insert('locations_geo', $mysql_data);
            }
            if ($this->input->post('type') == 'manual') {
                if ($this->input->post('status') == 'taken') {
                    // store user name info only.
                    $mysql_data = array(
                        'location_text' => '',
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'asset_id' => $asset->asset_id,
                        'company_id' => $this->auth->company_id(),
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id()
                    );
                    $this->db->insert('locations_text', $mysql_data);
                } else if ($this->input->post('status') == 'left') {
                    // store manually given location
                    $manual = $this->input->post('manual');
                    $mysql_data = array(
                        'location_text' => $manual,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'asset_id' => $asset->asset_id,
                        'company_id' => $this->auth->company_id(),
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id()
                    );
                    $this->db->insert('locations_text', $mysql_data);
                }
            }

            $data = array(
                'status' => 'success',
                'message' => 'Location record added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function validate_location_form($asset_id)
    {
        $asset_id = (int)$asset_id;
        $type = $this->input->post('type');
        $latlng = $this->input->post('latlng');
        $geolocation = $this->input->post('geolocation');
        $status = $this->input->post('status');
        $manual = $this->input->post('manual');

        $asset = $this->db->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->get('assets')->row();
        if (!$asset) {
            $this->form_valiation->set_message('validate_location_form', 'Asset not available to update.');
            return false;
        }

        if ($type == 'geolocation' && ($geolocation == '' || count(explode(',', $latlng)) != 2)) {
            $this->form_validation->set_message('validate_location_form', 'You must auto select your location.');
            return false;
        }
        if ($type == 'manual' && $status == '') {
            $this->form_validation->set_message('validate_location_form', 'You must select status.');
            return false;
        }
        if ($type == 'manual' && $status == 'left' && $manual == '') {
            $this->form_validation->set_message('validate_location_form', 'You must enter a location.');
            return false;
        }
        return true;
    }

    public function get_geo_records()
    {
        if (!$this->has_permission(1, 'index') && !is_allowed(1, 'assets', '', 'location')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to read locations'
                )
            );
            echo json_encode($data);
            return false;
        }
        $asset_id = (int)$this->input->post('asset_id');
        $this->datatables->select('locations_geo.*,
        locations_geo.latitude,
        locations_geo.longitude,
        locations_geo.hdop,
        locations_geo.location_geo_id,
        date_format(locations_geo.created_on, "%M %e, %Y") as created_on_formatted,
        unix_timestamp( ' . $this->timezone->convertSql('locations_geo.created_on') .') as created_on_timestamp,
        concat_ws(" ", users.first_name, users.last_name) as user_formatted', false)
            ->join('users', 'locations_geo.created_by = users.user_id')
            ->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->from('locations_geo');
        $actions = '<a onclick="delete_geo_record(this)"><i class="fa fa-trash" style="font-size: 16px;" id="datatable-item-$1"></i></a>';
        $this->datatables->add_column('actions', $actions, 'location_geo_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function get_txt_records()
    {
        if (!$this->has_permission(1, 'index') && !is_allowed(1, 'assets', '', 'location')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to read locations'
                )
            );
            echo json_encode($data);
            return false;
        }
        $asset_id = (int)$this->input->post('asset_id');
        $this->datatables->select('locations_text.*,
        locations_text.location_text_id,
        locations_text.location_text,
        date_format(locations_text.created_on, "%M %e, %Y") as created_on_formatted,
        unix_timestamp( ' . $this->timezone->convertSql('locations_text.created_on') .') as created_on_timestamp,
        if(locations_text.location_text <> "", "Left", "Taken") as type,
        concat_ws(" ", users.first_name, users.last_name) as user_formatted', false)
            ->join('users', 'locations_text.created_by = users.user_id')
            ->where('asset_id', $asset_id)
            ->where('company_id', $this->auth->company_id())
            ->from('locations_text');
        $actions = '<a onclick="delete_txt_record(this)"><i class="fa fa-trash" style="font-size: 16px;" id="datatable-item-$1"></i></a>';
        $this->datatables->add_column('actions', $actions, 'location_text_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function deletegeo()
    {
        $location_id = (int)$this->input->post('rid');
        if (!$this->has_permission(2, 'index')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to edit/del locations'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->db->where('location_geo_id', $location_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('locations_geo');

        $data = array(
            'status' => 'success',
            'message' => 'Location record removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function deletetxt()
    {
        $location_id = (int)$this->input->post('rid');
        if (!$this->has_permission(2, 'index')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to edit/del locations'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->db->where('location_text_id', $location_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('locations_text');

        $data = array(
            'status' => 'success',
            'message' => 'Location record removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function getlastlocation()
    {
        $asset_id = (int)$this->input->post('asset_id');
        $geo = $this->db
            ->select('locations_geo.*,
            concat_ws(" ", users.first_name, users.last_name) as username,
            date_format( ' . $this->timezone->convertSql('locations_geo.created_on') .', "%M %e, %Y") as created_on_date,
            date_format( ' . $this->timezone->convertSql('locations_geo.created_on') .', "%l:%i %p") as created_on_time,
            date_format( ' . $this->timezone->convertSql('locations_geo.updated_on') .', "%M %e, %Y") as updated_on_date,
            date_format( ' . $this->timezone->convertSql('locations_geo.updated_on') .', "%l:%i %p") as updated_on_time',false)
            ->join('users', 'locations_geo.created_by = users.user_id')
            ->where('asset_id', $asset_id)
            ->order_by('created_on', 'desc')
            ->limit(20)
            ->get('locations_geo')->result();
        $text = $this->db
            ->select('locations_text.*,
            concat_ws(" ", users.first_name, users.last_name) as username,
            date_format( ' . $this->timezone->convertSql('locations_text.created_on') .', "%M %e, %Y") as created_on_date,
            date_format( ' . $this->timezone->convertSql('locations_text.created_on') .', "%l:%i %p") as created_on_time,
            if(locations_text.location_text <> "", "Left", "Taken") as type
            ',false)
            ->join('users', 'locations_text.created_by = users.user_id', false)
            ->where('asset_id', $asset_id)
            ->order_by('created_on', 'desc')
            ->limit(20)
            ->get('locations_text')->result();

        $data = array(
            'geo' => $geo,
            'text' => $text
        );
        echo json_encode($data);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/24/2015
 * Time: 3:35 PM
 */
class Maintenance extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('edit','get'));
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('bckcadmin/maintenance.php');
    }

    /**
     * Get maintenance records
     * @return json
     */
    public function get()
    {
        if (!$this->has_permission(1) && !is_allowed(1, 'assets', '', 'maintenance')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to Maintenance.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $asset_id = (int)$this->input->post('asset_id');
        $this->db->query('(SELECT @curRow:=0)');
        $select = '
        @curRow := @curRow + 1 AS row_number,
        maintenance.*,
        maintenance.maintenance_type,
        maintenance.maintenance_id as m_id,
        date_format('. $this->timezone->convertSql('maintenance.logged_datetime') .',"%D %b %Y %h:%i %p") as logged_datetime_formatted,
        unix_timestamp('. $this->timezone->convertSql('maintenance.logged_datetime') .') as logged_datetime_unix,
        date_format('. $this->timezone->convertSql('maintenance.due_date') .', "%D %b %Y %h:%i %p") as due_date_formatted ,
        concat_ws(" ",users.first_name, users.last_name) as entered_by
        ';
        $this->datatables->select($select, false);
        $this->datatables->where('company_id', $this->auth->company_id());
        $this->datatables->where('asset_id', $asset_id);
        $this->datatables->join('users', 'users.user_id = maintenance.created_by');
        $this->datatables->from('maintenance');
        $actions = "
            <a href='#' onclick='edit(this)'><i class='fa fa-edit' id='datatable-item-$1'></i> Edit</a> |
            <a href='#' onclick='remove_record(this)'><i class='fa fa-trash' id='datatable-item-$1'></i> Delete</a>
        ";
        $this->datatables->add_column('actions', $actions, 'm_id');
        $data = json_decode($this->datatables->generate('json', 'utf-8'), true);
        foreach ($data['data'] as $key => $row) {
            $column = $this->maintenance_model->get_column($row['maintenance_type']);
            if (strtotime($row[$column]) > 0) {
                $value = $this->timezone->convertDate($row[$column], 'd M Y, g:i a');
            } else {
                $value = $row[$column];
            }
            $data['data'][$key]['value'] = $value;
        }
        echo json_encode($data);
    }

    /**
     * Ajax add new maintenance request
     * @return bool
     */
    public function add()
    {
        if (!$this->has_permission(2) && !is_allowed(2, 'assets', '', 'maintenance')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to add Maintenance.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('comments', 'Comments', '');
        $this->form_validation->set_rules('notes_next_service', 'Notes for next service', '');
        $this->form_validation->set_rules('maintenance_type', 'Maintenance Type', 'required|callback_add_due_fuel_entry');
        $this->form_validation->set_rules('odo_hour', 'Odo Hour', 'numeric');
        $this->form_validation->set_rules('asset_id', 'Asset', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // validate photos before uploading
            $valid_type = true;
            if (isset($_FILES['files'])) {
                for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
                    $ext = end(explode('.', $_FILES['files']['name'][$i]));
                    if (in_array($ext, array('php', 'html', 'js', 'exe', 'deb'))) {
                        $valid_type = false;
                    }
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, these files are not allowed for security reasons.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $mysql_data = array(
                'comments' => $this->input->post('comments'),
                'notes_next_service' => $this->input->post('notes_next_service'),
                'logged_datetime' => date('Y-m-d H:i:s'),
                'maintenance_type' => $this->input->post('maintenance_type'),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'company_id' => $this->auth->company_id(),
                'asset_id' => $this->input->post('asset_id'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );

            foreach ($this->maintenance_model->get_columns() as $key => $type_column) {
                if ($mysql_data['maintenance_type'] == $key) {
                    if ($key != 2) {
                        // type = date
                        $mysql_data[$type_column] = $this->timezone->convertDate($this->input->post($type_column), 'Y-m-d H:i:s', 'user-to-server');
                    } else {
                        $mysql_data[$type_column] = $this->input->post($type_column);
                    }
                }
            }


            $this->db->insert('maintenance', $mysql_data);
            $maintenance_id = $this->db->insert_id();

            // upload the photos selected if submitted
            if (isset($_FILES['files'])) {
                for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
                    $tmp_name = $_FILES['files']['tmp_name'][$i];
                    $attachment_name = $_FILES['files']['name'][$i];
                    $ext = end(explode('.', $_FILES['files']['name'][$i]));
                    $random_name = md5(microtime(true)) . '.' . $ext;
                    $save_path = './uploads/' . $this->auth->company_id() . '/maintenance/attachments/';
                    if (!is_dir($save_path)) {
                        mkdir($save_path, 0777, true);
                    }
                    $save_path .= $random_name;
                    move_uploaded_file($tmp_name, $save_path);

                    // add to database
                    $mysql_data = array(
                        'attachment' => $random_name,
                        'attachment_name' => $attachment_name,
                        'maintenance_id' => $maintenance_id,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id()
                    );
                    $this->db->insert('maintenance_attachments', $mysql_data);
                }
            }

            $data = array(
                'status' => 'success',
                'message' => 'New maintenance added successfully',
                'errors' => ''
            );
            echo json_encode($data);
        }

    }

    /**
     * Ajax add new maintenance request
     * @return bool
     */
    public function update()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to edit Maintenance.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $maintenance = $this->db->where('maintenance_id', $this->input->post('maintenance_id'))
            ->where('company_id', $this->auth->company_id())
            ->get('maintenance')
            ->row();
        if (!$maintenance) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You can only edit records that you own.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('comments', 'Comments', '');
        $this->form_validation->set_rules('notes_next_service', 'Notes for next service', '');
        $this->form_validation->set_rules('maintenance_type', 'Maintenance Type', 'required|callback_add_due_fuel_entry');
        $this->form_validation->set_rules('odo_hour', 'Odo Hour', 'numeric');
        $this->form_validation->set_rules('maintenance_id', 'Maintenance', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // validate photos before uploading
            $valid_type = true;
            if (isset($_FILES['files'])) {
                for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
                    $ext = end(explode('.', $_FILES['files']['name'][$i]));
                    if (in_array($ext, array('php', 'html', 'js', 'exe', 'deb'))) {
                        $valid_type = false;
                    }
                }
            }

            // if not valid then show error
            if ($valid_type == false) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'Invalid file types submitted, these files are not allowed for security reasons.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $mysql_data = array(
                'comments' => $this->input->post('comments'),
                'notes_next_service' => $this->input->post('notes_next_service'),
                'maintenance_type' => $this->input->post('maintenance_type'),
                'warranty_date' => '0000-00-00 00:00:00',
                'due_date' => '0000-00-00 00:00:00',
                'odo_hour' => 0,
                'testing_date' => '0000-00-00 00:00:00',
                'end_of_life_date' => '0000-00-00 00:00:00',
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );

            foreach ($this->maintenance_model->get_columns() as $key => $type_column) {
                if ($key != 2) {
                    // type = date
                    $mysql_data[$type_column] = $this->timezone->convertDate($this->input->post($type_column), 'Y-m-d H:i:s', 'user-to-server');
                } else {
                    $mysql_data[$type_column] = $this->input->post($type_column);
                }
            }

            $this->db->where('maintenance_id', $maintenance->maintenance_id);
            $this->db->update('maintenance', $mysql_data);


            // upload the photos selected if submitted
            if (isset($_FILES['files'])) {
                for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
                    $tmp_name = $_FILES['files']['tmp_name'][$i];
                    $attachment_name = $_FILES['files']['name'][$i];
                    $ext = end(explode('.', $_FILES['files']['name'][$i]));
                    $random_name = md5(microtime(true)) . '.' . $ext;
                    $save_path = './uploads/' . $this->auth->company_id() . '/maintenance/attachments/';
                    if (!is_dir($save_path)) {
                        mkdir($save_path, 0777, true);
                    }
                    $save_path .= $random_name;
                    move_uploaded_file($tmp_name, $save_path);

                    // add to database
                    $mysql_data = array(
                        'attachment' => $random_name,
                        'attachment_name' => $attachment_name,
                        'maintenance_id' => $maintenance->maintenance_id,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'created_by' => $this->auth->id(),
                        'updated_by' => $this->auth->id()
                    );
                    $this->db->insert('maintenance_attachments', $mysql_data);
                }
            }

            $data = array(
                'status' => 'success',
                'message' => 'Maintenance updated successfully',
                'errors' => ''
            );
            echo json_encode($data);
        }

    }

    /**
     * Form validation for add maintenance
     * @param $value
     * @return bool
     */
    public function add_due_fuel_entry()
    {
        $result = $this->maintenance_model->validate_maintenance($this->input->post());
        if ($result['status'] == 'fail') {
            $this->form_validation->set_message('add_due_fuel_entry', $result['message']);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Form validation for add due date
     * @param $value
     * @return bool
     */
    public function add_due_date($value)
    {
        $currenttime = time();
        $giventime = strtotime($value);
        if ($currenttime >= $giventime) {
            $this->form_validation->set_message('add_due_date', 'Due date should always be in future.');
            return false;
        } else {
            return true;
        }
    }

    public function edit($maintenance_id = 0)
    {
        if (!$this->has_permission(2)) {
            show_error('You do not have permission to edit maintenance.', 403, 'No Permission');
            return false;
        }
        $maintenance_id = (int)$maintenance_id;
        $maintenance = $this->db
            ->select('maintenance.*, assets.friendly_name, concat_ws(" ", users.first_name, users.last_name) as username', false)
            ->join('assets', 'assets.asset_id = maintenance.asset_id')
            ->join('users', 'maintenance.created_by = users.user_id')
            ->where('maintenance_id', $maintenance_id)
            ->where('maintenance.company_id', $this->auth->company_id())
            ->get('maintenance')
            ->row();
        if ($maintenance) {
            $data = array(
                'maintenance' => $maintenance
            );
            $this->load->view('bckcadmin/maintenance.edit.php', $data);
        } else {
            show_error('This maintenance is not available for edit.', 403, 'Not allowed');
        }

    }

    public function delete()
    {
        $maintenance_id = (int)$this->input->post('rid');
        if (!$this->has_permission(2, 'index')) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to edit/del maintenance'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->db->where('maintenance_id', $maintenance_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('maintenance');

        // delete attachments also
        $attachments = $this->db->where('maintenance_id', $maintenance_id)
            ->get('maintenance_attachments')
            ->result();

        foreach ($attachments as $attachment) {
            if ($attachment) {
                $filename = $attachment->attachment;
                $path = './uploads/' . $this->auth->company_id() . '/maintenance/attachments/' . $filename;
                if (file_exists($path)) {
                    @unlink($path);
                }
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'Maintenance record removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function get_attachments()
    {
        $maintenance_id = (int)$this->input->post('maintenance_id');
        $maintenance = $this->db->where('maintenance_id', $maintenance_id)
            ->where('company_id', $this->auth->company_id())
            ->get('maintenance')
            ->row();

        if ($maintenance) {
            $attachments = $this->db->where('maintenance_id', $maintenance_id)
                ->get('maintenance_attachments')
                ->result();
        } else {
            $attachments = array();
        }

        $data = array(
            'status' => 'success',
            'message' => 'Maintenance attachments listed successfully.',
            'errors' => '',
            'attachments' => $attachments
        );
        echo json_encode($data);
    }

    public function remove_attachment()
    {
        $attachment_id = (int)$this->input->post('attachment_id');
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_asset' => 'You do not have permission to edit/del maintenance'
                )
            );
            echo json_encode($data);
            return false;
        }

        $attachments = $this->db->where('maintenance_attachment_id', $attachment_id)
            ->get('maintenance_attachments')
            ->result();

        $this->db->where('maintenance_attachment_id', $attachment_id)
            ->delete('maintenance_attachments');

        foreach ($attachments as $attachment) {
            if ($attachment) {
                $filename = $attachment->attachment;
                $path = './uploads/' . $this->auth->company_id() . '/maintenance/attachments/' . $filename;
                if (file_exists($path)) {
                    @unlink($path);
                }
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'Attachment removed successfully.',
            'errors' => '',
        );
        echo json_encode($data);
    }

    public function getlatest()
    {
        $asset_id = (int)$this->input->post('asset_id');
        $company_id = $this->auth->company_id();
        $maintenances = $this->maintenance_model->getStatus($asset_id, $company_id);
        $data = array(
            'status' => 'success',
            'message' => implode('', $maintenances['data']),
            'class' => $maintenances['class'],
            'maintenance' => $maintenances['data']
        );
        echo json_encode($data);
        return false;


    }
}
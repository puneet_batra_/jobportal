<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/25/2015
 * Time: 3:31 PM
 */
class Preferences extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('*'));
    }

    public function index()
    {
        $this->load->view('bckcadmin/preferences.php');
    }

    public function update()
    {
        $theme_id = (int)$this->input->post('theme_id');
        $this->company_model->set_preference('theme_id', $theme_id);

        $data = array(
            'status' => 'success',
            'message' => 'Preferences updated successfully.'
        );
        echo json_encode($data);
    }
}
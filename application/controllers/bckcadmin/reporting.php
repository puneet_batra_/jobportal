<?php
/**
 * Created by PhpStorm.
 * User: Davinder
 * Date: 5/16/2015
 * Time: 10:30 AM
 */
class Reporting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function index()
    {
        $this->load->model('asset_types_model');
        $this->load->view('bckcadmin/reporting.php');
    }

    public function get()
    {
        header('content-type: application/json');
        $this->load->library('datatables');
        $this->datatables->select('
        assets.asset_id,
        asset_types.name as asset_type,
        assets.friendly_name,
        assets.description,
        assets.description as description_plain,
        assets.nfc_uuid as barcode,
        assets.enabled,
        assets.enabled as status,
        (assets.feature_maintenance + assets.feature_exception + assets.feature_maintenance + assets.feature_location) as features,
        assets.feature_maintenance,
        assets.feature_exception,
        assets.feature_location,
        assets.feature_fuel', false)
            ->join('asset_types', 'assets.asset_type_id = asset_types.asset_type_id')
            ->where('deleted_on' , '0000-00-00 00:00:00')
            ->where('assets.company_id', $this->auth->company_id());

        if ($this->input->post('query_name') != '') {
            $this->datatables->where('friendly_name like', '%'.$this->input->post('query_name').'%');
        }
        if ($this->input->post('query_description') != '') {
            $this->datatables->where('description like', '%'.$this->input->post('query_description').'%');
        }
        if ($this->input->post('query_barcode') != '') {
            $this->datatables->where('nfc_uuid like', '%'.$this->input->post('query_barcode').'%');
        }
        if ($this->input->post('query_status') != '') {
            $this->datatables->where('enabled', $this->input->post('query_status'));
        }
        if ($this->input->post('query_features') != '') {
            $this->datatables->where('assets.' . $this->input->post('query_features'), 1);
        }
        if ($this->input->post('query_type') != '') {
            $this->datatables->where('assets.asset_type_id', $this->input->post('query_type'));
        }

        $this->datatables->from('assets');
        $json = json_decode($this->datatables->generate('json', 'utf-8'), true);

        foreach ($json['data'] as $key => $row) {
            // status column
            if ($row['enabled'] == 1) {
                $row['status'] = '<span class="label label-success">Active</span>';
            } else {
                $row['status'] = '<span class="label label-danger">Inactive</span>';
            }

            // features column
            $exception = ($row['feature_exception'] == 1)?'inline-block':'none';
            $maintenance = ($row['feature_maintenance'] == 1)?'inline-block':'none';
            $fuel = ($row['feature_fuel'] == 1)?'inline-block':'none';
            $location = ($row['feature_location'] == 1)?'inline-block':'none';
            $row['features'] = '<span class="label label-success has-tooltip" title="Exception" style="display:'.$exception.'">E</span> '
                . '<span class="label label-info has-tooltip" title="Maintenance" style="display:'.$maintenance.'">M</span> '
                . '<span class="label label-danger has-tooltip" title="Fuel" style="display:'.$fuel.'">F</span> '
                . '<span class="label label-warning has-tooltip" title="Location" style="display:'.$location.'">L</span> ';

            // image column
            $images = $this->db->where('asset_id', $row['asset_id'])
                ->order_by('asset_photo_id', 'desc')
                ->get('asset_photos')
                ->result();
            $image_row = site_url('image/uploads/default.jpg');
            foreach ($images as $image) {
                $path = 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $image->filename;
                if (file_exists($path)) {
                    $image_url = site_url('image/' . $path);
                    $image_row = $image_url;
                    break;
                }
            }
            $row['image'] = $image_row . '?s=45x45&method=crop';

            // plain description
            $row['description_plain'] = '<div class="plain-text-force" style="height: 45px">' . $row['description'] . '</div>';

            $json['data'][$key] = $row;
        }
        echo json_encode($json);

    }

    public function getfuel()
    {
        header('content-type: application/json');
        $this->load->library('datatables');
        $this->datatables->select('
        assets.asset_id,
        asset_types.name as asset_type,
        assets.friendly_name,
        assets.description,
        assets.description as description_plain,
        assets.enabled,
        assets.enabled as status,
        (assets.feature_maintenance + assets.feature_exception + assets.feature_maintenance + assets.feature_location) as features,
        assets.feature_maintenance,
        assets.feature_exception,
        assets.feature_location,
        assets.feature_fuel', false)
            ->join('asset_types', 'assets.asset_type_id = asset_types.asset_type_id')
            ->where('deleted_on' , '0000-00-00 00:00:00')
            ->where('assets.feature_fuel', 1)
            ->where('assets.company_id', $this->auth->company_id());

        if ($this->input->post('query_name') != '') {
            $this->datatables->where('friendly_name like', '%'.$this->input->post('query_name').'%');
        }
        if ($this->input->post('query_description') != '') {
            $this->datatables->where('description like', '%'.$this->input->post('query_description').'%');
        }
        if ($this->input->post('query_status') != '') {
            $this->datatables->where('enabled', $this->input->post('query_status'));
        }
        if ($this->input->post('query_type') != '') {
            $this->datatables->where('assets.asset_type_id', $this->input->post('query_type'));
        }

        $this->datatables->from('assets');
        $json = json_decode($this->datatables->generate('json', 'utf-8'), true);

        foreach ($json['data'] as $key => $row) {
            // status column
            if ($row['enabled'] == 1) {
                $row['status'] = '<span class="label label-success">Active</span>';
            } else {
                $row['status'] = '<span class="label label-danger">Inactive</span>';
            }

            // features column
            $exception = ($row['feature_exception'] == 1)?'inline-block':'none';
            $maintenance = ($row['feature_maintenance'] == 1)?'inline-block':'none';
            $fuel = ($row['feature_fuel'] == 1)?'inline-block':'none';
            $location = ($row['feature_location'] == 1)?'inline-block':'none';
            $row['features'] = '<span class="label label-success has-tooltip" title="Exception" style="display:'.$exception.'">E</span> '
                . '<span class="label label-info has-tooltip" title="Maintenance" style="display:'.$maintenance.'">M</span> '
                . '<span class="label label-danger has-tooltip" title="Fuel" style="display:'.$fuel.'">F</span> '
                . '<span class="label label-warning has-tooltip" title="Location" style="display:'.$location.'">L</span> ';

            // image column
            $images = $this->db->where('asset_id', $row['asset_id'])
                ->order_by('asset_photo_id', 'desc')
                ->get('asset_photos')
                ->result();
            $image_row = site_url('image/uploads/default.jpg');
            foreach ($images as $image) {
                $path = 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $image->filename;
                if (file_exists($path)) {
                    $image_url = site_url('image/' . $path);
                    $image_row = $image_url;
                    break;
                }
            }
            $row['image'] = $image_row . '?s=45x45&method=crop';

            // plain description
            $row['description_plain'] = '<div class="plain-text-force" style="height: 45px">' . $row['description'] . '</div>';

            $json['data'][$key] = $row;
        }
        echo json_encode($json);

    }

    public function getexception()
    {
        header('content-type: application/json');
        $this->load->library('datatables');
        $this->datatables->select('
        assets.asset_id,
        asset_types.name as asset_type,
        assets.friendly_name,
        assets.description,
        assets.description as description_plain,
        assets.enabled,
        assets.enabled as status,
        (assets.feature_maintenance + assets.feature_exception + assets.feature_maintenance + assets.feature_location) as features,
        assets.feature_maintenance,
        assets.feature_exception,
        assets.feature_location,
        assets.feature_fuel', false)
            ->join('asset_types', 'assets.asset_type_id = asset_types.asset_type_id')
            ->where('deleted_on' , '0000-00-00 00:00:00')
            ->where('assets.feature_exception', 1)
            ->where('assets.company_id', $this->auth->company_id());

        if ($this->input->post('query_name') != '') {
            $this->datatables->where('friendly_name like', '%'.$this->input->post('query_name').'%');
        }
        if ($this->input->post('query_description') != '') {
            $this->datatables->where('description like', '%'.$this->input->post('query_description').'%');
        }
        if ($this->input->post('query_status') != '') {
            $this->datatables->where('enabled', $this->input->post('query_status'));
        }
        if ($this->input->post('query_type') != '') {
            $this->datatables->where('assets.asset_type_id', $this->input->post('query_type'));
        }

        $this->datatables->from('assets');
        $json = json_decode($this->datatables->generate('json', 'utf-8'), true);

        foreach ($json['data'] as $key => $row) {
            // status column
            if ($row['enabled'] == 1) {
                $row['status'] = '<span class="label label-success">Active</span>';
            } else {
                $row['status'] = '<span class="label label-danger">Inactive</span>';
            }

            // features column
            $exception = ($row['feature_exception'] == 1)?'inline-block':'none';
            $maintenance = ($row['feature_maintenance'] == 1)?'inline-block':'none';
            $fuel = ($row['feature_fuel'] == 1)?'inline-block':'none';
            $location = ($row['feature_location'] == 1)?'inline-block':'none';
            $row['features'] = '<span class="label label-success has-tooltip" title="Exception" style="display:'.$exception.'">E</span> '
                . '<span class="label label-info has-tooltip" title="Maintenance" style="display:'.$maintenance.'">M</span> '
                . '<span class="label label-danger has-tooltip" title="Fuel" style="display:'.$fuel.'">F</span> '
                . '<span class="label label-warning has-tooltip" title="Location" style="display:'.$location.'">L</span> ';

            // image column
            $images = $this->db->where('asset_id', $row['asset_id'])
                ->order_by('asset_photo_id', 'desc')
                ->get('asset_photos')
                ->result();
            $image_row = site_url('image/uploads/default.jpg');
            foreach ($images as $image) {
                $path = 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $image->filename;
                if (file_exists($path)) {
                    $image_url = site_url('image/' . $path);
                    $image_row = $image_url;
                    break;
                }
            }
            $row['image'] = $image_row . '?s=45x45&method=crop';

            // plain description
            $row['description_plain'] = '<div class="plain-text-force" style="height: 45px">' . $row['description'] . '</div>';

            $json['data'][$key] = $row;
        }
        echo json_encode($json);

    }

    private function _exception_data($limit = '')
    {
        $asset_url = site_url('bckcadmin/assets/view') . '/';
        // Query config
        $temp_dir = 'temp/reports/' . $this->auth->company_id() . '/';

        // filters
        $filters = array();
        if ($this->input->post('query_name') != '') {
            $filters[] = "assets.friendly_name like '%{$this->db->escape_like_str($this->input->post('query_name'))}%'";
        }
        if ($this->input->post('query_description') != '') {
            $filters[] = "assets.description like '%{$this->db->escape_like_str($this->input->post('query_description'))}%'";
        }
        if ($this->input->post('query_status') != '') {
            $filters[] = "assets.enabled like '%{$this->db->escape_like_str($this->input->post('query_status'))}%'";
        }
        if ($this->input->post('query_features') != '') {
            $filters[] = "assets.{$this->input->post('query_features')} = 1";
        }
        if ($this->input->post('query_type') != '') {
            $filters[] = "assets.asset_type_id = {$this->input->post('query_type')}";
        }
        if ($this->input->post('query_ids')) {
            $filters[] = "assets.asset_id IN (". implode(',', $this->input->post('query_ids')) .")";
        }
        if (count($filters) > 0) {
            $filters = ' AND ' . implode(' AND ', $filters);
        } else {
            $filters = '';
        }

        $config = array(
            'overdue_text' => 'Overdue',
            'okay_text' => 'Okay',
            'not_available_text' => '-',
            'limit_sql' => $limit, //'LIMIT 10000'
            'extra_sql' => "where assets.company_id = '{$this->auth->company_id()}'
                and assets.deleted_on = '0000-00-00 00:00:00'
                and assets.feature_exception = 1
                {$filters}
                order by friendly_name asc"
        );


        $query = "select
                assets.asset_id,
                assets.friendly_name,
                assets.description,
                asset_types.name as asset_type,
                assets.nfc_uuid,
                IFNULL(exceptions_open_table.exceptions_open_count, 0) as exceptions_open_count,
                IFNULL(exceptions_progress_table.exceptions_progress_count, 0) as exceptions_progress_count,
                IFNULL(exceptions_closed_table.exceptions_closed_count, 0) as exceptions_closed_count,
                concat('','{$asset_url}', assets.asset_id) as hyperlink
                from assets

                join asset_types on asset_types.asset_type_id = assets.asset_type_id

                left outer join (
                        select exceptions.asset_id, status, count(*) as exceptions_open_count
                                        from exceptions
                                        where status = 'new'
                                        group by exceptions.asset_id
                    ) as exceptions_open_table on exceptions_open_table.asset_id = assets.asset_id

                left outer join (
                        select exceptions.asset_id, status, count(*) as exceptions_progress_count
                                        from exceptions
                                        where status = 'in_progress'
                                        group by exceptions.asset_id
                    ) as exceptions_progress_table on exceptions_progress_table.asset_id = assets.asset_id

                left outer join (
                        select exceptions.asset_id, status, count(*) as exceptions_closed_count
                                        from exceptions
                                        where status = 'closed'
                                        group by exceptions.asset_id
                    ) as exceptions_closed_table on exceptions_closed_table.asset_id = assets.asset_id


                     {$config['extra_sql']}
                     ";

        $num_results = $this->db->query("select count(*) as num_results from ($query) as tbl")->row()->num_results;
        $results = $this->db->query($query . $config['limit_sql'])->result();

        if (!is_dir($temp_dir)) {
            mkdir($temp_dir, 0777, true);
        }
        return array(
            'num_results' => $num_results,
            'results' => $results,
            'path' => $temp_dir
        );
    }

    public function exception_csv()
    {
        if ($this->config->item('csv_record_limit') > 0) {
            $limit_int = $this->config->item('csv_record_limit');
            $limit = 'LIMIT ' . $this->config->item('csv_record_limit');
        } else {
            $limit_int = 0;
            $limit = '';
        }
        $reports = $this->_exception_data($limit);

        $temp_dir = $reports['path'];
        $results = $reports['results'];

        if ($reports['num_results'] > $limit_int && $limit_int > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Please filter your data, Limit exceeds.',
            );
            echo json_encode($data);
            return false;
        }

        $filename = 'exception_' . date('Y_m_d_H_i_s') . '.csv';
        if (count($results) > 0) {
            $file = fopen($temp_dir . $filename, 'a+');
            $line = '"Name",' .
                '"Description",' .
                '"Asset Type",' .
                '"Barcode",' .
                '"Exceptions New",' .
                '"Exceptions Open",' .
                '"Exceptions Closed",' .
                '"Hyperlink"' . "\r\n";
            fwrite($file, $line);

            foreach ($results as $row) {
                $description_plain = preg_replace('#<[^>]+>#', ' ', $row->description);
                $line = '"' .$row->friendly_name . '",' .
                    '"' . strip_tags($description_plain) . '",' .
                    '"' . $row->asset_type . '",' .
                    '"' . $row->nfc_uuid . '",' .
                    '"' . $row->exceptions_open_count . '",' .
                    '"' . $row->exceptions_progress_count . '",' .
                    '"' . $row->exceptions_closed_count . '",' .
                    '"' . $row->hyperlink . '"' . "\r\n";
                fwrite($file, $line);
            }
            fclose($file);
            $data = array(
                'status' => 'success',
                'message' => 'Success! starting download..',
                'fileurl' => base_url($temp_dir . $filename)
            );
            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'fail',
                'message' => 'Nothing to export',
            );
            echo json_encode($data);
        }
    }

    private function _fuel_data($limit = '')
    {
        $asset_url = site_url('bckcadmin/assets/view') . '/';
        // Query config
        $temp_dir = 'temp/reports/' . $this->auth->company_id() . '/';

        // filters
        $filters = array();
        if ($this->input->post('query_name') != '') {
            $filters[] = "assets.friendly_name like '%{$this->db->escape_like_str($this->input->post('query_name'))}%'";
        }
        if ($this->input->post('query_description') != '') {
            $filters[] = "assets.description like '%{$this->db->escape_like_str($this->input->post('query_description'))}%'";
        }
        if ($this->input->post('query_status') != '') {
            $filters[] = "assets.enabled like '%{$this->db->escape_like_str($this->input->post('query_status'))}%'";
        }
        if ($this->input->post('query_features') != '') {
            $filters[] = "assets.{$this->input->post('query_features')} = 1";
        }
        if ($this->input->post('query_type') != '') {
            $filters[] = "assets.asset_type_id = {$this->input->post('query_type')}";
        }
        if ($this->input->post('query_ids')) {
            $filters[] = "assets.asset_id IN (". implode(',', $this->input->post('query_ids')) .")";
        }
        if (count($filters) > 0) {
            $filters = ' AND ' . implode(' AND ', $filters);
        } else {
            $filters = '';
        }

        $config = array(
            'overdue_text' => 'Overdue',
            'okay_text' => 'Okay',
            'not_available_text' => '-',
            'limit_sql' => $limit, //'LIMIT 10000'
            'extra_sql' => "where assets.company_id = '{$this->auth->company_id()}'
                and assets.deleted_on = '0000-00-00 00:00:00'
                and assets.feature_fuel = 1
                {$filters}
                order by friendly_name asc"
        );


        $query = "select
                assets.asset_id,
                assets.friendly_name,
                assets.description,
                IFNULL(fuel_table.last_fueled_date,'{$config['not_available_text']}') as last_fueled_date,
                IFNULL(exceptions_open_table.exceptions_open_count, 0) as exceptions_open_count,
                IFNULL(exceptions_progress_table.exceptions_progress_count, 0) as exceptions_progress_count,
                IF(fuel_table.entries_count > 0 and maintenance_table.entries_count > 0,
                   IFNULL(maintenance_table.odo_hour - fuel_table.odometer, 0)
                   , '{$config['not_available_text']}') as km_left,
                IFNULL(fuel_table.total_volume,0) as total_volume,
                IFNULL(fuel_table.odometer, 0) as odometer,
                IFNULL(fuel_table.total_volume / (fuel_table.odometer * 100), '{$config['not_available_text']}') as l_per_n_km,
                concat('','{$asset_url}', assets.asset_id) as hyperlink,
                images_table.filename
                from assets

                left outer join (
                        select asset_id,sum(volume) as total_volume, count(*) as entries_count, odometer,
                    max(fueled_date) as last_fueled_date from fuel group by asset_id
                    ) as fuel_table on fuel_table.asset_id = assets.asset_id

                left outer join (
                        select exceptions.asset_id, status, count(*) as exceptions_open_count
                                        from exceptions
                                        where status = 'new'
                                        group by exceptions.asset_id
                    ) as exceptions_open_table on exceptions_open_table.asset_id = assets.asset_id

                left outer join (
                        select exceptions.asset_id, status, count(*) as exceptions_progress_count
                                        from exceptions
                                        where status = 'in_progress'
                                        group by exceptions.asset_id
                    ) as exceptions_progress_table on exceptions_progress_table.asset_id = assets.asset_id

                left outer join (
                        select asset_id, max(odo_hour) as odo_hour, count(*) as entries_count from maintenance where maintenance_type = 2
                    group by asset_id
                    ) as maintenance_table on maintenance_table.asset_id = assets.asset_id

                left outer join (
                  select max(asset_photo_id), asset_id, filename from asset_photos group by asset_id
                ) as images_table on images_table.asset_id = assets.asset_id

                     {$config['extra_sql']}
                     ";

        $num_results = $this->db->query("select count(*) as num_results from ($query) as tbl")->row()->num_results;
        $results = $this->db->query($query . $config['limit_sql'])->result();

        if (!is_dir($temp_dir)) {
            mkdir($temp_dir, 0777, true);
        }
        return array(
            'num_results' => $num_results,
            'results' => $results,
            'path' => $temp_dir
        );
    }

    public function fuel_csv()
    {
        if ($this->config->item('csv_record_limit') > 0) {
            $limit_int = $this->config->item('csv_record_limit');
            $limit = 'LIMIT ' . $this->config->item('csv_record_limit');
        } else {
            $limit_int = 0;
            $limit = '';
        }
        $reports = $this->_fuel_data($limit);

        $temp_dir = $reports['path'];
        $results = $reports['results'];

        if ($reports['num_results'] > $limit_int && $limit_int > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Please filter your data, Limit exceeds.',
            );
            echo json_encode($data);
            return false;
        }

        $filename = 'fuel_' . date('Y_m_d_H_i_s') . '.csv';
        if (count($results) > 0) {
            $file = fopen($temp_dir . $filename, 'a+');
            $line = '"Name",' .
                '"Description",' .
                '"Last Fueled",' .
                '"Km till next service",' .
                '"Exceptions",' .
                '"Average L/100Km",' .
                '"Hyperlink"' . "\r\n";
            fwrite($file, $line);

            foreach ($results as $row) {
                $description_plain = preg_replace('#<[^>]+>#', ' ', $row->description);
                $line = '"' .$row->friendly_name . '",' .
                    '"' . strip_tags($description_plain) . '",' .
                    '"' . $row->last_fueled_date . '",' .
                    '"' . $row->km_left . '",' .
                    '"' . $row->exceptions_open_count . ' New/' . $row->exceptions_progress_count . ' Open' . '",' .
                    '"' . $row->l_per_n_km . '",' .
                    '"' . $row->hyperlink . '"' . "\r\n";
                fwrite($file, $line);
            }
            fclose($file);
            $data = array(
                'status' => 'success',
                'message' => 'Success! starting download..',
                'fileurl' => base_url($temp_dir . $filename)
            );
            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'fail',
                'message' => 'Nothing to export',
            );
            echo json_encode($data);
        }
    }

    public function fuel_pdf()
    {
        if (function_exists('proc_nice')) {
            proc_nice(19);
        }
        if ($this->config->item('pdf_record_limit') > 0) {
            $limit_int = $this->config->item('pdf_record_limit');
            $limit = 'LIMIT ' . $this->config->item('pdf_record_limit');
        } else {
            $limit_int = 0;
            $limit = '';
        }
        $reports = $this->_fuel_data($limit);

        $temp_dir = $reports['path'];
        $filename = 'fuel_' . date('Y_m_d_H_i_s') . '.pdf';
        $results = $reports['results'];

        if (count($results) == 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Nothing to export',
            );
            echo json_encode($data);
            return false;
        }
        if ($reports['num_results'] > $limit_int && $limit_int > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Please filter your data, Limit exceeds.',
            );
            echo json_encode($data);
            return false;
        }

        $htmls = array();
        $buffer = '';

        foreach ($results as $i => $row) {
            $data = array(
                'row' => $row
            );
            $buffer .= $this->load->view('layouts/reporting/reporting.fuel.pdf.php', $data, true);
            if ($i % 5 == 0) {
                $htmls[] = $buffer;
                $buffer = '';
            }
        }

        $htmls[] = $buffer;

        // Include the main TCPDF library (search for installation path).

        require_once('application/third_party/tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('AirApp');
        $pdf->SetTitle('Reporting');
        $pdf->SetSubject('Fuel Reporting');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Fuel Reporting', 'Listing: ' . count($results) . ' records' . "\r\n");

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('helvetica', '', 10);
        $pdf->setFontSubsetting(false);

        // add a page
        $pdf->AddPage();

        // output the HTML content
        foreach ($htmls as $html) {
            $pdf->writeHTML($html, false, false, false, false, '');
        }


        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output(FCPATH . $temp_dir . $filename, 'F');

        $data = array(
            'status' => 'success',
            'message' => 'Success! starting download..',
            'fileurl' => base_url($temp_dir . $filename)
        );
        echo json_encode($data);
    }

    private function _assets_data($limit = '')
    {
        // Query config
        $temp_dir = 'temp/reports/' . $this->auth->company_id() . '/';

        // filters
        $filters = array();
        if ($this->input->post('query_name') != '') {
            $filters[] = "assets.friendly_name like '%{$this->db->escape_like_str($this->input->post('query_name'))}%'";
        }
        if ($this->input->post('query_description') != '') {
            $filters[] = "assets.description like '%{$this->db->escape_like_str($this->input->post('query_description'))}%'";
        }
        if ($this->input->post('query_status') != '') {
            $filters[] = "assets.enabled like '%{$this->db->escape_like_str($this->input->post('query_status'))}%'";
        }
        if ($this->input->post('query_features') != '') {
            $filters[] = "assets.{$this->input->post('query_features')} = 1";
        }
        if ($this->input->post('query_type') != '') {
            $filters[] = "assets.asset_type_id = {$this->input->post('query_type')}";
        }
        if ($this->input->post('query_ids')) {
            $filters[] = "assets.asset_id IN (". implode(',', $this->input->post('query_ids')) .")";
        }
        if (count($filters) > 0) {
            $filters = ' AND ' . implode(' AND ', $filters);
        } else {
            $filters = '';
        }

        $config = array(
            'overdue_text' => 'Overdue',
            'okay_text' => 'Okay',
            'not_available_text' => '-',
            'limit_sql' => $limit, //'LIMIT 10000'
            'extra_sql' => "where assets.company_id = '{$this->auth->company_id()}'
                and assets.deleted_on = '0000-00-00 00:00:00'
                {$filters}
                order by friendly_name asc"
        );

        // Query column definitions
        $columns = array(
            // Checks if odometer is due
            'status_odometer' => "IF(maintenance_odo_table.num_entries > 0,
                       IF(fuel_odo_table.odometer_now > maintenance_odo_table.odometer > 0, '{$config['overdue_text']}', '{$config['okay_text']}')
                       , '{$config['not_available_text']}') as status_odometer",
            // Checks if warranty is due
            'status_warranty_date' => "IF(maintenance_warranty_table.num_entries > 0,
                       IF(maintenance_warranty_table.warranty_date < NOW(), '{$config['overdue_text']}','{$config['okay_text']}')
                       ,'{$config['not_available_text']}') as status_warranty_date",
            // Checks if service is due
            'status_due_date' => "IF(maintenance_due_table.num_entries > 0,
                       IF(maintenance_due_table.due_date < NOW(), '{$config['overdue_text']}','{$config['okay_text']}')
                       ,'{$config['not_available_text']}') as status_due_date",
            // Checks if testing date is due
            'status_testing_date' => "IF(maintenance_testing_table.num_entries > 0,
                       IF(maintenance_testing_table.testing_date < NOW(), '{$config['overdue_text']}','{$config['okay_text']}')
                       ,'{$config['not_available_text']}') as status_testing_date",
            // Checks if end of life date is due
            'status_end_of_life_date' => "IF(maintenance_eol_table.num_entries > 0,
                       IF(maintenance_eol_table.eol_date < NOW(), '{$config['overdue_text']}','{$config['okay_text']}')
                       ,'{$config['not_available_text']}') as status_end_of_life_date",
            // Counts new status exceptions
            'exceptions_open_count' => "IFNULL(exceptions_open_table.exceptions_open_count,0) as exceptions_open_count",
            // Counts in progress status exceptions
            'exceptions_progress_count' => "IFNULL(exceptions_progress_table.exceptions_progress_count, 0) as exceptions_progress_count",
            // Generates hyperlink
            'hyperlink' => "concat('', '". site_url('bckcadmin/assets/view') . "/', assets.asset_id) as hyperlink",
            'filename' => "images_table.filename"
        );

        // Query Join table sqls
        $joins = array(
            // Counts number of exceptions with status "new"
            'exceptions_open_table' => "select exceptions.asset_id, status, count(*) as exceptions_open_count
                        from exceptions
                        where status = 'new'
                        group by exceptions.asset_id",

            // Counts number of exceptions with status "in_progress"
            'exceptions_progress_table' => "select exceptions.asset_id, status, count(*) as exceptions_progress_count
                        from exceptions
                        where status = 'in_progress'
                        group by exceptions.asset_id",

            // Gives last odometer value in maintenance
            'maintenance_odo_table' => "select count(*) as num_entries, max(odo_hour) as odometer,asset_id from maintenance
                        where maintenance_type = 2
                        group by asset_id",

            // Gives last odometer value in fuel entries
            'fuel_odo_table' => "select asset_id, max(odometer) as odometer_now from fuel group by asset_id",

            // Gives last warranty date maintenance entries
            'maintenance_warranty_table' => "select asset_id, count(*) as num_entries ,max(warranty_date) as warranty_date from maintenance
                        where maintenance_type = 0
                        group by asset_id",

            // Gives last service due date maintenance entries
            'maintenance_due_table' => "select asset_id, count(*) as num_entries ,max(due_date) as due_date from maintenance
                        where maintenance_type = 1
                        group by asset_id",

            // Gives last testing date maintenance entries
            'maintenance_testing_table' => "select asset_id, count(*) as num_entries ,max(testing_date) as testing_date from maintenance
                        where maintenance_type = 3
                        group by asset_id",

            // Gives last end of life maintenance entries
            'maintenance_eol_table' => "select asset_id, count(*) as num_entries ,max(end_of_life_date) as eol_date from maintenance
                        where maintenance_type = 3
                        group by asset_id",
            'images_table' => "select max(asset_photo_id), asset_id, filename from asset_photos group by asset_id"

        );

        // Time to compile Query
        $query = "select friendly_name,
                    description,
                    nfc_uuid as barcode,
                    asset_types.name as asset_type,
                    {$columns['status_odometer']},
                    {$columns['status_warranty_date']},
                    {$columns['status_due_date']},
                    {$columns['status_testing_date']},
                    {$columns['status_end_of_life_date']},
                    {$columns['exceptions_open_count']},
                    {$columns['exceptions_progress_count']},
                    {$columns['hyperlink']},
                    {$columns['filename']}
                    from assets
                    join asset_types on assets.asset_type_id = asset_types.asset_type_id
                    left outer join (
                        {$joins['exceptions_open_table']}
                        ) as exceptions_open_table on assets.asset_id = exceptions_open_table.asset_id

                    left outer join (
                        {$joins['exceptions_progress_table']}
                        ) as exceptions_progress_table on assets.asset_id = exceptions_progress_table.asset_id

                    left outer join (
                        {$joins['maintenance_odo_table']}
                        ) as maintenance_odo_table on maintenance_odo_table.asset_id = assets.asset_id

                    left outer join (
                        {$joins['fuel_odo_table']}
                        ) as fuel_odo_table on fuel_odo_table.asset_id = assets.asset_id

                    left outer join (
                        {$joins['maintenance_warranty_table']}
                        ) as maintenance_warranty_table on maintenance_warranty_table.asset_id = assets.asset_id

                    left outer join (
                        {$joins['maintenance_due_table']}
                        ) as maintenance_due_table on maintenance_due_table.asset_id = assets.asset_id

                    left outer join (
                        {$joins['maintenance_testing_table']}
                        ) as maintenance_testing_table on maintenance_testing_table.asset_id = assets.asset_id

                    left outer join (
                        {$joins['maintenance_eol_table']}
                        ) as maintenance_eol_table on maintenance_eol_table.asset_id = assets.asset_id

                    left outer join (
                      {$joins['images_table']}
                    ) as images_table on images_table.asset_id = assets.asset_id

                    {$config['extra_sql']}
                    ";

        $count = "select count(*) as num_results from ($query) as tbl";
        $num_results = $this->db->query($count)->row()->num_results;
        $results = $this->db->query($query . $config['limit_sql'])->result();

        if (!is_dir($temp_dir)) {
            mkdir($temp_dir, 0777, true);
        }
        return array(
            'num_results' => $num_results,
            'results' => $results,
            'path' => $temp_dir
        );
    }

    public function assets_csv()
    {
        if ($this->config->item('csv_record_limit') > 0) {
            $limit_int = $this->config->item('csv_record_limit');
            $limit = 'LIMIT ' . $limit_int;
        } else {
            $limit_int = 0;
            $limit = '';
        }
        $reports = $this->_assets_data($limit);

        $temp_dir = $reports['path'];
        $results = $reports['results'];

        if ($reports['num_results'] > $limit_int && $limit_int > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Please filter your data, Limit exceeds.',
            );
            echo json_encode($data);
            return false;
        }

        $filename = 'assets_' . date('Y_m_d_H_i_s') . '.csv';
        if (count($results) > 0) {
            $file = fopen($temp_dir . $filename, 'a+');
            $line = '"Name",' .
                '"Description",' .
                '"Barcode",' .
                '"Exceptions New",' .
                '"Exceptions Open",' .
                '"Maintenance Odometer",' .
                '"Maintenance Warranty",' .
                '"Maintenance Service",' .
                '"Maintenance Testing",' .
                '"Maintenance End of Life",' .
                '"Type",' .
                '"Hyperlink"' . "\r\n";
            fwrite($file, $line);

            foreach ($results as $row) {
                $description_plain = preg_replace('#<[^>]+>#', ' ', $row->description);
                $line = '"' .$row->friendly_name . '",' .
                    '"' . strip_tags($description_plain) . '",' .
                    '"' . $row->barcode . '",' .
                    '"' . (int)$row->exceptions_open_count  . '",' .
                    '"' . (int)$row->exceptions_progress_count  . '",' .
                    '"' . $row->status_odometer . '",' .
                    '"' . $row->status_warranty_date . '",' .
                    '"' . $row->status_due_date . '",' .
                    '"' . $row->status_testing_date . '",' .
                    '"' . $row->status_end_of_life_date . '",'.
                    '"' . $row->asset_type . '",' .
                    '"' . $row->hyperlink . '"' . "\r\n";
                fwrite($file, $line);
            }
            fclose($file);
            $data = array(
                'status' => 'success',
                'message' => 'Success! starting download..',
                'fileurl' => base_url($temp_dir . $filename)
            );
            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'fail',
                'message' => 'Nothing to export',
            );
            echo json_encode($data);
        }
    }

    public function assets_pdf()
    {
        if (function_exists('proc_nice')) {
            proc_nice(19);
        }
        if ($this->config->item('pdf_record_limit') > 0) {
            $limit_int = $this->config->item('pdf_record_limit');
            $limit = 'LIMIT ' . $limit_int;
        } else {
            $limit_int = 0;
            $limit = '';
        }
        $reports = $this->_assets_data($limit);

        $temp_dir = $reports['path'];
        $filename = 'assets_' . date('Y_m_d_H_i_s') . '.pdf';
        $results = $reports['results'];

        if (count($results) == 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Nothing to export',
            );
            echo json_encode($data);
            return false;
        }

        if ($reports['num_results'] > $limit_int && $limit_int > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'Please filter your data, Limit exceeds.',
            );
            echo json_encode($data);
            return false;
        }

        $htmls = array();
        $buffer = '';

        foreach ($results as $i => $row) {
            $data = array(
                'row' => $row
            );
            $buffer .= $this->load->view('layouts/reporting/reporting.assets.pdf.php', $data, true);
            if ($i % 5 == 0) {
                $htmls[] = $buffer;
                $buffer = '';
            }
        }

        $htmls[] = $buffer;

        // Include the main TCPDF library (search for installation path).

        require_once('application/third_party/tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('AirApp');
        $pdf->SetTitle('Reporting');
        $pdf->SetSubject('Assets Reporting');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Assets Reporting', 'Listing: ' . count($results) . ' records' . "\r\n");

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('helvetica', '', 10);
        $pdf->setFontSubsetting(false);

        // add a page
        $pdf->AddPage();

        // output the HTML content
        foreach ($htmls as $html) {
            $pdf->writeHTML($html, false, false, false, false, '');
        }


        // ---------------------------------------------------------

        //Close and output PDF document
        $pdf->Output(FCPATH . $temp_dir . $filename, 'F');

        $data = array(
            'status' => 'success',
            'message' => 'Success! starting download..',
            'fileurl' => base_url($temp_dir . $filename)
        );
        echo json_encode($data);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/17/2015
 * Time: 4:57 PM
 */
class Search extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('*'));
    }

    public function index()
    {
        $this->load->view('bckcadmin/search.php');
    }
}
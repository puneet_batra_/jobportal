<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/15/2015
 * Time: 12:06 PM
 */
class Suppliers extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
    }

    public function index()
    {
        $this->load->view('bckcadmin/suppliers.php');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('supplier_name', 'Supplier Name', 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add suppliers.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'supplier_name' => $this->input->post('supplier_name'),
                'company_id' => $this->auth->company_id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('suppliers', $mysql_data);
            $supplier_id = $this->db->insert_id();

            $data = array(
                'status' => 'success',
                'message' => 'Supplier added successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('supplier_name', 'Supplier Name', 'required');

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
        } else {
            // check for permissions
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit suppliers.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // insert data
            $mysql_data = array(
                'supplier_name' => $this->input->post('supplier_name'),
                'updated_on' => date('Y-m-d H:i:s'),
                'updated_by' => $this->auth->id()
            );
            $supplier_id = (int)$this->input->post('supplier_id');
            $this->db->where('supplier_id', $supplier_id)
                ->where('company_id', $this->auth->company_id())
                ->update('suppliers', $mysql_data);

            $data = array(
                'status' => 'success',
                'message' => 'Supplier updated successfully.',
                'errors' => ''
            );
            echo json_encode($data);
        }
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read suppliers.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $this->load->library('datatables');
        $this->datatables->select('supplier_id, supplier_name');
        $this->datatables->where('company_id', $this->auth->company_id());
        $this->datatables->from('suppliers');
        if ($this->has_permission(2)) {
            $actions = ' <a onclick="edit_supplier(this)"><i class="fa fa-edit" id="datatable-item-$1"></i> Edit</a> |
                    <a onclick="delete_record(this)"><i class="fa fa-trash" id="datatable-item-$1"></i> Delete</a>';
        } else {
            $actions = '<span class="label label-sm label-danger">Not Authorized</span>';
        }
        $this->datatables->add_column('actions', $actions, 'supplier_id');
        echo $this->datatables->generate('json', 'utf-8');
    }

    public function delete()
    {
        if (!$this->has_permission(2)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to delete suppliers.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $supplier_id = (int)$this->input->post('rid');

        $fuel = $this->db->where('supplier_id', $supplier_id)
            ->get('fuel')->result();
        if (count($fuel) > 0) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Cannot delete: It is in use by ' . count($fuel) . ' ' . ((count($fuel)>1)?'fuel entries':'fuel entry') .'.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $this->db->where('supplier_id', $supplier_id)
            ->where('company_id', $this->auth->company_id())
            ->delete('suppliers');
        $data = array(
            'status' => 'success',
            'message' => 'Supplier removed successfully.',
            'errors' => ''
        );
        echo json_encode($data);
    }

    public function single()
    {
        $supplier_id = (int)$this->input->post('rid');
        $supplier = $this->db->where('supplier_id', $supplier_id)->where('company_id', $this->auth->company_id())
            ->get('suppliers')
            ->row();
        if (!$supplier) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Supplier not found.'
                )
            );
            echo json_encode($data);
            return false;
        } else {

            $data = array(
                'status' => 'success',
                'message' => 'Supplier read successfully.',
                'errors' => '',
                'supplier' => $supplier
            );
            echo json_encode($data);
            return false;
        }
    }


}
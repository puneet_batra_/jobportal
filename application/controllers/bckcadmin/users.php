<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/13/2015
 * Time: 10:48 AM
 */
class Users extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array());
        $this->load->library('datatables');
    }

    public function index()
    {
        $roles = $this->db
            ->where('company_id', $this->auth->company_id())
            ->get('roles')->result();
        $data = array(
            'roles' => $roles,
        );
        $this->load->view('bckcadmin/users', $data);
    }

    public function single()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read users.'
                )
            );
            echo json_encode($data);
            return false;
        }
        $uid = (int)$this->input->post('uid');
        $user = $this->db->where('user_id', $uid)->get('users')->row();
        if (!$user) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_user' => 'No user found, it may be already removed.'
                )
            );
        } else {
            $role = $this->db->select('roles.role_id, roles.name, user_roles.user_role_id')
                ->join('roles', 'user_roles.role_id = roles.role_id')
                ->where('user_roles.user_id', $user->user_id)
                ->where('roles.company_id', $this->auth->company_id())
                ->get('user_roles')->row();

            $company_admin = $this->db->where('company_id', $this->auth->company_id())
                ->where('user_id', $user->user_id)
                ->get('company_admins')
                ->row();

            $user->role = $role;
            $user->company_admin = $company_admin;
            $data = array(
                'status' => 'success',
                'message' => 'User found',
                'errors' => '',
                'record' => $user
            );
        }
        echo json_encode($data);
    }

    public function get()
    {
        if (!$this->has_permission(1)) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'You do not have permission to read users.'
                )
            );
            echo json_encode($data);
            return false;
        }

        $sql = $this->company_model->get_company_users('all', true);
        $this->datatables->query($sql);


        if ($this->has_permission(2)) {
            $actions = '<a onclick="show_edit(this)" title="Edit">
                        <i class="fa fa-edit" id="datatable-item-$1"></i> Edit
                    </a> |
                    <a onclick="do_delete(this)" title="Delete">
                        <i class="fa fa-trash" id="datatable-item-$1"></i> Remove
                    </a>';
        } else {
            $actions = '<span class="label label-danger">Not Authorized</span>';
        }

        $this->datatables->add_column('actions', $actions, 'user_id');
        $out = $this->datatables->generateCustom('raw');
        foreach ($out['data'] as $key => $record) {
            if ($record['status'] == 0) {
                $actions = '
                    <a onclick="do_remove_invite(this)" title="Delete">
                        <i class="fa fa-trash" id="datatable-item-'.$record['invite_id'].'"></i> Cancel Invite
                    </a>';
                $out['data'][$key]['actions'] = $actions;
            }

            $company_admin = $this->db->where('user_id', $record['user_id'])
                ->where('company_id', $this->auth->company_id())
                ->get('company_admins')
                ->row();
            if ($company_admin) {
                $out['data'][$key]['role'] = 'Company Admin';
            }
        }
        echo json_encode($out);
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name','First Name', 'alpha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_already_invited');
        $this->form_validation->set_rules('role_id', 'Role', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to add users.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            // check if user exists from users and authentication table
            $user = $this->db->where('email', $this->input->post('email'))->get('users')->row();
            if (!$user) {
                // user not found may be we can find something in authentications
                $auth_user = $this->db->where('email', $this->input->post('email'))->get('authentications')->row();
                if ($auth_user) {
                    // if user found then refer to original user account.
                    // and if not found then it would stay false
                    $user = $this->db->where('user_id', $auth_user->user_id)->get('users')->row();
                }
            }

            $mysql_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'user_id' => 0, // by default keep it zero, if user found we will update it.
                'company_id' => $this->auth->company_id(),
                'accepted' => 0,
                'role_id' => $this->input->post('role_id'),
                'invited_by' => $this->auth->id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $extra = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name')
            );
            if ($user){
                // update if user found.
                $mysql_data['first_name'] = $user->first_name;
                $mysql_data['last_name'] = $user->last_name;
                $mysql_data['user_id'] = $user->user_id;

                $extra['first_name'] = $user->first_name;
                $extra['last_name'] = $user->last_name;
            }
            $this->db->insert('invites', $mysql_data);
            $invite_id = $this->db->insert_id();


            $this->user_model->send_invite($invite_id, $extra);

            $data = array(
                'status' => 'success',
                'message' => 'Invited successfully.',
                'errors' => ''
            );
        }

        echo json_encode($data);
    }

    public function already_invited($email)
    {
        $company_id = $this->auth->company_id();
        $sql = "SELECT * FROM `users` as u WHERE u.user_id in
                (
                    select user_id from user_roles as ur
                    join roles as r on ur.role_id = r.role_id
                    where r.company_id = {$company_id}
                    )
                    and email = '$email'";
        $user = $this->db->query($sql)->row();
        if ($user) {
            $this->form_validation->set_message('already_invited', 'This user has already invited');
            return false;
        } else {
            return true;
        }

    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_id', 'Role', 'numeric');
        $this->form_validation->set_rules('uid', 'User', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to edit users.'
                    )
                );
                echo json_encode($data);
                return false;
            }
            if ($this->input->post('role_id') == '' || $this->input->post('role_id') == 0) {
                if ($this->input->post('company_admin') == 0) {
                    $data = array(
                        'status' => 'fail',
                        'message' => 'There were errors',
                        'errors' => array(
                            'invalid' => 'You must select a role.'
                        )
                    );
                    echo json_encode($data);
                    return false;
                }
            }

            $user_role = $this->db->where('user_role_id', $this->input->post('uid'))
                ->get('user_roles')
                ->row();
            if ($user_role) {
                if ($this->input->post('company_admin') == 0) {
                    $user_id = $user_role->user_id;
                    $this->db->where('user_id', $user_id)
                        ->where('company_id', $this->auth->company_id())
                        ->delete('company_admins');

                    $mysql_data = array(
                        'role_id' => $this->input->post('role_id')
                    );
                    $this->db->where('user_role_id', $this->input->post('uid'))
                        ->update('user_roles', $mysql_data);

                } else {
                    $company_admin = $this->db->where('user_id', $user_role->user_id)
                        ->where('company_id', $this->auth->company_id())
                        ->get('company_admins')
                        ->row();
                    if (!$company_admin) {
                        $mysql_data = array(
                            'user_id' => $user_role->user_id,
                            'company_id' => $this->auth->company_id(),
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s'),
                            'created_by' => $this->auth->id(),
                            'updated_by' => $this->auth->id()
                        );
                        $this->db->insert('company_admins', $mysql_data);
                    }
                }
            }
        }

        $data = array(
            'status' => 'success',
            'message' => 'User updated successfully',
            'errors' => ''
        );
        echo json_encode($data);
        return false;

    }

    public function delete()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to delete users.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $role = $this->db->select('roles.role_id, roles.name, user_roles.user_role_id')
                ->join('roles', 'user_roles.role_id = roles.role_id')
                ->where('user_roles.user_id', $this->input->post('uid'))
                ->where('roles.company_id', $this->auth->company_id())
                ->get('user_roles')->row();

            if ($role) {
                $this->db->where('user_role_id', $role->user_role_id)
                    ->delete('user_roles');
            }

            $this->db->where('user_id', $this->input->post('uid'))
                ->where('company_id', $this->auth->company_id())
                ->delete('invites');

            $this->db->where('user_id', $this->input->post('uid'))
                ->where('company_id', $this->auth->company_id())
                ->delete('company_admins');

            $this->db->where('user_id', $this->input->post('uid'))
                ->where('company_id', $this->auth->company_id())
                ->delete('asset_managers');

            $data = array(
                'status' => 'success',
                'message' => 'User removed successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;

        }
    }

    public function deleteinvite()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('uid','UID', 'required|numeric');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => (array)$this->form_validation->errors_array()
            );
            echo json_encode($data);
            return false;
        } else {
            if (!$this->has_permission(2)) {
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to delete users.'
                    )
                );
                echo json_encode($data);
                return false;
            }

            $invite = $this->db->where('invite_id', $this->input->post('uid'))
                ->get('invites')
                ->row();

            $this->db->where('invite_id', $this->input->post('uid'))
                ->delete('invites');

            if ($invite && $invite->user_id > 0) {
                $this->db->where('user_id', $invite->user_id)
                    ->where('company_id', $this->auth->company_id())
                    ->delete('company_admins');

                $this->db->where('user_id', $invite->user_id)
                    ->where('company_id', $this->auth->company_id())
                    ->delete('asset_managers');
            }


            $data = array(
                'status' => 'success',
                'message' => 'Invite cancelled successfully.',
                'errors' => ''
            );
            echo json_encode($data);
            return true;

        }
    }

    public function getusers()
    {
        // get current users
        $roles = $this->db
            ->select('user_roles.user_id', false)
            ->join('roles', 'roles.role_id = user_roles.role_id')
            ->where('roles.company_id', $this->auth->company_id())
            ->get('user_roles')
            ->result();

        // collect active users'ids of this company
        $active_users = array(0);
        foreach ($roles as $active_user) {
            $active_users[] = $active_user->user_id;
        }

        if ($this->input->post('include_admin')) {
            $company_admin = $this->db->where('company_id', $this->auth->company_id())
                ->get('companies')
                ->row();
            $active_users[] = (int)$company_admin->user_id;
        }

        $sql = "SELECT * from users where user_id in (" . implode(',', $active_users) . ") AND (
            first_name LIKE '%{$this->input->post('q')}%'
            OR last_name LIKE '%{$this->input->post('q')}%'
            OR email LIKE '%{$this->input->post('q')}%'
        )";
        $users = $this->db->query($sql)->result();
        echo json_encode($users);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/18/2015
 * Time: 11:46 AM
 */
class Company extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(false, array('*'));
    }

    /**
     * Do company login for the current user
     *
     * @param $id
     * @return bool
     */
    public function login($id)
    {
        $company_id = (int)$id;
        // first of all check if user belongs to this company or not.
        $belongs = false;
        foreach ($this->auth->get()->companies as $company) {
            if ($company_id == $company->company_id) {
                $belongs = true;
                break;
            }
        }
        if ($belongs == false) {
            show_error('You do not belong to this company.', 200, 'Unauthorized access');
            return false; // exits the function
        }

        // save to database for later quick access
        $mysql_data = array(
            'last_company' => $company_id
        );
        $this->user_model->update($mysql_data, $this->auth->id());

        // no we can safely login to this company as this user belongs to this company.
        $this->session->set_userdata('company_id', $company_id);
        if ($this->input->get('ret') == 'assets') {
            $id = (int)$this->input->get('id');
            redirect(site_url('bckcadmin/assets/view/' . $id));
        } else {
            redirect('bckcadmin/home');
        }
    }

    /**
     * Company logout, not utilized yet.
     *
     * @param $id
     */
    public function logout($id)
    {
        $company_id = (int)$id;
        if ($this->auth->company()) {
            $this->session->unset_userdata('company_id', $company_id);
        }
        redirect('bckcadmin/home/welcome');
    }
}
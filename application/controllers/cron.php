<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/7/2015
 * Time: 5:52 PM
 */
class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->input->is_cli_request()) {
            //show_error('Server Admin Only.', '403', 'CLI Only');
            //exit;
        }
        $allowed_ips = $this->config->item('email_cron_allowed_ip');
        if (!in_array($this->input->ip_address(), $allowed_ips)) {
            show_error('Sorry you cant access this webpage.', '403', 'Not allowed');
            exit;
        }
        $this->load->model('cron_model');
        ignore_user_abort(true);
        set_time_limit(0);
    }
    public function create_exceptions()
    {
        // create exceptions
        $pending = $this->maintenance_model->getPendingMaintenanceExceptions();
        header('content-type: text/plain');
        echo 'Server Date: ' . date('Y-m-d g:i:s a') . PHP_EOL;
        echo 'Timezone Date: ' . $this->timezone->convertDate(date('Y-m-d H:i:s', time()), 'Y-m-d g:i:s a') . PHP_EOL;
        print_r($pending['entries']);
        foreach ($pending['entries'] as $entry) {
            $maintenance = $this->db->where('maintenance_id', $entry['maintenance_id'])
                ->get('maintenance')
                ->row();
            if ($maintenance && $maintenance->exception_created == 0) {
                // skip lower entries if exists
                $column = $this->maintenance_model->get_column($maintenance->maintenance_type);
                $skip_query = "
                update maintenance set exception_created = 1 where maintenance_id in(
                  select maintenance_id from (

                  select maintenance_id from maintenance
                    where asset_id = {$maintenance->asset_id}
                    and maintenance_type = {$maintenance->maintenance_type}
                    and exception_created = 0
                    and {$column} < '{$maintenance->$column}' order by
                    {$column} desc
                    ) as mtable
                    )
                ";
                $this->db->query($skip_query);

                $defaults = array(
                    'title' => 'Exception for Maintenance Entry #'.$maintenance->maintenance_id
                );
                // insert data
                $exception_mysql_data = array(
                    'company_id' => $maintenance->company_id,
                    'title' => $defaults['title'],
                    'status' => 'new',
                    'asset_id' => $maintenance->asset_id,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $maintenance->created_by,
                    'updated_by' => $maintenance->updated_by,
                    'created_user_id' => $maintenance->created_by,
                    'closed_user_id' => $maintenance->updated_by
                );
                $this->db->insert('exceptions', $exception_mysql_data);
                $exception_id = $this->db->insert_id();

                $comments = $entry['message'];

                // add to database
                $note_mysql_data = array(
                    'comments' => $comments,
                    'exception_id' => $exception_id,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $maintenance->created_by,
                    'updated_by' => $maintenance->updated_by
                );
                $this->db->insert('exception_notes', $note_mysql_data);
                $exception_note_id = $this->db->insert_id();

                // update as exception created
                $update_mysql_data = array(
                    'exception_created' => 1
                );
                $this->db->where('maintenance_id', $maintenance->maintenance_id)
                    ->update('maintenance', $update_mysql_data);

                $this->cron_model->createExceptionCommentEmail($exception_id, $exception_note_id, $maintenance->created_by);
            }
        }
    }

    public function process_exceptions()
    {
        $queue_length = (int)$this->config->item('email_queue_length');
        $email_queue = $this->db->where('status_sent', 0)
            ->limit($queue_length)
            ->get('cron_emails')
            ->result();

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        header('content-type: text/plain');
        echo 'Found: ' . count($email_queue) . ' emails to process.' . PHP_EOL;
        $sent = array();
        $failed = array();
        foreach ($email_queue as $single) {
            $content = $single->message;
            $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
            $this->email->to($single->to);
            if ($single->cc != '') {
                $this->email->cc($single->cc);
            }
            if ($single->bcc != '') {
                $this->email->bcc($single->bcc);
            }

            $this->email->subject($single->subject);
            $this->email->message($content);

            if ($this->email->send()) {
                $this->db->where('exception_id', $single->exception_id)
                    ->where('exception_note_id', $single->exception_note_id)
                    ->where('user_id', $single->user_id)
                    ->where('to', $single->to)
                    ->delete('cron_emails');
                $sent[] = $single;
            } else {
                $failed[] = $failed;
            }
        }

        if (count($sent) > 0) {
            echo 'Sent ' . count($sent) . ' Emails.' . PHP_EOL;
            foreach ($sent as $record) {
                echo 'to: ' . $record->to . PHP_EOL;
            }
        }

        if (count($failed) > 0) {
            echo 'Failed ' . count($failed) . ' Emails.' . PHP_EOL;
            foreach ($failed as $record) {
                echo 'to: ' . $record->to . PHP_EOL;
            }
        }

    }
}
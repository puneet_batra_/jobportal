<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 7/28/2015
 * Time: 4:18 PM
 */
class Employerrecruiter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(true);
        if ($this->auth->check()) {
            redirect('home');
        }
    }

    public function index()
    {
        if ($this->auth->verify_remember_me()) {

            redirect('home');
        }
        $this->load->library('form_validation');
        $validator = $this->form_validation;
        $validator->set_rules('email', 'Email', 'required|valid_email');
        $validator->set_rules('password', 'Password', 'required|min_length[4]|callback_validate_login');

        if ($validator->run() == false) {
            $return_url = $this->input->get('return_url');
            if ($return_url != '') {
                $return_url = '?return_url=' . urlencode($return_url);
            }

            $data = array(
                'active_form' => 'login-form',
                'return_url' => $return_url,
                'user_type' =>$this->db->get('user_type')->result()
            );
            $this->load->view('userlogin', $data);
        } else {
            if ($this->input->post('remember') == 1) {
                $this->auth->set_remember_me();
            } else {
                delete_cookie('remember_me_token');
            }
            if ($this->input->get('return_url') && $this->input->get('return_url') != $_SERVER['REQUEST_URI']) {
                redirect($this->input->get('return_url'));
            }
            redirect('home');
        }
    }
    public function validate_login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $credentials = array(
            'email' => $email,
            'password' => md5($password),
            'active' => 1,
            'is_superuser >' => 0
        );

        if ($this->auth->login($credentials)) {
            return true;
        } else {
            $this->form_validation->set_message('validate_login', 'Invalid credentials');
            return false;
        }
    }


    public function register()
    {

        $this->load->library('form_validation');
        $validator = $this->form_validation;
        $validator->set_rules('first_name', 'First Name', 'required|alpha');
        $validator->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $validator->set_rules('user_type', 'User Type', 'required');
        $validator->set_rules('company_name', 'Company Name', 'required');
        $validator->set_rules('recruit_industry', 'Industry', 'required');
        $validator->set_rules('address', 'Address', '');
        $validator->set_rules('country', 'Country', 'required');
        $validator->set_rules('state', 'State', 'required');
        $validator->set_rules('city', 'City', 'required');
        $validator->set_rules('phone', 'Phone', 'required');
        $validator->set_rules('password', 'Password', 'required');
        //$validator->set_rules('confirm_password', 'Repeat password', 'required|matches[password]');
        //$validator->set_rules('recaptcha_response_field', 'Rechaptcha code', 'required|callback_validate_recaptcha');


        if ($validator->run() == false) {
            $data = array(
                'active_form' => 'register-form',

            );
            $data['countries'] = $this->db->where('location_type','0')->get('location')->result();
            $data['job_industry'] = $this->db->where('active',1)->get('job_industry')->result();
            $this->load->view("employer_recruiter_signup",$data);

        } else {
            // Proceed Registration
            $mysql_data = array(
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'recruit_industry' => $this->input->post('recruit_industry'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'user_type' => $this->input->post('user_type'),
                'phone' => $this->input->post('phone'),
                'first_name' => $this->input->post('first_name'),
                'active' => 1,
                'is_verified' => 0,
                'is_superuser' => 0,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s')
            );
            echo $this->db->insert('users', $mysql_data);
            $user_id = $this->user_model->create($mysql_data);

            $mysql_data = array(
                'created_by' => $user_id,
                'updated_by' => $user_id
            );
            $this->user_model->update($mysql_data, $user_id);
            // auto login after register
            $this->auth->loginUsingId($user_id);
            $this->session->set_flashdata('success', 'Welcome to your new account, You have been registered successfully.');
            redirect('employerrecruiters/profile');

        }
    }
}
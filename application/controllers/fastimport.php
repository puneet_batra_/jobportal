<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/30/2015
 * Time: 1:10 PM
 */
class Fastimport extends CI_Controller
{
    public function start()
    {
        ?>
        <form action="<?php echo site_url('fastimport/parse') ?>" enctype="multipart/form-data" method="post">
            <label for="">Upload csv (Max: <?php echo ini_get("upload_max_filesize"); ?>)</label>
            <input type="file" name="file" id=""/>
            <input type="submit" value="Submit"/>
        </form>
        <?php
    }

    public function parse()
    {
        // user configurable fields to column number relation
        $fields = array(
            'email' => 0,
            'first_name' => 1,
            'last_name' => 2,
            'password' => 3,
            'company_id' => 4,
            'role_id' => 5
        );

        // start script processing
        set_time_limit(0);
        ignore_user_abort(true);
        if (isset($_FILES['file']) && $_FILES['file']['name'] != '') {
            $file_data = file_get_contents($_FILES['file']['tmp_name']);

            // process rawdata
            $rows = explode("\r", $file_data);
            // storage of actual data found in the csv
            $rowdata = array();
            if (count($rows) == 1) {
                // may be \n formatted
                $rows = explode("\n", $file_data);
            }
            foreach ($rows as $row) {
                $regex = "/([^,]+)/i";
                preg_match_all($regex, $row, $matches);
                $rowdata[] = $matches[1];
            }

            // start process of insertion
            $fails = array();
            $existing = array();
            $skipped = array();
            foreach ($rowdata as $record_id => $record) {
                if (filter_var($record[$fields['email']], FILTER_VALIDATE_EMAIL) === false) {
                    // email not valid ignore the record.
                    $skipped[] = $record_id;
                    continue;
                }
                $user = $this->db->where('email', $record[$fields['email']])
                    ->get('users')->row();
                $invite = $this->db->where('email', $record[$fields['email']])
                    ->where('company_id', $record[$fields['company_id']])
                    ->get('invites')->row();
                if ($record[$fields['password']] == '') {
                    $password = rand(111111, 9999999);
                } else {
                    $password = $record[$fields['password']];
                }
                $mysql_data_users = array(
                    'email' => $record[$fields['email']],
                    'first_name' => $record[$fields['first_name']],
                    'last_name' => $record[$fields['last_name']],
                    'password' => md5($password),
                    'active' => 1,
                    'is_superuser' => 0,
                    'is_verified' => 1,
                    'last_company' => $record[$fields['company_id']],
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'deleted_on' => '0000-00-00 00:00:00',
                    'created_by' => 0,
                    'updated_by' => 0,
                    'deleted_by' => 0
                );
                if (!$user) {
                    $this->db->insert('users', $mysql_data_users);
                    $user_id = $this->db->insert_id();
                    $mysql_data_users['user_id'] = $user_id;
                    $mysql_data_users['password_plain'] = $password;
                } else {
                    $user_id = $user->user_id;
                    $mysql_data_users['user_id'] = $user_id;
                    $mysql_data_users['password_plain'] = '(Current Password)';
                }

                // add to invites table
                $mysql_data_invite = array(
                    'first_name' => $record[$fields['first_name']],
                    'last_name' => $record[$fields['last_name']],
                    'email' => $record[$fields['email']],
                    'user_id' => $user_id,
                    'company_id' => $record[$fields['company_id']],
                    'accepted' => 1,
                    'role_id' => $record[$fields['role_id']],
                    'invited_by' => 0,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => 0,
                    'updated_by' => 0
                );
                if (!$invite) {
                    $this->db->insert('invites', $mysql_data_invite);
                    $invite_id = $this->db->insert_id();
                    $mysql_data_invite['invite_id'] = $invite_id;
                } else {
                    $invite_id = $invite->invite_id;
                    $mysql_data_invite['invite_id'] = $invite_id;
                }
                // user roles
                $user_role = $this->db->where('role_id', $record[$fields['role_id']])
                    ->where('user_id', $user_id)
                    ->get('user_roles')
                    ->row();
                $mysql_data_user_role = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'role_id' => $record[$fields['role_id']],
                    'user_id' => $user_id
                );
                if (!$user_role) {
                    $this->db->insert('user_roles', $mysql_data_user_role);
                    $user_role_id = $this->db->insert_id();
                } else {
                    $user_role_id = $user_role->user_role_id;
                }


                if ($user && $invite && $user_role) {
                    $existing[] = $record_id;
                } else {
                    if ($this->send_invite($mysql_data_invite, $mysql_data_users) == false) {
                        $fails[] = $record_id;
                        $this->db->where('invite_id', $invite_id)->delete('invites');
                        $this->db->where('user_id', $user_id)->delete('users');
                        $this->db->where('user_role_id', $user_role_id)->delete('user_roles');
                    }
                }
            }
            echo '<h3>Email sending completed.</h3>';
            if (count($skipped) > 0) {
                echo "<p>The following record rows were skipped, due to invalid fields:</p>";
                foreach ($skipped as $skipped_record_id) {
                    echo '<div style="margin: 5px">'. $skipped_record_id . ' => ' . implode(', ', $rowdata[$skipped_record_id]) . '</div>';
                }
            }

            if (count($existing) > 0) {
                echo "<p>The following record rows were skipped because they already exists:</p>";
                foreach ($existing as $existing_record_id) {
                    echo '<div style="margin: 5px">'. $existing_record_id . ' => ' . implode(', ', $rowdata[$existing_record_id]) . '</div>';
                }
            }

            if (count($fails) > 0) {
                echo "<p>The following record rows were failed to send email:</p>";
                foreach ($fails as $failed_record_id) {
                    echo '<div style="margin: 5px">'. $failed_record_id . ' => ' . implode(', ', $rowdata[$failed_record_id]) . '</div>';
                }
            }
        } else {
            show_error('please upload a file', '200', 'Error');
        }
    }

    public function send_invite($invite_data, $user_data)
    {
        $company = $this->db->where('company_id', $invite_data['company_id'])->get('companies')->row();
        //$md5_sum = md5($invite_data['created_on']);
        //$link = site_url('invite/accept/'. $invite_data['invite_id'] . '/' . $md5_sum);
        $link = site_url('login');
        $data = array(
            'link' => $link,
            'user' => $user_data,
            'company' => $company
        );
        $content = $this->load->view('emails/user.invite.register.script.php', $data, true);


        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($invite_data['email']);

        $this->email->subject('Welcome to AirApp!');
        $this->email->message($content);

        if ($this->email->send() == true) {
            return true;
        } else {
            return false;
        }
    }
}
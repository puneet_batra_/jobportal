<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 7/28/2015
 * Time: 3:56 PM
 */
class Home extends MY_Controller
{
    public function __construct()
    {
        // skip verification and handle manually in index function.
        parent::__construct(true);
    }

    public function index()
    {
        if ($this->auth->check()) {

            /*if ($this->auth->is_superuser() || $this->auth->is_superadmin()) {

               // redirect('admin/home');
            } else {

                redirect('bckcadmin');
            }*/
            if ($this->auth->is_superuser() || $this->auth->is_superadmin()){

                redirect('admin/home');

            }
            if ($this->auth->is_employerrecruiter()) {

                redirect('employerrecruiters/profile');

            } else if($this->auth->is_jobseeker()) {

                redirect('jobseekers/profile');
            }
            else {

                redirect('login');
            }

        }

    }
}
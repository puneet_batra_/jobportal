<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/24/2015
 * Time: 2:31 PM
 */
class Image extends CI_Controller
{
    public function resize($string, $segments)
    {
        $this->load->library('image_moo');
        if (!file_exists($string)) {
            $string = 'public/img/default_image.jpg';
        }
        $this->image_moo->load($string);
        if ($this->input->get('s') || $this->input->get('size')) {
            $size = ($this->input->get('s')) ? $this->input->get('s') :$this->input->get('size');
            list($width, $height) = explode('x', $size);

            if ($this->input->get('method') == 'crop') {
                $this->image_moo->resize_crop($width, $height);
            } else {
                $this->image_moo->resize($width, $height);
            }
        }
        if ($this->input->get('round')) {
            $this->image_moo->round((int)$this->input->get('round'));
        }
        $this->image_moo->save_dynamic();
    }

    public function _remap($method, $params = array())
    {
        $raw_params = array_merge(array($method), $params);
        $pass_params = array(implode('/', $raw_params), $raw_params);
        return call_user_func_array(array($this, 'resize'), $pass_params);
    }
}
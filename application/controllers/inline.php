<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/5/2015
 * Time: 3:05 PM
 */
class Inline extends MY_Controller
{
    public function __construct()
    {
        // CSS, JS are publically accessible
        parent::__construct(true);
    }
    public function asset()
    {
        $args = func_get_args();
        $filename = end($args);
        $ext = end(explode('.', $filename));

        $path = __DIR__ . '/../views/' . $this->dyanamic_assets_path . implode('/', $args) . '.inline.php';

        ob_start();
        include($path);
        $contents = ob_get_clean();

        if (file_exists($path)) {

            $mimes = array(
                'css' => 'text/css',
                'js' => 'text/javascript',
                'jpg' => 'image/jpg',
                'jpeg' => 'image/jpeg',
                'png' => 'image/png'
            );


            $contents = str_replace('<script>', '', $contents);
            $contents = str_replace('</script>', '', $contents);
            $contents = str_replace('<style>', '', $contents);
            $contents = str_replace('</style>', '', $contents);

            if (in_array($ext, array('css'))) {
                $contents = $this->compress($contents);
            }

            header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + $this->config->item('cache_length') ));
            header("Cache-Control: max-age=0, must-revalidate");
            header('Content-Type: '. $mimes[$ext]);

            $last_modified_time = filemtime($path);
            $etag = md5_file($path);

            header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
            header("Etag: $etag");

            if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
                ob_start("ob_gzhandler");
            } else {
                ob_start();
            }
            echo $contents;
            ob_flush();
        }
    }

    function compress($output)
    {
        ini_set("pcre.recursion_limit", "16777");
        $buffer = $output;

        $re = '%# Collapse whitespace everywhere but in blacklisted elements.
        (?>             # Match all whitespans other than single space.
          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
        | \s{2,}        # or two or more consecutive-any-whitespace.
        ) # Note: The remaining regex consumes no text at all...
        (?=             # Ensure we are not in a blacklist tag.
          [^<]*+        # Either zero or more non-"<" {normal*}
          (?:           # Begin {(special normal*)*} construct
            <           # or a < starting a non-blacklist tag.
            (?!/?(?:textarea|pre|script)\b)
            [^<]*+      # more non-"<" {normal*}
          )*+           # Finish "unrolling-the-loop"
          (?:           # Begin alternation group.
            <           # Either a blacklist start tag.
            (?>textarea|pre|script)\b
          | \z          # or end of file.
          )             # End alternation group.
        )  # If we made it here, we are not in a blacklist tag.
        %Six';

        $new_buffer = preg_replace($re, " ", $buffer);

        // We are going to check if processing has working
        if ($new_buffer === null)
        {
            $new_buffer = $buffer;
        }

        return $new_buffer;
    }

}
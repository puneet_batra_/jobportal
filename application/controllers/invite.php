<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/18/2015
 * Time: 5:44 PM
 */
class Invite extends CI_Controller
{
    public function accept($encoded_invite_id = 0, $md5sum = '')
    {
        $invite_id = (int)$encoded_invite_id;
        $invite = $this->db->where('invite_id', $invite_id)->get('invites')->row();
        if ($invite) {
            if (md5($invite->created_on) == $md5sum) {
                // we have a legit invite
                if ($this->auth->check()) {
                    // user is logged in check if its for him.
                    if ($this->auth->id() == $invite->user_id || $this->auth->get()->email == $invite->email) {
                        // yes its for logged in user, create the roles
                        $mysql_data = array(
                            'role_id' => $invite->role_id,
                            'user_id' => $this->auth->id(),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('user_roles', $mysql_data);

                        $mysql_data = array(
                            'accepted' => 1,
                            'user_id' => $this->auth->id()
                        );
                        $this->db->update('invites', $mysql_data, array('invite_id' => $invite->invite_id));

                        $this->session->set_userdata('company_id', $invite->company_id);
                        redirect('bckcadmin/home');
                    } else {
                        // this is not same user that was invited
                        show_error('This invite link is not for logged in account.', 403, 'Invalid invite');
                        exit;
                    }
                } else {
                    // user is not logged in, so we can save this verification for later use
                    $this->session->set_userdata('invite_id', $invite->invite_id);
                    if ($invite->user_id > 0) {
                        redirect('login');
                    } else {
                        redirect('register');
                    }
                }
            } else {
                // this may be a tampered invite, block it.
                show_error('This is invalid invite link.', 403, 'Invalid invite');
                exit;
            }
        } else {
            show_error('This is invalid invite link.', 403, 'Invalid invite');
            exit;
        }
    }

    public function do_accept()
    {
        $invite_id = (int)$this->input->post('rid');
        $invite = $this->db->where('invite_id', $invite_id)->get('invites')->row();
        if ($invite) {
            if (
                $this->auth->is_superuser()
                || $this->auth->is_superadmin()
                || $this->auth->is_emulated_by_superuser()
                || $this->auth->is_emulated_by_superadmin()
            ) {
                // we have a legit invite
                // yes its for existing user, create the roles

                $mysql_data = array(
                    'role_id' => $invite->role_id,
                    'user_id' => $invite->user_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('user_roles', $mysql_data);

                $mysql_data = array(
                    'accepted' => 1
                );
                $this->db->update('invites', $mysql_data, array('invite_id' => $invite->invite_id));

                $data = array(
                    'status' => 'success',
                    'message' => 'Invited accepted successfully.',
                    'errors' => ''
                );
                echo json_encode($data);
                return false;

            } else {
                // this may be a tampered invite, block it.
                $data = array(
                    'status' => 'fail',
                    'message' => 'There were errors',
                    'errors' => array(
                        'no_permission' => 'You do not have permission to accept invite or session logged out.'
                    )
                );
                echo json_encode($data);
                return false;
            }
        } else {
            $data = array(
                'status' => 'fail',
                'message' => 'There were errors',
                'errors' => array(
                    'no_permission' => 'Invite is not valid.'
                )
            );
            echo json_encode($data);
            return false;
        }
    }
}
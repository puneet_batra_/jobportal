<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/5/2015
 * Time: 4:26 PM
 */
class Login extends MY_Controller
{
    public function __construct()
    {
        // this is a public page so, pass true to show this page.
        parent::__construct(true);
        // redirect if already logged in
        if ($this->auth->check()) {

            redirect('home');
            exit;
        }
    }
    public function index()
    {
        if ($this->auth->verify_remember_me()) {

            redirect('home');
        }
        $this->load->library('form_validation');
        $validator = $this->form_validation;
        $validator->set_rules('email', 'Email', 'required|valid_email');
        $validator->set_rules('password', 'Password', 'required|min_length[4]|callback_validate_login');

        if ($validator->run() == false) {
            $return_url = $this->input->get('return_url');
            if ($return_url != '') {
                $return_url = '?return_url=' . urlencode($return_url);
            }
            $data = array(
                'active_form' => 'login-form',
                'return_url' => $return_url
            );
            $this->load->view('login', $data);
        } else {
            if ($this->input->post('remember') == 1) {
                $this->auth->set_remember_me();
            } else {
                delete_cookie('remember_me_token');
            }
            if ($this->input->get('return_url') && $this->input->get('return_url') != $_SERVER['REQUEST_URI']) {
                redirect($this->input->get('return_url'));
            }
            redirect('home');
        }
    }

    public function email()
    {
        $this->user_model->send_email_signup_password(15, 'Google', $this->user_model->getById(15), 'Tesxsf');

        echo $this->email->print_debugger();
    }

    public function validate_login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $credentials = array(
            'email' => $email,
            'password' => md5($password),
            'active' => 1,
            'is_superuser >' => 0
        );

        if ($this->auth->login($credentials)) {
            return true;
        } else {
            $this->form_validation->set_message('validate_login', 'Invalid credentials');
            return false;
        }
    }

    public function forgot()
    {
        $this->load->library('form_validation');
        $validator = $this->form_validation;
        $this->form_validation->set_rules('email_forgot', 'Email', 'required|valid_email|callback_verify_forgot_email');

        if ($validator->run() == false) {
            $data = array(
                'active_form' => 'forget-form'
            );
            $this->load->view('login', $data);
        } else {
            $this->session->set_flashdata('success', 'Email sent to your email address.');
            redirect('login/index');
        }
    }

    public function verify_forgot_email($email)
    {
        $user = $this->db->where('email', $email)->get('users')->row();
        if ($user) {
            if ($user->active == 0) {
                $this->form_validation->set_message('verify_forgot_email', 'Your account is disabled, please contact administrator');
                return false;
            } else {
                $this->user_model->send_email_forgot($user);
                return true;
            }
        } else {
            $this->form_validation->set_message('verify_forgot_email', 'No associated account found.');
            return false;
        }
    }

    public function recover($id, $secret)
    {
        $user = $this->db->where('user_id' , $id)->get('users')->row();
        if ($user) {
            if (md5($user->updated_on) == $secret) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');
                $this->form_validation->set_rules('rpassword', 'Repeat Password', 'required|matches[password]');
                if ($this->form_validation->run() == false) {
                    // show password change form
                    $data = array(
                        'user' => $user,
                        'id' => $id,
                        'secret' => $secret
                    );
                    $this->load->view('login.recover.php', $data);
                } else {
                    // now change the password.
                    $mysql_data = array(
                        'password' => md5($this->input->post('password')),
                        'updated_by' => $user->user_id
                    );
                    $this->user_model->update($mysql_data, $user->user_id);
                    $this->auth->loginUsingId($user->user_id);
                    redirect('home');
                }
            } else {
                show_error('Invalid reset password link', 200, 'Reset password');
            }
        } else {
            show_error('Invalid request', 200, 'Error 404');
        }
    }


}
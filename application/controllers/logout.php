<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 5:25 PM
 */
class Logout extends CI_Controller
{
    public function index()
    {
        $return_login_id = (int)$this->session->userdata('return_login_id');
        $this->auth->logout();
        //$this->session->sess_destroy();
        if ($return_login_id > 0) {
            $this->auth->loginUsingId($return_login_id);
            $this->session->unset_userdata('return_login_id');
            redirect('home');
        } else {
            delete_cookie('remember_me_token');
        }
        $return_url = '';
        if ($this->input->get('return_url')) {
            $return_url = '?return_url=' . $this->input->get('return_url');
        }
        redirect('login' .$return_url);
    }
    public function userlogout(){
        $user_type = $this->session->userdata('user_type');
        echo $user_type;
        $this->auth->logout();
        redirect('welcome');

    }

}
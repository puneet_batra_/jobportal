<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 12:32 PM
 */
class Register extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(true);
        if ($this->auth->check()) {
            redirect('home');
        }
    }
    public function index()
    {

        $this->load->library('form_validation');
        $validator = $this->form_validation;

        $validator->set_rules('first_name', 'First Name', 'required|alpha');

        $validator->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');

        $validator->set_rules('user_type', 'User Type', 'required');
        $validator->set_rules('company_name', 'Company Name', 'required');
        $validator->set_rules('recruit_industry', 'Industry', 'required');

        $validator->set_rules('address', 'Address', '');
        $validator->set_rules('city', 'City', 'alpha');
        $validator->set_rules('country', 'Country', 'required');
        $validator->set_rules('state', 'State', 'required');
        $validator->set_rules('phone', 'Phone', 'required');
        $validator->set_rules('password', 'Password', 'required|min_length[4]');
        $validator->set_rules('confirm_password', 'Repeat password', 'required|matches[password]');
        //$validator->set_rules('recaptcha_response_field', 'Rechaptcha code', 'required|callback_validate_recaptcha');
        $validator->set_rules('tnc', 'Terms and Conditions', 'required');

        if ($validator->run() == false) {
            $data = array(
                'active_form' => 'register-form'
            );
            $this->load->view('login', $data);
        } else {
            // Proceed Registration
            $mysql_data = array(
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'confirm_password' => $this->input->post('confirm_password'),
                'industry' => $this->input->post('recruit_industry'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'phone' => $this->input->post('phone'),
                'first_name' => $this->input->post('first_name'),
                'tnc' => $this->input->post('tnc'),
                'active' => 1,
                'is_verified' => 0,
                'is_superuser' => 0,
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->insert('users', $mysql_data);
            $user_id = $this->user_model->create($mysql_data);

            $mysql_data = array(
                'created_by' => $user_id,
                'updated_by' => $user_id
            );
            $this->user_model->update($mysql_data, $user_id);
            // auto login after register
            $this->auth->loginUsingId($user_id);
            $this->session->set_flashdata('success', 'Welcome to your new account, You have been registered successfully.');
            redirect('bckcadmin');
        }
    }

    public function validate_recaptcha()
    {
        return true;
        // Get a key from https://www.google.com/recaptcha/admin/create
        $publickey = $this->config->item('google_captcha_public_key');
        $privatekey = $this->config->item('google_captcha_private_key');

        # the response from reCAPTCHA
        $resp = null;
        # the error code from reCAPTCHA, if any
        $error = null;
        $resp = recaptcha_check_answer ($privatekey,
            $_SERVER["REMOTE_ADDR"],
            $_POST["recaptcha_challenge_field"],
            $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {
            return true;
        } else {
            # set the error code so that we can display it
            $error = $resp->error;
            $this->form_validation->set_message('validate_recaptcha', 'Invalid captcha code, please retry again.');
            return false;
        }
    }
}
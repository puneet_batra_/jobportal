<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 7/29/2015
 * Time: 10:34 AM
 */
class Registration extends MY_Controller
{
    public function __construct()
    {
        parent::__construct(true);
        if ($this->auth->check()) {

            redirect('home');
            exit;
        }

    }
    public function index(){
        $this->load->view('register');
    }
    public function jobseeker_signup(){

        $data['countries'] = $this->db->where('location_type','0')->get('location')->result();
        $this->load->view('jobseeker_signup',$data);
    }
    public function employer_recruiter_signup(){

        $data['countries'] = $this->db->where('location_type','0')->get('location')->result();
        $data['job_industry'] = $this->db->where('active',1)->get('job_industry')->result();
        $this->load->view('employer_recruiter_signup',$data);
    }
    public function state()
    {

        $states = $this->db->where('parent_id', $this->input->get('id'))->get('location')->result();

        foreach ($states as $state) {
            echo $state_list = "<option value='" . $state->location_id . "'>" . $state->name . "</option>";
        }
    }

    public function city()
    {
        $cities = $this->db->where('parent_id', $this->input->get('id'))->get('location')->result();
        foreach ($cities as $city) {
            echo $state_list = "<option value='" . $city->location_id . "'>" . $city->name . "</option>";
        }
    }

    public function getIplocation()
    {
        $country = $this->input->post('country');

        $location = $this->db->where('location_id', $country)->get('location')->row();
        $country_name = substr($location->name, 0, 2);
        $final_country = strtoupper($country_name);

        $ip = $_SERVER['REMOTE_ADDR'];
        $details = json_decode(file_get_contents("http://ipinfo.io/"));

        if ($final_country == $details->country) {
            echo 'true';
        } else {

            echo 'false';
        }
    }

}
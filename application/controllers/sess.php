<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/10/2015
 * Time: 4:02 PM
 */
class Sess extends CI_Controller
{
    public function index()
    {
        if ($this->auth->check()) {
            echo 'OK';
        } else {
            echo 'FALSE';
        }
    }

    public function userdata()
    {
        $this->session->set_userdata('user_timezone', $this->input->post('timezone'));
    }
}
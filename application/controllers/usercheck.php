<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 7/31/2015
 * Time: 12:30 PM
 */
class Usercheck extends MY_Controller{
    public function __construct()
    {
        // skip verification and handle manually in index function.
        parent::__construct(true);
    }
    public function index(){
        $this->load->view('usercheck');
    }
    public function check($user_type){

        $this->session->unset_userdata('user_type');

        $user_id = $this->session->userdata('user_id');
        $mysql_data = array(
            'updated_on' => date('Y-m-d H:i:s'),
            'user_type' => $user_type
        );


        if($this->db->where('user_id',$user_id)->update('users', $mysql_data)){
           $this->session->set_userdata('user_type',$user_type);
           $this->session->userdata('user_type');
            redirect('home');
        }

    }
}
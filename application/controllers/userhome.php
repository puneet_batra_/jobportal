<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 7/31/2015
 * Time: 11:35 AM
 */

class Userhome extends MY_Controller
{
    public function __construct()
    {
        // skip verification and handle manually in index function.
        parent::__construct(true);
    }

    public function index()
    {
        if ($this->auth->usercheck()) {

            if ($this->auth->is_employerrecruiter()) {

                redirect('employerrecruiters/profile');
            } if($this->auth->is_jobseeker()) {

                redirect('jobseekers/profile');
            }
        } else {
            redirect('userlogin');
        }
    }
}
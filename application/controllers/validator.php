<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/9/2015
 * Time: 11:50 AM
 */
class Validator extends CI_Controller
{
    public function validate()
    {
        $this->load->library('form_validation');
        $postdata = $this->input->post();
        $rules = $postdata['rules'];
        unset($postdata['rules']);
        list($key, $value) = each($postdata);
        $this->form_validation->set_rules($key, $key, $rules);
        if ($this->form_validation->run() == false) {
            echo 'false';
        } else {
            echo 'true';
        }

    }

    public function validate_odometer($odometer)
    {
        $odometer = (int)$odometer;
        $asset_id = (int)$this->input->post('asset_id');
        $company_id = (int)$this->auth->company_id();

        $last_entry = $this->db->where('company_id', $company_id)
            ->where('asset_id', $asset_id)
            ->order_by('odometer', 'desc')
            ->limit(1)
            ->get('fuel')
            ->row();
        if ($last_entry) {
            if ($odometer > $last_entry->odometer) {
                return true;
            } else {
                $this->form_validation->set_message('validate_odometer', 'Odometer value cannot be less than ' . $last_entry->odometer);
                return false;
            }
        } else {
            // no previous entry exists, no need to validate
            return true;
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/5/2015
 * Time: 5:37 PM
 */
class Welcome extends MY_Controller
{
    public function __construct()
    {
        // skip verification and handle manually in index function.
        parent::__construct(true);
    }
    public function index()
    {

        $data['content']=$this->homepage->get();  
        $data['page']=$this->db->where('active','1')->get('pages')->result();          
        $this->load->view('index',$data);
    }
   /* public function register(){
        $this->load->view('register');
    }*/
   /* public function employersignup(){
        $this->load->view('employersignup');
    }
    public function jobseekersignup(){
        $this->load->view('jobseekersignup');
    }*/
}
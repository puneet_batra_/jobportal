<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/5/2015
 * Time: 12:54 PM
 */
class MY_Controller extends CI_Controller
{
    public $css = '';
    public $js = '';
    public $is_public_page = false;
    protected $dyanamic_assets_path = '_assets/'; // Pick page dependent assets from views/{path}/
    protected $permission_exceptions = array(); // Collection of methods to skip verification
    protected $permission_mode = 'auto';

    /**
     * Constructor for MY_Controller
     * @param bool $is_public_page
     * @param array $permission_exception
     */
    public function __construct($is_public_page = false, $permission_exception = array())
    {
        parent::__construct();
        // set if page is public and permission exceptions if not public page
        $this->is_public_page = $is_public_page;
        $this->permission_exceptions = $permission_exception;

        // automatically load dynamic css and js files from views/_assets folder for each page.
        $this->_load_assets();

        // apply roles and permissions
        $this->check_permissions();

        // expire the caches to continue development.

        $this->output->unset_header('X-Powered-By');
    }

    /**
     * Generates dynamic path for automatic css and js files loading
     * @return string
     */
    protected function get_dynamic_path()
    {
        $view = (!empty($this->view)) ? $this->view : $this->router->class . '.' . $this->router->method;
        return __DIR__ . '/../views/' . $this->dyanamic_assets_path . $this->router->directory . $view;
    }

    /**
     * Loads dynamic assets files for each page if exists
     * This works as a block of content, as used in template engines.
     */
    protected function _load_assets()
    {
        if (file_exists($this->get_dynamic_path() . '.css.php')) {
            ob_start();
            include($this->get_dynamic_path() . '.css.php');
            $this->css = ob_get_clean();
        }
        if (file_exists($this->get_dynamic_path() . '.js.php')) {
            ob_start();
            include($this->get_dynamic_path() . '.js.php');
            $this->js = ob_get_clean();
        }

    }

    /**
     * Function that applies roles and permissions
     * @return bool
     */
    protected function check_permissions()
    {
        if ($this->is_public_page == false) {
            // if this is NOT a publically accessible page
            if (!$this->auth->check()) {

                // if not logged in redirect to login

                $uri = $this->uri->segment(1);
                if($uri == 'admin'){
                    redirect('login' . '?return_url=' . urlencode($_SERVER['REQUEST_URI']));
                    exit;
                }
                else{
                    // if not logged in redirect to login

                    redirect('welcome' . '?return_url=' . urlencode($_SERVER['REQUEST_URI']));
                    exit;
                }

            }

            // do not allow company user to access any page of admin
            if (
                !$this->auth->is_superadmin()
                && $this->uri->segment(1) == 'admin'
                && !$this->auth->is_superuser()
            ) {
                redirect('bckcadmin/home');
                return false; // exits this function
            }

            // otherwise check if user is allowed to view the page, superadmins are always allowed
            if ($this->auth->is_allowed(false, $this->router->class, $this->router->method, true)) {

            } else {
                // if there are exceptions specified then check
                if (count($this->permission_exceptions) > 0) {
                    // * means all methods of this controller are allowed if logged in
                    if ($this->permission_exceptions[0] == '*') {
                        return false; // exits this function
                    }

                    // allow access to only specific methods of this controller
                    if (in_array($this->router->method, $this->permission_exceptions)) {
                        // method was specified to be not to block
                        $this->permission_mode = 'manual';
                        return true;
                    }
                }
                // permission exceptions was not defined
                if ($this->input->is_ajax_request()) {
                    // for ajax request this should be manually processed,
                    // so will allow here and will handle in method definition
                    $this->permission_mode = 'manual';
                    return true; // exits the function without blocking
                } else {
                    // this is not ajax request so we will apply block filter to it.
                    $this->_apply_permissions();
                    return false; // exists the function
                }
            }
        }
    }

    /**
     * Permissions check helper function to check where manual processing is required
     *
     * @param $level
     * @param string $action_name
     * @return bool
     */
    public function has_permission($level, $action_name = '')
    {
        // if mode is manual then this should be ajax call
        if ($this->permission_mode == 'manual') {
            // check how much permission is allowed for this controller and method
            $has_permissions = $this->auth->get_permissions_by_controller($this->router->class, $action_name);

            // if user has more or equal access as required, allow him
            if ($has_permissions >= $level) {
                return true;
            } else {
                return false;
            }
        } else {
            // otherwise access denied
            return $this->_apply_permissions();
        }
    }

    /**
     * This function is a callback when permission is denied
     * It handles how the access should be denied.
     */
    protected function _apply_permissions()
    {
        // this is not ajax call, but if it is superadmin, then allow him.
        if ($this->auth->is_superadmin()) {
            return true;
        } else if ($this->auth->is_superuser()) {
            // logged in user is superuser with permission not allowed
            // case when logged in user is also a company admin
            if ($this->auth->is_companyadmin()) {
                return true;
            } else {
                show_error('Not authorized', '200', 'Permission Required');
                return false;
            }
        } else if ($this->auth->is_companyadmin()) {
            // if logged in user is admin of current logged in company
            return true;
        } else {
            // in case user is company user
            if ($this->auth->company_id() == false) {
                // he has not selected a company, he should go to empty dashboard.
                redirect('bckcadmin/home/welcome');
                return false;
            } else {
                // if he is in company and tries to do anything funny, kill him (No access).
                show_error('Not authorized', '200', 'Permission Required');
                return false;
            }
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/28/2015
 * Time: 12:47 PM
 */
class MY_Output extends CI_Output
{
    public function unset_header($header_name)
    {
        if (!headers_sent()) {
            header_remove($header_name);
        }
    }
}
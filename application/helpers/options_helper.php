<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 11:01 AM
 */

function get_option($option_name = ''){
    $ci =& get_instance();
    $option = $ci->db->where('option', $option_name)
        ->get('options')
        ->row();

    if ($option) {
        return $option->value;
    } else {
        return '';
    }
}

function set_option($option_name = '', $value = ''){
    $ci =& get_instance();
    $option = $ci->db->where('option', $option_name)
        ->get('options')
        ->row();

    if ($option) {
        return $option->value;
    } else {
        return '';
    }
}
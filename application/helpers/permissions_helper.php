<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/17/2015
 * Time: 2:45 PM
 */

function is_allowed($level = 0, $controller='', $action='', $sub_module='')
{
    $ci =& get_instance();
    $company_id = (int)$ci->session->userdata('company_id');
    $user_id = (int)$ci->session->userdata('user_id');
    $company = $ci->db->where('company_id', $company_id)
        ->where('user_id', $user_id)
        ->get('companies')
        ->row();
    // super admin has all the access
    if ($ci->auth->is_superadmin()) {
        return true;
    }
    if ($ci->auth->is_companyadmin() || $company)
    {
        return true;
    }
    // otherwise check for permissions.
    $permission_available = $ci->auth->get_permissions_by_controller($controller, $action, true);
    if ($sub_module != '') {
        if (isset($permission_available->sub_permissions)) {
            $sub_permissions = (array)json_decode($permission_available->sub_permissions, true);
            if (in_array($sub_module, array_keys($sub_permissions))) {
                if ($sub_permissions[$sub_module] >= $level) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    if ($permission_available && $permission_available->permissions >= $level) {
        return true;
    } else {
        return false;
    }
}
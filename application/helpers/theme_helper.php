<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/5/2015
 * Time: 2:58 PM
 */

function get_page_css()
{
    $ci =& get_instance();
    return $ci->session->CI->css;
}

function get_page_js()
{
    $ci =& get_instance();
    return $ci->session->CI->js;
}

function public_url($path = '', $append_str = true)
{
    $ci =& get_instance();
    $q_exploded = explode('?', $path);
    if (count($q_exploded) == 1) {
        // no query string appended
        $append = '?v=' . $ci->config->item('asset_version');
    } else {
        $append = '&v=' . $ci->config->item('asset_version');
    }
    if ($append_str == false) {
        $append = '';
    }
    return base_url('public/' . $path) . $append;
}

function asset_url($path = '')
{
    $ci =& get_instance();
    $q_exploded = explode('?', $path);
    if (count($q_exploded) == 1) {
        // no query string appended
        $append = '?v=' . $ci->config->item('asset_version');
    } else {
        $append = '&v=' . $ci->config->item('asset_version');
    }
    return site_url($path) . $append;
}
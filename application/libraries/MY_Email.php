<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/23/2015
 * Time: 12:38 PM
 */
class MY_Email extends CI_Email {

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public function send()
    {
        $ci =& get_instance();
        if (!$ci->input->is_cli_request()) {
            if ($ci->config->item('email_bcc_address') != '') {
                $this->bcc($ci->config->item('email_bcc_address'));
            }
        }
        return parent::send();
    }
}
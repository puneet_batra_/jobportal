<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 12:45 PM
 */
class MY_Form_validation extends CI_Form_validation {

    public function __construct()
    {
        parent::__construct();

        $this->_error_prefix = '<div class="alert alert-danger" role="alert"><span class="title"><i class="icon-remove-sign"></i></span>';
        $this->_error_suffix = '</div>';

    }

    public function errors_array() {
        return $this->_error_array;
    }

    public function get_message($key)
    {
        return $this->_error_array[$key];
    }

    public function alpha($value)
    {
        $regex = '/^([a-zA-Z\s]+)$/i';
        if (preg_match($regex, $value)) {
            return true;
        } else {
            return false;
        }
    }

}
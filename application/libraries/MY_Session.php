<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Session extends CI_Session
{
    function __construct()
    {
        parent::__construct();
        $this->CI->session = $this;
    }
    function sess_update()
    {
        parent::sess_update();
    }
}
/* End of file MY_Session.php */
/* Location: ./application/libraries/MY_Session.php */
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/6/2015
 * Time: 5:26 PM
 */
class Api
{
    public $ci;
    public $json;
    protected $data = array();

    public function __construct()
    {
        $this->read();
        $this->ci =& get_instance();
    }

    public function read($string = '')
    {
        if ($string == '') {
            $jsondata = file_get_contents('php://input');
        } else {
            $jsondata = $string;
        }
        $this->json = $jsondata;

        // decode and store in array
        $post_data = json_decode($jsondata, true);
        if (json_last_error() != 0) {
            $data = array(
                'status' => 'fail',
                'message' => json_last_error_msg()
            );
            $this->sendJson($data);
            exit;
        }
        $this->data = (array)$post_data;
    }

    public function all()
    {
        return $this->data;
    }

    public function get($varname)
    {
        if (isset($this->data[$varname])) {
            return $this->data[$varname];
        } else {
            return '';
        }
    }

    public function getJson($array)
    {
        return json_encode((array)$array);
    }

    public function sendJson($array)
    {
        if (!headers_sent()) {
            header('Content-Type: application/json');
        }
        echo $this->getJson($array);
    }

    public function log()
    {

        $mysql_data = array(
            'device_udid' => $this->get('device_udid'),
            'type' => $this->get('_type'),
            'json_payload' => $this->json,
            'created_on' => date('Y-m-d H:i:s')
        );

        $this->ci->db->insert('device_json_payload', $mysql_data);
    }
}
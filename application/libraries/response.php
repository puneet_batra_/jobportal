<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 12:11 PM
 */
class Response
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function success($array)
    {
        $default = array(
            'status' => 'success'
        );
        $output = array_merge($default, $array);
        $this->ci->output->set_content_type('application/json')
            ->set_output(json_encode($output));
        return true;
    }

    public function error($array)
    {
        $default = array(
            'status' => 'fail',
            'message' => 'There were errors',
            'errors' => array()
        );
        $output = array_merge($default, $array);
        $this->ci->output->set_content_type('application/json')
            ->set_output(json_encode($output));
        return false;
    }
}
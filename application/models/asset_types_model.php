<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 12:00 PM
 */
class Asset_Types_Model extends CI_Model
{
    public function getList()
    {
        $asset_types = $this->db
            ->where('company_id', $this->auth->company_id())
            ->get('asset_types')
            ->result();
        return $asset_types;
    }

    public function get($id = 0)
    {
        $id = (int)$id;
        $asset_types = $this->db->where('asset_type_id', $id)->get('asset_types')->row();
        return $asset_types;
    }
}
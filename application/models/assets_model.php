<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 11:34 AM
 */
class Assets_Model extends CI_Model
{
    public function getList()
    {
        $assets = $this->db->where('company_id', $this->auth->company_id())
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->get('assets')
            ->result();
        return $assets;
    }

    public function getCount()
    {
        $assets = $this->db->where('company_id', $this->auth->company_id())
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->count_all_results('assets');
        return $assets;
    }

    public function get($id)
    {
        $asset_id = (int)$id;
        $asset = $this->db->where('asset_id', $asset_id)
            ->where('deleted_on', '0000-00-00 00:00:00')
            ->get('assets')->row();
        return $asset;
    }

    public function getPhotos($asset_id)
    {
        $asset_id = (int)$asset_id;
        $photos = $this->db->where('asset_id', $asset_id)->get('asset_photos')->result();
        return $photos;
    }

    public function getDocuments($asset_id)
    {
        $asset_id = (int)$asset_id;
        $documents = $this->db->where('asset_id', $asset_id)->get('asset_documents')->result();
        return $documents;
    }

    public function add($data)
    {
        // insert data
        $mysql_data = array(
            'company_id' => $this->auth->company_id(),
            'friendly_name' => '',
            'description' => '',
            'nfc_uuid' => '',
            'asset_key' => '',
            'asset_type_id' => '',
            'enabled' => '',
            'feature_fuel' => 0,
            'feature_maintenance' => 0,
            'feature_exception' => 0,
            'feature_location' => 0,
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s'),
            'created_by' => $this->auth->id(),
            'updated_by' => $this->auth->id()
        );
        $mysql_data = array_merge($mysql_data, $data);
        $this->db->insert('assets', $mysql_data);
        $asset_id = $this->db->insert_id();
        return $asset_id;
    }

    public function update($data, $asset_id)
    {
        // insert data
        $mysql_data = array(
            'updated_on' => date('Y-m-d H:i:s'),
            'updated_by' => $this->auth->id()
        );
        $mysql_data = array_merge($mysql_data, $data);
        $this->db->where('asset_id', $asset_id);
        $this->db->update('assets', $mysql_data);
        return $asset_id;
    }

    public function addAttachment($data)
    {
        // add to database
        $mysql_data = array(
            'filename' => '',
            'asset_id' => '',
            'company_id' => $this->auth->company_id(),
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s'),
            'created_by' => $this->auth->id(),
            'updated_by' => $this->auth->id()
        );
        $mysql_data = array_merge($mysql_data, $data);
        $this->db->insert('asset_photos', $mysql_data);
        return $this->db->insert_id();
    }

    public function addDocument($data)
    {
        // add to database
        $mysql_data = array(
            'title' => '',
            'filename' => '',
            'asset_id' => '',
            'company_id' => $this->auth->company_id(),
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s'),
            'created_by' => $this->auth->id(),
            'updated_by' => $this->auth->id()
        );
        $mysql_data = array_merge($mysql_data, $data);
        $this->db->insert('asset_documents', $mysql_data);
        return $this->db->insert_id();
    }

    public function getPhoto($asset_photo_id)
    {
        $asset_photo = $this->db->where('asset_photo_id', $asset_photo_id)
            ->get('asset_photos')->row();
        return $asset_photo;
    }

    public function deletePhoto($asset_photo_id)
    {
        $asset_photo = $this->db->where('asset_photo_id', $asset_photo_id)
            ->get('asset_photos')
            ->row();
        $this->db->where('asset_photo_id', $asset_photo_id)
            ->delete('asset_photos');
        //get filename
        if ($asset_photo) {
            $filename = $asset_photo->filename;
            $path = './uploads/' . $asset_photo->company_id . '/assets/photos/' . $filename;
            if (file_exists($path)) {
                @unlink($path);
            }
        }
    }

    public function getDocument($asset_document_id)
    {
        $asset = $this->db->where('asset_document_id', $asset_document_id)
            ->get('asset_documents')->row();
        return $asset;
    }

    public function deleteDocument($asset_document_id)
    {
        $asset = $this->db->where('asset_document_id', $asset_document_id)
            ->get('asset_documents')
            ->row();

        $this->db->where('asset_document_id', $asset_document_id)
            ->delete('asset_documents');

        if ($asset) {
            $filename = $asset->filename;
            $path = './uploads/' . $asset->company_id . '/assets/documents/' . $filename;
            if (file_exists($path)) {
                @unlink($path);
            }
        }
    }

    public function delete($asset_id)
    {
        $mysql_data = array(
            'deleted_on' => date('Y-m-d H:i:s'),
            'deleted_by' => $this->auth->id()
        );
        $this->db->where('asset_id', $asset_id)
            ->update('assets', $mysql_data);
    }

    public function favourite($user_id, $asset_id, $company_id = 0)
    {
        $asset_id = (int)$asset_id;
        if ($company_id == 0) {
            $company_id = $this->auth->company_id();
        }
        $mysql_data = array(
            'asset_id' => $asset_id,
            'user_id' => $user_id,
            'created_by' => $this->auth->id(),
            'updated_by' => $this->auth->id(),
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s')
        );
        $this->db->insert('favourites', $mysql_data);
    }

    public function unfavourite($user_id, $asset_id, $company_id = 0)
    {
        $asset_id = (int)$asset_id;
        if ($company_id == 0) {
            $company_id = $this->auth->company_id();
        }
        $this->db->where('asset_id', $asset_id)
            ->where('user_id', $user_id)
            ->delete('favourites');
    }

    public function getFavourite($user_id, $asset_id, $company_id = 0)
    {
        $asset_id = (int)$asset_id;
        if ($company_id == 0) {
            $company_id = $this->auth->company_id();
        }
        $favourite = $this->db->where('asset_id', $asset_id)
            ->where('user_id', $user_id)
            ->get('favourites')->row();

        if ($favourite) {
            return true;
        } else {
            return false;
        }
    }

}
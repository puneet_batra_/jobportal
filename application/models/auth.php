<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 12:12 PM
 */
class Auth extends CI_Model
{
    protected $user = false;
    protected $company = false;
    protected $permissions = false;

    /**
     * Main Login handler function, All the functions uses this function to login
     *
     * @param $credentials
     * @return bool
     */
    public function login($credentials)
    {
        $users = $this->user_model->getUsers($credentials);

        if (count($users) == 1) {
            // user found successfully.
            $user = $users[0];
            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('user_id', $user->user_id);
            $this->session->set_userdata('user_type', $user->user_type);
            $this->session->set_userdata('is_superuser', $user->is_superuser);

            // handle invitation if invite link was clicked
            if ($this->session->userdata('invite_id')) {
                echo "invite";;exit;
                // it has came from invite link
                $invite = $this->db->where('invite_id', $this->session->userdata('invite_id'))->get('invites')->row();
                if ($invite) {
                    $invite_matched = false;
                    // if userid matches
                    if ($invite->user_id == $user->user_id) {
                        $invite_matched = true;
                    } else if($invite->email == $user->email) {
                        // if email matches with logged in user email
                        $invite_matched = true;
                    } else {
                        // check authentications for email match
                        $authentications = $this->db->where('email', $invite->email)->get('authentications')->result();
                        foreach ($authentications as $authentication) {
                            if ($authentication->user_id == $user->user_id) {
                                // if this matched email does relate to logged in user
                                $invite_matched = true;
                            }
                        }
                    }

                    // now we step further, create a role and accept the request
                    if ($invite_matched) {
                        $mysql_data = array(
                            'role_id' => $invite->role_id,
                            'user_id' => $user->user_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('user_roles', $mysql_data);

                        $mysql_data = array(
                            'user_id' => $user->user_id,
                            'accepted' => 1
                        );
                        $this->db->update('invites', $mysql_data, array('invite_id' => $invite->invite_id));

                        $mysql_data = array(
                            'is_verified' => 1
                        );
                        $this->user_model->update($mysql_data, $user->user_id);
                    }
                }
            } // end invite function

            // visit last company if set
            if ($user->last_company > 0) {
                echo "last_compnay";exit;
                $company_id = (int)$user->last_company;
                // first of all check if user belongs to this company or not.
                $belongs = false;
                foreach ($this->auth->get()->companies as $company) {
                    if ($company_id == $company->company_id) {
                        $belongs = true;
                        break;
                    }
                }
                // only set if this user still belongs to this company after last visit
                if ($belongs == true) {
                    $this->session->set_userdata('company_id', $user->last_company);
                }
            } // last visit functionality


            return true;
        } else {

            return false;
        }
    }

    /**
     * Admin Login as user functionality wrapper
     *
     * @param $new_user_id
     * @return bool
     */
    public function loginas($new_user_id)
    {
        $return_id = $this->id();
        $this->session->set_userdata('return_login_id', $return_id);
        return $this->loginUsingId($new_user_id);
    }

    /**
     * After logging in, get current user data row where needed information.
     * @return bool
     */
    public function get()
    {
        // check for cached value
        if ($this->user != false) {
            return $this->user;
        } else {
            // retrieve user information by id of current user
            $user = $this->user_model->getById($this->auth->id());
            // attach authentications
            $authentications = $this->db->where('user_id', $user->user_id)->get('authentications')->result();
            $user->authentications = $authentications;
            $user->profile_pic = '';
            foreach($authentications as $authentication)
            {
                if ($authentication->profile_pic != false) {
                    $this->profile_pic = $authentication->profile_pic;
                }
            }
            // attach companies
            $my_companies_sql = "SELECT * from companies as c
                                    where (user_id = {$this->id()}
                                    or c.company_id in (
                                        select r.company_id from roles as r
                                        join user_roles as ur
                                        on ur.role_id = r.role_id
                                        where ur.user_id = {$this->id()}
                                        ))";
            //if (!$this->is_superadmin() && !$this->is_superuser()) {
                $my_companies_sql .= "and c.deleted_on = '0000-00-00 00:00:00'
                                        and c.active = 1";
            //}

            $companies = $this->db->query($my_companies_sql)->result();
            /*
             * Case: When admin has disabled the company, and user logged in is the admin of company, but is not a
             * super user or super admin, so he should not be allowed to see that company or login to it.
             * */
            if(!$this->is_superuser() && !$this->is_superadmin())
            {
                // fix do not allow simple user to access the company if it is disabled
                for ($i = 0; $i < count($companies); $i++) {
                    // remove the companies from list that are not enabled by admin
                    if ($companies[$i]->active == 0 || $companies[$i]->deleted_on != '0000-00-00 00:00:00') {
                        unset($companies[$i]);
                    }
                }
            }


            $user->companies = $companies;

            // set updated user info in class for multi time access
            $this->user = $user;
            return $user;
        }
    }

    /**
     * Api login, to login as any user via id.
     * @param $id
     * @return bool
     */
    public function loginUsingId($id)
    {
        //$this->session->unset_userdata('company_id');

        $credentials = array(
            'user_id' => (int)$id
        );
        if (!$this->is_superuser()) {
            $credentials['active'] = 1;
        }
        return $this->login($credentials);
    }

    /**
     * Login using social account
     * @param $type       Type of account (Google/Live)
     * @param $user_data  Received User's data
     *
     * In major there are three scenarios,
     * 1. User is logged in
     * 2. User is not logged in but has records with us
     * 3. User is no previous records
     *
     * Tasks:
     * 1. User is logged in
     * In this type attach the account if it is not already connected to anyone else.
     *
     * 2. User is not logged in but has records with us
     * In this try to find if this email that user has authenticated is already attached? if yes log in to that account.
     * If not then move to step 3.
     *
     * 3. User has no previous records
     * Create accounts both manual and auth attach.
     *
     */
    public function loginUsingSocial($type, $user_data)
    {

        // fix up the null values before processing further
        foreach ($user_data as $key => $val) {
            if (is_null($val)) {
                $user_data->$key = '';
            }
        }

        // look into authentications table once
        $auth_exists = $this->db->where('email', $user_data->email)
            ->where('account_type', $type)
            ->get('authentications')->row();

        if ($this->auth->check()) {

            // attach to the user account if it is already logged in
            $user = $this->db->where('user_id', $this->auth->id())->get('users')->row();

            // check if someone else has already used this account
            $authentication = $this->db
                ->where('email', $user_data->email)
                ->where('account_type', $type)
                ->get('authentications')->row();

            if (!$authentication) {
                // No one has used this account, attach it
                $mysql_data = array(
                    'email' => $user_data->email,
                    'account_type' => $type,
                    'token' => '',
                    'user_id' => $user->user_id,
                    'profile_pic' => $user_data->photoURL,
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'created_by' => $user->user_id,
                    'updated_by' => $user->user_id
                );
                $this->db->insert('authentications', $mysql_data);
                //redirect('employerrecruiters/profile');
                redirect('usercheck');
                //redirect('userhome');
                exit;

            } else if($authentication){
                // there is already a record of authentication
                if ($user->user_id != $authentication->user_id) {
                    // somewhere else account is already used, show error
                    show_error('This email is already attached to some other account. Please unlink from there first', 200, 'Already used in another account');
                    exit;
                } else {
                    // Record exists and the same user is trying to authenticate twice, do nothing
                    //redirect('employerrecruiters/profile');
                    redirect('usercheck');
                    //redirect('userhome');
                    exit;
                }
            }

        } else if($auth_exists) {
            // Case 2: No login, this account is already exists in authentications.
            $this->loginUsingId($auth_exists->user_id);
            redirect('usercheck');
            //redirect('userhome');
            exit;
        } else {

            // no auth and no user account exists, check first for manual account existance with same email
            $user_exists = $this->db->where('email', $user_data->email)->get('users')->row();
            if ($user_exists) {
                show_error('The manual account for the same email exists. Try logging in with that account then you can link this account.');
                exit;
            } else {

                $user_id = $this->user_model->createFromSocial($type, $user_data);
                // user has no record, let's create both entries
                $this->auth->loginUsingId($user_id);

                redirect('usercheck');
                //redirect('bckcadmin/home');
                exit;
            }

        }
    }

    /**
     * Checks if user is logged in or not
     * @return bool
     */
    public function check()
    {

        //if ($this->session->userdata('logged_in') == true && $this->session->userdata('user_id') > 0 && $this->session->userdata('is_superuser') == 2) {
        if ($this->session->userdata('logged_in') == true && $this->session->userdata('user_id') > 0 ) {

            return true;
        } else {

            return false;
        }
    }
    /*public function usercheck()
    {

        if ($this->session->userdata('logged_in') == true && $this->session->userdata('user_id') > 0 ) {

            return true;
        } else {

            return false;
        }
    }*/
    /**
     * Super users are helpers for admin management, they have given limited access by Super Admin.
     * These are for managing the admin panel.
     *
     * @return bool
     */
    public function is_superuser()
    {
        if ($this->session->userdata('is_superuser') == 2 ) {

            return true;
        } else {

            return false;
        }
    }

    public function is_employerrecruiter(){
        if(($this->session->userdata('user_type') == 2 ||  $this->session->userdata('user_type') == 3) && $this->session->userdata('is_supreuser') == 0){

            return true;
        }
        else{
            return false;
        }
    }
    public function is_jobseeker(){
        if($this->session->userdata('user_type') == 1 && $this->session->userdata('is_supreuser') == 0){

            return true;
        }
        else{
            return false;
        }
    }
    /**
     * Check if logged in by someone, is a superuser
     * @return bool
     */
    public function is_emulated_by_superuser()
    {
        if ($this->session->userdata('return_login_id') != '') {
            $user_id = (int)$this->session->userdata('return_login_id');
            $user = $this->db->where('user_id', $user_id)->get('users')->row();
            if ($user) {
                if ($user->is_superuser == 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Check if logged in by someone, is a superadmin
     * @return bool
     */
    public function is_emulated_by_superadmin()
    {
        if ($this->session->userdata('return_login_id') != '') {
            $user_id = (int)$this->session->userdata('return_login_id');
            $user = $this->db->where('user_id', $user_id)->get('users')->row();
            if ($user) {
                if ($user->is_superuser == 2) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Super admin is the main admin of this application and no limitation can be applied on it,
     * He can do anything he wants, either in company or admin or user
     *
     * @return bool
     */
    public function is_superadmin()
    {
        if ($this->session->userdata('is_superuser') == 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Admin has assigned a user to be admin of a company so in that company no limitation can be applied on
     * this user.
     *
     * @return bool
     */
    public function is_companyadmin()
    {
        if (
            $this->company()
            &&
            ($this->company()->user_id == $this->id()
                ||
                in_array($this->id(),$this->company()->admins)
            )
            && $this->uri->segment(1) == 'bckcadmin'
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get current user id.
     *
     * @return bool
     */
    public function id()
    {
        if ($this->session->userdata('user_id')) {
            return (int)$this->session->userdata('user_id');
        } else {
            return 0;
        }
    }

    /**
     * Get current company id if set
     *
     * @return bool
     */
    public function company_id()
    {
        if ($this->session->userdata('company_id')) {
            return (int)$this->session->userdata('company_id');
        } else {
            return 0;
        }
    }

    /**
     * Get current company information
     * @return bool
     */
    public function company()
    {
        // if there is cached company data then return it.
        if ($this->company != false) {
            return $this->company;
        } else {
            // otherwise check if user is logged into a company
            if ($this->company_id()) {
                $company = $this->db->where('company_id', $this->company_id())->get('companies')->row();
                if ($company) {
                    $company_admins = $this->db
                        ->where('company_id', $this->company_id())
                        ->get('company_admins')
                        ->result();
                    $company_admin_ids = array();
                    foreach($company_admins as $company_admin_id)
                    {
                        $company_admin_ids[] = $company_admin_id->user_id;
                    }
                    $company->admins = $company_admin_ids;
                    // if company found then return and also save it in cache for later access.
                    $this->company = $company;
                    return $company;
                } else {
                    return false;
                }
            } else {
                // if no company is logged in
                return false;
            }
        }
    }

    /**
     * User logout handler
     */
    public function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('company_id');
    }
    public function userlogout(){
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_type');
        $this->session->unset_userdata('company_id');
    }
    /**
     * Get permissions list for the current user
     *
     * @return bool
     */
    public function permissions()
    {
        if ($this->permissions != false) {
            return $this->permissions;
        }
        $user_id = (int) $this->id();
        $company_id = (int) $this->company_id();
        // query for company user
        $sql = "SELECT m.module_id,
                       m.name,
                       mr.permissions,
                       mr.sub_permissions,
                       m.controller_name,
                       m.action_name
                FROM   modules AS m
                       JOIN module_roles AS mr
                         ON m.module_id = mr.module_id
                WHERE  mr.role_id IN (SELECT r.role_id
                                      FROM   roles AS r
                                             JOIN user_roles AS ur
                                               ON r.role_id = ur.role_id
                                      WHERE  ur.user_id = '$user_id'
                                            AND r.company_id = '$company_id'
                                      )
                       AND m.permissions_required <= mr.permissions
                ";
        // query for super user
        $sql_su = "SELECT m.module_id,
                       m.name,
                       mr.permissions,
                       m.controller_name,
                       m.action_name
                FROM   su_modules AS m
                       JOIN su_module_roles AS mr
                         ON m.module_id = mr.module_id
                WHERE  mr.role_id IN (SELECT r.role_id
                                      FROM   su_roles AS r
                                             JOIN su_user_roles AS ur
                                               ON r.role_id = ur.role_id
                                      WHERE  ur.user_id = '$user_id'

                                      )
                       AND m.permissions_required <= mr.permissions
                ";

        if ($this->is_superuser()) {
            $permissions_data = $this->db->query($sql_su)->result_array();
        } else {
            $permissions_data = $this->db->query($sql)->result_array();
        }
        $this->permissions = $permissions_data;
        return $permissions_data;
    }

    /**
     * Helper function to be used in MY_Controller for automatic permissions check
     *
     * @param $module_name
     * @param $controller_name
     * @param $action_name
     * @param bool $auto
     * @return bool
     */
    public function is_allowed($module_name, $controller_name, $action_name, $auto = false)
    {
        // allow if it is admin super user
        if($this->is_superadmin()){
            return true;
        }
        // allow if it is company admin
        if($this->is_companyadmin())
        {
            return true;
        }
        // auto mode is used for MY_Controller 's automatic permission detection
        $allowed = false;
        $permissions_available = $this->permissions();
        if (count($permissions_available) > 0) {
            foreach ($permissions_available as $permission) {
                if (($permission['name'] == $module_name || $auto)
                    && $permission['controller_name'] == $controller_name
                    && $permission['action_name'] == $action_name
                ) {
                    // conditions matched and i am allowed to access
                    $allowed = true;
                }
            }

        }
        return $allowed;
    }

    /**
     * Permission checker function to help predict which button to show which not, according to permission available
     * for the given controller or optional action name
     *
     * Used by permissions helper file.
     *
     * @param $controller_name
     * @param string $action_name
     * @return int
     */
    public function get_permissions_by_controller($controller_name, $action_name = '', $array = false)
    {

        $given = 0;
        $user = $this->get();
        if ($this->is_superuser()) {
            $sql = "SELECT
            su_user_roles.user_id,
            controller_name,
            permissions_required,
            permissions
            FROM `su_module_roles`
            JOIN su_roles on su_module_roles.role_id = su_roles.role_id
            JOIN su_user_roles on su_roles.role_id = su_user_roles.role_id
            JOIN su_modules on su_modules.module_id = su_module_roles.module_id
            where su_user_roles.user_id = {$this->id()}
            and controller_name = '$controller_name'";
        } else {
            $sql = "SELECT
            user_roles.user_id,
            controller_name,
            permissions_required,
            permissions,
            sub_permissions
            FROM `module_roles`
            JOIN roles on module_roles.role_id = roles.role_id
            JOIN user_roles on roles.role_id = user_roles.role_id
            JOIN modules on modules.module_id = module_roles.module_id
            where user_roles.user_id = {$this->id()}
            and roles.company_id = {$this->company_id()}
            and controller_name = '$controller_name'";
        }
        if ($action_name) {
            $sql .= " and action_name = '$action_name'";
        }
        $permissions = $this->db->query($sql)->row();

        if ($array == true) {
            return $permissions;
        }
        if ($permissions) {
            return $permissions->permissions;
        } else {
            return 0;
        }
    }

    public function set_remember_me()
    {
        $this->load->helper('hash');
        $user_id = $this->id();
        $options = array(
            'cost' => 10,
        );
        $hashed = mt_rand(111, 999) . password_hash($user_id, PASSWORD_BCRYPT, $options);

        $cookie = array(
            'name'   => 'remember_me_token',
            'value'  => $hashed ,
            'expire' => $this->config->item('remember_length'),  // Two weeks
            'domain' => '',
            'path'   => '/'
        );
        set_cookie($cookie);

        $mysql_data = array(
            'user_id' => $user_id,
            'token' => $hashed,
            'created_on' => date('Y-m-d H:i:s'),
            'expire_on' => date('Y-m-d H:i:s', time() + $this->config->item('remember_length'))
        );
        $this->db->insert('remember_tokens', $mysql_data);
    }

    public function verify_remember_me()
    {
        $this->load->helper('hash');
        if (get_cookie('remember_me_token') != '') {
            $token = get_cookie('remember_me_token');
            $remember = $this->db->where('token', $token)
                ->where('expire_on >', date('Y-m-d H:i:s'))
                ->limit(1)
                ->get('remember_tokens')
                ->row();
            if ($remember) {
                $user = $this->db->where('user_id', $remember->user_id)
                    ->where('active', 1)
                    ->where('deleted_on', '0000-00-00 00:00:00')
                    ->get('users')
                    ->row();
                if ($user) {
                    $token = substr($token, 3);
                    if (password_verify($user->user_id, $token)) {
                        $this->loginUsingId($user->user_id);
                        return true;
                    } else {
                        delete_cookie('remember_me_token');
                        return false;
                    }
                } else {
                    delete_cookie('remember_me_token');
                    $this->db->where('remember_token_id', $remember->remember_token_id)
                        ->delete('remember_tokens');
                    return false;
                }
            } else {
                delete_cookie('remember_me_token');
                return false;
            }
        } else {
            return false;
        }

    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 4:38 PM
 */
class Company_model extends CI_Model
{
    public function create($mysql_data)
    {
        $mysql_data['created_on'] = date('Y-m-d H:i:s');
        $this->db->insert('companies', $mysql_data);
        $company_id = $this->db->insert_id();

        if (isset($mysql_data['user_id']) && $mysql_data['user_id'] > 0) {
            $this->user_model->notify_company_assigned($company_id, $mysql_data['user_id']);
        }
        return $company_id;
    }

    public function update($mysql_data, $company_id, $notify_new_admin = true)
    {
        $company = $this->db->where('company_id', $company_id)->get('companies')->row();
        if (isset($mysql_data['user_id'])) {
            if ($company->user_id != $mysql_data['user_id'] && $notify_new_admin == true) {
                $this->user_model->notify_company_assigned($company_id, $mysql_data['user_id']);
            }
        }
        $mysql_data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('company_id', (int)$company_id);
        return $this->db->update('companies', $mysql_data);
    }

    public function getById($company_id)
    {
        $this->db->where('company_id', $company_id);
        return $this->db->get('companies')->row();
    }

    public function getCompanies($where = array())
    {
        return $this->db->where($where)->get('companies')->result();
    }

    public function remove($company_id)
    {
        return $this->db->where('company_id', $company_id)->delete('companies');
    }

    // User level functions
    public function get_preference($preference_name, $default = '')
    {
        $preference = $this->db->where('company_id', $this->auth->company_id())
            ->where('user_id', $this->auth->id())
            ->get('company_preferences')
            ->row();
        if ($preference) {
            if ($preference->$preference_name == '') {
                return $default;
            } else {
                return $preference->$preference_name;
            }
        } else {
            return $default;
        }
    }

    public function set_preference($preference_name, $value)
    {
        $preference = $this->db->where('company_id', $this->auth->company_id())
            ->where('user_id', $this->auth->id())
            ->get('company_preferences')
            ->row();
        if ($preference) {
            $mysql_data = array(
                $preference_name => $value
            );
            $this->db->where('company_preference_id', $preference->company_preference_id)
                ->update('company_preferences', $mysql_data);
        } else {
            $mysql_data = array(
                $preference_name => $value,
                'company_id' => $this->auth->company_id(),
                'user_id' => $this->auth->id(),
                'created_on' => date('Y-m-d H:i:s'),
                'updated_on' => date('Y-m-d H:i:s'),
                'created_by' => $this->auth->id(),
                'updated_by' => $this->auth->id()
            );
            $this->db->insert('company_preferences', $mysql_data);
        }
    }

    public function get_themes()
    {
        $themes = array(
            '0' => array(
                'name' => 'Dark Blue',
                'css' => 'darkblue.css'
            ),
            '1' => array(
                'name' => 'Blue',
                'css' => 'blue.css'
            ),
            '2' => array(
                'name' => 'Default',
                'css' => 'default.css'
            ),
            '3' => array(
                'name' => 'Grey',
                'css' => 'grey.css'
            ),
            '4' => array(
                'name' => 'Light',
                'css' => 'light.css'
            ),
            '5' => array(
                'name' => 'Light 2',
                'css' => 'light.css'
            )
        );
        return $themes;
    }

    public function get_css()
    {
        $id = $this->get_preference('theme_id', 2);
        return $this->get_themes()[$id]['css'];
    }

    public function get_company_users($type = 'all', $raw = false)
    {
        if ($type == 'all') {
            $where = '1';
        } else if ($type == 'accepted') {
            $where = 'status = 1';
        } else if ($type == 'pending') {
            $where = 'status = 0';
        } else if ($type == 'existing') {
            $where = 'user_id > 0';
        } else if ($type == 'active') {
            $where = 'status = 1 and user_id > 0';
        }
        $sql = "
        select * from
            (
            SELECT
            if(u.user_id, 0, 0) as invite_id,
            u.user_id,
            concat_ws(' ', first_name, last_name) as name,
            u.email,
            r.name as role,
            if(r.role_id, 1, 0) as status

            FROM (`users` as u)
            RIGHT JOIN `user_roles` as ur ON `ur`.`user_id` = `u`.`user_id`
            LEFT JOIN `roles` as r ON `r`.`role_id` = `ur`.`role_id`
            WHERE `r`.`company_id` =  '{$this->auth->company_id()}'
             and u.user_id <> {$this->auth->company()->user_id}

            UNION

            Select
            i.invite_id as invite_id,
            i.user_id,
            if(i.user_id = 0, concat_ws(' ', i.first_name, i.last_name), concat_ws(' ', u.first_name, u.last_name)) as name,
            i.email,
            r.name as role,
            i.accepted as status

            from invites as i
            join roles as r on r.role_id = i.role_id
            left outer join users as u on i.user_id = u.user_id

            where

            i.company_id = {$this->auth->company_id()}

            and i.user_id <> {$this->auth->company()->user_id}

            order by status desc
        ) as final_filtered where $where group by email";

        if ($raw == true) {
            return $sql;
        }
        $users = $this->db->query($sql)->result();
        return $users;
    }

    public function getCompanyUsersCount($type = 'all')
    {
        $sql = $this->get_company_users($type, true);
        $count_sql = "select count(*) as numrows from ($sql) as countable";
        $count = $this->db->query($count_sql)->row()->numrows;
        return $count;
    }

    public function getMyFavourites()
    {
        $favourites = $this->db
            ->select('assets.*, asset_photos.filename as photo', false)
            ->join('favourites', 'favourites.asset_id = assets.asset_id')
            ->join('asset_photos', 'asset_photos.asset_id = assets.asset_id', 'LEFT OUTER')
            ->where('assets.company_id', $this->auth->company_id())
            ->where('favourites.user_id', $this->auth->id())
            ->where('assets.deleted_on', '0000-00-00 00:00:00')
            ->where('assets.enabled', '1')
            ->group_by('assets.asset_id')
            ->get('assets')
            ->result();
        return $favourites;
    }
}
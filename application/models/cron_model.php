<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 5/11/2015
 * Time: 3:43 PM
 */
class Cron_model extends CI_Model
{
    public function createExceptionCommentEmail($exception_id, $exception_note_id, $created_by)
    {
        $exception = $this->db->where('exception_id', $exception_id)->get('exceptions')->row();
        $exception_note = $this->db->where('exception_note_id', $exception_note_id)->get('exception_notes')->row();
        $exception_note->user = $this->db->where('user_id', $exception_note->created_by)->get('users')->row();
        $asset = $this->db->where('asset_id', $exception->asset_id)->get('assets')->row();
        $company = $this->db->where('company_id', $exception->company_id)->get('companies')->row();

        // Create cron entry for each user
        $users = $this->user_model->getExceptionUsers($exception_id);
        foreach ($users as $user) {
            $data = array(
                'user' => $user,
                'exception' => $exception,
                'exception_note' => $exception_note,
                'asset' => $asset,
                'company' => $company
            );
            $message = $this->load->view('emails/exception.note.subscribe.php', $data, true);
            $subject = 'New comment on exception: '. $exception->title;

            $mysql_data = array(
                'exception_id' => $exception_id,
                'exception_note_id' => $exception_note_id,
                'user_id' => $user->user_id,
                'to' => $user->email,
                'subject' => $subject,
                'message' => $message,
                'cc' => '',
                'bcc' => '',
                'asset_id' => $exception->asset_id,
                'company_id' => $exception->company_id,
                'status_sent' => 0,
                'created_on' => date('Y-m-d H:i:s')
            );
            $this->db->insert('cron_emails', $mysql_data);
        }
    }
}
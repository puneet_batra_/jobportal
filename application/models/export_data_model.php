<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 11:52 AM
 */
class Export_data_model extends CI_Model
{

    public function job_seeker(){

       $query = $this->db
            ->join('education', 'users.user_id = education.user_id', 'left outer')
            ->join('experience', 'users.user_id = experience.user_id', 'left outer')
            ->join('hobbies', 'users.user_id = hobbies.user_id', 'left outer')
            ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
            ->join('skills', 'users.user_id = skills.user_id', 'left outer')
            ->group_by('users.user_id')
            ->where('users.is_superuser !=', 2)
            ->where('users.is_superuser !=', 1)
            ->where('users.user_type !=', 2)
            ->where('users.user_type !=', 3)
            ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,users.phone,
            users.dob,users.email,users.address,users.city,users.country,users.nationality,users.age,users.height,users.weight,users.relegion,
            users.martial_status,users.passport_num,users.mobile_num,users.telephone_num,users.skype_id,users.job_seeker_status,users.languages,
            users.job_position,users.job_industry,users.project_type, GROUP_CONCAT(hobbies.hobbies_name) as hobbies_name,
            GROUP_CONCAT(education.edu_level) as edu_level,GROUP_CONCAT(education.edu_institute_name) as edu_institute_name,GROUP_CONCAT(education.edu_city) as edu_city,
            GROUP_CONCAT(education.edu_country) as edu_country,GROUP_CONCAT(education.edu_year) as edu_year,GROUP_CONCAT(education.edu_degree) as edu_degree,
            GROUP_CONCAT(education.edu_grade) as edu_grade, GROUP_CONCAT(experience.exp_company_name) as exp_company_name,GROUP_CONCAT(experience.exp_city) as exp_city,
            GROUP_CONCAT(experience.exp_country) as exp_country,GROUP_CONCAT(experience.exp_years) as exp_years,GROUP_CONCAT(experience.exp_job_position) as exp_job_position,
            GROUP_CONCAT(experience.exp_job_description) as exp_job_description,GROUP_CONCAT(experience.company_reference) as company_reference,
            GROUP_CONCAT(skills.soft_skills) as soft_skills,GROUP_CONCAT(skills.hard_skills) as hard_skills', false)
            ->get('users');
       return $query;

    }

     public function employer(){

          $query = $this->db
                  ->join('contact', 'users.user_id = contact.user_id', 'left outer')
                  ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
                  //->join('job_post', ' users.user_id = job_post.user_id', 'left outer')
                  ->group_by('users.user_id')
                  ->where('users.is_superuser !=', 2)
                  ->where('users.is_superuser !=', 1)
                  ->where('users.user_type !=', 1)
                  ->where('users.user_type !=', 3)
                  ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,
                  users.company_logo,users.company_name,users.company_url,users.address,users.city,users.state,users.country,
                  GROUP_CONCAT(contact.contact_email)as contact_email,GROUP_CONCAT(contact.contact_number) as contact_number,GROUP_CONCAT(contact.contact_person) as contact_person,users.recruiter_required,users.vat_no', false)
                  ->get('users');
          return $query;
     }

     public function recruiter(){
          $query =  $this->db
                   ->join('contact', 'users.user_id = contact.user_id', 'left outer')
                   ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
                   //->join('job_post', ' users.user_id = job_post.user_id', 'left outer')
                   ->group_by('users.user_id')
                   ->where('users.is_superuser !=', 2)
                   ->where('users.is_superuser !=', 1)
                   ->where('users.user_type !=', 1)
                   ->where('users.user_type !=', 2)
                   ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,
                       users.company_logo,users.company_name,users.company_url,users.recruit_industry,users.address,users.city,users.state,users.country,
                       GROUP_CONCAT(contact.contact_email)as contact_email,GROUP_CONCAT(contact.contact_number) as contact_number,GROUP_CONCAT(contact.contact_person) as contact_person,users.recruiter_required,users.vat_no', false)
                   ->get('users');
          return $query;
     }

     public function job_seeker_print(){

          $query = $this->db
              ->join('education', 'users.user_id = education.user_id', 'left outer')
              ->join('experience', 'users.user_id = experience.user_id', 'left outer')
              ->join('hobbies', 'users.user_id = hobbies.user_id', 'left outer')
              ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
              ->join('skills', 'users.user_id = skills.user_id', 'left outer')
              ->group_by('users.user_id')
              ->where('users.is_superuser !=', 2)
              ->where('users.is_superuser !=', 1)
              ->where('users.user_type !=', 2)
              ->where('users.user_type !=', 3)
              ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,users.phone,
            users.dob,users.email,users.address,users.city,users.country,users.nationality,users.age,users.height,users.weight,users.relegion,
            users.martial_status,users.passport_num,users.mobile_num,users.telephone_num,users.skype_id,users.job_seeker_status,users.languages,
            users.job_position,users.job_industry,users.project_type, GROUP_CONCAT(hobbies.hobbies_name) as hobbies_name,
            GROUP_CONCAT(education.edu_level) as edu_level,GROUP_CONCAT(education.edu_institute_name) as edu_institute_name,GROUP_CONCAT(education.edu_city) as edu_city,
            GROUP_CONCAT(education.edu_country) as edu_country,GROUP_CONCAT(education.edu_year) as edu_year,GROUP_CONCAT(education.edu_degree) as edu_degree,
            GROUP_CONCAT(education.edu_grade) as edu_grade, GROUP_CONCAT(experience.exp_company_name) as exp_company_name,GROUP_CONCAT(experience.exp_city) as exp_city,
            GROUP_CONCAT(experience.exp_country) as exp_country,GROUP_CONCAT(experience.exp_years) as exp_years,GROUP_CONCAT(experience.exp_job_position) as exp_job_position,
            GROUP_CONCAT(experience.exp_job_description) as exp_job_description,GROUP_CONCAT(experience.company_reference) as company_reference,
            GROUP_CONCAT(skills.soft_skills) as soft_skills,GROUP_CONCAT(skills.hard_skills) as hard_skills', false)
              ->get('users')->result();
          return $query;

     }

     public function employer_print(){

          $query = $this->db
              ->join('contact', 'users.user_id = contact.user_id', 'left outer')
              ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
              ->join('job_post', ' users.user_id = job_post.user_id', 'left outer')
              ->group_by('users.user_id')
              ->where('users.is_superuser !=', 2)
              ->where('users.is_superuser !=', 1)
              ->where('users.user_type !=', 1)
              ->where('users.user_type !=', 3)
              ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,
                  users.company_logo,users.company_name,users.company_url,users.address,users.city,users.state,users.country,job_post.job_applications,
                  GROUP_CONCAT(contact.contact_email)as contact_email,GROUP_CONCAT(contact.contact_number) as contact_number,GROUP_CONCAT(contact.contact_person) as contact_person,users.recruiter_required,users.vat_no', false)
              ->get('users')->result();
          return $query;
     }

     public function recruiter_print(){
          $query =  $this->db
              ->join('contact', 'users.user_id = contact.user_id', 'left outer')
              ->join('user_type', 'user_type.user_type_id = users.user_type', 'left outer')
              ->join('job_post', ' users.user_id = job_post.user_id', 'left outer')
              ->group_by('users.user_id')
              ->where('users.is_superuser !=', 2)
              ->where('users.is_superuser !=', 1)
              ->where('users.user_type !=', 1)
              ->where('users.user_type !=', 2)
              ->select('users.user_id,users.user_type,user_type.user_type_name,users.username,users.first_name,users.middle_name,users.last_name,
                       users.company_logo,users.company_name,users.company_url,users.recruit_industry,users.address,users.city,users.state,users.country,job_post.job_applications,
                       GROUP_CONCAT(contact.contact_email)as contact_email,GROUP_CONCAT(contact.contact_number) as contact_number,GROUP_CONCAT(contact.contact_person) as contact_person,users.recruiter_required,users.vat_no', false)
              ->get('users')->result();
          return $query;
     }

}
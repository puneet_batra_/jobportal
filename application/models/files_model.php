<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 11:52 AM
 */
class Files_Model extends CI_Model
{
    public function getSize($dir)
    {
        $f = $dir;
        if (is_dir($f)) {
            $io = popen('/usr/bin/du -s --block-size 1 ' . $f, 'r'); // This will return a line such as '1017876 /home/chs/'   -sk = summarise in 1 line, k = use 1K block  (--block-size 1 will result in same as what windows returns, instead of k.  I have done this for compatibilty)
            $size = fgets($io, 4096);                                                                // Retrieve that above line
            $size = substr($size, 0, strpos($size, "\t"));            // Get the fitst substring, the tab is what seperates the size and the folder
            pclose($io);
            // Now we use your code for formatting the result. Because I used --block-size 1, we have 1 Byte Blocks, as windows returns.
            //  -k would use --blobk-size 1024, -m is Megabytes.  they can be used, to simplify.
        } else {
            $size = 0;
        }

        $unit = 'B';
        if ($size > 1024) {
            $size = $size / 1024;
            $unit = 'KB';
            if ($size > 1024) {
                $size = $size / 1024;
                $unit = 'MB';
                if ($size > 1024) {
                    $size = $size / 1024;
                    $unit = 'GB';
                }
            }
        }
        $data = array(
            'size' => round($size, 2),
            'unit' => $unit
        );
        return $data;
    }

    public function validExtension($input_name, $valid_extensions = array(), $inverse = false)
    {
        $valid_type = true;
        foreach ($valid_extensions as $key => $extension) {
            $valid_extensions[$key] = strtolower($extension);
        }
        if (isset($_FILES[$input_name])) {
            for ($i = 0; $i < count($_FILES[$input_name]['name']); $i++) {
                $ext = end(explode('.', $_FILES[$input_name]['name'][$i]));
                if ($inverse == false) {
                    if (!in_array($ext, $valid_extensions)) {
                        $valid_type = false;
                    }
                } else {
                    if (in_array($ext, $valid_extensions)) {
                        $valid_type = false;
                    }
                }
            }
        }
        return $valid_type;
    }

    public function validExtensionSingle($input_name, $valid_extensions = array(), $inverse = false)
    {
        $valid_type = true;
        foreach ($valid_extensions as $key => $extension) {
            $valid_extensions[$key] = strtolower($extension);
        }
        if (isset($_FILES[$input_name])) {

            $ext = end(explode('.', $_FILES[$input_name]['name']));
            if ($inverse == false) {
                if (!in_array($ext, $valid_extensions)) {
                    $valid_type = false;
                }
            } else {
                if (in_array($ext, $valid_extensions)) {
                    $valid_type = false;
                }
            }

        }
        return $valid_type;
    }

    public function bulkSave($input_name, $path)
    {
        $files_data = array();
        $this->load->library('image_moo');
        if (isset($_FILES[$input_name])) {
            for ($i = 0; $i < count($_FILES[$input_name]['name']); $i++) {
                $tmp_name = $_FILES[$input_name]['tmp_name'][$i];
                $file_parts = explode('.', $_FILES[$input_name]['name'][$i]);
                $ext = strtolower(end($file_parts));
                $random_name = md5(microtime(true)) . '.' . $ext;
                $save_path = $path;
                if (!is_dir($save_path)) {
                    mkdir($save_path, 0777, true);
                }
                $save_path .= $random_name;
                if (move_uploaded_file($tmp_name, $save_path)) {
                    if (in_array($ext, array('png', 'jpg', 'jpeg', 'gif'))) {
                        $this->image_moo->load($save_path)
                            ->resize($this->config->item('asset_resize_max_x'), $this->config->item('asset_resize_max_y'))
                            ->save($save_path, true);
                    }

                    $files_data[] = array(
                        'uploaded_name' => $random_name,
                        'original_name' => $_FILES[$input_name]['name'][$i],
                        'tmp_name' => $_FILES[$input_name]['tmp_name'][$i]
                    );
                }
            }
        }

        return $files_data;
    }

    public function save($input_name, $path)
    {
        $files_data = array();
        $this->load->library('image_moo');
        if (isset($_FILES[$input_name])) {
            $tmp_name = $_FILES[$input_name]['tmp_name'];
            $file_parts = explode('.', $_FILES[$input_name]['name']);
            $ext = strtolower(end($file_parts));
            $random_name = md5(microtime(true)) . '.' . $ext;
            $save_path = $path;
            if (!is_dir($save_path)) {
                mkdir($save_path, 0777, true);
            }
            $save_path .= $random_name;
            if (move_uploaded_file($tmp_name, $save_path)) {
                if (in_array($ext, array('png', 'jpg', 'jpeg', 'gif'))) {
                    $this->image_moo->load($save_path)
                        ->resize($this->config->item('asset_resize_max_x'), $this->config->item('asset_resize_max_y'))
                        ->save($save_path, true);
                }

                $files_data[] = array(
                    'uploaded_name' => $random_name,
                    'original_name' => $_FILES[$input_name]['name'],
                    'tmp_name' => $_FILES[$input_name]['tmp_name']
                );
            }

        }

        return $files_data;
    }
}
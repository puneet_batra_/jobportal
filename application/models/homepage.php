<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 12:00 PM
 */
class Homepage extends CI_Model
{
    public function get()
    {
        $data['page']=$this->db->where('active','1')->get('pages')->result();

        $data['no_of_jobseekers']=$this->db->where('user_type','1')->get('users')->num_rows();
        $data['no_of_employers']=$this->db->where('user_type','2')->get('users')->num_rows();
        $data['no_of_recruiters']=$this->db->where('user_type','3')->get('users')->num_rows();
        $data['no_of_jobs']=$this->db->get('job_posted')->num_rows();
        
        return $data;
    }

    
}
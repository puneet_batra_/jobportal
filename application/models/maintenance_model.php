<?php

/**
 * Created by PhpStorm.
 * User: Step
 * Date: 4/15/2015
 * Time: 5:17 PM
 */
class Maintenance_Model extends CI_Model
{
    protected $types = array();
    protected $type_column = array();
    protected $type_validations = array();

    public function __construct()
    {
        $this->types = array(
            3 => 'Testing (Date)',
            0 => 'Warranty (Date)',
            1 => 'Service (Date)',
            2 => 'Service (ODO)',
            4 => 'End of Life (Date)'
        );

        $this->type_column = array(
            3 => 'testing_date',
            0 => 'warranty_date',
            1 => 'due_date',
            2 => 'odo_hour',
            4 => 'end_of_life_date'
        );

        $this->type_validations = array(
            3 => '_validate_testing_date',
            0 => '_validate_warranty_date',
            1 => '_validate_due_date',
            2 => '_validate_odo_hour',
            4 => '_validate_end_of_life_date'
        );
    }

    function get_types()
    {
        return $this->types;
    }

    function get_type($type_id)
    {
        return $this->types[$type_id];
    }

    function get_columns()
    {
        return $this->type_column;
    }

    function get_column($type_id)
    {
        return $this->type_column[$type_id];
    }

    function validate_maintenance($data)
    {
        $maintenance_type = $data['maintenance_type'];
        $callable = $this->type_validations[$maintenance_type];
        $validation_status = call_user_func_array(array($this, $callable), array(0 => $data));
        $message = '';
        $status = '';
        if ($validation_status['status'] == 'fail') {
            $status = 'fail';
            $message = $validation_status['message'];
        } else {
            $status = 'success';
            $message = '';
        }

        return array(
            'status' => $status,
            'message' => $message
        );

    }

    public function _validate_warranty_date($data)
    {
        if ($data['warranty_date'] == '') {
            $message = 'You must fill in date in warranty date field';
            return array(
                'status' => 'fail',
                'message' => $message
            );
        } else {
            $maintenance = $this->db
                ->where('asset_id', $data['asset_id'])
                ->where('company_id', $this->auth->company_id())
                ->order_by('warranty_date', 'desc')
                ->get('maintenance')
                ->row();
            if ($maintenance && strtotime($data['warranty_date']) < $maintenance->warranty_date) {
                $message = 'Warranty date cannot be less than ' . $maintenance->warranty_date;
                return array(
                    'status' => 'fail',
                    'message' => $message
                );
            }
            return array(
                'status' => 'success',
                'message' => ''
            );
        }
    }

    function _validate_due_date($data)
    {
        if ($data['due_date'] == '') {
            $message = 'You must fill in date in due date field';
            return array(
                'status' => 'fail',
                'message' => $message
            );
        } else {
            $maintenance = $this->db
                ->where('asset_id', $data['asset_id'])
                ->where('company_id', $this->auth->company_id())
                ->order_by('due_date', 'desc')
                ->get('maintenance')
                ->row();
            if ($maintenance && strtotime($data['due_date']) < $maintenance->due_date) {
                $message = 'Due date cannot be less than ' . $maintenance->due_date;
                return array(
                    'status' => 'fail',
                    'message' => $message
                );
            }
            return array(
                'status' => 'success',
                'message' => ''
            );
        }
    }

    function _validate_odo_hour($data)
    {
        $odometer = $data['odo_hour'];
        $asset_id = (int)$data['asset_id'];
        $company_id = (int)$this->auth->company_id();

        if ($odometer == '') {
            return array(
                'status' => 'fail',
                'message' => 'Odo meter value cannot be empty.'
            );
        }

        $last_entry = $this->db->where('company_id', $company_id)
            ->where('asset_id', $asset_id)
            ->order_by('odometer', 'desc')
            ->limit(1)
            ->get('fuel')
            ->row();
        if ($last_entry) {
            if ($odometer > $last_entry->odometer) {
                return array(
                    'status' => 'success',
                    'message' => ''
                );
            } else {
                $message = 'Odometer value cannot be less than ' . $last_entry->odometer;
                return array(
                    'status' => 'fail',
                    'message' => $message
                );
            }
        } else {
            // no previous entry exists, no need to validate
            return array(
                'status' => 'success',
                'message' => ''
            );
        }
    }

    function _validate_testing_date($data)
    {
        if ($data['testing_date'] == '') {
            $message = 'You must fill in date in testing date field';
            return array(
                'status' => 'fail',
                'message' => $message
            );
        } else {
            $maintenance = $this->db
                ->where('asset_id', $data['asset_id'])
                ->where('company_id', $this->auth->company_id())
                ->order_by('testing_date', 'desc')
                ->get('maintenance')
                ->row();
            if ($maintenance && strtotime($data['testing_date']) < $maintenance->testing_date) {
                $message = 'Testing date cannot be less than ' . $maintenance->testing_date;
                return array(
                    'status' => 'fail',
                    'message' => $message
                );
            }
            return array(
                'status' => 'success',
                'message' => ''
            );
        }
    }

    function _validate_end_of_life_date($data)
    {
        if ($data['end_of_life_date'] == '') {
            $message = 'You must fill in date in end of life date field';
            return array(
                'status' => 'fail',
                'message' => $message
            );
        } else {
            $maintenance = $this->db
                ->where('asset_id', $data['asset_id'])
                ->where('company_id', $this->auth->company_id())
                ->order_by('end_of_life_date', 'desc')
                ->get('maintenance')
                ->row();
            if ($maintenance && strtotime($data['end_of_life_date']) < $maintenance->end_of_life_date) {
                $message = 'End of life date cannot be less than ' . $maintenance->end_of_life_date;
                return array(
                    'status' => 'fail',
                    'message' => $message
                );
            }
            return array(
                'status' => 'success',
                'message' => ''
            );
        }
    }

    function getPendingMaintenanceExceptions()
    {
        $pending_entries = array();

        $maintenance_types_data = $this->maintenance_model->get_types();
        // empty all types data first
        foreach ($maintenance_types_data as $key => $val) {
            $maintenance_types_data[$key] = '';
        }

        // loop through each type
        foreach ($this->maintenance_model->get_types() as $maintenance_type_id => $maintenance_info) {
            if ($maintenance_type_id == 2) {
                // get maintenance entry
                $maintenance_sql = "select * from
                  (
                  SELECT * FROM (`maintenance`)
                  WHERE `exception_created` = 0
                  AND `maintenance_type` = $maintenance_type_id
                  ORDER BY `odo_hour` desc
                  )
                  as tbl_maintenance
                  where asset_id not in(select asset_id from assets where deleted_on > '0000-00-00 00:00:00' or enabled = 0 )
                  group by asset_id;";
                $maintenances = $this->db->query($maintenance_sql)->result();
                foreach($maintenances as $maintenance){
                    // check odometer
                    $odometer = 0;
                    if ($maintenance) {
                        // entry exists

                        // check for fuel entry
                        $fuel = $this->db
                            ->where('asset_id', $maintenance->asset_id)
                            ->where('company_id', $maintenance->company_id)
                            ->order_by('odometer', 'desc')
                            ->get('fuel')
                            ->row();
                        if ($fuel) {
                            $odometer = $fuel->odometer;
                        }

                        // compare the entry with odometer value
                        if ($odometer > $maintenance->odo_hour) {
                            // current odometer is more than scheduled entry
                            $remaining = $odometer - $maintenance->odo_hour;
                            $message = "<p class='text-danger'>Maintenance is due at <strong>{$maintenance->odo_hour} km</strong>, and is {$remaining} km over.</p>";
                            $maintenance_types_data[$maintenance_type_id] = $message;

                            $pending_entries[] = array(
                                'type' => $maintenance_type_id,
                                'maintenance_id' => $maintenance->maintenance_id,
                                'message' => $message
                            );
                        }
                    }
                }
            } else {
                // other maintenance type entries are of date type
                $column = $this->maintenance_model->get_column($maintenance_type_id);
                $type = $this->maintenance_model->get_type($maintenance_type_id);
                $maintenance_sql = "SELECT * from
                        (
                            SELECT
                            maintenance.*,
                            datediff(NOW(), $column) as due_days
                            FROM (`maintenance`)
                            WHERE `exception_created` = 0
                            AND `maintenance_type` = $maintenance_type_id
                            ORDER BY $column desc
                        ) as tbl_maintenance
                        where asset_id not in(select asset_id from assets where deleted_on > '0000-00-00 00:00:00' or enabled = 0 )
                        group by asset_id;

                  ";
                $maintenances = $this->db->query($maintenance_sql)->result();
                foreach ($maintenances as $maintenance) {
                    if ($maintenance && time() > strtotime($maintenance->$column)) {
                        $notes_next_service = '.';
                        if ($maintenance->notes_next_service != '') {
                            $notes_next_service = ' for "' . $maintenance->notes_next_service . '".';
                        }
                        $message = "<p class='text-danger'>Maintenance - $type was due on <strong>" . $this->timezone->convertDate(date('Y-m-d H:i:s', strtotime($maintenance->$column)), 'd M Y, g:i a') . "</strong>, <strong>{$maintenance->due_days}</strong> days time{$notes_next_service}</p>";
                        $maintenance_types_data[$maintenance_type_id] = $message;

                        $pending_entries[] = array(
                            'type' => $maintenance_type_id,
                            'maintenance_id' => $maintenance->maintenance_id,
                            'message' => $message
                        );
                    }
                }
            }
        }

        foreach ($maintenance_types_data as $key => $val) {
            if ($val == '') {
                unset($maintenance_types_data[$key]);
            }
        }
        return array(
            'data' => $maintenance_types_data,
            'entries' => $pending_entries
        );
    }

    function getStatus($asset_id, $company_id)
    {
        $class = '';
        $message = '';

        $maintenance_types_data = $this->maintenance_model->get_types();
        // empty all types data first
        foreach ($maintenance_types_data as $key => $val) {
            $maintenance_types_data[$key] = '';
        }

        // loop through each type
        foreach ($this->maintenance_model->get_types() as $maintenance_type_id => $maintenance_info) {


            // coming maintenance entry
            if ($maintenance_type_id == 2) {
                // check odometer
                $fuel = $this->db
                    ->where('asset_id', $asset_id)
                    ->where('company_id', $company_id)
                    ->order_by('odometer', 'desc')
                    ->get('fuel')
                    ->row();
                if ($fuel) {
                    $odometer = $fuel->odometer;
                } else {
                    $odometer = 0;
                }

                $maintenance_coming = $this->db
                    ->where('asset_id', $asset_id)
                    ->where('company_id', $company_id)
                    ->where('maintenance_type', $maintenance_type_id)
                    ->where($this->maintenance_model->get_column($maintenance_type_id) . ' >', $odometer)
                    ->order_by($this->maintenance_model->get_column($maintenance_type_id), 'asc')
                    ->get('maintenance')
                    ->row();

                $maintenance_expired = $this->db
                    ->where('asset_id', $asset_id)
                    ->where('company_id', $company_id)
                    ->where('maintenance_type', $maintenance_type_id)
                    ->where($this->maintenance_model->get_column($maintenance_type_id) . ' <', $odometer)
                    ->order_by($this->maintenance_model->get_column($maintenance_type_id), 'desc')
                    ->get('maintenance')
                    ->row();
                // maintenance check

                if ($maintenance_coming) {
                    $maintenance = $maintenance_coming;
                } else if ($maintenance_expired) {
                    $maintenance = $maintenance_expired;
                } else {
                    $maintenance = false;
                }

                if ($maintenance) {
                    if ($odometer > $maintenance->odo_hour) {
                        // current odometer is more than scheduled entry
                        $remaining = $odometer - $maintenance->odo_hour;
                        $message = "<p class='text-danger'>Maintenance is due at <strong>{$maintenance->odo_hour} km</strong>, and is {$remaining} km over.</p>";
                        $maintenance_types_data[$maintenance_type_id] = $message;
                        //create exception if not created
                    } else {
                        $diff = $maintenance->odo_hour - $odometer;
                        $message = "<p class=''>Maintenance is due at <strong>{$maintenance->odo_hour} km</strong>, in {$diff} km.</p>";
                        $maintenance_types_data[$maintenance_type_id] = $message;
                    }
                }

            } else {
                $maintenance_coming = $this->db
                    ->select('maintenance.*,
                            datediff(' . $this->maintenance_model->get_column($maintenance_type_id) . ', NOW()) as due_days
                        ', false)
                    ->where($this->maintenance_model->get_column($maintenance_type_id) . ' >=', date('Y-m-d H:i:s'))
                    ->where('asset_id', $asset_id)
                    ->where('company_id', $company_id)
                    ->where('maintenance_type', $maintenance_type_id)
                    ->order_by($this->maintenance_model->get_column($maintenance_type_id), 'desc')
                    ->get('maintenance')
                    ->row();

                $maintenance_expired = $this->db
                    ->select('maintenance.*,
                            datediff(' . $this->maintenance_model->get_column($maintenance_type_id) . ', NOW()) as due_days
                        ', false)
                    ->where($this->maintenance_model->get_column($maintenance_type_id) . ' <=', date('Y-m-d H:i:s'))
                    ->where('asset_id', $asset_id)
                    ->where('company_id', $company_id)
                    ->where('maintenance_type', $maintenance_type_id)
                    ->order_by($this->maintenance_model->get_column($maintenance_type_id), 'desc')
                    ->get('maintenance')
                    ->row();
                // maintenance check

                if ($maintenance_coming) {
                    $maintenance = $maintenance_coming;
                } else if ($maintenance_expired) {
                    $maintenance = $maintenance_expired;
                } else {
                    $maintenance = false;
                }
                // check date
                $column = $this->maintenance_model->get_column($maintenance_type_id);
                $type = $this->maintenance_model->get_type($maintenance_type_id);
                if ($maintenance) {
                    if (time() > strtotime($maintenance->$column)) {
                        $current_date = date('Y-m-d g:i a');
                        $message = "<p class='text-danger'>Maintenance - $type is due on <strong>" . $this->timezone->convertDate(date('Y-m-d H:i:s', strtotime($maintenance->$column)), 'd M Y, g:i a') . "</strong>, <strong>{$maintenance->due_days}</strong> days time.</p>";
                        $maintenance_types_data[$maintenance_type_id] = $message;
                        //create exception if not created
                    } else {
                        $message = "<p class=''>Maintenance - $type is due on <strong>" . $this->timezone->convertDate( date('Y-m-d H:i:s', strtotime($maintenance->$column)), 'd M Y, g:i a') . "</strong>, <strong>{$maintenance->due_days}</strong> days time.</p>";
                        $maintenance_types_data[$maintenance_type_id] = $message;
                    }
                }
            }

        }

        foreach ($maintenance_types_data as $key => $val) {
            if ($val == '') {
                unset($maintenance_types_data[$key]);
            }
        }
        return array(
            'data' => $maintenance_types_data,
            'class' => $class,
            'message' => $message
        );
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 3/7/2015
 * Time: 4:38 PM
 */
class Newsletter_model extends CI_Model
{
    public function create($mysql_data, $send_email = true)
    {
        $mysql_data['created_on'] = date('Y-m-d H:i:s');
        $this->db->insert('users', $mysql_data);
        $user_id = $this->db->insert_id();
        if ($send_email == true) {
            $this->send_email_verification($user_id);
        }
        return $user_id;
    }

    public function send_email_verification($user_id)
    {
        $user = $this->db->where('user_id',$user_id)->get('users')->row();
        $link = site_url('activate/account/'. $user->user_id .'/' .md5($user->created_on));
        $data = array(
            'link' => $link,
            'user' => $user
        );
        $content = $this->load->view('emails/user.verification.php', $data, true);

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($user->email);

        $this->email->subject('Activate your account.');
        $this->email->message($content);

        $this->email->send();
    }

    public function send_invite($invite_id, $extra)
    {
        $invite = $this->db->where('invite_id' , $invite_id)->get('invites')->row();
        $company = $this->db->where('company_id', $invite->company_id)->get('companies')->row();
        $invite_encoded = $invite_id;
        $md5_sum = md5($invite->created_on);
        $link = site_url('invite/accept/'. $invite_encoded . '/' . $md5_sum);
        if ($invite->user_id == 0) {
            $data = array(
                'link' => $link,
                'user' => $extra,
                'company' => $company
            );
            $content = $this->load->view('emails/user.invite.register.php', $data, true);
        } else {
            $data = array(
                'link' => $link,
                'user' => $extra,
                'company' => $company
            );
            $content = $this->load->view('emails/user.invite.php', $data, true);
        }


        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($invite->email);

        $this->email->subject('Invited to join '. $company->name . '.');
        $this->email->message($content);

        $this->email->send();
    }

    public function notify_company_assigned($company_id, $user_id)
    {
        $company = $this->db->where('company_id', $company_id)->get('companies')->row();
        $user = $this->db->where('user_id', $user_id)->get('users')->row();
        if ($user->is_verified != 1) {
            return false;
        }
        $data = array(
            'company' => $company,
            'user' => $user
        );
        $content = $this->load->view('emails/user.company.invite.php', $data, true);

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($user->email);

        $this->email->subject('Invited to join company.');
        $this->email->message($content);

        $this->email->send();
    }

    public function getExceptionUsers($exception_id)
    {
        $exception = $this->db->where('exception_id', $exception_id)->get('exceptions')->row();
        $asset = $this->db->where('asset_id', $exception->asset_id)->get('assets')->row();

        // subscribers
        $subscribers = array(0);
        $subscribers_user_ids = $this->db
            ->select('users.user_id',false)
            ->join('users', 'users.user_id = exception_subscriptions.user_id')
            ->where('exception_subscriptions.exception_id', $exception_id)
            ->where('exception_subscriptions.company_id', $this->auth->company_id())
            ->where('users.is_verified', 1)
            ->get('exception_subscriptions')
            ->result();

        // asset managers for this exception
        $asset_managers_user_id = $this->db
            ->select('user_id', false)
            ->where('asset_type_id', $asset->asset_type_id)
            ->where('company_id', $this->auth->company_id())
            ->order_by('asset_manager_id', 'desc')
            ->get('asset_managers')
            ->result();

        // collect subscriber ids
        foreach ($subscribers_user_ids as $single_subs_uid) {
            $subscribers[] = $single_subs_uid->user_id;
        }

        // add asset managers to the list
        foreach ($asset_managers_user_id as $asset_mng_uid) {
            $subscribers[] = $asset_mng_uid->user_id;
        }

        // verify if they still belong to this company?
        $roles = $this->db
            ->select('user_roles.user_id', false)
            ->join('roles', 'roles.role_id = user_roles.role_id')
            ->where('roles.company_id', $this->auth->company_id())
            ->get('user_roles')
            ->result();

        // collect active users'ids of this company
        $active_users = array();
        foreach ($roles as $active_user) {
            $active_users[] = $active_user->user_id;
        }

        // remove the people that are not the part of this company now
        foreach ($subscribers as $key => $subscriber) {
            if (!in_array($subscriber, $active_users)) {
                unset($subscribers[$key]);
            }
        }

        $subscribers[] = 0;

        $subscribers_list = $this->db
            ->where_in('user_id', $subscribers)
            ->where('is_verified', 1)
            ->get('users')
            ->result();
        return $subscribers_list;
    }

    public function send_new_exception_note_email($exception_id, $exception_note_id, $return_html = false)
    {
        $exception = $this->db->where('exception_id', $exception_id)->get('exceptions')->row();
        $exception_note = $this->db->where('exception_note_id', $exception_note_id)->get('exception_notes')->row();
        $exception_note->user = $this->db->where('user_id', $exception_note->created_by)->get('users')->row();
        $asset = $this->db->where('asset_id', $exception->asset_id)
            ->join('asset_types', 'asset_types.asset_type_id = assets.asset_type_id')
            ->get('assets')->row();
        $company = $this->db->where('company_id', $exception->company_id)->get('companies')->row();

        $subscribers_list = $this->getExceptionUsers($exception_id);

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        foreach ($subscribers_list as $user) {
            $data = array(
                'user' => $user,
                'exception' => $exception,
                'exception_note' => $exception_note,
                'asset' => $asset,
                'company' => $company
            );
            $content = $this->load->view('emails/exception.note.subscribe.php', $data, true);

            if ($return_html == true) {
                return $content;
                exit;
            }

            $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
            $this->email->to($user->email);

            $this->email->subject('New comment on exception: '. $exception->title);
            $this->email->message($content);

            $this->email->send();
        }
    }

    public function createFromSocial($type, $user_data)
    {
        $random_pass = rand(888957, 4545455);
        $mysql_data = array(
            'email' => $user_data->email,
            'first_name' => $user_data->firstName,
            'last_name' => $user_data->lastName,
            'address' => $user_data->address,
            'city' => $user_data->city,
            'country' => $user_data->country,
            'is_superuser' => 0,
            'is_verified' => ($user_data->email == $user_data->emailVerified) ? 1 : 0,
            'active' => 1,
            'password' => md5($random_pass),
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s')
        );
        $this->db->insert('users', $mysql_data);
        $user_id = $this->db->insert_id();

        $this->db->where('user_id', $user_id);
        $this->db->update('users', array('created_by' => $user_id, 'updated_by' => $user_id));

        // user has not authenticated with this type, attach it
        $mysql_data = array(
            'email' => $user_data->email,
            'account_type' => $type,
            'token' => '',
            'user_id' => $user_id,
            'profile_pic' => $user_data->photoURL,
            'created_on' => date('Y-m-d H:i:s'),
            'updated_on' => date('Y-m-d H:i:s'),
            'created_by' => $user_id,
            'updated_by' => $user_id
        );
        $this->db->insert('authentications', $mysql_data);

        $this->send_email_signup_password($user_id, $type, $user_data, $random_pass);

        return $user_id;
    }

    public function send_email_signup_password($user_id, $type, $user_data, $new_pass)
    {
        $user = $this->db->where('user_id',$user_id)->get('users')->row();
        $data = array(
            'password' => $new_pass,
            'type' => $type,
            'user_data' => $user_data,
            'user' => $user
        );
        $content = $this->load->view('emails/user.signup.password.php', $data, true);

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($user->email);

        $this->email->subject('Welcome to new account.');
        $this->email->message($content);

        $this->email->send();
    }

    public function update($mysql_data, $news_id)
    {
        $mysql_data['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('news_id', (int)$news_id);
        return $this->db->update('newsletter', $mysql_data);
    }

    public function getById($user_id)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->get('users')->row();
    }

    public function getUsers($where = array())
    {
        return $this->db->where($where)->get('users')->result();
    }

    public function remove($user_id)
    {
        return $this->db->where('user_id', $user_id)->delete('users');
    }

    public function send_email_forgot($user)
    {
        $link = site_url('login/recover/' . $user->user_id . '/' . md5($user->updated_on));
        $data = array(
            'user' => $user,
            'link' => $link
        );
        $content = $this->load->view('emails/user.recover.php', $data, true);

        $config = $this->config->item('email_config');
        $this->load->library('email', $config);

        $this->email->from($this->config->item('email_from_address'), $this->config->item('email_from_name'));
        $this->email->to($user->email);

        $this->email->subject('Reset account password.');
        $this->email->message($content);

        $this->email->send();

    }
}
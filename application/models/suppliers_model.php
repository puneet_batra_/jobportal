<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/6/2015
 * Time: 1:26 PM
 */
class Suppliers_Model extends CI_Model
{
    public function getList()
    {
        $suppliers = $this->db->where('company_id', $this->auth->company_id())->get('suppliers')->result();
        return $suppliers;
    }
}
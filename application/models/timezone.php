<?php
/**
 * Created by PhpStorm.
 * User: Step
 * Date: 5/1/2015
 * Time: 12:33 PM
 */
class Timezone extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function timezones_list() {
        $zones_array = array();
        $original_timezone = date_default_timezone_get();
        foreach(timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones_array[$zone] = date('P');
        }
        date_default_timezone_set($original_timezone);

        $zones_array['Asia/Calcutta'] = '+05:30';
        return $zones_array;
    }

    function get_offset()
    {
        $timezone = $this->config->item('default_timezone');
        return $this->timezones_list()[$timezone];
    }

    function getTimezone($user_offset)
    {
        $user_timezone = 'UTC';
        foreach ($this->timezones_list() as $timezone => $offset) {
            if ($offset == $user_offset) {
                $user_timezone = $timezone;
            }
        }
        return $user_timezone;
    }

    function convertSql($field_name)
    {
        $server_timezone = date_default_timezone_get();
        $server_offset = $this->timezones_list()[$server_timezone];
        return 'CONVERT_TZ('. $field_name. ',"'. $server_offset .'","'. $this->get_offset() .'")';
    }

    function convertDate($date, $format, $type = 'server-to-config')
    {
        switch ($type) {
            case 'server-to-config':
                $from_timezone = date_default_timezone_get();
                $to_timezone = $this->config->item('default_timezone');
                break;
            case 'server-to-user':
                $from_timezone = date_default_timezone_get();
                $to_timezone = $this->getTimezone($this->session->userdata('user_timezone'));
                break;
            case 'user-to-server':
                $from_timezone = $this->getTimezone($this->session->userdata('user_timezone'));
                $to_timezone = date_default_timezone_get();
                break;
        }
        $date = new DateTime($date, new DateTimeZone($from_timezone));
        $date->setTimeZone(new DateTimeZone($to_timezone));
        return $date->format($format);
    }

}
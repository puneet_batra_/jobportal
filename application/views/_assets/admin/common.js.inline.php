<script>
    $(function () {
        UIIdleTimeout();
    });
    function uiLoader(element, task) {
        if(task == 'show') {
            Metronic.blockUI({
                target: element,
                animate: true
            });
        } else if (task == 'hide') {
            Metronic.unblockUI(element);
        }
    }
    function UIIdleTimeout() {

        // cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
        var $countdown;

        $('body').append('<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static"><div class="modal-dialog modal-small"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Your session is about to expire.</h4></div><div class="modal-body"><p><i class="fa fa-warning"></i> You session will be locked in <span id="idle-timeout-counter"></span> seconds.</p><p>Do you want to continue your session?</p></div><div class="modal-footer"><button id="idle-timeout-dialog-logout" type="button" class="btn btn-default">No, Logout</button><button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Keep Working</button></div></div></div></div>');

        // start the idle timer plugin
        $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
            idleAfter: <?php echo $this->config->item('sess_time_to_update') - 10 ?>, // 5 seconds
            timeout: 60000, //30 seconds to timeout
            pollingInterval: 5, // 5 seconds
            keepAliveURL: '<?php echo site_url('sess') ?>',
            serverResponseEquals: 'OK',
            onTimeout: function () {
                window.location = "<?php echo site_url('logout'); ?>";
            },
            onIdle: function () {
                $('#idle-timeout-dialog').modal('show');
                $countdown = $('#idle-timeout-counter');

                $('#idle-timeout-dialog-keepalive').on('click', function () {
                    $('#idle-timeout-dialog').modal('hide');
                });

                $('#idle-timeout-dialog-logout').on('click', function () {
                    $('#idle-timeout-dialog').modal('hide');
                    $.idleTimeout.options.onTimeout.call(this);
                });
            },
            onCountdown: function (counter) {
                $countdown.html(counter); // update the counter
            }
        });
    }
    function ajaxLoader(task, message) {
        var html = '<img src="<?php echo public_url('img', false); ?>/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (message ? message : 'Loading...') + '</span>';
        if(task == 'show')
        {
            $('.page-loading').remove();
            $('body').append('<div class="page-loading">' + html + '</div>');
        } else if(task == 'hide')
        {
            $('.page-loading').remove();
        } else if(task == 'html')
        {
            return html;
        }
    }

    $(function(){
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        // Asset Search
        var custom = new Bloodhound({
            datumTokenizer: function(d) { return d.tokens; },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: 'demo/typeahead_custom.php?query=%QUERY',
            limit: 10
        });

        custom.initialize();
        $('#typeahead_example_3').typeahead(null, {
            name: 'datypeahead_example_3',
            displayKey: 'value',
            source: custom.ttAdapter(),
            hint: (Metronic.isRTL() ? false : true),
            templates: {
                suggestion: Handlebars.compile([
                    '<div class="media">',
                    '<div class="pull-left">',
                    '<div class="media-object">',
                    '<img src="{{img}}" width="50" height="50"/>',
                    '</div>',
                    '</div>',
                    '<div class="media-body {{class}}"><a class="no-underline" href="{{link}}">',
                    '<h4 class="media-heading">{{value}}</h4>',
                    '<p>{{desc}}</p>',
                    '</a></div>',
                    '</div>',
                ].join(''))
            }
        });
        companies_list_adjuster();
        $(document).find('.dataTables_wrapper select').select2();
    });

    function companies_list_adjuster() {
        var li = $(document).find('.dropdown-company .slimScrollDiv li');
        var count = li.length;
        var total_height = 0;
        if(count <= 4) {
            li.each(function(ele){
                total_height += $(this).actual('height');
            });
            $(document).find('.dropdown-company .slimScrollDiv').css('height', total_height + 'px');
            $(document).find('.dropdown-company .slimScrollDiv .dropdown-menu-list.scroller').css('height', total_height + 'px');
        }
    }
</script>
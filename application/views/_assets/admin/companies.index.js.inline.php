<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/companies/get') ?>?a=index',
            type: 'POST'
        },
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'company_id'},
            { mData: 'user'},
            { mData: 'name'},
            { mData: 'email'},
            { mData: 'active'},
            { mData: 'actions'}
        ],
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [5]},
            {'bSearchable' : false, 'aTargets': [5] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 4
            },
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    function login_as(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/companies/loginas') ?>', {cid: r_id}, function(data){
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                toastr['success'](data.message);
                setTimeout(function () {
                    window.location.replace('<?php echo site_url('home') ?>')
                }, 600);
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json')
    }

    $('#select2_add_userid').select2(
        {
            placeholder: 'Select a user',
            allowClear: true,
            id: function(item){
                return item.user_id;
            },
            formatResult: function(item) {
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += "ID:" + item.user_id + ', ' + item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            formatSelection: function(item){
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += "ID:" + item.user_id + ', ' + item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            initSelection: function(){
                return '';
            },
            ajax: {
                url: '<?php echo site_url('admin/companies/getusers') ?>',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    return {
                        q: term,
                        page_limit: 10
                    };
                },
                results: function(data, page) {
                    return {
                        results: data.users
                    };
                }
            }
        }
    );
    $('#a').click(function(e){
        e.preventDefault();
        alert($('#select2_add_userid').val());
    })

    $('#select2_edit_userid').select2(
        {
            placeholder: 'Select a user',
            allowClear: true,
            id: function(item){
                return item.user_id;
            },
            formatResult: function(item) {
                var markup = "";
                if (item.user_id !== undefined) {
                    markup += "ID:" + item.user_id + ', ' + item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            formatSelection: function(item){
                var markup = "";
                if (item.user_id !== undefined) {
                    markup += "ID:" + item.user_id + ', ' + item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            initSelection: function(element, callback) {
                var id = $(element).val();
                callback({user_id: 4, first_name:'hhhff', email: 'davine@shsss.ch'});
            },
            ajax: {
                url: '<?php echo site_url('admin/companies/getusers') ?>',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    return {
                        q: term,
                        page_limit: 10
                    };
                },
                results: function(data, page) {
                    return {
                        results: data.users
                    };
                }
            }
        }
    );


    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                var form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/companies/add') ?>', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');
                        $('#select2_add_userid').val('');
                        $('#select2_add_userid').change();
                        form[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            ignore: "",
            rules:{
                user_id: {
                    required: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        method: 'POST',
                        data: {
                            rules: 'required|numeric'
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email|is_unique[companies.email]'
                        }
                    }
                },
                active: {
                    required: true
                }
            },
            messages:{
                email: {
                    remote: 'Email not valid or already exists.'
                }

            }
        }
    );

    // edit form
    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/companies/single') ?>', {cid: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                $('[name="cid"]', form).val(r_id);
                $('[name="name"]', form).val(data.record.name);
                $('[name="email"]', form).val(data.record.email);
                $('[name="user_id"]', form).val(data.record.user_id);
                $('#select2_edit_userid').select2('data',
                    {user_id: data.record.user_id, first_name: data.record.user.first_name, email: data.record.email}, true);
                $('[name="active"] option[value="' + data.record.active + '"]').attr('selected', 'selected');

                $('[name="active"]', form).change();
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');

    }


    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/companies/edit') ?>', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');
                        $('#select2_edit_userid').val('');
                        $('#select2_edit_userid').change();
                        form[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            ignore: "",
            rules:{
                user_id: {
                    required: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        method: 'POST',
                        data: {
                            rules: 'required|numeric'
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email'
                        }
                    }
                },
                active: {
                    required: true
                }
            },
            messages:{
                email: {
                    remote: 'Email not valid or already exists.'
                }

            }
        }
    );


    // delete
    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/companies/delete') ?>?a=index', {cid: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to deactivate?', function(result){
           if(result == true) {
               uiLoader('#portlet-body', 'show');
               $.post('<?php echo site_url('admin/companies/inactivate') ?>', {cid: r_id}, function(data){
                   if(data.status == 'fail')
                   {
                       for(var key in data.errors) {
                           toastr['error'](data.errors[key]);
                       }
                   } else if(data.status == 'success') {
                       toastr['success'](data.message);
                       mydatatable._fnAjaxUpdate();
                   }
                   uiLoader('#portlet-body', 'hide');
               }, 'json');
           }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to make it active?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/companies/activate') ?>', {cid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }

        });

    }

    $('#active_select2').select2();
    $('#edit_active_select2').select2();

</script>
<script>
    toastr.options = {
        "closeButton": true
    };
    function customFun(target_val) {
        var cv_id = target_val.parentNode.parentNode.childNodes[0].innerHTML;
        $('#custom_id').val(cv_id);
        mydatatable1._fnAjaxUpdate();
    }

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/cv_url/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [4]},
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 2
            }
        ],
        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'cv_id'},
            { mData: 'username'},
            { mData: 'active'},
            { mData: 'cv_url'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });


    // 2nd datatable

        var dataTableOptions1 = {
            'filter': true,
            'processing': false,
            "serverSide": true,
            "ajax": {
                url: '<?php echo site_url('admin/cv_url/getCvDetail') ?>?a=index',
                type: 'POST',
                "data": function (d) {
                    d.custom_id = $('#custom_id').val();
                    return d;
                }
            },
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "processing": ajaxLoader('html')
            },
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [5]},
                {'bSearchable': false, 'aTargets': [5]},
                {
                    // The `data` parameter refers to the data for the cell (defined by the
                    // `data` option, which defaults to the column being worked with, in
                    // this case `data: 0`.
                    "render": function (data, type, row) {
                        if (data == 1) {
                            newData = '<a onclick="cv_detail_disapprove(this)" class="label label-sm label-success">Yes</a>';
                        } else {
                            newData = '<a onclick="cv_detail_approve(this)" class="label label-sm label-danger">No</a>';
                        }
                        return newData;
                    },
                    "targets": 4
                }
            ],
            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 15,

            aoColumns: [
                {mData: 'cv_detail_id'},
                {mData: 'position_name'},
                {mData: 'industry_type'},
                {mData: 'project_type'},
                {mData: 'active'},
                {mData: 'actions'}
            ]
        };

        var mydatatable1 = $('#sample_3').dataTable(dataTableOptions1);
        $('#sample_3')
            .on('preXhr.dt', function (e, settings, data) {

                uiLoader('#portlet-body', 'show');
            }).on('draw.dt', function () {

                uiLoader('#portlet-body', 'hide');
            });



    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }



    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }


    function cv_detail_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/cvDetailDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function cv_detail_approve(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to approve?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/cv_detail_approve') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function cv_detail_disapprove(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to disapprove?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/cv_url/cv_detail_disapprove') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

</script>
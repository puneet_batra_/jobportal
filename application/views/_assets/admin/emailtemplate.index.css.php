<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'); ?>"/>

<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/select2/select2.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.css'); ?>"/>


<script>
    toastr.options = {
        "closeButton": true
    };
    function customFun(target_val) {
        var cv_id = target_val.parentNode.parentNode.childNodes[0].innerHTML;
        $('#custom_id').val(cv_id);
        mydatatable1._fnAjaxUpdate();
    }

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/export_data/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [2]},
            {'bSearchable' : false, 'aTargets': [2] },
            {"targets": [ 3 ], "visible": false},
            {"targets": [ 4 ], "visible": false},
            {"targets": [ 5 ], "visible": false},
            {"targets": [ 6], "visible": false},
            {"targets": [ 7 ], "visible": false},
            {"targets": [ 8 ], "visible": false},
            {"targets": [ 9 ], "visible": false},
            {"targets": [ 10 ], "visible": false},
            {"targets": [ 11 ], "visible": false},
            {"targets": [ 12 ], "visible": false},
            {"targets": [ 13 ], "visible": false},
            {"targets": [ 14 ], "visible": false},
            {"targets": [ 15 ], "visible": false},
            {"targets": [ 16 ], "visible": false},
            {"targets": [ 17 ], "visible": false},
            {"targets": [ 18 ], "visible": false},
            {"targets": [ 19 ], "visible": false},
            {"targets": [ 20 ], "visible": false},
            {"targets": [ 21 ], "visible": false},
            {"targets": [ 22 ], "visible": false},
            {"targets": [ 23 ], "visible": false},
            {"targets": [ 24 ], "visible": false},
            {"targets": [ 25 ], "visible": false},
            {"targets": [ 26 ], "visible": false},
            {"targets": [ 27 ], "visible": false},
            {"targets": [ 28 ], "visible": false},
            {"targets": [ 29 ], "visible": false},
            {"targets": [ 30 ], "visible": false},
            {"targets": [ 31 ], "visible": false},
            {"targets": [ 32 ], "visible": false},
            {"targets": [ 33 ], "visible": false},
            {"targets": [ 34 ], "visible": false},
            {"targets": [ 35 ], "visible": false},
            {"targets": [ 36 ], "visible": false},
            {"targets": [ 37 ], "visible": false},
            {"targets": [ 38 ], "visible": false},
            {"targets": [ 39 ], "visible": false},
            {"targets": [ 40 ], "visible": false},
            {"targets": [ 41 ], "visible": false},
            {"targets": [ 42 ], "visible": false},
            {"targets": [ 43 ], "visible": false},
            {"targets": [ 44 ], "visible": false}

        ],
        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],

        // set the initial value
        "pageLength": 15,

        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "<?php echo public_url('plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf'); ?>",
            "aButtons": [{
                "sExtends": "pdf",
                "sButtonText": "PDF"
            }, {
                "sExtends": "csv",
                "sButtonText": "CSV"
            }, {
                "sExtends": "xls",
                "sButtonText": "Excel"
            }, {
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        },

        aoColumns: [
            { mData: 'user_id'},
            { mData: 'user_type_name'},
            { mData: 'actions'},
            { mData: 'job_position'},
            { mData: 'job_industry'},
            { mData: 'project_type'},
            { mData: 'username'},
            { mData: 'first_name'},
            { mData: 'middle_name'},
            { mData: 'last_name'},
            { mData: 'phone'},
            { mData: 'dob'},
            { mData: 'email'},
            { mData: 'address'},
            { mData: 'city'},
            { mData: 'country'},
            { mData: 'nationality'},
            { mData: 'age'},
            { mData: 'height'},
            { mData: 'weight'},
            { mData: 'relegion'},
            { mData: 'martial_status'},
            { mData: 'passport_num'},
            { mData: 'mobile_num'},
            { mData: 'telephone_num'},
            { mData: 'skype_id'},
            { mData: 'job_seeker_status'},
            { mData: 'languages'},
            { mData: 'hobbies_name'},
            { mData: 'edu_level'},
            { mData: 'edu_institute_name'},
            { mData: 'edu_city'},
            { mData: 'edu_country'},
            { mData: 'edu_year'},
            { mData: 'edu_degree'},
            { mData: 'edu_grade'},
            { mData: 'exp_company_name'},
            { mData: 'exp_city'},
            { mData: 'exp_country'},
            { mData: 'exp_years'},
            { mData: 'exp_job_position'},
            { mData: 'exp_job_description'},
            { mData: 'company_reference'},
            { mData: 'soft_skills'},
            { mData: 'hard_skills'}

        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
    });



</script>
<script>
    function get_storage_block() {
        var sblock = $('#storage-block');
        $.post('<?php echo site_url('admin/home/getsize') ?>', function (data) {
            $('.number', sblock).html(data.size + ' ' + data.unit);
        }, 'json');
    }

    get_storage_block();
</script>
<script>
    toastr.options = {
        "closeButton": true
    };
    function customFun(target_val) {
        var cv_id = target_val.parentNode.parentNode.childNodes[0].innerHTML;
        $('#custom_id').val(cv_id);
        mydatatable1._fnAjaxUpdate();
    }
    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/newsletter/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [2]},
            {'bSearchable' : false, 'aTargets': [2] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    console.log(data);
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 3
            }
            ],

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'news_id'},
            { mData: 'news_title'},
            { mData: 'news_description'},
            { mData: 'active'},
            { mData: 'sub_user'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    // 2nd datatable

    var dataTableOptions1 = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/newsletter/subscriberList') ?>?a=index',
            type: 'POST',
            "data": function (d) {
                d.custom_id = $('#custom_id').val();
                return d;
            }

        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [1]},
            {'bSearchable' : false, 'aTargets': [1] },
        ],
        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'sub_name'},
            { mData: 'sub_email'}
        ]
    };

    var mydatatable1 = $('#sample_3').dataTable(dataTableOptions1);
    $('#sample_3')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });



    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/newsletter/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/newsletter/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }



    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/newsletter/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/newsletter/getEdit') ?>?a=index', {id: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
               $('[name="news_id"]', form).val(r_id);
                CKEDITOR.instances.editior2.setData( data.news_content );
                $('[name="news_title"]', form).val(data.news_title);
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');
    }




    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var add_news_title = $('#add_news_title').val();
                var add_news_content = CKEDITOR.instances.editor1.getData();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/newsletter/add') ?>', {'news_title':add_news_title,'news_content':add_news_content}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');

                        form[0].reset();
                        $('.form-group', form).removeClass('has-error');
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                news_title: {
                    required: true
                },
                news_content: {
                    required: false
                }
            }
        }
    );


    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var edit_news_title = $('#edit_news_title').val();
                var edit_news_content = CKEDITOR.instances.editior2.getData();
                var edit_news_id = $('#edit_news_id').val();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/newsletter/postUpdate') ?>?a=index',{'news_id':edit_news_id,'news_title':edit_news_title,'news_content':edit_news_content},function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');

                        form[0].reset();
                        $('select', form).trigger('change');
                        $('.form-group', form).removeClass('has-error');

                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                news_title: {
                    required: true
                },
                news_content: {
                    required: false
                }
            }
        }
    );


</script>
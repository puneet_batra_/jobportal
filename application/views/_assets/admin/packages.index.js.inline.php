<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/packages/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [8]},
            {'bSearchable' : false, 'aTargets': [8] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    console.log(data);
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 7
            }
            ],

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,
        

        aoColumns: [
            { mData: 'package_id'},
            { mData: 'package_name'},            
            { mData: 'user_type_name'},
            { mData: 0 , //or address field
             "mRender" : function ( data, type, full ) {              
                return full['package_price_in']+' '+full['currency_symbol'];}
            },
            { mData: 'package_validity'},
            { mData: 'total_credits'},            
            { mData: 'credit_per_user_activity'},
            { mData: 'active'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });



    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }



    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        
        var form = $('#form-edit');

        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/packages/getEdit') ?>?a=index', {id: r_id}, function (data) {
            
            if(data.status == 'fail')
            { 
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {              
                $('[name="package_id"]', form).val(r_id);
                CKEDITOR.instances.editior2.setData( data.package_facilities );
                $("#edit_user_type").html(data.options);
                $("#edit_package_currency").html(data.options1);
                $('[name="package_name"]', form).val(data.package_name);

                if(data.user_type) {
                    $('[name="user_type"] option[value="' + data.user_type + '"]', form).attr('selected', 'selected');
                }
                $('[name="user_type"]', form).change();   

               // $('[name="user_type"]', form).val(data.user_type);   

                $('[name="package_price_in"]', form).val(data.package_price_in);
                
                if(data.package_currency) {
                    $('[name="package_currency"] option[value="' + data.package_currency + '"]', form).attr('selected', 'selected');
                }
                $('[name="package_currency"]', form).change();   

                $('[name="package_validity"]', form).val(data.package_validity);
                $('[name="total_credits"]', form).val(data.total_credits);
                $('[name="credit_per_user_activity"]', form).val(data.credit_per_user_activity);
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');    
    }



    $('#form-add').validate(
        {   
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {

                form = $(form);

                $('[type=submit]', form).attr('disabled', 'disabled');               
                var add_package_name = $('#add_package_name').val();                
                var add_package_facilities = CKEDITOR.instances.editor1.getData();               
                var add_user_type = $('#add_user_type').val();
                var add_package_price_in = $('#add_package_price_in').val();
                var add_package_currency = $('#add_package_currency').val();
                var add_package_validity = $('#add_package_validity').val();
                var add_total_credits = $('#add_total_credits').val();
                var add_credit_per_user_activity = $('#add_credit_per_user_activity').val();               
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/packages/add') ?>', {'package_name':add_package_name,'package_facilities':add_package_facilities,'user_type':add_user_type,'package_price_in':add_package_price_in,'package_currency':add_package_currency,'package_validity':add_package_validity,'total_credits':add_total_credits,'credit_per_user_activity':add_credit_per_user_activity}, function(data){
                   
                    if(data.status == 'fail')
                    {    
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');

                        form[0].reset();
                        $('.form-group', form).removeClass('has-error');
                        toastr['success'](data.message);                      
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                package_name: {
                    required: true
                },
                package_price_in: {
                    required: true
                },
                package_validity: {
                    required: true
                },
                user_type:{
                     required: true
                },
                package_currency:{
                     required: true
                }
            }
        }
    );

    /*Get Category*/
    function get_user_type(){        
        $.get(
            "<?php echo site_url('admin/packages/get_user_type')?>",
            function(data){
                if(data.status == "success"){                    
                    $("#add_user_type").html(data.options);
                    $("#add_package_currency").html(data.options1);
                    $("#createModal").modal("show");
                }
            }
        ,'json');        
    };


    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var edit_package_id = $('#edit_package_id').val();                
                var edit_package_name = $('#edit_package_name').val();                
                var edit_package_facilities = CKEDITOR.instances.editior2.getData();                                
                var edit_user_type = $('#edit_user_type').val();                
                var edit_package_price_in= $('#edit_package_price_in').val();                
                var edit_package_currency= $('#edit_package_currency').val();  
                var edit_package_validity = $('#edit_package_validity').val();                
                var edit_total_credits = $('#edit_total_credits').val();                
                var edit_credit_per_user_activity = $('#edit_credit_per_user_activity').val();
               
               
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/packages/postUpdate') ?>?a=index',{'package_id':edit_package_id,'package_name':edit_package_name,'package_facilities':edit_package_facilities,'user_type':edit_user_type,'package_price_in':edit_package_price_in,'package_currency':edit_package_currency,'package_validity':edit_package_validity,'total_credits':edit_total_credits,'credit_per_user_activity':edit_credit_per_user_activity},function(data){                    
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');

                        form[0].reset();
                        $('select', form).trigger('change');
                        $('.form-group', form).removeClass('has-error');

                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                package_name: {
                    required: true
                },
                package_price: {
                    required: true
                },
                package_validity: {
                    required: true
                },
                user_type:{
                    required: true
                },
                package_currency:{
                    required: true 
                }
            }
        }
    );


</script>
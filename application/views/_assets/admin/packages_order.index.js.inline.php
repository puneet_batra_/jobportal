<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/packages_order/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
       // });

        /*"aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [2]},
            {'bSearchable' : false, 'aTargets': [2] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    console.log(data);
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">view</a>';
                    } else{

                    }newData = '<a onclick="make_active(this)" class="label label-sm label-success">view</a>';
                    return newData;
                },
                "targets": 8
            }
            ],*/

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'package_order_id'},
            { mData: 'user_type_name'},            
            { mData: 'username'},
            { mData: 'package_name'},
            { mData: 'package_validity'},
            { mData: 'purchased_at'},            
            { mData: 'expired_at'},
            { mData: 'amount'},
           // { mData: 'view'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });



    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }



    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/packages/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function view_invoice(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
      
        var form = $('#form-edit');
       
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/packages_order/getInvoice') ?>?a=index', {id: r_id}, function (data) {           
            if(data.status == 'fail')
            { 
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {              
                
                $("#package_order_id").html(data.package_order_id);
                $("#user_type").html(data.user_type);
                $("#user_name").html(data.user_name);
                $("#package_name").html(data.package_name);
                $("#package_validity").html(data.package_validity);
                $("#amount").html(data.amount);
                $("#purchased_at").html(data.purchased_at);
                $("#expired_at").html(data.expired_at);
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');    
    }
     
    function print_invoice(){
       

      //  var divToPrint=document.getElementById("invoice");
    var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
      disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25"; 
  var content_vlue = document.getElementById("invoice").innerHTML; 
 
  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('<html><head><title>Credit Package Invoice</title>'); 
   docprint.document.write('<style> #btn{display:none;} </style>');
   docprint.document.write('</head><body>');          
   docprint.document.write(content_vlue);          
   docprint.document.write('</body></html>'); 
   //docprint.document.close(); 
    docprint.print();
 // docprint.focus(); 
   /*newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();*/
    }

    $('#form-add').validate(
        {   
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {

                form = $(form);

                $('[type=submit]', form).attr('disabled', 'disabled');               
                var add_package_name = $('#add_package_name').val();                
                var add_package_facilities = CKEDITOR.instances.editor1.getData();               
                var add_user_type = $('#add_user_type').val();
                var add_package_price_in = $('#add_package_price_in').val();
                var add_package_validity = $('#add_package_validity').val();
                var add_total_credits = $('#add_total_credits').val();
                var add_credit_per_user_activity = $('#add_credit_per_user_activity').val();               
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/packages/add') ?>', {'package_name':add_package_name,'package_facilities':add_package_facilities,'user_type':add_user_type,'package_price_in':add_package_price_in,'package_validity':add_package_validity,'total_credits':add_total_credits,'credit_per_user_activity':add_credit_per_user_activity}, function(data){
                   
                    if(data.status == 'fail')
                    {    
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');

                        form[0].reset();
                        $('.form-group', form).removeClass('has-error');
                        toastr['success'](data.message);                      
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                package_name: {
                    required: true
                },
                package_price_in: {
                    required: true
                },
                package_validity: {
                    required: true
                },
                user_type:{
                     required: true
                }
            }
        }
    );

    /*Get Category*/
    function get_user_types(){        
        $.get(
            "<?php echo site_url('admin/packages/get_user_types')?>",
            function(data){
                if(data.status == "success"){                    
                    $("#add_user_type").html(data.options);
                    $("#createModal").modal("show");
                }
            }
        ,'json');
    };


   


</script>
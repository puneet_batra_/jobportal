<script>
    $(function(){
        $('.profile-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                first_name: {
                    required: true,
                    remote:{
                        url: '<?php echo site_url('validator/validate'); ?>',
                        type: 'POST',
                        data: {
                            rules: 'required|alpha'
                        }
                    }
                },
                last_name: {
                    remote:{
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data: {
                            rules: 'alpha'
                        }
                    }
                },
                email: {
                    required: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email'
                        }
                    }
                },
                address: {
                    required: false
                },
                city: {
                    required: false,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data: {
                            rules: 'required|alpha'
                        }
                    }
                },
                country: {
                    required: false
                },
                password: {
                    minlength: 4
                },
                rpassword: {
                    equalTo: '#password'
                }
            },

            messages: {
                first_name: {
                    remote: 'First name should be characters only.'
                },
                last_name:{
                    remote: 'Last name should be characters only.'
                },
                email: {
                    required: "Email is required.",
                    remote: 'Invalid email address.'
                },
                city: {
                    remote: 'City name should contain characters only.'
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                $('.alert-danger', $('.profile-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.profile-form input').keyup(function(e) {
            $('.profile-form').validate().element($(this));
            if (e.which == 13) {
                if ($('.profile-form').validate().form()) {
                    $('.profile-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='<?php echo public_url('img/flags', false); ?>/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        if (jQuery().select2) {
            $("#select2_sample7").select2({
                placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function(m) {
                    return m;
                }
            });


            $('#select2_sample7').change(function() {
                $('.profile-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

        $('.unconnect').click(function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm('Are you sure you want to unlink this account?',function(result){
                if(result == true) {
                    window.location.replace(href);
                }
            })
        })
    });


</script>
<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/project_type/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [4]},
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 3
            }
        ],
        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'project_type_id'},
            { mData: 'industry_type'},
            { mData: 'project_type'},
            { mData: 'active'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });


// 2nd datatable

    var dataTableOptions1 = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/project_type/getSuggestion') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [4]},
            {'targets' : [6],'visible' : false,'searchable':false},
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

                    if(data == 1) {
                        newData = '<a onclick="sug_disapprove('+ row.project_sug_id +','+ row.project_type +','+ row.industry_id +')" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="sug_approve('+ row.project_sug_id +',\''+ row.project_type +'\','+ row.industry_id+')" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 4
            }
        ],
        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'project_sug_id'},
            { mData: 'user_type'},
            { mData: 'industry_type'},
            { mData: 'project_type'},
            { mData: 'active'},
            { mData: 'actions'},
            { mData: 'industry_id'}
        ]
    };

    var mydatatable1 = $('#sample_3').dataTable(dataTableOptions1);
    $('#sample_3')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });





    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }




    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function sug_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/sugDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function sug_approve(r_id,project_type,ind_id) {
//        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
//        var ind_type = target.parentNode.parentNode.childNodes[2].innerHTML;
//        var project_type = target.parentNode.parentNode.childNodes[3].innerHTML;
//        var ind_id = target.parentNode.parentNode.childNodes[6].innerHTML;

        bootbox.confirm('Are you sure you want to approve?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/sug_approve') ?>?a=index', {uid: r_id,project_type: project_type,ind_id: ind_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function sug_disapprove(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to disapprove?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/project_type/sug_disapprove') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable1._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }



    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/project_type/getEdit') ?>?a=index', {id: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                $('[name="project_type_id"]', form).val(r_id);
                $('[name="project_type"]', form).val(data.project_type);
                if(data.industry_type) {
                    $('[name="ind_sec"] option[value="' + data.industry_type + '"]', form).attr('selected', 'selected');
                }
                $('[name="ind_sec"]').change();
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');

    }



    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/project_type/add') ?>', formData , function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');
                        form[0].reset();
                        $('.form-group', form).removeClass('has-error');
                        toastr['success'](data.message);
                        $('select', form).trigger('change');
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();

                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                project_type: {
                    required: true
                },
                ind_sec: {
                    required: false
                }
            }
        }
    );


    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/project_type/postUpdate') ?>?a=index',formData,function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');

                        form[0].reset();
                        $('select', form).trigger('change');
                        $('.form-group', form).removeClass('has-error');

                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                project_type: {
                    required: true
                },
                ind_sec: {
                    required: false
                }
            }
        }
    );

//    Import CSV

    $('#form-csv').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                var formData = new FormData(form);
                var l = Ladda.create($('[type=submit]', form)[0]);
                $.ajax({
                    url: '<?php echo site_url('admin/project_type/importCSV') ?>',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        }
                        else if(data.status == 'success') {
                            $('#csvModal').modal('hide');
                            toastr['success'](data.message);
                            mydatatable._fnAjaxUpdate();
                        }
                        l.stop();
                    }
                });
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                csv_file: {
                    required: true
                }
            }
        }
    );



</script>
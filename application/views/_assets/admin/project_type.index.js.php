<script type="text/javascript" src="<?php echo public_url('plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.js"'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ckeditor/adapters/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('scripts/table-advanced.js'); ?>"></script>
<script src="<?php echo asset_url('inline/asset/admin/project_type.index.js') ?>"></script>

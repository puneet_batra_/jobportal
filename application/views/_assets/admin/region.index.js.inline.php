<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/region/get') ?>?a=index',
            type: 'POST'
        },
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [3]},
            {'bSearchable' : false, 'aTargets': [3] },
            {'bSortable' : false,'aTargets' : [4]},
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    console.log(data);
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 2
            }
            ],

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'region_id'},
            { mData: 'region_name'},            
            { mData: 'active'},
            { mData: 'countries'},
            { mData: 'actions'}
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });



    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;

        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/region/inactivate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;            
        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/region/activate') ?>?a=index', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }
    function get_countries(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;                   
        var link="<?php echo site_url('admin/country/index')?>"+"/"+r_id;             
        window.location.href=link;
    }


    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?(By deleting a region all countries of particular region are also deleted)', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/region/getDelete') ?>?a=index', {id: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;        
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/region/getEdit') ?>?a=index', {id: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
               $('[name="region_id"]', form).val(r_id);
              //  CKEDITOR.instances.editior2.setData( data.news_content );
                //$('[name="page_content"]', form).val(data.page_content);
                $('[name="region_name"]', form).val(data.region_name);

                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');

    }
    

    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var add_region_name = $('#add_region_name').val();
               
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/region/add') ?>', {'region_name':add_region_name}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');

                        form[0].reset();
                        $('.form-group', form).removeClass('has-error');
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                region_name: {
                    required: true
                }
            }
        }
    );


    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
               // var formData = form.serialize();
                var edit_region_name = $('#edit_region_name').val();                
                var edit_region_id = $('#edit_region_id').val();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/region/postUpdate') ?>?a=index',{'region_id':edit_region_id,'region_name':edit_region_name},function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');

                        form[0].reset();
                        $('select', form).trigger('change');
                        $('.form-group', form).removeClass('has-error');

                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                region_name: {
                    required: true
                }
            }
        }
    );

//    Import CSV

    $('#form-csv').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                var formData = new FormData(form);
                var l = Ladda.create($('[type=submit]', form)[0]);
                $.ajax({
                    url: '<?php echo site_url('admin/region/importCSV') ?>',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        }
                        else if(data.status == 'success') {
                            $('#csvModal').modal('hide');
                            toastr['success'](data.message);
                            mydatatable._fnAjaxUpdate();
                        }
                        l.stop();
                    }
                });
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                csv_file: {
                    required: true
                }
            }
        }
    );
</script>
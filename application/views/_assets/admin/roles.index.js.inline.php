<script>
    toastr.options = {
        "closeButton": true
    };
    $(function(){
        $('#select_role').change(function () {
            var val = $(this).val();

            if (val == false) {
                $('#text_role_name').val('');
                $('#text_role_description').val('');
                $('#btn_update').hide();
                $('#btn_delete').hide();
                $('#btn_add').show();
                reset_input();
            } else {
                //uiLoader('#portlet-body', 'show');
                uiLoader('#form-body', 'show');
                $.post('<?php echo site_url('admin/roles/get') ?>', {role_id: val}, function (data) {
                    $('#text_role_name').val(data.role.name);
                    $('#text_role_description').val(data.role.description);

                    data.modules.forEach(function(module){

                        $('[name="permission['+ module.module_id + ']"]').each(function(){
                            $(this).removeAttr('checked');
                            $(this).parent('div').removeClass('checked');
                            if($(this).val() == module.permissions) {
                                $(this).attr('checked', 'checked');
                                $(this).parent('div').addClass('checked');
                            }

                        });
                    });

                    $('#btn_add').hide();
                    $('#btn_update').show();
                    $('#btn_delete').show();
                    //uiLoader('#portlet-body', 'hide');
                    uiLoader('#form-body', 'hide');
                }, 'json');

            }

        });
        $('#select_role').change();


        $('#role-form').validate({
            rules: {
                role_name: {
                    required: true
                },
                role_description: {
                    required: true
                }
            },
            messages: {

            },
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            invalidHandler: function(event, validator) { //display error alert on form submit

            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {

            }
        });

        $('#btn_add').click(function(){
            var btn = $(this);

            if($('#role-form').validate().form()) {
                btn.attr('disabled', 'disabled');
                var l = Ladda.create($('#btn_add')[0]);
                l.start();
                $.post('<?php echo site_url('admin/roles/add') ?>', $('#role-form').serialize(),
                    function(data){
                        if(data.status == 'fail')
                        {
                            for(var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else {
                            toastr['success'](data.message);
                            $('#role-form')[0].reset();
                            $('#select-role').change();
                            reset_input();
                        }
                        getRoles();
                        btn.removeAttr('disabled');
                        l.stop();
                    }, 'json')
            }
        });

        $('#btn_update').click(function(){
            if($('#role-form').validate().form()) {
                var l = Ladda.create($('#btn_update')[0]);
                l.start();
                $.post('<?php echo site_url('admin/roles/update') ?>', $('#role-form').serialize(),
                    function(data){
                        if(data.status == 'fail')
                        {
                            for(var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            $('#select-role').change();
                        }
                        getRoles();
                        l.stop();
                    }, 'json')
            }
        });

        $('#btn_delete').click(function(e){
            e.preventDefault();
            bootbox.confirm('Are you sure you want to delete?', function(result) {
                if(result == true) {
                    var l = Ladda.create($('#btn_delete')[0]);
                    l.start();
                    $.post('<?php echo site_url('admin/roles/delete') ?>', $('#role-form').serialize(),
                        function(data){
                            getRoles();
                            if(data.status == 'fail')
                            {
                                for(var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                            } else if(data.status == 'success') {
                                toastr['success'](data.message);
                                $('#role-form')[0].reset();
                                reset_input();
                            }
                            l.stop();
                        }, 'json')
                }
            });

        })

    });
    function getRoles() {
        $.post('<?php echo site_url('admin/roles/get_roles') ?>',function(data){
            var output = '<option value=""></option>';
            data.forEach(function(role){
                output += '<option value="' + role.role_id + '">' + role.name + '</option>';
            });
            $('#select_role').html(output);
            $('#select_role').change();
        }, 'json')
    }

    function reset_input() {
        $('[name^="permission"]').each(function(){
            $(this).removeAttr('checked');
            $(this).parent('div').removeClass('checked');
            if($(this).val() == 0) {
                $(this).attr('checked', 'checked');
                $(this).parent('div').addClass('checked');
            }

        });
    }

</script>
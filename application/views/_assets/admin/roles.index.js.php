<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<!-- START ICHECK SCRIPT -->
<script src="<?php echo public_url('plugins/icheck/icheck.min.js'); ?>"></script>
<script src="<?php echo public_url('scripts/form-icheck.js'); ?>"></script>
<!-- END ICHECK SCRIPT -->
<script src="<?php echo asset_url('inline/asset/admin/roles.index.js') ?>"></script>
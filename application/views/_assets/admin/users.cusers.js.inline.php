<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('admin/users/getcusers') ?>?a=cusers',
            type: 'POST'
        },
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'user_id'},
            { mData: 'username'},
            { mData: 'email'},
            { mData: 'active'},
            { mData: 'is_verified'},
            { mData: 'actions'}
            ],
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [5]},
            {'bSearchable' : false, 'aTargets': [5] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a onclick="make_inactive(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_active(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 3
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a onclick="make_nonverified(this)" class="label label-sm label-success">Yes</a>';
                    } else{
                        newData = '<a onclick="make_verified(this)" class="label label-sm label-danger">No</a>';
                    }
                    return newData;
                },
                "targets": 4
            }
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    function make_inactive(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to deactivate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/users/inactivate') ?>?a=cusers', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_active(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to activate?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/users/activate') ?>?a=cusers', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        })

    }

    function make_nonverified(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to make it unverified?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/users/unverify') ?>?a=cusers', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function make_verified(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to make it verified?', function(result) {
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/users/verify') ?>?a=cusers', {uid: r_id}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    function login_as(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/users/loginas') ?>?a=cusers', {uid: r_id}, function(data){
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                toastr['success'](data.message);
                setTimeout(function () {
                    window.location.replace('<?php echo site_url('home') ?>?a=cusers')
                }, 600);
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json')
    }


    //add
    function format(state) {
        if (!state.id) return state.text; // optgroup
        return "<img class='flag' src='<?php echo public_url('img/flags', false); ?>/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
    }

    $("#select2_sample4").select2({
        placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
        allowClear: true,
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function(m) {
            return m;
        }
    });


    $('#select2_sample4').change(function() {
        $('#form-add').validate().element($(this));
    });

    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/users/add') ?>?a=cusers', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');
                        $('#form-add')[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                first_name: {
                    required: true
                },
                last_name: {
                    required: false
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email|is_unique[users.email]'
                        }
                    }
                },
                password: {
                    required: false,
                    minlength: 4,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'min_length[4]'
                        }
                    }

                },
                rpassword: {
                    required: ($('#add-password').val() != '')?true:false,
                    minlength: 4,
                    equalTo: '#add-password'
                },

                active: {
                    required: true
                },
                address: {
                    required: false
                },
                city: {
                    required: false,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data: {
                            rules: 'alpha'
                        }
                    }
                },
                country: {
                    required: false
                }
            },
            messages:{
                email: {
                    remote: 'Email not valid or already exists.'
                },
                city: {
                    remote: 'City name should be characters only.'
                }
            }
        }
    );

    // edit form
    function show_edit(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('admin/users/single') ?>?a=cusers', {uid: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                $('[name="uid"]', form).val(r_id);
                $('[name="first_name"]', form).val(data.record.first_name);
                $('[name="last_name"]', form).val(data.record.last_name);
                $('[name="username"]', form).val(data.record.username);
                if(data.record.user_type) {
                    $('[name="user_type"] option[value="' + data.record.user_type_id + '"]', form).attr('selected', 'selected');
                    $('[name="user_type"]').change();
                }
                $('[name="email"]', form).val(data.record.email);
                if(data.record.role) {
                    $('[name="role_id"] option[value="' + data.record.role.role_id + '"]', form).attr('selected', 'selected');
                }
                $('[name="active"] option[value="' + data.record.active + '"]').attr('selected', 'selected');
                $('[name="country"] option[value="' + data.record.country + '"]').attr('selected', 'selected');
                $('[name="address"]', form).val(data.record.address);
                $('[name="city"]', form).val(data.record.city);

                $('[name="role_id"]', form).change();
                $('[name="country"]', form).change();
                $('[name="active"]', form).change();
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');

    }

    $("#select2_sample5").select2({
        placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
        allowClear: true,
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function(m) {
            return m;
        }
    });


    $('#select2_sample5').change(function() {
        $('#form-add').validate().element($(this));
    });



    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('admin/users/edit') ?>?a=cusers', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');
                        $('#form-edit')[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit], form').removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                first_name: {
                    required: true
                },
                last_name: {
                    required: false
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email'
                        }
                    }
                },
                password: {
                    required: false,
                    minlength: 4,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'min_length[4]'
                        }
                    }

                },
                rpassword: {
                    required: ($('#edit-password').val() != '')?true:false,
                    minlength: 4,
                    equalTo: '#edit-password'
                },

                active: {
                    required: true
                },
                address: {
                    required: false
                },
                city: {
                    required: false,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data: {
                            rules: 'alpha'
                        }
                    }
                },
                country: {
                    required: false
                }
            },
            messages:{
                email: {
                    remote: 'Email not valid or already exists.'
                },
                city: {
                    remote: 'City name should be characters only.'
                }
            }
        }
    );


    // delete
    function do_delete(target) {
        var r_id = target.parentNode.parentNode.childNodes[0].innerHTML;
        bootbox.confirm('Are you sure you want to delete?' ,function(result) {
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('admin/users/delete') ?>?a=cusers', {uid: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

    $('#active_select2').select2();
    $('#edit_active_select2').select2();
</script>
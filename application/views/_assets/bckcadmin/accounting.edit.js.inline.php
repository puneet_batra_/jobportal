<script>
    toastr.options = {
        "closeButton": true
    };
    var mydatatable = null;
    $(function () {
        ComponentsPickers.init();
        $('#select_asset').select2(
            {
                placeholder: 'Select a asset',
                allowClear: true,
                id: function(item){
                    return item.id;
                },
                formatResult: function(item) {
                    var markup = "";
                    if (item.id !== undefined) {
                        markup =  [
                            '<div class="media small">',
                            '<div class="pull-left">',
                            '<div class="media-object">',
                            '<img src="' + item.img + '" width="40" height="40"/>',
                            '</div>',
                            '</div>',
                            '<div class="media-body">',
                            '<h4 class="media-heading">' + item.value + '</h4>',
                            '<p style="height: 29px; overflow:hidden;">' + item.desc + '</p>',
                            '</div>',
                            '</div>',
                        ].join('');
                    }
                    return markup;
                },
                formatSelection: function(item){
                    var markup = "";
                    if (item.id !== undefined) {
                        markup +=  item.value;
                    }
                    return markup;
                },
                initSelection: function(input){
                    return '';
                },
                ajax: {
                    url: '<?php echo site_url('bckcadmin/assets/search') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: function(term, page) {
                        return {
                            q: term,
                            hide_deleted: true,
                            hide_inactive: true,
                            page_limit: 25
                        };
                    },
                    results: function(data, page) {
                        return {
                            results: data
                        };
                    }
                }
            }
        );
        $('#select_asset').change(function (e) {
            if(parseInt($(this).val()) > 0) {
                $('#asset-btn-link').show();
            } else {
                $('#asset-btn-link').hide();
            }
        });
        $('.select2-styled').select2();




        $('#add-entry-form').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function (form) {
                    form = $(form);
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    var l = Ladda.create($('[type=submit]', form)[0]);
                    l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 'fail') {
                                for (var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if (data.status == 'success') {

                                $('.form-group', form).removeClass('has-error');
                                $('input', form).iCheck('update');

                                toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                setTimeout(function(){
                                    window.location.replace('<?php echo site_url('bckcadmin/accounting') ?>')
                                }, 1000)
                            } else {

                            }
                            l.stop();
                        }
                    });

                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    asset_id: {
                        required: true
                    },
                    purchase_date: {
                        required: true
                    },
                    purchase_value: {
                        required: true,
                        number: true
                    },
                    effective_life: {
                        required: true,
                        digits: true
                    },
                    depreciation_method: {
                        required: true
                    }
                },
                messages: {}
            }
        );

    });
    function getselected() {
        var value = $('#select_asset').val();
        $.post('<?php echo site_url('bckcadmin/assets/getsingle'); ?>', {asset_id: value}, function (data) {
            if(data.status == 'success') {
                $('#select_asset').select2('data', {
                    id: data.result.asset_id,
                    value: data.result.friendly_name
                });
                $('#select_asset').select2('val', data.result.asset_id).trigger('change');
            }

        }, 'json');

    }
    getselected();

</script>
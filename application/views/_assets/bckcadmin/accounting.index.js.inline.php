<script>
    toastr.options = {
        "closeButton": true
    };
    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    var asset_types_table = null;
    /* Set tabletools buttons and button container */

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('bckcadmin/accounting/get') ?>',
            type: 'POST'
        },

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'friendly_name'},
            { mData: 'purchase_date'},
            { mData: 'purchase_value'},
            { mData: 'effective_life'},
            { mData: 'depreciation_method'},
            { mData: 'created_at'},
            { mData: 'actions'}
        ],
        "aoColumnDefs" : [
            {
                "render": function ( data, type, row ) {
                    return '<a href="<?php echo site_url('bckcadmin/assets/view') ?>/' + row.asset_id +  '" target="_blank">' + data + '</a>';
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var date = new Date(data);
                    return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
                },
                "targets": 1
            },
            {
                "render": function ( data, type, row ) {
                    var date = new Date(data);
                    return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear() + ' ' + ((date.getHours() >= 12)? date.getHours() -12: date.getHours()) + ':' + date.getMinutes() + ' ' + ((date.getHours() >= 12)?'pm':'am');
                },
                "targets": 5
            },
            {'bSortable' : false, 'aTargets' : [6] },
            {'bSearchable' : false, 'aTargets': [6] }
        ]
    };

    asset_types_table = $('#asset_types_table').dataTable(dataTableOptions);

    $('#asset_types_table')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    function delete_record(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to delete this entry?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/accounting/delete') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        asset_types_table._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function edit_fuel(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('bckcadmin/accounting/single') ?>', {rid: rid}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                window.location.replace('<?php echo site_url('bckcadmin/accounting/edit') ?>/' + data.asset_meta.asset_meta_id);
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');
    }
</script>
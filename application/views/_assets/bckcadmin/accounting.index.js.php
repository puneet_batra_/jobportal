<?php
$this->minify->js(array(
    'plugins/bootstrap-toastr/toastr.min.js',
    'plugins/datatables/media/js/jquery.dataTables.min.js',
    'plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
    'plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js',
    'plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js',
    'plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js',
    'plugins/bootbox/bootbox.min.js',
    'plugins/ladda-bootstrap/dist/spin.min.js',
    'plugins/ladda-bootstrap/dist/ladda.min.js',
    'plugins/jquery-validation/js/jquery.validate.min.js',
    'plugins/jquery-validation/js/additional-methods.min.js',
    'plugins/ajaxform/jquery.form.min.js'
), 'bckcadmin');
echo $this->minify->deploy_js(false,'accounting.index.js');
?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo asset_url('inline/asset/bckcadmin/accounting.index.js') ?>"></script>
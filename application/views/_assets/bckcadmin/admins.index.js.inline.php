<script>
    toastr.options = {
        "closeButton": true
    };
    var admins_table = null;
    /* Set tabletools buttons and button container */

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('bckcadmin/admins/get') ?>',
            type: 'POST'
        },

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'name'},
            { mData: 'email'},
            { mData: 'actions'}
        ],
        "aoColumnDefs" : [
            {'bSortable' : false, 'aTargets' : [2] },
            {'bSearchable' : false, 'aTargets': [2] }
        ]
    };

    admins_table = $('#admins_table').dataTable(dataTableOptions);

    $('#admins_table')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    $('#add-admin-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            $('input', form).iCheck('update');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#addModal').modal('hide');
                            admins_table._fnAjaxUpdate();
                        } else {

                        }
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                user_id: {
                    required: true
                }
            },
            messages: {}
        }
    );

    function delete_record(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to delete this admin?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/admins/delete') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        admins_table._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    $('#select2_add_userid').select2(
        {
            placeholder: 'Select a user',
            allowClear: true,
            id: function(item){
                return item.user_id;
            },
            formatResult: function(item) {
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            formatSelection: function(item){
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            initSelection: function(){
                return '';
            },
            ajax: {
                url: '<?php echo site_url('bckcadmin/admins/getlist') ?>',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    return {
                        q: term,
                        page_limit: 10
                    };
                },
                results: function(data, page) {
                    return {
                        results: data.users
                    };
                }
            }
        }
    );
</script>
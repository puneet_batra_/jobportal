<script>
    toastr.options = {
        "closeButton": true
    };

    var asset_types_table = null;
    /* Set tabletools buttons and button container */

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('bckcadmin/asset_types/get') ?>',
            type: 'POST'
        },

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'name'},
            { mData: 'feature_fuel'},
            { mData: 'feature_maintenance'},
            { mData: 'feature_exception'},
            { mData: 'feature_location'},
            { mData: 'actions'}
        ],
        "aoColumnDefs" : [
            {
                "render": function ( data, type, row ) {
                    var newData = '';
                    if(data == 1) {
                        newData = '<a class="label label-sm label-success">Enabled</a>';
                    } else if(data == 0){
                        newData = '<a class="label label-sm label-danger">Disabled</a>';
                    }
                    return newData;
                },
                "targets": 1
            },
            {
                "render": function ( data, type, row ) {
                    var newData = '';
                    if(data == 1) {
                        newData = '<a class="label label-sm label-success">Enabled</a>';
                    } else if(data == 0){
                        newData = '<a class="label label-sm label-danger">Disabled</a>';
                    }
                    return newData;
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {
                    var newData = '';
                    if(data == 1) {
                        newData = '<a class="label label-sm label-success">Enabled</a>';
                    } else if(data == 0){
                        newData = '<a class="label label-sm label-danger">Disabled</a>';
                    }
                    return newData;
                },
                "targets": 3
            },
            {
                "render": function ( data, type, row ) {
                    var newData = '';
                    if(data == 1) {
                        newData = '<a class="label label-sm label-success">Enabled</a>';
                    } else if(data == 0){
                        newData = '<a class="label label-sm label-danger">Disabled</a>';
                    }
                    return newData;
                },
                "targets": 4
            },
            {'bSortable' : false, 'aTargets' : [5] },
            {'bSearchable' : false, 'aTargets': [5] }
        ]
    };

    asset_types_table = $('#asset_types_table').dataTable(dataTableOptions);

    $('#asset_types_table')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    $('#taggable').select2(
        {
            tags: true,
            tokenSeparators: [",", " "],
            multiple: true,
            ajax: {
                url: '<?php echo site_url('bckcadmin/users/getusers') ?>',
                type: 'POST',
                dataType: "json",
                data: function(term, page) {
                    return {
                        q: term,
                        include_admin: 1
                    };
                },
                results: function(data, page) {
                    var returndata = [];
                    data.forEach(function (row) {
                        var formatted_row = {
                            id: row.user_id,
                            text: row.first_name + ' ' + row.last_name + ' (' + row.email + ')'
                        };
                        returndata.push(formatted_row);
                    });
                    return {
                        results: returndata
                    };
                }
            }
        }
    );

    $('#edit-taggable').select2(
        {
            tags: true,
            tokenSeparators: [",", " "],
            multiple: true,
            ajax: {
                url: '<?php echo site_url('bckcadmin/users/getusers') ?>',
                type: 'POST',
                dataType: "json",
                data: function(term, page) {
                    return {
                        q: term,
                        include_admin: 1
                    };
                },
                results: function(data, page) {
                    var returndata = [];
                    data.forEach(function (row) {
                        var formatted_row = {
                            id: row.user_id,
                            text: row.first_name + ' ' + row.last_name + ' (' + row.email + ')'
                        };
                        returndata.push(formatted_row);
                    });
                    return {
                        results: returndata
                    };
                }
            }
        }
    );

    $('#add-asset-type-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-add-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            $('input', form).iCheck('update');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#addModal').modal('hide');
                            asset_types_table._fnAjaxUpdate();
                        } else {

                        }
                        l.stop();
                        //uiLoader('#form-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                name: {
                    required: true
                },
                feature_fuel: {
                    required: true
                },
                feature_maintenance: {
                    required: true
                },
                feature_exception: {
                    required: true
                },
                feature_location: {
                    required: true
                }
            },
            messages: {}
        }
    );

    $('#edit-asset-type-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-edit-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            $('input', form).iCheck('update');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#editModal').modal('hide');
                            asset_types_table._fnAjaxUpdate();
                        } else {

                        }
                        l.stop();
                        //uiLoader('#form-edit-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                name: {
                    required: true
                },
                feature_fuel: {
                    required: true
                },
                feature_maintenance: {
                    required: true
                },
                feature_exception: {
                    required: true
                },
                feature_location: {
                    required: true
                }
            },
            messages: {}
        }
    );


    function delete_record(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to delete this asset type?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/asset_types/delete') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        asset_types_table._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function edit_fuel(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('bckcadmin/asset_types/single') ?>', {rid: rid}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                var form = $('#edit-asset-type-form');
                var single = data.asset_type;
                var managers = single.managers;

                var selected_managers = [];

                managers.forEach(function (row) {
                    var formatted_row = {
                        id: row.user_id,
                        text: row.first_name + ' ' + row.last_name + ' (' + row.email + ')'
                    };
                    selected_managers.push(formatted_row);
                });

                $("#edit-taggable").select2("data", selected_managers, true);
                $('[name="name"]', form).val(single.name);
                $('[name="asset_type_id"]', form).val(single.asset_type_id);
                $('[name="feature_fuel"][value="'+ single.feature_fuel +'"]', form).attr('checked', 'checked');
                $('[name="feature_maintenance"][value="'+ single.feature_maintenance +'"]', form).attr('checked', 'checked');
                $('[name="feature_exception"][value="'+ single.feature_exception +'"]', form).attr('checked', 'checked');
                $('[name="feature_location"][value="'+ single.feature_location +'"]', form).attr('checked', 'checked');
                $('input').iCheck('update');
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');
    }
</script>
<script>
    toastr.options = {
        "closeButton": true
    };
    $(function () {
        $('.wysihtml5').wysihtml5({
            "stylesheets": ["<?php echo public_url('plugins/bootstrap-wysihtml5/wysiwyg-color.css', false); ?>"]
        });
        $('#my_multi_select1').multiSelect();

        $('#asset_type_id').select2();
        $('#enabled_select').select2();

        $('#asset_type_id').change(function(e){
            var value = $(this).val();
            if(value != '') {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/assets/getassetfeatures') ?>', {aid: value}, function(data){
                    $.each(data.features, function(key, single){
                        if(single == 1) {
                            $('#my_multi_select1 option[value="' + key + '"]').attr('selected', 'selected');
                            $('#my_multi_select1').multiSelect('refresh');
                        } else {
                            $('#my_multi_select1 option[value="' + key + '"]').removeAttr('selected');
                            $('#my_multi_select1').multiSelect('refresh');
                        }
                    });
                    uiLoader('#portlet-body', 'hide');
                })
            }

        });

        $('#add-asset-form').validate(
            {
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function(form) {
                    form = $(form);
                    //uiLoader('#portlet-body', 'show');
                    var l = Ladda.create($('[type=submit]', form)[0]);
                    l.start();
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function(data){
                            if(data.status == 'fail')
                            {
                                for(var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if(data.status == 'success') {
                                $('#add-asset-form')[0].reset();
                                $('#my_multi_select1').multiSelect('refresh');
                                toastr['success'](data.message);
                                toastr['success']('Redirecting..');
                                $('[type=submit]', form).removeAttr('disabled');
                                setTimeout(function(){
                                    window.location.replace(data.redirect_url)
                                }, 1000)
                            }
                            l.stop();
                            //uiLoader('#portlet-body', 'hide');
                        }
                    });

                },
                errorPlacement: function(error, element){
                    error.insertAfter(element);
                },
                rules:{
                    friendly_name: {
                        required: true
                    },
                    description: {
                        required: true
                    },
                    nfc_uuid: {
                        required: false
                    },
                    asset_key: {
                        required: true
                    },
                    asset_type_id: {
                        required: true
                    },
                    features: {
                        required: false
                    },
                    enabled: {
                        required: true
                    }
                },
                messages:{

                }
            }
        );

    });



</script>
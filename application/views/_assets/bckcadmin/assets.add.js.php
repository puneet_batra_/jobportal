<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/ajaxform/jquery.form.min.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.add.js') ?>"></script>
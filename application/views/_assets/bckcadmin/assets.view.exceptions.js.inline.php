<script>
    // Exceptions

    $('#add-exception-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                var aid = form.attr('data-item');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-exception-add-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {asset_id: aid},
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('select', form).trigger('change');
                            $('.form-group', form).removeClass('has-error');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#exceptionModal').modal('hide');

                            show_hide_portlets();
                        } else {

                        }
                        l.stop();
                        //uiLoader('#form-exception-add-body', 'hide');
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                title: {
                    required: true
                },
                comments: {
                    required: false
                },
                status: {
                    required: true
                }
            },
            messages: {}
        }
    );

    var exception_datatable_options = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url: '<?php echo site_url('bckcadmin/exceptions/getlist') ?>',
            type: 'POST',
            "data": function (d) {
                return $.extend({}, d, {
                    "asset_id": function(){
                        return $('#overview_portlet').attr('data-item');
                    }
                });
            }
        },
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [2, 'desc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            {mData: 'status'},
            {mData: 'title'},
            {mData: 'created_on_formatted'},
        ],
        "aoColumnDefs": [
            {
                "render": function (data, type, row) {
                    if (data == 'new') {
                        newData = 'New';
                    } else if (data == 'in_progress') {
                        newData = 'In Progress';
                    } else if (data == 'closed') {
                        newData = 'Closed'
                    }
                    return newData;
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.title_formatted;
                },
                "targets": 1
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    var newDate = new Date(data * 1000);
                    return months[newDate.getMonth()] + ' ' + newDate.getDate() + ', ' + newDate.getFullYear();
                },
                "targets": 2
            }

        ]
    }
    if( $.fn.dataTable.isDataTable( '#exception_table' )) {
        var exception_table = $('#exception_table').dataTable();
    } else {
        var exception_table = $('#exception_table').dataTable(exception_datatable_options);
    }

    $('#exception_table')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#form-exception-update-body', 'show');
        }).on( 'draw.dt', function (data) {
            if(exception_table.fnSettings().fnRecordsTotal() > 0) {
                $('#portlet-exception').show();
            } else
            {
                $('#portlet-exception').hide();
            }

            uiLoader('#form-exception-update-body', 'hide');
        });

    $('#update-exception-form').submit(function (e) {
        e.preventDefault();
        $(this).ajaxSubmit({
            dataType: 'json',
            data: {
                exception_id: $('#update-exception-form').attr('data-item')
            },
            beforeSubmit: function () {
                uiLoader('#form-exception-update-body', 'show');
            },
            success: function (data) {
                if (data.status == 'fail') {
                    for (var key in data.errors) {
                        toastr['error'](data.errors[key]);
                    }
                } else if (data.status == 'success') {
                    toastr['success'](data.message);
                    exception_table._fnAjaxUpdate();
                } else {

                }
                uiLoader('#form-exception-update-body', 'hide');
            }
        });
    });


    function animate_exception_detail_show() {
        var listing = $('#exception-list');
        var detail = $('#exception-detail');

        if(detail.is(':visible') == false) {
            listing.animate({'width': '49.8%'}, 400, function () {
                listing.removeAttr('style');
                listing.removeClass('col-md-12');
                listing.removeClass('col-md-6');
                listing.addClass('col-md-6');
                detail.slideDown(200);
            });
        }

    }

    function animate_exception_detail_hide() {
        var listing = $('#exception-list');
        var detail = $('#exception-detail');

        detail.slideUp(200, function(){
            listing.animate({'width': '100%'}, 400, function () {
                listing.removeAttr('style');
                listing.removeClass('col-md-6');
                listing.removeClass('col-md-12');
                listing.addClass('col-md-12');
            });
        });
    }

    animate_exception_detail_hide();

    function showException(target, id) {
        if (id == undefined) {
            var rid = target.attributes.id.value.replace('datatable-item-', '');
        } else {
            var rid = id;
        }
        var originalScrollOffset = $(window).scrollTop();
        uiLoader('#form-exception-update-body', 'show');
        $.post('<?php echo site_url('bckcadmin/exceptions/getcomments') ?>', {rid: rid}, function (data) {
            var exception_update_form = $('#update-exception-form');
            $('[name=status] option[value=' + data.exception.status + ']', exception_update_form).attr('selected', 'selected').change();
            $('[name=title]', exception_update_form).val(data.exception.title);
            exception_update_form.attr('data-item', data.exception.exception_id);
            $('#chats-container').html('');
            data.comments.forEach(function (comment) {
                $('#chats-container').append(_exceptionFormat(comment));
            });
            $("#chats-main-container").slimScroll({scrollBy: '6000000px'})
            animate_exception_detail_show();
            check_subscription();
            $(window).scrollTop(originalScrollOffset);
            uiLoader('#form-exception-update-body', 'hide');
        }, 'json');
    }

    function _exceptionFormat(data) {
        var attachment_format = '';
        if (data.attachment_name != '') {
            attachment_format = '<div class="attachment">' +
            '           <br/>' +
            '           <a target="_blank" href="<?php echo base_url('uploads/'. $this->auth->company_id() .'/exceptions/attachments') ?>/' + data.attachment + '"><i class="fa fa-paperclip"></i> ' + data.attachment_name + '</a>' +
            '       </div>';
        }
        var formatted = '<li class="' + data.class + '">' +
            '    <img class="avatar" alt="" src="' + data.profile_pic + '"/>' +
            '    <div class="message">' +
            '       <span class="arrow"></span>' +
            '        <a href="#" class="name">' +
            '            ' + data.username + ' </a>' +
            '       <span class="datetime">' +
            '       at ' + data.created_on_formatted + ' </span>' +
            '       <span class="body">' +
            '            ' + data.comments +
            '       </span>' +
            attachment_format
        '   </div>' +
        '</li>'
        return formatted;
    }

    function remove_exception() {
        var exception_id = $('#update-exception-form').attr('data-item');
        bootbox.confirm('Are you sure you really want to remove this exception?', function(result){
            if(result == true) {
                uiLoader('#form-exception-update-body', 'show');
                $.post('<?php echo site_url('bckcadmin/exceptions/remove_exception') ?>', {exception_id: exception_id},
                    function(data){
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            $('#select_asset').change();
                            exception_table._fnAjaxUpdate();
                            animate_exception_detail_hide();
                            check_subscription();
                        } else {

                        }
                        uiLoader('#form-exception-update-body', 'hide');
                    }, 'json')
            }
        })
    }

    function check_subscription() {
        var exception_id = $('#update-exception-form').attr('data-item');
        var btn_subscribe = $('#btn-subscribe');
        var btn_unsubscribe = $('#btn-unsubscribe');
        if(exception_id == 0 || exception_id == '') {
            btn_subscribe.hide();
            btn_unsubscribe.hide();
        } else {
            $.post('<?php echo site_url('bckcadmin/exceptions/check_subscription') ?>', {exception_id: exception_id}, function (data) {
                if(data.status == 'fail')
                {
                    btn_subscribe.hide();
                    btn_unsubscribe.hide();
                } else if(data.status == 'success') {
                    if(data.subscribed == 1) {
                        btn_unsubscribe.show();
                        btn_subscribe.hide();
                    } else {
                        btn_unsubscribe.hide();
                        btn_subscribe.show();
                    }
                }
            }, 'json');
        }
    }

    $('#btn-subscribe').click(function(e){
        e.preventDefault();
        var exception_id = $('#update-exception-form').attr('data-item');
        uiLoader('#form-exception-update-body', 'show');
        $.post('<?php echo site_url('bckcadmin/exceptions/subscribe') ?>', {exception_id: exception_id}, function (data) {
            if (data.status == 'fail') {
                for (var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if (data.status == 'success') {
                toastr['success'](data.message);
                check_subscription();
            } else {

            }
            uiLoader('#form-exception-update-body', 'hide');
        }, 'json');
    });

    $('#btn-unsubscribe').click(function(e){
        e.preventDefault();
        var exception_id = $('#update-exception-form').attr('data-item');
        uiLoader('#form-exception-update-body', 'show');
        $.post('<?php echo site_url('bckcadmin/exceptions/unsubscribe') ?>', {exception_id: exception_id}, function (data) {
            if (data.status == 'fail') {
                for (var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if (data.status == 'success') {
                toastr['success'](data.message);
                check_subscription();
            } else {

            }
            uiLoader('#form-exception-update-body', 'hide');
        }, 'json');
    });

    check_subscription();

    $('#add-exception-comment-form').submit(function (e) {
        e.preventDefault();
        var fileselected = $('#input-attachment-exception-comments').val();
        var text = $('[name="comments"]', $('#add-exception-comment-form')).val();
        var exception_id = $('#update-exception-form').attr('data-item');
        if (text == false && fileselected == false) {
            toastr['error']('Please enter something to comment.');
            return false;
        }
        $(this).ajaxSubmit({
            dataType: 'json',
            data: {
                exception_id: exception_id
            },
            beforeSubmit: function () {
                uiLoader('#form-exception-udpate-body', 'show');
            },
            success: function (data) {
                if (data.status == 'fail') {
                    for (var key in data.errors) {
                        toastr['error'](data.errors[key]);
                    }
                } else if (data.status == 'success') {
                    showException(false, exception_id);
                    $('#add-exception-comment-form')[0].reset();
                } else {

                }
                uiLoader('#form-exception-update-body', 'hide');
            }
        });
    });

    $('#send-message-exception').click(function (e) {
        e.preventDefault();
        $('#add-exception-comment-form').submit();
    });

    $('#input-file-unselected').click(function (e) {
        e.preventDefault();
        $('#input-attachment-exception-comments').focus().trigger('click');
    });

    $('#input-file-selected').click(function (e) {
        e.preventDefault();
        $('#input-attachment-exception-comments').focus().val('').change();
    });

    $('#input-attachment-exception-comments').change(function () {
        var val = $(this).val();
        if (val == '' || val == undefined) {
            $('#input-file-unselected').show();
            $('#input-file-selected').hide();
        } else {
            $('#input-file-unselected').hide();
            $('#input-file-selected').show();
        }
    })

    function _reset_attachment_btn() {
        $('#input-file-selected').click();

        $('#input-file-unselected').show();
        $('#input-file-selected').hide();
    }
    _reset_attachment_btn();

    $('#exception_status').select2();

    $('.dataTables_length select').select2();
</script>
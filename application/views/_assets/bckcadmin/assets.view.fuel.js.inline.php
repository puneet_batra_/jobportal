<script>
    // fuel
    function _format_last_fueled(data) {
        var newData = '';
        if(typeof data.fueled_on_date != 'undefined' && typeof data.fueled_on_date != null) {
            newData = '<p>Last Fueled by: <strong>' + data.username + '</strong> on <strong>'+ data.fueled_on_date + ' ' +'</strong></p>';
        }
        return newData;
    }
    var chart;
    function display_fuel_chart() {
        var asset_id = $('#chart_3').attr('data-item');
        $.post('<?php echo site_url('bckcadmin/fuel/getchartdata') ?>', {asset_id: asset_id}, function (data) {
            if(data.records.length == 0) {
                $('#portlet-fuel').hide();
            } else {
                $('#portlet-fuel').show();
            }
            $('#last_fueled_by').html(_format_last_fueled(data.last_fueled));
            var chart_options =  {
                handleReleaseOutside: function(){},
                "type": "serial",
                "theme": "light",

                "fontFamily": 'Open Sans',
                "color":    '#888888',

                "pathToImages": "<?php echo public_url('plugins/amcharts/amcharts/images', false) ?>/",

                "dataProvider": data.records,
                "balloon": {
                    "cornerRadius": 6
                },
                
                "valueAxes": [{
                	"minimum": 0
                }],

                "graphs": [{
                		"id":"economy",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletBorderThickness": 1,
                    "fillAlphas": 0.3,
                    "fillColorsField": "lineColor",
                    "legendValueText": "[[value]]",
                    "lineColorField": "lineColor",
                    "title": "L/100km",
                    "valueField": "lphk"
                }],
                "chartScrollbar": {
                	"autoGridCount": true,
                	"graph": "economy",
                	"scrollbarHeight": 35
                	},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                
                "dataDateFormat": "YYYY-MM-DD",
                "categoryField": "date",
                "categoryAxis": {
                    "dateFormats": [{
                        "period": "DD",
                        "format": "DD"
                    }, {
                        "period": "WW",
                        "format": "MMM DD"
                    }, {
                        "period": "MM",
                        "format": "MMM"
                    }, {
                        "period": "YYYY",
                        "format": "YYYY"
                    }],
                    "parseDates": true,
                    "autoGridCount": true,
                    "equalSpacing": true
                }
    
                	
            }
            try{
                chart = AmCharts.makeChart("chart_3",chart_options);
                match_height();
            } catch(e){

            }
        }, 'json');

    }


    display_fuel_chart();

    $('#towing').select2();
    $('#pre_purchased').select2();
    $('#supplier').select2();

    $('#add-fuel-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                var aid = form.attr('data-item');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-fuel-add-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {asset_id: aid},
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('select', form).trigger('change');
                            $('input', form).iCheck('update');
                            $('input', form).trigger('ifChecked');
                            $('.form-group', form).removeClass('has-error');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#fuelModal').modal('hide');

                            show_hide_portlets();
                        } else {

                        }
                        //uiLoader('#form-fuel-add-body', 'hide');
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                odometer: {
                    required: true,
                    number: true
                },
                fueled_date: {
                    required: true,
                    date: true
                },
                towing: {
                    required: true
                },
                volume: {
                    required: true,
                    number: true
                },
                pre_purchased: {
                    required: false
                },
                cost: {
                    required: true,
                    number: true
                },
                supplier: {
                    required: false
                },
                exception_title: {
                    required: function() {
                        return ($('.has_exception').is(':visible')) ? true : false
                    }
                }
            },
            messages: {

            }
        }
    );
    $('[name="has_exception"]').on('ifChecked', function () {
        if($(this).val() == 1) {
            $('.has_exception').show();
        } else {
            $('.has_exception').hide();
        }
    }).trigger('ifChecked');

</script>
<script>
    toastr.options = {
        "closeButton": true
    };
    var editable_enabled = false;
    ComponentsPickers.init();
    $('.make-switch').bootstrapSwitch();
    var asset_features_multiselect = $('#my_multi_select1');
    // apply plugin
    asset_features_multiselect.multiSelect();

    function refreshDocuments() {
        var rid = document.getElementById('documents-list-container').parentNode.attributes.id.value.replace('documents-list-', '');
        var records = [];
        var template = '<a target="_blank" href="<?php echo base_url('uploads/'. $this->auth->company_id() .'/assets/documents') ?>/{{filename}}">' +
                '<i class="fa fa-paperclip"></i> {{title}}' +
                '</a>';

        // default friendlyname
        var editable = '';

        <?php if (is_allowed(2, 'assets')){ ?>
            template = '<span class="label label-sm label-danger" ' +
            'onclick = "delete_document(this)" ' +
            'id="document-item-{{asset_document_id}}">' +
            '<i class="fa fa-times" style="font-size: 10px;"></i></span> ' + template;
            editable = ' - <span data-name="friendly_name" data-disabled="true" data-type="text" data-pk="{{asset_document_id}}" class="document-editable">{{friendly_name}}</span> ';
        <?php } ?>

        template = '<div style="margin-bottom: 5px;">' + template + editable + '</div>';

        uiLoader('#portlet-overview-body', 'show');
        $.post('<?php echo site_url('bckcadmin/assets/getdocuments') ?>', {rid: rid}, function (data) {
            if (data.status == 'fail') {
                for (var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if (data.status == 'success') {
                $.each(data.records, function (record) {
                    var single_record = this;
                    <?php if(!is_allowed(2, 'assets')){ ?>
                    if (single_record.friendly_name != '') {
                        single_record.title = single_record.friendly_name;
                    }
                    <?php } ?>
                    var formatted = template.replace(/\{\{([^\}]+)\}\}/gi,
                        function (x, y) {
                            return eval('single_record.' + y);
                        });
                    records.push(formatted);
                });
                $('#documents-list-container').html(records.join("\n"));
                $('.document-editable').editable({
                    url: '<?php echo site_url('bckcadmin/assets/updatedocument') ?>',
                    success: function (data) {
                        if (data.status == 'fail') {
                            return data.errors;
                        } else if (data.status == 'success') {
                            toastr['success'](data.message)
                        } else {
                            toastr['error']('Something went wrong, please retry again.')
                        }
                    }
                });
            }
            uiLoader('#portlet-overview-body', 'hide');
        }, 'json')
    }

    function refreshPhotos() {
        var rid = document.getElementById('asset-photos-container').parentNode.attributes.id.value.replace('asset-photos-', '');
        var records = [];
        var template = ' <div class="" style="margin-bottom: 7px">' +
            '<a class="fancybox" data-rel="fancybox-button" data-fancybox-gallery="gallery"' +
            'rel="group1" href="<?php echo site_url('image/uploads/'. $this->auth->company_id() .'/assets/photos') ?>/{{filename}}?s=1000x1000">' +
            '<img src="<?php echo site_url('image/uploads/'. $this->auth->company_id() .'/assets/photos') ?>/{{filename}}?s=150x60"' +
            ' alt="" height="50"/>' +
            '</a>' +
            ' <a class="btn btn-danger btn-sm" onclick="removephoto()" href="#"' +
            ' id="photo-item-{{asset_photo_id}}">Remove</a> ' +
            '<a class="btn btn-primary btn-sm" target="_blank" href="<?php echo base_url('uploads/'. $this->auth->company_id() .'/assets/photos') ?>/{{filename}}"><i class="icon-cloud-download"></i> Download</a>' +
            ' </div>';
        uiLoader('#portlet-overview-body', 'show');
        $.post('<?php echo site_url('bckcadmin/assets/getphotos') ?>', {rid: rid}, function (data) {
            if (data.status == 'fail') {
                for (var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if (data.status == 'success') {
                $.each(data.records, function (record) {
                    var single_record = this;
                    var formatted = template.replace(/\{\{([^\}]+)\}\}/gi,
                        function (x, y) {
                            return eval('single_record.' + y);
                        });

                    records.push(formatted);
                })
                $('#asset-photos-container').html(records.join("\n"));
            }
            uiLoader('#portlet-overview-body', 'hide');
        }, 'json')
    }

    function removephoto() {
        event.preventDefault();
        var rid = event.currentTarget.attributes.id.value.replace('photo-item-', '');
        bootbox.confirm('Are you sure you want to remove it?', function (result) {
            if (result == true) {
                uiLoader('#portlet-overview-body', 'show');
                $.post('<?php echo site_url('bckcadmin/assets/removephoto') ?>', {rid: rid},
                    function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            refreshPhotos();
                        }
                        uiLoader('#portlet-overview-body', 'hide');
                    }, 'json')
            }
        });
    }

    function delete_document(target) {
        var did = target.attributes.id.value.replace('document-item-', '');
        bootbox.confirm('Are you sure you want to remove this document?', function (result) {
            if (result == true) {
                uiLoader('#portlet-overview-body', 'show');
                $.post('<?php echo site_url('bckcadmin/assets/removedocument') ?>', {did: did},
                    function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            refreshDocuments();
                        }
                        uiLoader('#portlet-overview-body', 'hide');
                    }, 'json')
            }
        });
    }

    function getFeatureButtons() {
        return;
    }

    function getFavourite() {
        var aid = $('.favourite-actions').attr('id').replace('actions-item-', '');
        uiLoader('#portlet-overview-body', 'show');
        $.post('<?php echo site_url('bckcadmin/assets/get_favourite') ?>', {aid: aid},
            function (data) {
                if (data.status == 'fail') {
                    $('#make-favourite').show();
                    $('#make-unfavourite').hide();
                } else if (data.status == 'success') {
                    $('#make-unfavourite').show();
                    $('#make-favourite').hide();
                }
                uiLoader('#portlet-overview-body', 'hide');
            }, 'json');
    }

    function show_hide_portlets(options) {
        var aid = $('#overview_portlet').attr('data-item');
        $.post('<?php echo site_url('bckcadmin/assets/getEnabledPortlets') ?>', {aid: aid}, function (data) {
            // portlets - has data
            var timeout = 400;
            if (data.features_hasdata.fuel == true && data.features_enabled.fuel == true && data.features_view.fuel == true) {
                $.get(window.location.href + '?partial=fuel', function (data) {
                    $('.portlet-content', $('#portlet-fuel')).html(data);
                }, 'html');
                timeout += 400;
            } else {
                $('.portlet-content', $('#portlet-fuel')).html('');
            }

            if (data.features_hasdata.exception == true && data.features_enabled.exception == true && data.features_view.exception == true) {
                $.get(window.location.href + '?partial=exception', function (data) {
                    $('.portlet-content', $('#portlet-exception')).html(data);
                }, 'html');
                timeout += 400;
            } else {
                $('.portlet-content', $('#portlet-exception')).html('');
            }

            if (data.features_hasdata.location == true && data.features_enabled.location == true && data.features_view.location == true) {
                $.get(window.location.href + '?partial=location', function (data) {
                    $('.portlet-content', $('#portlet-location')).html(data);
                }, 'html');
                timeout += 400;
            } else {
                $('.portlet-content', $('#portlet-location')).html('');
            }

            if (data.features_hasdata.maintenance == true && data.features_enabled.maintenance == true && data.features_view.maintenance == true) {
                $.get(window.location.href + '?partial=maintenance', function (data) {
                    $('.portlet-content', $('#portlet-maintenance')).html(data);
                }, 'html');
                timeout += 400;
            } else {
                $('.portlet-content', $('#portlet-maintenance')).html('');
            }

            // action menu
            if (data.features_add.fuel == true && data.features_enabled.fuel == true) {
                $('#btn_feature_fuel').show();
            } else {
                $('#btn_feature_fuel').hide();
            }

            if (data.features_add.exception == true && data.features_enabled.exception == true) {
                $('#btn_feature_exception').show();
            } else {
                $('#btn_feature_exception').hide();
            }

            if (data.features_add.location == true && data.features_enabled.location == true) {
                $('#btn_feature_location').show();
            } else {
                $('#btn_feature_location').hide();
            }

            if (data.features_add.maintenance == true && data.features_enabled.maintenance == true) {
                $('#btn_feature_maintenance').show();
            } else {
                $('#btn_feature_maintenance').hide();
            }

            // action button show/hide
            if (data.features_enabled.fuel == true
                || data.features_enabled.exception == true
                || data.features_enabled.location == true
                || data.features_enabled.maintenance == true
            ) {
                if (data.features_add.fuel == false
                    && data.features_add.exception == false
                    && data.features_add.location == false
                    && data.features_add.maintenance == false
                ) {
                    $('#features_sidebar').hide();
                } else {
                    $('#features_sidebar').show();
                }

            } else {
                $('#features_sidebar').hide();
            }

            // callbacks
            setTimeout(function () {
                $('#internal-page-content').slideDown(function(){
                    pageLoader('hide');
                    try{
                        google.maps.event.trigger(map, "resize");
                        chart.handleResize();
                    } catch(e) {

                    }

                });

            }, timeout);
            google.maps.event.trigger(map, "resize");

        }, 'json');
    }


    $(function () {
        <?php if(is_allowed(2, 'assets')){ ?>
        refreshPhotos();
        <?php } ?>
        getFavourite();
        refreshDocuments();

        $('.wysihtml5').wysihtml5({
            "stylesheets": ["<?php echo public_url('plugins/bootstrap-wysihtml5/wysiwyg-color.css', false); ?>"]
        });

        $('.delete-asset-btn').click(function (e) {
            e.preventDefault();
            var aid = $(this).attr('id').replace('asset-item-', '');
            bootbox.confirm('Are you sure you want to delete this asset?', function (result) {
                if (result == true) {
                    uiLoader('#portlet-overview-body', 'show');
                    $.post('<?php echo site_url('bckcadmin/assets/delete') ?>', {aid: aid}, function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            setTimeout(function () {
                                window.location.replace('<?php echo site_url('bckcadmin') ?>')
                            }, 1000);
                        } else {

                        }
                        uiLoader('#portlet-overview-body', 'hide');
                    }, 'json');
                }
            });
        });


        $('#inline').attr("checked", true);
        $.fn.editable.defaults.mode = 'inline';
        jQuery.uniform.update('#inline');

        $.fn.editable.defaults.inputclass = 'form-control';
        $.fn.editable.defaults.url = '<?php echo site_url('bckcadmin/assets/updatesingle') ?>';

        $('.editable').editable({
            url: '<?php echo site_url('bckcadmin/assets/updatesingle') ?>',
            success: function (data) {
                if (data.status == 'fail') {
                    return data.errors;
                } else if (data.status == 'success') {
                    toastr['success'](data.message)
                } else {
                    toastr['error']('Something went wrong, please retry again.')
                }
            }
        });

        $('#pencil').click(function (e) {
            e.preventDefault();
            var note = $(document).find('#note');
            if(note.hasClass('editable-disabled')) {
                $('#enable-edit').click();

            }
            setTimeout(function () {
                $(document).find('#note').editable('toggle');
            }, 500);
        });

        var asset_type_id_select = $('#asset_type_id');
        asset_type_id_select.select2();
        $('#enabled_select').select2();

        asset_type_id_select.change(function (e) {
            var value = $(this).val();
            if (value != '') {
                uiLoader('#portlet-overview-body', 'show');
                $.post('<?php echo site_url('bckcadmin/assets/getassetfeatures') ?>', {aid: value}, function (data) {
                    $.each(data.features, function (key, single) {
                        if (single == 1) {
                            $('option[value="' + key + '"]', asset_features_multiselect).attr('selected', 'selected');
                        } else {
                            $('option[value="' + key + '"]', asset_features_multiselect).removeAttr('selected');
                        }
                    });
                    asset_features_multiselect.multiSelect('refresh');
                    uiLoader('#portlet-overview-body', 'hide');
                })
            }

        });


        var update_asset_validation_rules = {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                asset_features_multiselect.removeAttr('disabled');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#portlet-overview-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {
                            asset_features_multiselect.multiSelect('refresh');
                            toastr['success'](data.message);

                            $('input:file', form).val('');
                            $('.multifile-files-container', form).html('');

                            $('[type=submit]', form).removeAttr('disabled');
                            refreshDocuments();
                            refreshPhotos();
                            show_hide_portlets();
                            $('#asset_type_id_text').html(
                                $('#asset_type_id [value="' + $('#asset_type_id',form).select2('val') + '"]', form).html()
                            );
                            //$('#enable-edit').click();
                        } else {

                        }
                        //uiLoader('#portlet-overview-body', 'hide');
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                asset_type_id: {
                    required: true
                },
                features: {
                    required: false
                },
                enabled: {
                    required: true
                }
            },
            messages: {}
        };
        $('#update-asset-form').validate(update_asset_validation_rules);

        $('#make-favourite').click(function (e) {
            e.preventDefault();
            var aid = $('.favourite-actions').attr('id').replace('actions-item-', '');

            uiLoader('#portlet-overview-body', 'show');
            $.post('<?php echo site_url('bckcadmin/assets/make_favourite') ?>', {aid: aid},
                function (data) {
                    if (data.status == 'fail') {
                        for (var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if (data.status == 'success') {
                        $('#make-unfavourite').show();
                        $('#make-favourite').hide();
                    }
                    uiLoader('#portlet-overview-body', 'hide');
                }, 'json');
        });

        $('#make-unfavourite').click(function (e) {
            e.preventDefault();
            var aid = $('.favourite-actions').attr('id').replace('actions-item-', '');

            uiLoader('#portlet-overview-body', 'show');
            $.post('<?php echo site_url('bckcadmin/assets/make_unfavourite') ?>', {aid: aid},
                function (data) {
                    if (data.status == 'fail') {
                        for (var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if (data.status == 'success') {
                        $('#make-favourite').show();
                        $('#make-unfavourite').hide();
                    }
                    uiLoader('#portlet-overview-body', 'hide');
                }, 'json');
        });


        <?php if(is_allowed(2, 'assets')){ ?>
            asset_features_multiselect.change(function () {
                var aid = $(this).parent('.features-multiselect-container').attr('id').replace('asset-features-item-', '');
                var unselecteds = $(this).children(':not(:selected)');
                unselecteds.each(function (index, item) {
                    uiLoader('#portlet-overview-body', 'show');
                    asset_features_multiselect.attr('disabled', 'disabled');
                    $.post('<?php echo site_url('bckcadmin/assets/checkfeature') ?>', {aid: aid, item: item.value},
                        function (data) {
                            setTimeout(function () {
                                asset_features_multiselect.removeAttr('disabled');
                                asset_features_multiselect.multiSelect('refresh');
                            }, 3000);
                            if (data.status == 'fail') {
                                for (var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                                $(item).attr('selected', 'selected');
                                asset_features_multiselect.multiSelect('refresh');
                            } else if (data.status == 'success') {
                            }
                            uiLoader('#portlet-overview-body', 'hide');
                        }, 'json')
                });
            });

            //asset_features_multiselect.change();
        <?php } ?>

        Dropzone.options.photosZone = {
            paramName: "photos",
            init: function() {
                var hiddenInput = $(this.hiddenFileInput);
                hiddenInput.attr('capture', 'camera');
                hiddenInput.attr('accept', 'image/*');
                this.on("addedfile", function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                    var hiddenInput = $(this.hiddenFileInput);
                    hiddenInput.attr('capture', 'camera');
                    hiddenInput.attr('accept', 'image/*');
                });
                this.on("complete", function(file) {
                    this.removeFile(file);
                    var hiddenInput = $(this.hiddenFileInput);
                    hiddenInput.attr('capture', 'camera');
                    hiddenInput.attr('accept', 'image/*');
                });
                this.on("success", function(file, data) {
                    if (data.status == 'fail') {
                        for (var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if (data.status == 'success') {
                        toastr['success'](data.message);
                        refreshPhotos();
                    }
                    var hiddenInput = $(this.hiddenFileInput);
                    hiddenInput.attr('capture', 'camera');
                    hiddenInput.attr('accept', 'image/*');
                });

            }
        };

        Dropzone.options.documentsZone = {
            paramName: "documents",
            init: function() {
                this.on("addedfile", function(file) {
                    // Create the remove button
                    var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                    // Capture the Dropzone instance as closure.
                    var _this = this;

                    // Listen to the click event
                    removeButton.addEventListener("click", function(e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        _this.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });
                this.on("complete", function(file) {
                    this.removeFile(file);
                });
                this.on("success", function(file, data) {
                    if (data.status == 'fail') {
                        for (var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if (data.status == 'success') {
                        toastr['success'](data.message);
                        refreshDocuments();
                    }
                });

            }
        };

    });
    $(document).change(function () {
        $('.fancybox').fancybox({
            margin: [67, 20, 10, 20],
            padding: 0
        });
        match_height();
    });

    var floating_menu = $('.floating-menu');
    floating_menu.click(function (e) {
        e.preventDefault();
        floating_menu.toggleClass('open');
        $('.menu-links', floating_menu).toggleClass('open');
    });

    $('#map-canvas').click(function(){
        var style = '#map-canvas:after{position: static;height: 0 !important;}';
        $("<style type='text/css'></style>").appendTo("head").html(style);
    })

    $('#channels-btn').click(function(e){
        e.preventDefault();
        if($('[data-name="device_udid"]').html() != 'Empty') {
            window.location.replace($(this).attr('href'));
        } else {
            bootbox.alert('Please add a device id first.');
        }
    });

    function match_height() {
        $('.match-height').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    }

    $('.view-control').show();
    $('.edit-control').hide();
    $('.hide-control').hide();
    $('.form-actions', $('#overview_portlet')).hide();
    $('#enable-edit').click(function (e) {
        e.preventDefault();
        $('.editable').editable('toggleDisabled');
        $(this).toggleClass('open');
        if($(this).hasClass('open') == false) {
            // view mode
            editable_enabled = false;
            $('.view-control').show();
            $('.edit-control').hide();
            $('.hide-control').hide();
            $('.form-actions', $('#overview_portlet')).hide();
        } else {
            // edit mode
            editable_enabled = true;
            $(document).find('.edit-control').show();
            $(document).find('.view-control').hide();
            $(document).find('.hide-control').show();
            $(document).find('.form-actions', $('#overview_portlet')).show();
        }
    });
    window.setInterval(function () {
        chart.handleResize();
    }, 1000);
</script>
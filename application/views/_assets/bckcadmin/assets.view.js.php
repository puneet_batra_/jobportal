<script>
    $('#internal-page-content').hide();
    pageLoader('show');

    $(window).load(function(){
        $('#internal-page-content').hide();
        show_hide_portlets();
    })
</script>
<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-match-height/jquery.matchHeight-min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/ajaxform/jquery.form.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/amcharts/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/amcharts/amcharts/serial.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/amcharts/amcharts/themes/light.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/amcharts/amcharts/themes/chalk.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/fancybox/source/jquery.fancybox.pack.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('scripts/components-pickers.js'); ?>"></script>
<script src="<?php echo public_url('plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<!-- START THUMBNAIL JS SCRIPT -->
<script src="<?php echo public_url('plugins/anytimejs/anytime.5.0.6.min.js'); ?>" type="text/javascript"></script>

<script src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/dropzone/dropzone.min.js'); ?>"></script>


<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.view.location.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.view.maintenance.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.view.fuel.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.view.exceptions.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/assets.view.js') ?>"></script>

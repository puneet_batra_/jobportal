<script>
    var add_location_form = $('#add-location-form');
    var map;
    var center_done = 0;
    function locations_showmap() {
        try {
            var aid = $('#map-canvas').attr('data-item');
            $.post('<?php echo site_url('bckcadmin/locations/getlastlocation') ?>', {asset_id: aid}, function (data) {
                var output = _location_showmap_format(data);
                if (data.geo.length == 0 && data.text.length == 0) {
                    $('#portlet-location').hide();
                } else {
                    $('#portlet-location').show();
                }
                $('#location-info-text').html(output);
                if (data.geo.length > 0) {
                    $('#map-canvas').show();
                    var geolocation = new google.maps.LatLng(data.geo[0].latitude, data.geo[0].longitude);
                    var mapOptions = {
                        zoom: 13,
                        center: geolocation
                    };
                    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                    var single = data.geo[0];
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(single.latitude, single.longitude),
                        map: map,
                        title: "User: " + single.username + "\r\nLat Long: " + single.latitude + ", " + single.longitude + " \r\nDateTime: " + single.created_on_date + " " + single.created_on_time,
                        info: single
                    });

                    var polyOptions = {
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    };
                    var poly = new google.maps.Polyline(polyOptions);
                    poly.setMap(map);
                    var points = [];

                    for (var j = 0; j < data.geo.length; j++) {
                        points.push(new google.maps.LatLng(data.geo[j].latitude, data.geo[j].longitude));
                    }

                    poly.setPath(points);

                    google.maps.event.addListener(map, 'tilesloaded', function() {
                        // 3 seconds after the center of the map has changed, pan back to the
                        // marker.
                        map.panTo(marker.getPosition());
                    });

                    match_height();
                } else {
                    $('#map-canvas').hide();
                }

            }, 'json');
        } catch (e) {

        }

    }
    function _location_showmap_format(data) {
        var newData = '';
        var geo_template = '';
        var manual_template = '';
        if (data.geo.length > 0) {
            var single = data.geo[0];
            geo_template = 'GeoLocation: last updated by <strong>' + single.username + '</strong> on <strong>' + single.updated_on_date + '</strong> ' + single.updated_on_time + '';
        }
        if (data.text.length > 0) {
            var single = data.text[0];
            manual_template = 'Manual Location: <a>' + single.location_text + '</a> <strong>' + single.type + ' by ' + single.username + '</strong> on <strong>' + single.created_on_date + '</strong> ' + single.created_on_time + ' <br>';
        }

        newData = manual_template + geo_template;

        return newData;
    }
    $(function () {
        locations_showmap();
    })

    // locations

    add_location_form.validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                var aid = form.attr('data-item');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-location-add-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {asset_id: aid},
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('input', form).iCheck('update');
                            $('[name=type][checked]', form).trigger('ifChecked')
                            $('.form-group', form).removeClass('has-error');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#locationModal').modal('hide');
                            show_hide_portlets();
                        } else {

                        }
                        //uiLoader('#form-location-add-body', 'show');
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                type: {
                    required: true
                }
            },
            messages: {}
        }
    );
    $('[name="type"]', add_location_form).on('ifChecked', function () {
        var value = this.value;
        if (value == 'geolocation') {
            $('.when-geolocation', add_location_form).show();
            $('.when-manual', add_location_form).hide();
            $('.when-manual-left', add_location_form).hide();
        } else if (value == 'manual') {
            $('.when-manual', add_location_form).show();
            $('.when-geolocation', add_location_form).hide();
        } else if (value == '') {
            $('.when-geolocation', add_location_form).hide();
            $('.when-manual', add_location_form).hide();
            $('.when-manual-left', add_location_form).hide();
        }
        $('[name=status][value=taken]', add_location_form).attr('checked', 'checked').iCheck('update').trigger('ifChecked');
    });

    $('[name=status]', add_location_form).on('ifChecked', function () {
        var value = this.value;
        if (value == 'left') {
            $('.when-manual-left', add_location_form).show();
        } else if (value == 'taken') {
            $('.when-manual-left', add_location_form).hide();
        }
    }).trigger('ifChecked');

    $('[name="type"][checked]', add_location_form).trigger('ifChecked');

    $('#crosshairs_btn', add_location_form).click(function (e) {
        e.preventDefault();

        if (navigator.geolocation) {
            $('#location_autocomplete').val('Finding you, please wait...');
            navigator.geolocation.getCurrentPosition(function (position) {
                    geocoder = new google.maps.Geocoder();
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    geocoder.geocode({'latLng': latlng}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                var formatted_address = results[1].formatted_address;
                                $('#location_autocomplete').val(formatted_address);
                                $('#lnglat').val(position.coords.latitude + ',' + position.coords.longitude);
                            } else {
                                alert('No results found');
                            }
                        } else {
                            alert('Geocoder failed due to: ' + status);
                        }
                    });
                }, function (error) {
                    if (error.code == 1) {
                        bootbox.alert('Please enable and allow us to track your location.');
                    } else if (error.code == 3) {
                        bootbox.alert('Location tracking timed out, Try again or use manual location.');
                    }
                }, {maximumAge: 600000, timeout: 30000}
            );
        } else {
            bootbox.alert("Geolocation is not supported by this browser.");
        }
    })

    $('.map-wrapper .locker i').click(function () {
        $('.map-wrapper').toggleClass('enabled');
    })


</script>
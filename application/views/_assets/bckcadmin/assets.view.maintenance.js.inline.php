<script>
    // Maintenance listing
    var maintenance_container = $('#maintenance-container');

    function get_maintenance_block() {
        var aid = maintenance_container.attr('data-item');
        maintenance_container.html('');
        uiLoader('#portlet-maintenance-body', 'show');
        $.post('<?php echo site_url('bckcadmin/maintenance/getlatest') ?>', {asset_id: aid}, function (data) {

            if(data.message == '') {
                $('#portlet-maintenance').hide();
            } else {
                $('#portlet-maintenance').show();
            }
            $('#maintenance-container').html(_maintenance_block_format(data.message));
            uiLoader('#portlet-maintenance-body', 'hide');
        }, 'json');
    }
    function _maintenance_block_format(data) {
        var newData = data;
        return newData;
    }

    try{
        var anytime_picker_options = {
            format: "%Y-%m-%d %H:%i:%s",
            formatUtcOffset: "%: (%@)",
            hideInput: true,
            placement: "inline",
            earliest: new Date(<?php
            echo $this->timezone->convertDate(date('Y'), 'Y', 'server-to-user') .',' .
            ($this->timezone->convertDate(date('Y-m-d H:i:s'), 'm', 'server-to-user') - 1) . ',' .
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'd','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'H','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'i','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 's','server-to-user')
            ?>)
        };
        $("#anytime-picker0").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker1").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker3").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker4").AnyTime_picker(anytime_picker_options);
    } catch(e){

    }


    $('#add-maintenance-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                var aid = form.attr('data-item');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //uiLoader('#form-maintenance-add-body', 'show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {asset_id: aid},
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('select', form).trigger('change');
                            $('.form-group', form).removeClass('has-error');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#maintenanceModal').modal('hide');
                            show_hide_portlets();
                        } else {

                        }
                        //uiLoader('#form-maintenance-add-body', 'show');
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                comments: {
                    required: false
                }
            },
            messages: {}
        }
    );

    $('#due_fuel_entry').select2();

    $('#due_fuel_entry').change(function () {
        $('.conditional_div').hide();
        var val = $(this).val();
        $('.conditional_div.div-' + val).show();
    }).trigger('change');

    get_maintenance_block();

</script>
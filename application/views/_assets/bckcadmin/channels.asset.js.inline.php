<script>
 function attachEventsChannels() {
     var widget_channel = $('.widget-channel');
     $('.overview', widget_channel).click(function () {
         $(this).parents('.widget-channel').toggleClass('open');
     });

     var widget_channel_add = $('.widget-channel-add');
     $('.btn-open', widget_channel_add).click(function (e) {
         e.preventDefault();
         widget_channel_add.addClass('open');
     });
     $('.btn-cancel', widget_channel_add).click(function (e) {
         e.preventDefault();
         widget_channel_add.removeClass('open');
         $('form', widget_channel_add)[0].reset();
     });
 }
    attachEventsChannels();
</script>
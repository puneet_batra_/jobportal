<script>
    toastr.options = {
        "closeButton": true
    };

    $(function () {
        $('#btn_update').click(function(e){
            e.preventDefault();
            var html = $('#content-edit').html();
            var title = $('#content-title').html();
            bootbox.confirm('Ready to update the changes? This will replace the current dashboard content.', function(result){
                if(result == true) {
                    uiLoader('#portlet-body', 'show');
                    $.post('<?php echo site_url('bckcadmin/cms/update_dashboard') ?>', {html: html, title: title}, function (data) {
                        if(data.status == 'fail')
                        {
                            for(var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if(data.status == 'success') {
                            toastr['success'](data.message);
                        }
                        uiLoader('#portlet-body', 'hide');
                    }, 'json');
                }
            });
        })
    });

</script>
<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo public_url('plugins/ajaxform/jquery.form.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ckeditor/adapters/jquery.js'); ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/cms.index.js') ?>"></script>
<script>
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout
    function notifyMe(text, title, icon) {
        if (!Notification) {
            console.log('Please use a modern version of Chrome, Firefox, Opera or Firefox.');
            return;
        }

        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }

        if(typeof title == 'undefined') {
            title = 'Airapp'
        }

        if(typeof icon == 'undefined') {
            icon = '<?php echo public_url('img/notification-icon.png') ?>'
        }

        var notification = new Notification(title, {
            icon: icon,
            body: text
        });
        setTimeout(function () {
            notification.close();
        }, 15000);
        return notification;
    }
    function UIIdleTimeout() {

        // cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
        var $countdown;

        $('body').append('<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static"><div class="modal-dialog modal-small"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Your session is about to expire.</h4></div><div class="modal-body"><p><i class="fa fa-warning"></i> You session will be locked in <span id="idle-timeout-counter"></span> seconds.</p><p>Do you want to continue your session?</p></div><div class="modal-footer"><button id="idle-timeout-dialog-logout" type="button" class="btn btn-default">No, Logout</button><button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Keep Working</button></div></div></div></div>');

        var notification;
        // start the idle timer plugin
        $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
            idleAfter: <?php echo $this->config->item('sess_time_to_update') - 10 ?>, // 5 seconds
            timeout: 60000, //30 seconds to timeout
            pollingInterval: 5, // 5 seconds
            keepAliveURL: '<?php echo site_url('sess') ?>',
            serverResponseEquals: 'OK',
            onTimeout: function () {
                notifyMe('You have been logged out.', 'Airapp - Session Expired');
                window.location = "<?php echo site_url('logout'); ?>";
            },
            onIdle: function () {
                notification = notifyMe('Your session is about to expire', 'Airapp - Session Expiration');
                notification.onclick = function() {
                    window.focus();
                    this.close();
                };
                $('#idle-timeout-dialog').modal('show');
                $countdown = $('#idle-timeout-counter');

                $('#idle-timeout-dialog-keepalive').on('click', function () {
                    $('#idle-timeout-dialog').modal('hide');
                });

                $('#idle-timeout-dialog-logout').on('click', function () {
                    $('#idle-timeout-dialog').modal('hide');
                    $.idleTimeout.options.onTimeout.call(this);
                });
            },
            onCountdown: function (counter) {
                $countdown.html(counter); // update the counter
            }
        });
    }
    function ajaxLoader(task, message) {
        var html = '<img src="<?php echo public_url('img', false); ?>/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (message ? message : 'Loading...') + '</span>';
        if(task == 'show')
        {
            $('.page-loading').remove();
            $('body').append('<div class="page-loading">' + html + '</div>');
        } else if(task == 'hide')
        {
            $('.page-loading').remove();
        } else if(task == 'html')
        {
            return html;
        }
    }

    function uiLoader(element, task) {
        if($('#internal-page-content').is(':visible')) {
            if(task == 'show') {
                Metronic.blockUI({
                    target: element,
                    animate: true,
                    overlayColor: 'none'
                });
            } else if (task == 'hide') {
                Metronic.unblockUI(element);
            }
        } else {
            //console.log('page is loading')
        }

    }

    function pageLoader(task) {
        if(task == 'show') {
            Metronic.blockUI({
                target: '#page-block',
                animate: true,
                overlayColor: '#fff',
                opacity: 1
            });
        } else if (task == 'hide') {
            Metronic.unblockUI('#page-block');
        }
    }

    $(window).load(function(){
        $('#internal-page-content').fadeIn(200);
    })
    $(function(){
        var search_form_tag = $('#search-form-tag');
        var last_query_str = '';
        search_form_tag.submit(function (e) {
            e.preventDefault();
            var url = $(this).attr('action');
            var q = last_query_str;
            if(q == '' || $('input.tt-input').val()) {
                bootbox.alert('Please enter something to search..');
                return false;
            }
            $('[name="q"]', search_form_tag).val(q);


            var postdata = {
                is_search_page: true,
                offset: 0,
                limit: 4,
                q: q
            };
            $.post('<?php echo site_url('bckcadmin/assets/search') ?>', postdata, function (data) {
                var records = data;
                if(records.length == 1) {
                    window.location.replace(records[0].link);
                    return;
                } else {
                    window.location.replace(url + '?q=' + q);
                    return;
                }
            }, 'json');


        });

        var searchform = $('.search-form.search-form-expanded');
        $('input', searchform).live('blur',function () {
            var suggestion_box = $('.tt-dropdown-menu');
            searchform.removeClass('open');
            suggestion_box.css('display', 'none');

            if($('input.tt-input').val() != '') {
                last_query_str =  $('input.tt-input').val();
            }
            $('#typeahead_example_3').typeahead('val', '');

        }).live('focus',function () {
            searchform.addClass('open');
        });

        $('.file-selector').fileSelector();


        // Asset Search
        var custom = new Bloodhound({
            datumTokenizer: function(d) { return d.tokens; },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: "<?php echo site_url('bckcadmin/assets/search') ?>",
                replace: function(url, query) {
                    return url + "#" + query;
                },
                ajax : {
                    type: "POST",
                    data: {
                        q: function() {
                            return $('#typeahead_example_3').val()
                        },
                        hide_deleted: true
                    }
                }
            },
            limit: 10
        });

        custom.initialize();
        $('#typeahead_example_3').typeahead(null, {
            name: 'datypeahead_example_3',
            displayKey: 'value',
            source: custom.ttAdapter(),
            hint: (Metronic.isRTL() ? false : true),
            templates: {
                suggestion: function(data) {
                    return [
                        '<div class="media ' + data.class + '">',
                        '<div class="pull-left">',
                        '<div class="media-object">',
                        '<img src="' + data.img + '" width="50" height="50"/>',
                        '</div>',
                        '</div>',
                        '<div class="media-body">',
                        '<h4 class="media-heading">' + data.value + '</h4>',
                        '<p style="height: 34px; overflow:hidden;">' + data.desc + '</p>',
                        '</div>',
                        '</div>',
                    ].join('')
                }
            }
        }).bind('typeahead:selected', function (obj, datum) {
            window.location.replace('<?php echo site_url('bckcadmin/assets/view') ?>/' + datum.id);
        });
        companies_list_adjuster();
        $(document).find('.dataTables_wrapper select').select2();
    });
    function companies_list_adjuster() {
        var li = $(document).find('.dropdown-company .slimScrollDiv li');
        var count = li.length;
        var total_height = 0;
        if(count <= 4) {
            li.each(function(ele){
                total_height += $(this).actual('height');
            });
            $(document).find('.dropdown-company .slimScrollDiv').css('height', total_height + 'px');
            $(document).find('.dropdown-company .slimScrollDiv .dropdown-menu-list.scroller').css('height', total_height + 'px');
        }
    }
    function setTimezone() {
        var timezone = getTimeZone();
        $.post('<?php echo site_url('sess/userdata') ?>', {timezone: timezone});
    }
    setTimezone();
    function getTimeZone() {
        var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
        return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
    }
    $(function(){
        <?php if(get_cookie('remember_me_token') == ''){ ?>
        UIIdleTimeout();
        <?php } else { ?>
        var timeout = <?php echo $this->config->item('sess_time_to_update') - 30 ?> * 1000;
        window.setInterval(function(){
            $.post('<?php echo site_url('sess') ?>', {}, function (data) {
            }, 'html');
        }, timeout);
        <?php } ?>
    })
</script>
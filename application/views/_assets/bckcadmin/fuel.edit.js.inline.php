<script>
    toastr.options = {
        "closeButton": true
    };
    ComponentsPickers.init();

    $('#towing').select2();
    $('#pre_purchased').select2();
    $('#supplier').select2();

    $('#update-fuel-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                var aid = form.attr('data-item');
                $('[type=submit]', form).attr('disabled', 'disabled');
                //ajaxLoader('show');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    data: {fuel_id: aid},
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            window.location.reload();
                        } else {

                        }
                        //ajaxLoader('hide');
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                odometer: {
                    required: true,
                    number: true
                },
                fueled_date: {
                    required: true,
                    date: true
                },
                towing: {
                    required: true
                },
                volume: {
                    required: true,
                    number: true
                },
                pre_purchased: {
                    required: true
                },
                cost: {
                    required: true,
                    number: true
                },
                supplier: {
                    required: false
                }
            },
            messages: {}
        }
    );
    function delete_photo(target) {
        var fid = target.attributes.id.value.replace('data-item-', '');
        bootbox.confirm('Are you sure you want to delete this photo?', function (result) {
            if (result == true) {
                //ajaxLoader('show');
                $.post('<?php echo site_url('bckcadmin/fuel/deletephoto') ?>', {fid: fid}, function (data) {
                    if (data.status == 'fail') {
                        for (var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if (data.status == 'success') {
                        toastr['success'](data.message);
                        window.location.reload();
                    } else {

                    }
                    //ajaxLoader('hide');

                }, 'json');
            }
        })
    }
</script>
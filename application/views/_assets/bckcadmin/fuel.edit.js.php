<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-daterangepicker/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo public_url('scripts/components-pickers.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/ajaxform/jquery.form.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/fuel.edit.js') ?>"></script>
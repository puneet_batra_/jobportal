<script>
    toastr.options = {
        "closeButton": true
    };
    var fuel_table = null;
    /* Set tabletools buttons and button container */

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('bckcadmin/fuel/get') ?>',
            type: 'POST',
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "asset_id": $('#select_asset').val()
                } );
            }
        },

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ''
        },

        "fnInitComplete": function (oSettings, json) {
            uiLoader('#portlet-body', 'hide');
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [1, 'desc']
        ],

        "lengthMenu": [
            [25, 50, 100, -1],
            [25, 50, 100, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 25,

        aoColumns: [
            { mData: 'odometer'},
            { mData: 'fueled_date_formatted'},
            { mData: 'volume'},
            { mData: 'photo'},
            { mData: 'actions'}
        ],
        "aoColumnDefs" : [
            {'bSortable' : false, 'aTargets' : [4] },
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    var newDate = new Date(data * 1000);
                    return months[newDate.getMonth()] + ' ' + newDate.getDate() + ', ' + newDate.getFullYear();
                },
                "targets": 1
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    <?php if(is_allowed(2, 'fuel')){ ?>
                        var newData = '<a href="#" class="editable"\n   data-type="text"\n   data-pk="' + row.fuel_id + '"\n   data-name="odometer"\n   data-original-title="Enter Odometer">\n '+ data +'</a>';
                        return newData;
                    <?php } else { ?>
                        return data;
                    <?php } ?>
                },
                "targets": 0
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    <?php if(is_allowed(2, 'fuel')){ ?>
                    var newData = '<a href="#" class="editable"\n   data-type="text"\n   data-pk="' + row.fuel_id + '"\n   data-name="volume"\n   data-original-title="Enter Volume">\n '+ data +'</a>';
                    return newData;
                    <?php } else { ?>
                    return data;
                    <?php } ?>
                },
                "targets": 2
            },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var newData = '';
                    if(data == '') {
                        newData = '<i class="fa fa-times text-danger" style="font-size: 18px;"></i>';
                    } else {
                        newData = '<i class="fa fa-check text-success" style="font-size: 18px;cursor: pointer" onclick="showPhoto(\''+ data +'\')"></i>';
                    }
                    return newData;
                },
                "targets": 3
            }
        ]
    };


    fuel_table = $('#fuel_table').dataTable(dataTableOptions);

    $('#fuel_table')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    $('#fuel_table').on( 'draw.dt', function () {
        editable_columns();
    } );
    $('#asset-btn-link').click(function (e) {
        e.preventDefault();
        var url = '<?php echo site_url('bckcadmin/assets/view') ?>/' + $('#select_asset').val();
        window.location.replace(url);
    });
    function editable_columns() {
        //$.fn.editable.defaults.mode = 'inline';
        $('#inline').attr("checked", true);
        jQuery.uniform.update('#inline');

        $.fn.editable.defaults.inputclass = 'form-control input-small';
        $.fn.editable.defaults.url = '<?php echo site_url('bckcadmin/fuel/updatesingle') ?>';

        $('.editable').editable({
            url: '<?php echo site_url('bckcadmin/fuel/updatesingle') ?>',
            success: function (data) {
                if (data.status == 'fail') {
                    return data.errors;
                } else if (data.status == 'success') {
                    toastr['success'](data.message)
                } else {
                    toastr['error']('Something went wrong, please retry again.')
                }
            }
        });
    }

    function showPhoto(photo) {
        var modal = $('#photo-modal');
        $('.photo').attr('src', '<?php echo site_url('image/uploads/' . $this->auth->company_id() . '/fuel/photos') ?>/' + photo + '?s=800x900');
        modal.modal('show');
    }


    $('#select_asset').select2(
        {
            placeholder: 'Select a asset',
            allowClear: true,
            id: function(item){
                return item.id;
            },
            formatResult: function(item) {
                var markup = "";
                if (item.id !== undefined) {
                    markup =  [
                        '<div class="media small">',
                        '<div class="pull-left">',
                        '<div class="media-object">',
                        '<img src="' + item.img + '" width="40" height="40"/>',
                        '</div>',
                        '</div>',
                        '<div class="media-body">',
                        '<h4 class="media-heading">' + item.value + '</h4>',
                        '<p style="height: 29px; overflow:hidden;">' + item.desc + '</p>',
                        '</div>',
                        '</div>',
                    ].join('');
                }
                return markup;
            },
            formatSelection: function(item){
                var markup = "";
                if (item.id !== undefined) {
                    markup +=  item.value;
                }
                return markup;
            },
            initSelection: function(){
                return '';
            },
            ajax: {
                url: '<?php echo site_url('bckcadmin/assets/search') ?>',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    return {
                        q: term,
                        hide_deleted: true,
                        hide_inactive: true,
                        has_feature: 'fuel',
                        page_limit: 25
                    };
                },
                results: function(data, page) {
                    return {
                        results: data
                    };
                }
            }
        }
    );
    $('#select_asset').change(function (e) {
        if(parseInt($(this).val()) > 0) {
            $('#asset-btn-link').show();
        } else {
            $('#asset-btn-link').hide();
        }

        fuel_table._fnAjaxUpdate();
    });
    $('#asset-btn-link').hide();


    $('#fuel_table_wrapper select').select2();


    function delete_record(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to delete this entry?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/fuel/delete') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        fuel_table._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function edit_fuel(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        if(rid > 0) {
            window.location.replace('<?php echo site_url('bckcadmin/fuel/edit') ?>/' + rid);
        }
    }

    function getselected() {
        var value = $('#select_asset').val();
        $.post('<?php echo site_url('bckcadmin/assets/getsingle'); ?>', {asset_id: value}, function (data) {
            if(data.status == 'success') {
                $('#select_asset').select2('data', {
                    id: data.result.asset_id,
                    value: data.result.friendly_name
                });
                $('#select_asset').select2('val', data.result.asset_id).trigger('change');
            }

        }, 'json');

    }
    getselected();
</script>
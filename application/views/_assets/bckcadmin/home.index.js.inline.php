<script>
    jQuery(document).ready(function () {

        $('.dotdotdot').dotdotdot({
            /*	The text to add as ellipsis. */
            ellipsis	: '... ',

            /*	How to cut off the text/html: 'word'/'letter'/'children' */
            wrap		: 'letter',

            /*	Wrap-option fallback to 'letter' for long words */
            fallbackToLetter: true,

            /*	jQuery-selector for the element to keep and put after the ellipsis. */
            after		: null,

            /*	Whether to update the ellipsis: true/'window' */
            watch		: true,

            /*	Optionally set a max-height, if null, the height will be measured. */
            height		: 40,

            /*	Deviation for the height-option. */
            tolerance	: 0,

            /*	Callback function that is fired after the ellipsis is added,
             receives two parameters: isTruncated(boolean), orgContent(string). */
            callback	: function( isTruncated, orgContent ) {},

            lastCharacter	: {

                /*	Remove these characters from the end of the truncated text. */
                remove		: [ ' ', ',', ';', '.', '!', '?' ],

                /*	Don't add an ellipsis if this array contains
                 the last character of the truncated text. */
                noEllipsis	: []
            }
        });
    });

    function get_storage_block() {
        var sblock = $('#storage-block');
        $.post('<?php echo site_url('bckcadmin/home/getsize') ?>', function (data) {
            $('.number', sblock).html(data.size + ' ' + data.unit);
        }, 'json');
    }


    get_storage_block();
</script>
<script>
    toastr.options = {
        "closeButton": true
    };
    jQuery(document).ready(function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["<?php echo public_url('/plugins/bootstrap-wysihtml5/wysiwyg-color.css'); ?>"]
            });
        }

        var anytime_picker_options = {
            format: "%Y-%m-%d %H:%i:%s",
            formatUtcOffset: "%: (%@)",
            hideInput: true,
            placement: "inline",
            earliest: new Date(<?php
            echo $this->timezone->convertDate(date('Y'), 'Y', 'server-to-user') .',' .
            ($this->timezone->convertDate(date('Y-m-d H:i:s'), 'm', 'server-to-user') - 1) . ',' .
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'd','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'H','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 'i','server-to-user') . ','.
            $this->timezone->convertDate(date('Y-m-d H:i:s'), 's','server-to-user')
            ?>)
        };
        $("#anytime-picker0").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker1").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker3").AnyTime_picker(anytime_picker_options);
        $("#anytime-picker4").AnyTime_picker(anytime_picker_options);

        $('#add-maintenance-form').validate(
            {
                ignore: [],
                errorElement: 'span',
                errorClass: 'help-block',
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                submitHandler: function(form) {
                    form = $(form);
                    var aid = form.attr('data-item');
                    $('[type=submit]', form).attr('disabled', 'disabled');
                    var l = Ladda.create($('[type=submit]', form)[0]);
                    l.start();
                    form.ajaxSubmit({
                        dataType: 'json',
                        data: {asset_id: aid},
                        success: function(data){
                            if(data.status == 'fail')
                            {
                                for(var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                                $('[type=submit]', form).removeAttr('disabled');
                            } else if(data.status == 'success') {
                                toastr['success'](data.message);
                                $('[type=submit]', form).removeAttr('disabled');
                                get_attachments();
                            } else
                            {

                            }
                            l.stop();
                        }
                    });

                },
                errorPlacement: function(error, element){
                    error.insertAfter(element);
                },
                rules:{
                    comments: {
                        required: false
                    },
                    due_date: {
                        required: false
                    },
                    odo_hour: {
                        digits: true
                    }
                },
                messages:{

                }
            }
        );

        $('#due_fuel_entry').select2();

        $('#due_fuel_entry').change(function () {
            $('.conditional_div').hide();
            var val = $(this).val();
            $('.conditional_div.div-' + val).show();
        }).trigger('change');

        get_attachments();
    });

    function get_attachments() {
        var maintenance_id = $('#attachments_list').attr('data-item');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('bckcadmin/maintenance/get_attachments') ?>', {maintenance_id: maintenance_id},
            function(data){
                $('#attachments_list').html('');
                data.attachments.forEach(function(attachment){
                    $('#attachments_list').append(_format_attachments(attachment));
                })
                uiLoader('#portlet-body', 'hide');
        }, 'json')
    }

    function _format_attachments(data) {
        var removeDiv = '';
        <?php if(is_allowed(2, 'maintenance')){ ?>
         removeDiv = '<a class="label label-sm label-danger" onclick="remove_attachment(this)" id="data-item-' + data.maintenance_attachment_id + '">' +
            '<i class="fa fa-trash"></i>' +
            '</a>';
        <?php } ?>
        var newData = '<div style="margin-bottom: 5px;display: block">' +
            removeDiv + '' +
            ' <a target="_blank" href="<?php echo base_url('uploads/'. $this->auth->company_id() .'/maintenance/attachments') ?>/' + data.attachment + '">' + data.attachment_name + '</a>' +
            '</div>';

        return newData;
    }

    function remove_attachment(target) {
        var aid = target.attributes.id.value.replace('data-item-', '');
        bootbox.confirm('Really remove attachment?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/maintenance/remove_attachment') ?>', {attachment_id: aid}, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        get_attachments();
                    } else
                    {

                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });

    }

</script>
<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js'); ?>"></script>
<!-- END EDITABLE FORM JS -->
<script src="<?php echo public_url('plugins/anytimejs/anytime.5.0.6.js'); ?>" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('scripts/components-editors.js'); ?>"></script>

<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>


<script src="<?php echo public_url('plugins/ajaxform/jquery.form.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/maintenance.edit.js') ?>"></script>
<script>
    toastr.options = {
        "closeButton": true
    };
    var mydatatable = null;
    $(function () {
        /* Set tabletools buttons and button container */

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var dataTableOptions = {
            'filter': true,
            'processing': false,
            "serverSide": true,
            "ajax": {
                url:'<?php echo site_url('bckcadmin/maintenance/get') ?>',
                type: 'POST',
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "asset_id": $('#select_asset').val()
                    } );
                }
            },
            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "<?php echo public_url('plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf') ?>",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF",
                    "mColumns": [ 1, 2, 3, 4 ]
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV",
                    "mColumns": [ 1, 2, 3, 4 ]
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel",
                    "mColumns": [ 1, 2, 3, 4 ]
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables",
                    "mColumns": [ 1, 2, 3, 4 ]
                }]
            },
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "processing": ajaxLoader('html')
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            "order": [
                [2, 'desc']
            ],

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 15,

            aoColumns: [
                { mData: 'row_number' },
                { mData: 'entered_by'},
                { mData: 'logged_datetime_unix'},
                { mData: 'value'},
                { mData: 'maintenance_type'},
                { mData: 'actions'}
            ],
            "aoColumnDefs" : [
                {'bSortable' : false, 'aTargets' : [5] },
                {'bSortable' : false, 'aTargets' : [3] },
                {'bSearchable' : false, 'aTargets': [5] },
                {'bSearchable' : false, 'aTargets': [3] },
                {
                    // The `data` parameter refers to the data for the cell (defined by the
                    // `data` option, which defaults to the column being worked with, in
                    // this case `data: 0`.
                    "render": function ( data, type, row ) {
                        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                        var newDate = new Date(data * 1000);
                        var am_pm = 'am';
                        var hours = newDate.getHours();
                        var minutes = newDate.getMinutes();
                        var date = newDate.getDate();
                        if(newDate.getHours() > 11) {
                            am_pm = 'pm';
                            hours = newDate.getHours() - 12;
                        }
                        if(minutes < 10) {
                            minutes = '0' + minutes;
                        }
                        switch(date % 10) {
                            case 1:
                                date = date + 'st';
                                break;
                            case 2:
                                date = date + 'nd';
                                break;
                            case 3:
                                date = date + 'rd';
                                break;
                            default:
                                date = date + 'th';
                                break;
                        }
                        return date + ' ' + months[newDate.getMonth()] + ' ' + newDate.getFullYear() + ' ' + hours + ':' + minutes + ' ' + am_pm;
                    },
                    "targets": 2
                },
                {
                    // The `data` parameter refers to the data for the cell (defined by the
                    // `data` option, which defaults to the column being worked with, in
                    // this case `data: 0`.
                    "render": function ( data, type, row ) {
                        var newData = '';
                        if(data == 0) {
//                            newData = '<a class="label label-sm label-success">Warranty</a>';
                            newData = '<span class="">Warranty</span>';
                        } else if(data == 1){
//                            newData = '<a class="label label-sm label-success">Service - Date</a>';
                            newData = '<span class="">Service - Date</span>';
                        } else if(data == 2) {
//                            newData = '<a class="label label-sm label-success">Service - ODO</a>';
                            newData = '<span class="">Service - ODO</span>';
                        } else if(data == 3) {
//                            newData = '<a class="label label-sm label-success">Testing</a>';
                            newData = '<span class="">Testing</span>';
                        } else if(data == 4) {
//                            newData = '<a class="label label-sm label-success">End of Life</a>';
                            newData = '<span class="">End of Life</span>';
                        }
                        return newData;
                    },
                    "targets": 4
                }
            ]
        };

        mydatatable = $('#fuel_table').dataTable(dataTableOptions);

        $('#fuel_table')
            .on('preXhr.dt', function (e, settings, data) {
                uiLoader('#portlet-body', 'show');
            }).on( 'draw.dt', function () {
                uiLoader('#portlet-body', 'hide');
            });

        $('#asset-btn-link').click(function (e) {
            e.preventDefault();
            var url = '<?php echo site_url('bckcadmin/assets/view') ?>/' + $('#select_asset').val();
            window.location.replace(url);
        });
        $('#select_asset').select2(
            {
                placeholder: 'Select a asset',
                allowClear: true,
                id: function(item){
                    return item.id;
                },
                formatResult: function(item) {
                    var markup = "";
                    if (item.id !== undefined) {
                        markup =  [
                            '<div class="media small">',
                            '<div class="pull-left">',
                            '<div class="media-object">',
                            '<img src="' + item.img + '" width="40" height="40"/>',
                            '</div>',
                            '</div>',
                            '<div class="media-body">',
                            '<h4 class="media-heading">' + item.value + '</h4>',
                            '<p style="height: 29px; overflow:hidden;">' + item.desc + '</p>',
                            '</div>',
                            '</div>',
                        ].join('');;
                    }
                    return markup;
                },
                formatSelection: function(item){
                    var markup = "";
                    if (item.id !== undefined) {
                        markup +=  item.value;
                    }
                    return markup;
                },
                initSelection: function(){
                    return '';
                },
                ajax: {
                    url: '<?php echo site_url('bckcadmin/assets/search') ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: function(term, page) {
                        return {
                            q: term,
                            hide_deleted: true,
                            hide_inactive: true,
                            has_feature: 'maintenance',
                            page_limit: 25
                        };
                    },
                    results: function(data, page) {
                        return {
                            results: data
                        };
                    }
                }
            }
        );
        $('#select_asset').change(function (e) {
            if(parseInt($(this).val()) > 0) {
                $('#asset-btn-link').show();
            } else {
                $('#asset-btn-link').hide();
            }

            mydatatable._fnAjaxUpdate();
        });
        $('#asset-btn-link').hide();

        $('#fuel_table_wrapper select').select2();

    });

    function edit(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        window.location.replace('<?php echo site_url('bckcadmin/maintenance/edit') ?>/' + rid);
    }

    function remove_record(target) {
        var rid = target.childNodes[0].attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to delete this maintenance?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/maintenance/delete') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    function getselected() {
        var value = $('#select_asset').val();
        $.post('<?php echo site_url('bckcadmin/assets/getsingle'); ?>', {asset_id: value}, function (data) {
            if(data.status == 'success') {
                $('#select_asset').select2('data', {
                    id: data.result.asset_id,
                    value: data.result.friendly_name
                });
                $('#select_asset').select2('val', data.result.asset_id).trigger('change');
            }

        }, 'json');

    }
    getselected();
</script>
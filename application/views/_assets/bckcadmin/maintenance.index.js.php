<script src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js'); ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo public_url('plugins/ajaxform/jquery.form.min.js') ?>"></script>
<script src="<?php echo asset_url('inline/asset/bckcadmin/maintenance.index.js') ?>"></script>
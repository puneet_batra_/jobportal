<script>
    toastr.options = {
        "closeButton": true
    };
    $('#theme-select').select2(
        {
            placeholder: 'Select a theme',
            allowClear: false
        }
    );
    $('#theme-select').change(function (e) {
        var css = $('option:selected', this).attr('data-css');
        var style_link = $('#style_color');
        var url = '<?php echo public_url('css/themes', false); ?>/';
        style_link.attr('href', url + css);
    }).trigger('change');

    $('#preference-form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $('[type=submit]', form).attr('disabled', 'disabled');
        var l = Ladda.create($('[type=submit]', form)[0]);
        l.start();
        form.ajaxSubmit({
            dataType: 'json',
            success: function(data){
                if(data.status == 'fail')
                {
                    for(var key in data.errors) {
                        toastr['error'](data.errors[key]);
                    }
                    $('[type=submit]', form).removeAttr('disabled');
                } else if(data.status == 'success') {
                    toastr['success'](data.message);
                    $('[type=submit]', form).removeAttr('disabled');
                } else
                {

                }
                l.stop();
            }
        });
    })

</script>
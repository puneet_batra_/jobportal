<script>
    var TableAjax = function () {

        var initPickers = function () {
            //init date pickers
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true
            });
        };

        var assetRecords = function () {

            var grid = new Datatable();

            grid.init({
                src: $("#datatable_ajax"),
                onSuccess: function (grid) {
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                    $('.has-tooltip').tooltip();
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                    // So when dropdowns used the scrollable div should be removed.
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    "serverSide": true,
                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, 300],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": "<?php echo site_url('bckcadmin/reporting/get') ?>", // ajax source
                    },
                    "order": [
                        [0, "asc"]
                    ],// set first column as a default sort by asc
                    aoColumns: [
                        { mData: 'asset_id'},
                        { mData: 'friendly_name'},
                        { mData: 'description_plain'},
                        { mData: 'barcode'},
                        { mData: 'status'},
                        { mData: 'asset_type'},
                        { mData: 'features'}
                    ],
                    "aoColumnDefs" : [
                        {'bSortable' : true, 'aTargets' : [0] },
                        {'bSearchable' : true, 'aTargets': [0] },
                        {'bSortable' : false, 'aTargets' : [1] },
                        {'bSearchable' : false, 'aTargets': [1] },
                        {'bSortable' : false, 'aTargets' : [2] },
                        {'bSearchable' : false, 'aTargets': [2] },
                        {'bSortable' : false, 'aTargets' : [3] },
                        {'bSearchable' : false, 'aTargets': [3] },
                        {'bSortable' : false, 'aTargets' : [4] },
                        {'bSearchable' : false, 'aTargets': [4] },
                        {
                          "render": function( data, type, row ){
                              var template = '<input class="form-filter" type="checkbox" name="query_ids[]" value="' + data + '">'
                              return template;
                          },
                            "targets": 0
                        },
                        {
                            "render": function ( data, type, row ) {
                                var template = '<a style="text-decoration:none" target="_blank" href="<?php echo site_url("bckcadmin/assets/view") ?>/'+ row.asset_id +'"><img src="'+ row.image + '" style="display: inline-block;margin-right: 5px;"/> <span style="display: inline-block">'+ row.friendly_name +'</span></a>';
                                return template;
                            },
                            "targets": 1
                        }
                    ]
                }
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                } else if (action.val() == "") {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });
            var reporting_portlet = $('.portlet-reporting-asset');
            var timer = 0;
            $('.check_all', reporting_portlet).click(function () {
                if($(this).is(':checked')) {
                    $('input[type=checkbox]', reporting_portlet).attr('checked', 'checked');
                } else {
                    $('input[type=checkbox]', reporting_portlet).removeAttr('checked');
                }
                $.uniform.update();
            });
            $('.form-filter', reporting_portlet).keyup(function(e){
                clearTimeout(timer);
                timer = setTimeout(function () {
                    $('.filter-submit', reporting_portlet).click();
                }, 800);
            });
            $('.form-filter', reporting_portlet).change(function(e){
                clearTimeout(timer);
                $('.filter-submit', reporting_portlet).click();
            });
            $('.export-csv', reporting_portlet).click(function(e){
                e.preventDefault();
                var button = $(this);
                button.addClass('loading');
                var formdata = {};
                $('.form-filter', reporting_portlet).each(function () {
                    var key = $(this).attr('name');
                    var val = $(this).val();

                    if (key != undefined) {
                        var original_key = key;
                        key = key.replace('[]', '');
                        var values = $('[name="' + original_key + '"]:checked', reporting_portlet).map(function(){
                            return $(this).val();
                        }).get();
                        if(values.length > 0) {
                            val = values;
                        }
                        if(original_key != key && values.length == 0) {
                            val = [];
                        }
                        eval('formdata.' + key + ' = val');
                    }
                });
                $.post('<?php echo site_url('bckcadmin/reporting/assets_csv') ?>', formdata, function(data){
                    if(data.status == undefined) {
                        bootbox.alert('There may be an issue generating csv, please try again.');
                    } else {
                        if(data.status == 'fail') {
                            toastr['error'](data.message);
                        } else {
                            toastr['success'](data.message);
                            window.location.replace(data.fileurl);
                        }
                    }
                    button.removeClass('loading');
                }, 'json')
            });
            $('.export-pdf', reporting_portlet).click(function(e){
                e.preventDefault();
                var button = $(this);
                button.addClass('loading');
                var formdata = {};
                $('.form-filter', reporting_portlet).each(function () {
                    var key = $(this).attr('name');
                    var val = $(this).val();
                    if (key != undefined) {
                        var original_key = key;
                        key = key.replace('[]', '');
                        var values = $('[name="' + original_key + '"]:checked', reporting_portlet).map(function(){
                            return $(this).val();
                        }).get();
                        if(values.length > 0) {
                            val = values;
                        }
                        if(original_key != key && values.length == 0) {
                            val = [];
                        }
                        eval('formdata.' + key + ' = val');
                    }
                });
                $.post('<?php echo site_url('bckcadmin/reporting/assets_pdf') ?>', formdata, function (data) {
                    if (data.status == undefined) {
                        bootbox.alert('There may be an issue generating pdf, please try again.');
                    } else {
                        if (data.status == 'fail') {
                            toastr['error'](data.message);
                        } else {
                            toastr['success'](data.message);
                            window.location.replace(data.fileurl);
                        }
                    }
                    button.removeClass('loading');
                }, 'json');
            })
        };

        var fuelRecords = function () {

            var grid = new Datatable();

            grid.init({
                src: $("#datatable_ajax_fuel"),
                onSuccess: function (grid) {
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                    $('.has-tooltip').tooltip();
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                    // So when dropdowns used the scrollable div should be removed.
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    "serverSide": true,
                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, 2000],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": "<?php echo site_url('bckcadmin/reporting/getfuel') ?>", // ajax source
                    },
                    "order": [
                        [0, "asc"]
                    ],// set first column as a default sort by asc
                    aoColumns: [
                        { mData: 'asset_id'},
                        { mData: 'friendly_name'},
                        { mData: 'description_plain'},
                        { mData: 'status'},
                        { mData: 'asset_type'}
                    ],
                    "aoColumnDefs" : [
                        {'bSortable' : true, 'aTargets' : [0] },
                        {'bSearchable' : true, 'aTargets': [0] },
                        {'bSortable' : false, 'aTargets' : [1] },
                        {'bSearchable' : false, 'aTargets': [1] },
                        {'bSortable' : false, 'aTargets' : [2] },
                        {'bSearchable' : false, 'aTargets': [2] },
                        {'bSortable' : false, 'aTargets' : [3] },
                        {'bSearchable' : false, 'aTargets': [3] },
                        {
                            "render": function( data, type, row ){
                                var template = '<input class="form-filter" type="checkbox" name="query_ids[]" value="' + data + '">'
                                return template;
                            },
                            "targets": 0
                        },
                        {
                            "render": function ( data, type, row ) {
                                var template = '<a style="text-decoration:none" target="_blank" href="<?php echo site_url("bckcadmin/assets/view") ?>/'+ row.asset_id +'"><img src="'+ row.image + '" style="display: inline-block;margin-right: 5px;"/> <span style="display: inline-block">'+ row.friendly_name +'</span></a>';
                                return template;
                            },
                            "targets": 1
                        }
                    ]
                }
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                } else if (action.val() == "") {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });
            var reporting_portlet_fuel = $('.portlet-reporting-fuel');
            var timer = 0;
            $('.check_all', reporting_portlet_fuel).click(function () {
                if($(this).is(':checked')) {
                    $('input[type=checkbox]', reporting_portlet_fuel).attr('checked', 'checked');
                } else {
                    $('input[type=checkbox]', reporting_portlet_fuel).removeAttr('checked');
                }
                $.uniform.update();
            });
            $('.form-filter', reporting_portlet_fuel).keyup(function(e){
                clearTimeout(timer);
                timer = setTimeout(function () {
                    $('.filter-submit', reporting_portlet_fuel).click();
                }, 800);
            });
            $('.form-filter', reporting_portlet_fuel).change(function(e){
                clearTimeout(timer);
                $('.filter-submit', reporting_portlet_fuel).click();
            });
            $('.export-csv', reporting_portlet_fuel).click(function(e){
                e.preventDefault();
                var button = $(this);
                button.addClass('loading');
                var formdata = {};
                $('.form-filter', reporting_portlet_fuel).each(function () {
                    var key = $(this).attr('name');
                    var val = $(this).val();
                    if (key != undefined) {
                        var original_key = key;
                        key = key.replace('[]', '');
                        var values = $('[name="' + original_key + '"]:checked', reporting_portlet_fuel).map(function(){
                            return $(this).val();
                        }).get();
                        if(values.length > 0) {
                            val = values;
                        }
                        if(original_key != key && values.length == 0) {
                            val = [];
                        }
                        eval('formdata.' + key + ' = val');
                    }
                });
                $.post('<?php echo site_url('bckcadmin/reporting/fuel_csv') ?>', formdata, function(data){
                    if(data.status == undefined) {
                        bootbox.alert('There may be an issue generating csv, please try again.');
                    } else {
                        if(data.status == 'fail') {
                            toastr['error'](data.message);
                        } else {
                            toastr['success'](data.message);
                            window.location.replace(data.fileurl);
                        }
                    }
                    button.removeClass('loading');
                }, 'json')
            });
            $('.export-pdf', reporting_portlet_fuel).click(function(e){
                e.preventDefault();
                var button = $(this);
                button.addClass('loading');
                var formdata = {};
                $('.form-filter').each(function () {
                    var key = $(this).attr('name');
                    var val = $(this).val();
                    if (key != undefined) {
                        var original_key = key;
                        key = key.replace('[]', '');
                        var values = $('[name="' + original_key + '"]:checked', reporting_portlet_fuel).map(function(){
                            return $(this).val();
                        }).get();
                        if(values.length > 0) {
                            val = values;
                        }
                        if(original_key != key && values.length == 0) {
                            val = [];
                        }
                        eval('formdata.' + key + ' = val');
                    }
                });
                $.post('<?php echo site_url('bckcadmin/reporting/fuel_pdf') ?>', formdata, function (data) {
                    if (data.status == undefined) {
                        bootbox.alert('There may be an issue generating pdf, please try again.');
                    } else {
                        if (data.status == 'fail') {
                            toastr['error'](data.message);
                        } else {
                            toastr['success'](data.message);
                            window.location.replace(data.fileurl);
                        }
                    }
                    button.removeClass('loading');
                }, 'json');
            })
        };

        var exceptionRecords = function () {

            var grid = new Datatable();

            grid.init({
                src: $("#datatable_ajax_exception"),
                onSuccess: function (grid) {
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                    $('.has-tooltip').tooltip();
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                    // So when dropdowns used the scrollable div should be removed.
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    "serverSide": true,
                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, 2000],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": "<?php echo site_url('bckcadmin/reporting/getexception') ?>", // ajax source
                    },
                    "order": [
                        [0, "asc"]
                    ],// set first column as a default sort by asc
                    aoColumns: [
                        { mData: 'asset_id'},
                        { mData: 'friendly_name'},
                        { mData: 'description_plain'},
                        { mData: 'status'},
                        { mData: 'asset_type'}
                    ],
                    "aoColumnDefs" : [
                        {'bSortable' : true, 'aTargets' : [0] },
                        {'bSearchable' : true, 'aTargets': [0] },
                        {'bSortable' : false, 'aTargets' : [1] },
                        {'bSearchable' : false, 'aTargets': [1] },
                        {'bSortable' : false, 'aTargets' : [2] },
                        {'bSearchable' : false, 'aTargets': [2] },
                        {'bSortable' : false, 'aTargets' : [3] },
                        {'bSearchable' : false, 'aTargets': [3] },
                        {
                            "render": function( data, type, row ){
                                var template = '<input class="form-filter" type="checkbox" name="query_ids[]" value="' + data + '">'
                                return template;
                            },
                            "targets": 0
                        },
                        {
                            "render": function ( data, type, row ) {
                                var template = '<a style="text-decoration:none" target="_blank" href="<?php echo site_url("bckcadmin/assets/view") ?>/'+ row.asset_id +'"><img src="'+ row.image + '" style="display: inline-block;margin-right: 5px;"/> <span style="display: inline-block">'+ row.friendly_name +'</span></a>';
                                return template;
                            },
                            "targets": 1
                        }
                    ]
                }
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                } else if (action.val() == "") {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });
            var reporting_portlet_fuel = $('.portlet-reporting-exception');
            var timer = 0;
            $('.check_all', reporting_portlet_fuel).click(function () {
                if($(this).is(':checked')) {
                    $('input[type=checkbox]', reporting_portlet_fuel).attr('checked', 'checked');
                } else {
                    $('input[type=checkbox]', reporting_portlet_fuel).removeAttr('checked');
                }
                $.uniform.update();
            });
            $('.form-filter', reporting_portlet_fuel).keyup(function(e){
                clearTimeout(timer);
                timer = setTimeout(function () {
                    $('.filter-submit', reporting_portlet_fuel).click();
                }, 800);
            });
            $('.form-filter', reporting_portlet_fuel).change(function(e){
                clearTimeout(timer);
                $('.filter-submit', reporting_portlet_fuel).click();
            });
            $('.export-csv', reporting_portlet_fuel).click(function(e){
                e.preventDefault();
                var button = $(this);
                button.addClass('loading');
                var formdata = {};
                $('.form-filter', reporting_portlet_fuel).each(function () {
                    var key = $(this).attr('name');
                    var val = $(this).val();
                    if (key != undefined) {
                        var original_key = key;
                        key = key.replace('[]', '');
                        var values = $('[name="' + original_key + '"]:checked', reporting_portlet_fuel).map(function(){
                            return $(this).val();
                        }).get();
                        if(values.length > 0) {
                            val = values;
                        }
                        if(original_key != key && values.length == 0) {
                            val = [];
                        }
                        eval('formdata.' + key + ' = val');
                    }
                });
                $.post('<?php echo site_url('bckcadmin/reporting/exception_csv') ?>', formdata, function(data){
                    if(data.status == undefined) {
                        bootbox.alert('There may be an issue generating csv, please try again.');
                    } else {
                        if(data.status == 'fail') {
                            toastr['error'](data.message);
                        } else {
                            toastr['success'](data.message);
                            window.location.replace(data.fileurl);
                        }
                    }
                    button.removeClass('loading');
                }, 'json')
            });
        };

        return {

            //main function to initiate the module
            init: function () {

                initPickers();
                assetRecords();
                fuelRecords();
                exceptionRecords();
            }

        };

    }();
    TableAjax.init();

</script>
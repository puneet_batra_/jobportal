<script>
    toastr.options = {
        "closeButton": true
    };
    $(function(){

        $('[name^="permission"]').on('ifChecked', function (e) {
            var id = 0;
            var value = this.value;
            $(this).attr('name').replace(/[0-9]+/gi, function (a) {
                id = a;
                return a;
            });

            if (value == 1 || value == 2) {
                $('[name^="sub_permissions[' + id + ']"]').iCheck('enable');
            } else {
                $('[name^="sub_permissions[' + id + ']"]').iCheck('disable');
            }
        });

        $('#select_role').change(function () {
            var val = $(this).val();

            if (val == false) {
                $('#text_role_name').val('');
                $('#text_role_description').val('');
                $('#btn_update').hide();
                $('#btn_delete').hide();
                $('#btn_add').show();
                reset_input();
            } else {
                uiLoader('#form-body', 'show');
                //uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/roles/get') ?>', {role_id: val}, function (data) {
                    reset_input();
                    $('#text_role_name').val(data.role.name);
                    $('#text_role_description').val(data.role.description);

                    data.modules.forEach(function(module){

                        $('[name="permission['+ module.module_id + ']"]').each(function(){
                            $(this).removeAttr('checked').iCheck('update').trigger('ifChecked');
                            if($(this).val() == module.permissions) {
                                $(this).attr('checked', 'checked').iCheck('update');
                            }
                        });
                        var sub_permissions = {};
                        try {
                            sub_permissions = JSON.parse(module.sub_permissions);
                        } catch(e) {

                        }

                        // check mark enabled permissions
                        $.each(sub_permissions,function(key, value){
                            $('[name^="sub_permissions['+ module.module_id + ']"]').each(function(){
                                var name = $(this).attr('name');
                                var generated_name = "sub_permissions[" + module.module_id + "][" + key + "]";
                                if(name == generated_name) {
                                    if($(this).val() == value) {
                                        $(this).attr('checked', 'checked').iCheck('update');
                                    } else {
                                        $(this).removeAttr('checked').iCheck('update');
                                    }
                                }
                            });
                        })

                        refreshRadio();
                    });

                    $('#btn_add').hide();
                    $('#btn_update').show();
                    $('#btn_delete').show();
                    uiLoader('#form-body', 'hide');
                    //uiLoader('#portlet-body', 'hide');
                }, 'json');

            }

        });
        $('#select_role').change();


        $('#role-form').validate({
            rules: {
                role_name: {
                    required: true
                },
                role_description: {
                    required: true
                }
            },
            messages: {

            },
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            invalidHandler: function(event, validator) { //display error alert on form submit

            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {

            }
        });

        $('#btn_add').click(function(){
            var btn = $(this);

            if($('#role-form').validate().form()) {
                btn.attr('disabled', 'disabled');
                var l = Ladda.create($('#btn_add')[0]);
                l.start();
                $.post('<?php echo site_url('bckcadmin/roles/add') ?>', $('#role-form').serialize(),
                    function(data){
                        if(data.status == 'fail')
                        {
                            for(var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else {
                            toastr['success'](data.message);
                            $('#role-form')[0].reset();
                            $('#select-role').change();
                            reset_input();
                        }
                        getRoles();
                        btn.removeAttr('disabled');
                        l.stop();
                    }, 'json')
            }
        });

        $('#btn_update').click(function(){
            if($('#role-form').validate().form()) {
                var l = Ladda.create($('#btn_update')[0]);
                l.start();
                $.post('<?php echo site_url('bckcadmin/roles/update') ?>', $('#role-form').serialize(),
                    function(data){
                        if(data.status == 'fail')
                        {
                            for(var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                        } else if (data.status == 'success') {
                            toastr['success'](data.message);
                            $('#select-role').change();
                        }
                        getRoles();
                        l.stop();
                    }, 'json')
            }
        });

        $('#btn_delete').click(function(e){
            e.preventDefault();
            bootbox.confirm('Are you sure you want to delete?', function(result) {
                if(result == true) {
                    var l = Ladda.create($('#btn_delete')[0]);
                    l.start();
                    $.post('<?php echo site_url('bckcadmin/roles/delete') ?>', $('#role-form').serialize(),
                        function(data){
                            getRoles();
                            if(data.status == 'fail')
                            {
                                for(var key in data.errors) {
                                    toastr['error'](data.errors[key]);
                                }
                            } else if(data.status == 'success') {
                                toastr['success'](data.message);
                                $('#role-form')[0].reset();
                                reset_input();
                            }
                            l.stop();
                        }, 'json')
                }
            });

        })

        // on page load by default no access selected
        $('[name^="permission"][value=0]').attr('checked','checked').trigger('ifChecked');
    });
    function getRoles() {
        $.post('<?php echo site_url('bckcadmin/roles/get_roles') ?>',function(data){
            var output = '<option value=""></option>';
            data.forEach(function(role){
                output += '<option value="' + role.role_id + '">' + role.name + '</option>';
            });
            $('#select_role').html(output);
            $('#select_role').change();
        }, 'json')
    }

    function reset_input() {
        $('[name^="permission"]').each(function(){
            $(this).removeAttr('checked').iCheck('update').trigger('ifChecked');
            if($(this).val() == 0) {
                $(this).attr('checked', 'checked').iCheck('update').trigger('ifChecked');
            }

        });

        $('[name^="sub_permissions"]').each(function(){
            $(this).removeAttr('checked').iCheck('update').trigger('ifChecked');
            if($(this).val() == 0) {
                $(this).attr('checked', 'checked').iCheck('update');
            }

        });
    }

    function refreshRadio() {
        $('[name^="permission"]').each(function(){
            var id = 0;
            var value = this.value;
            $(this).attr('name').replace(/[0-9]+/gi, function (a) {
                id = a;
                return a;
            });
            if($(this).is(':checked')) {
                if($(this).val() == 1 || $(this).val() == 2) {
                    $('[name^="sub_permissions[' + id + ']"]').each(function(){
                        $(this).iCheck('enable');
                    });
                } else {
                    $('[name^="sub_permissions[' + id + ']"]').each(function(){
                        $(this).iCheck('disable');
                    });
                }
            }

        });


    }



</script>
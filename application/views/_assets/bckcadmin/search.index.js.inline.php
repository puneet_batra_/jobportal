<script>
    toastr.options = {
        "closeButton": true
    };

    var search_config = {
        offset: 0,
        perPage: 10,
        resultsContainer: '#results-container',
        searchBox: '#search-box',
        endOfSearch: '#end-of-search'
    };

    function do_search() {
        var search_box = $(search_config.searchBox);
        var results_container = $(search_config.resultsContainer);
        var endofsearch = $(search_config.endOfSearch);
        var btn_load_more = $('#btn-load-more');

        var q = search_box.val();
        if(q != '') {
            var postdata = {
                is_search_page: true,
                offset: search_config.offset,
                limit: search_config.perPage,
                q: q
            };
            ajaxLoader('show');
            endofsearch.hide();
            $.post('<?php echo site_url('bckcadmin/assets/search') ?>', postdata, function (data) {
                var records = data;
                if(records.length > 0) {
                    var formatted_records = [];
                    records.forEach(function (record) {
                        formatted_records.push(_format_record(record))
                    });
                    results_container.append(formatted_records.join(''));
                    if(records.length < search_config.perPage) {
                        endofsearch.append(_format_no_records());
                        btn_load_more.hide();
                    } else {
                        btn_load_more.show();
                    }
                } else {
                    endofsearch.append(_format_no_records())
                }
                ajaxLoader('hide');
            }, 'json');
        } else {
            btn_load_more.hide();
            endofsearch.html('<i class="icon-info"></i> Please enter something to search..');
        }
    }

    function _format_record(record) {
        var template = '<div onclick="window.location.replace(\'{{link}}\')" class="search-classic {{class}}" data-result-id="{{id}}">\n    \n    <div class="result-row">\n        <div class="image">\n            <img src="{{img}}"\n                 style="width:100%;border:1px solid #dadada" alt=""/>\n        </div>\n        <div class="description">\n            <h4>\n                <a href="{{link}}">{{value}}</a>\n            </h4>\n            <div class="desc" style="height: 76px;overflow: hidden">{{desc}}</div>\n        </div>\n        <div class="clear"></div>\n    </div>\n\n</div>';
        var newData = template.replace(/\{\{([^\}]+)\}\}/ig, function (x, y) {
            return eval('record.' + y);
        });
        return newData;
    }

    function _format_no_records() {
        var endofsearch = $(search_config.endOfSearch);
        var btn_load_more = $('#btn-load-more');
        endofsearch.html('<i class="icon-info"></i> No more results to display').show();
        btn_load_more.hide();
    }

    do_search();

    $('#search-btn').click(function () {
        search_config.offset = 0;
        var resultsContainer = $(search_config.resultsContainer);
        resultsContainer.html('');

        history.pushState({}, '', '<?php echo site_url('bckcadmin/search') ?>?q=' + $(search_config.searchBox).val());
        do_search();
    });

    $('#search-form').submit(function (e) {
        e.preventDefault();
        $('#search-btn').trigger('click');
    });

    $('#btn-load-more').click(function(e){
        e.preventDefault();
        search_config.offset = search_config.offset + search_config.perPage;
        do_search();
    })

</script>
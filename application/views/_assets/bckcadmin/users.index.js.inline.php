<script>
    toastr.options = {
        "closeButton": true
    };

    $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-btn-group pull-right",
        "buttons": {
            "normal": "btn btn-sm default",
            "disabled": "btn btn-sm default disabled"
        },
        "collection": {
            "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
    });

    var dataTableOptions = {
        'filter': true,
        'processing': false,
        "serverSide": true,
        "ajax": {
            url:'<?php echo site_url('bckcadmin/users/get') ?>',
            type: 'POST'
        },
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "processing": ajaxLoader('html')
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        "order": [
            [0, 'asc']
        ],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 15,

        aoColumns: [
            { mData: 'name'},
            { mData: 'email'},
            { mData: 'role'},
            { mData: 'status'},
            { mData: 'actions'}
            ],
        "aoColumnDefs" : [
            {'bSortable' : false,'aTargets' : [4]},
            {'bSearchable' : false, 'aTargets': [4] },
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    if(data == 1) {
                        newData = '<a class="label label-sm label-success">Accepted</a>';
                    } else{
                        newData = '<a class="label label-sm label-danger">Pending</a>';
                        <?php if (
                            $this->auth->is_superuser()
                            || $this->auth->is_superadmin()
                            || $this->auth->is_emulated_by_superuser()
                            || $this->auth->is_emulated_by_superadmin()
                        ) { ?>
                        if(row.user_id != 0) {
                            newData += ' <a onclick="do_accept(this)" id="datatable-item-'+ row.invite_id +'" class="label label-sm label-success">Accept</a>';
                        }
                        <?php } ?>
                    }
                    return newData;
                },
                "targets": 3
            }
        ]
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });

    <?php if (
        $this->auth->is_superuser()
        || $this->auth->is_superadmin()
        || $this->auth->is_emulated_by_superuser()
        || $this->auth->is_emulated_by_superadmin()
    ) { ?>
    function do_accept(target) {
        var rid = target.attributes.id.value.replace('datatable-item-', '');
        bootbox.confirm('Are you sure you want to make it accepted?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('invite/do_accept') ?>', {rid: rid}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }
    <?php } ?>

    //add

    $('#form-add').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('bckcadmin/users/add') ?>', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#createModal').modal('hide');
                        $('#form-add')[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{
                first_name: {
                    required: false
                },
                last_name: {
                    required: false
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?php echo site_url('validator/validate') ?>',
                        type: 'POST',
                        data:{
                            rules: 'required|valid_email'
                        }
                    }
                },
                role_id: {
                    required: true
                }

            },
            messages:{
                email: {
                    remote: 'Email not valid or already exists.'
                }

            }
        }
    );

    // edit form
    function show_edit(target) {
        var r_id = parseInt(target.childNodes[1].attributes.id.value.replace(/([^0-9]+)/, ''));
        var form = $('#form-edit');
        uiLoader('#portlet-body', 'show');
        $.post('<?php echo site_url('bckcadmin/users/single') ?>', {uid: r_id}, function (data) {
            if(data.status == 'fail')
            {
                for(var key in data.errors) {
                    toastr['error'](data.errors[key]);
                }
            } else if(data.status == 'success') {
                $('[name="uid"]', form).val(data.record.role.user_role_id);
                if(data.record.role) {
                    $('[name="role_id"] option[value="' + data.record.role.role_id + '"]', form).attr('selected', 'selected');
                }
                if(data.record.company_admin == false) {
                    $('[name=company_admin][value=0]').attr('checked', 'checked').trigger('ifChecked');
                } else {
                    $('[name=company_admin][value=1]').attr('checked', 'checked').trigger('ifChecked');
                }


                $('[name="role_id"]', form).change();
                $('#editModal').modal('show');
            }
            uiLoader('#portlet-body', 'hide');
        }, 'json');

    }




    $('#select2_sample5').change(function() {
        $('#form-add').validate().element($(this));
    });

    $('[name=company_admin]').on('ifChecked', function (e) {
        var value = $(this).val();
        if(value == 1) {
            $('.role-div').hide();
        } else {
            $('.role-div').show();
        }
        $('input').iCheck('update');
    });




    $('#form-edit').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var formData = form.serialize();
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                $.post('<?php echo site_url('bckcadmin/users/edit') ?>', formData, function(data){
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                        $('[type=submit]', form).removeAttr('disabled');
                    } else if(data.status == 'success') {
                        $('#editModal').modal('hide');
                        $('#form-edit')[0].reset();
                        toastr['success'](data.message);
                        $('[type=submit]', form).removeAttr('disabled');
                        mydatatable._fnAjaxUpdate();
                    }
                    l.stop();
                }, 'json');
            },
            errorPlacement: function(error, element){
                error.insertAfter(element);
            },
            rules:{

                role_id: {
                    required: function(){
                        if($('[name=company_admin]').val() == 0 && $('[name=role_id]').val == '') {
                            return false;
                        } else {
                            return true;
                        }
                    }
                }

            },
            messages:{

            }
        }
    );


    // delete
    function do_delete(target) {
        var r_id = parseInt(target.childNodes[1].attributes.id.value.replace(/([^0-9]+)/, ''));
        bootbox.confirm('Are you sure you want to delete?', function(result){
           if(result == true) {
               uiLoader('#portlet-body', 'show');
               $.post('<?php echo site_url('bckcadmin/users/delete') ?>?a=index', {uid: r_id}, function (data) {
                   if(data.status == 'fail')
                   {
                       for(var key in data.errors) {
                           toastr['error'](data.errors[key]);
                       }
                   } else if(data.status == 'success') {
                       toastr['success'](data.message);
                       mydatatable._fnAjaxUpdate();
                   }
                   uiLoader('#portlet-body', 'hide');
               }, 'json');
           }
        });

    }

    // cancel invite
    function do_remove_invite(target) {
        var r_id = parseInt(target.childNodes[1].attributes.id.value.replace(/([^0-9]+)/, ''));
        bootbox.confirm('Are you sure you want to cancel invite?', function(result){
            if(result == true) {
                uiLoader('#portlet-body', 'show');
                $.post('<?php echo site_url('bckcadmin/users/deleteinvite') ?>?a=index', {uid: r_id}, function (data) {
                    if(data.status == 'fail')
                    {
                        for(var key in data.errors) {
                            toastr['error'](data.errors[key]);
                        }
                    } else if(data.status == 'success') {
                        toastr['success'](data.message);
                        mydatatable._fnAjaxUpdate();
                    }
                    uiLoader('#portlet-body', 'hide');
                }, 'json');
            }
        });
    }

    $('#add-admin-form').validate(
        {
            ignore: [],
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                form = $(form);
                $('[type=submit]', form).attr('disabled', 'disabled');
                var l = Ladda.create($('[type=submit]', form)[0]);
                l.start();
                form.ajaxSubmit({
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 'fail') {
                            for (var key in data.errors) {
                                toastr['error'](data.errors[key]);
                            }
                            $('[type=submit]', form).removeAttr('disabled');
                        } else if (data.status == 'success') {

                            form[0].reset();
                            $('.form-group', form).removeClass('has-error');
                            $('input', form).iCheck('update');

                            toastr['success'](data.message);
                            $('[type=submit]', form).removeAttr('disabled');
                            $('#addModal').modal('hide');
                            mydatatable._fnAjaxUpdate();
                        } else {

                        }
                        l.stop();
                    }
                });

            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                user_id: {
                    required: true
                }
            },
            messages: {}
        }
    );

    $('#select2_add_userid').select2(
        {
            placeholder: 'Select a user',
            allowClear: true,
            id: function(item){
                return item.user_id;
            },
            formatResult: function(item) {
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            formatSelection: function(item){
                var markup = "";
                if (item.first_name !== undefined) {
                    markup += item.first_name +  ' (' + item.email + ')' + "";
                }
                return markup;
            },
            initSelection: function(){
                return '';
            },
            ajax: {
                url: '<?php echo site_url('bckcadmin/admins/getlist') ?>',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    return {
                        q: term,
                        page_limit: 10
                    };
                },
                results: function(data, page) {
                    return {
                        results: data.users
                    };
                }
            }
        }
    );
</script>
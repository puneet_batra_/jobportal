<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'); ?>"/>

<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda-themeless.min.css'); ?>"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo public_url('plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css'); ?>"/>
<link href="<?php echo public_url('plugins/bootstrap-datepicker/css/datepicker3.css'); ?>" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo public_url('plugins/icheck/skins/all.css'); ?>" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>"/>

<script>

        $("#country_id").change(function(){
            var id = $(this).val();
            $.get("<?php echo site_url('registration/state')?>",{'id':id},function(data){
                $("#state_id").html(data);
            });
        });
        $("#state_id").change(function(){
            var id = $(this).val();
            $.get("<?php echo site_url('registration/city')?>",{'id':id},function(data){
                $("#city_id").html(data);
            });
        });

      /*  $( "#signup" ).submit(function( event ) {
            alert("in");

            event.preventDefault();
        });*/

   $('#signup').validate(
        {
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {

                form.submit();

            },
            errorPlacement: function(error, element){
                if(element.attr("name") == "user_type"){
                    //error.insertAfter("#user_type");
                }
                else{
                    error.insertAfter(element);
                }

            },
            rules:{
                email: {
                    required: true
                },
                password:{
                    required:true
                },
                confirm_password:{
                    equalTo: "#password"
                },
                user_type:{
                    required:true
                },
                company_name:{
                    required:true
                },
                recruit_industry:{
                    required:true
                },
                address:{
                    required:true
                },
                country:{
                    required:true,
                    remote:{
                        url: '<?php echo base_url("registration/getIplocation") ?>',
                        type: 'POST',
                        data: {
                            country: function(){
                                return $('#country_id').val()
                            }
                        }
                    }
                },
                state:{
                    required:true
                },
                city:{
                    required:true
                },
                phone:{
                    required:true
                },
                first_name:{
                    required:true
                },
                tnc:{
                    required:true
                }

            },
            messages: {
                country: {
                    remote:"You dont seem to be visiting from the country You have selected !"
                }
            }
        }
    );
    $('._dropdown').select2();

</script>
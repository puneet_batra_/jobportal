<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Blog</h4>
            </div>
            <form id="form-add"  >
                <div class="modal-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        Your form validation is successful!
                    </div>
                    <div class="form-group">
                        <label class="control-label">Blog Category</label>
                        <select class="form-control" name="blog_category" id="add_blog_category">
                            <option value="">Select Blog Category</option>
                        </select>                       
                    </div>
                    <div class="form-group">
                        <label class="control-label">Blog Title</label>
                        <input class="form-control" type="text" name="blog_title" id="add_blog_title"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label ">Description <span class="required">* </span>
                        </label>
                        <div class="">
                            <textarea class="ckeditor form-control" name="blog_description" id="editor1" rows="10" cols="20" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Blog</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Page</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <input class="form-control" type="hidden" name="blog_id" id="edit_blog_id" value=""/>
                        <div class="form-group">
                            <label class="control-label">Blog Category</label>
                            <select class="form-control" name="blog_category" id="edit_blog_category">
                                <option value="">Select Blog Category</option>
                            </select>                       
                        </div>
                        <div class="form-group">
                            <label class="control-label">Blog Title</label>
                            <input class="form-control" type="text" name="blog_title" id="edit_blog_title" value=""/>
                            <input type="hidden" name="news_id" value="" id="edit_news_id" />
                        </div>
                      
                        <div class="form-group">
                            <label class="control-label ">Blog Description <span class="required">* </span>
                            </label>
                            <div class="">
                                <textarea class="ckeditor editor form-control" name="blog_decription" id="editior2" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Blog</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <h3 class="page-title">
        Manage Blog
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Blog
                    </div>
                    <div class="actions">
                        <a  onclick="get_category()" class="btn btn-sm btn-default ">
                            <i class="fa fa-plus"></i> Add Blog </a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Blog ID</th>
                            <th style="width: 30%">Category Name</th>
                            <th style="width: 30%">Blog Title</th>
                            <th style="width: 30%">Posted Date</th>
                            <th style="width: 30%">Active</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('layouts/admin/footer.php') ?>
<?php $this->load->view('layouts/admin/header'); ?>
<?php $this->load->view('layouts/admin/sidebar'); ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Company</h4>
                </div>
                <form  id="form-add">
                    <div class="modal-body">

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name <span class="required">* </span></label>
                            <input class="form-control" type="text" name="name" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label ">Email <span class="required">* </span>
                            </label>
                            <div class="">
                                <input name="email" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">User</label>
                            <div class="">
                                <input type="text" name="user_id" class="form-control" id="select2_add_userid" data-placeholder="Select...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Active <span class="required">* </span></label>
                            <select class="form-control" name="active" id="active_select2">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Company</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Company</h4>
                </div>
                <form  id="form-edit">
                    <div class="modal-body">
                        <input name="uid" type="hidden" value=""/>
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <input value="" name="cid" type="hidden"/>

                        <div class="form-group">
                            <label class="control-label">Name <span class="required">* </span></label>
                            <input class="form-control" type="text" name="name" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label ">Email <span class="required">* </span>
                            </label>
                            <div class="">
                                <input name="email" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">User</label>
                            <div class="">

                                <input type="hidden" name="user_id" class="form-control" id="select2_edit_userid" data-placeholder="Select...">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Active <span class="required">* </span></label>
                            <select class="form-control" name="active" id="edit_active_select2">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Company</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <h3 class="page-title">
        Companies <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Companies</a>
            </li>
        </ul>

    </div>
    <div class="row">
        <div class="col-md-12">

            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red-intense theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>Companies
                    </div>

                    <div class="actions">
                        <a data-toggle="modal" data-target="#createModal" href="javascript:;" class="btn btn-sm btn-default">
                            <i class="fa fa-plus"></i> Add Company </a>
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Company ID</th>
                            <th style="width: 20%">Company Admin User</th>
                            <th style="width: 20%">Company Name</th>
                            <th style="width: 30%">Company Email</th>
                            <th style="width: 10%">Active</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE HEADER-->
<?php $this->load->view('layouts/admin/footer'); ?>
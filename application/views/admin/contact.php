<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
<?php //echo "ser=".$_SERVER['HTTP_HOST'];
?>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Reply</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-da+-nger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <input class="form-control" type="hidden" name="contact_id" id="edit_contact_id" value=""/>
                     
                        <!-- <div class="form-group">
                            <label class="control-label">Username</label>
                            <input class="form-control" type="text" name="username" id="edit_username" value=""/>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label">Email<span class="required">* </span></label>
                            <input class="form-control " type="text" name="email" id="edit_email" value="" disabled/>
                        </div>                      
                        <div class="form-group">
                            <label class="control-label ">Message<span class="required">* </span>
                            </label>
                            <div class="">
                                <textarea class="ckeditor editor form-control" name="message" id="editior2" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Reply</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <h3 class="page-title">
        Manage Contact Us
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Contact Us</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Contact Us
                    </div>                    
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">ID</th>
                            <th style="width: 30%">Username</th>
                            <th style="width: 30%">Email</th>
                            <th style="width: 30%">Message</th>
                            <th style="width: 30%">Created At</th>
                            <th style="width: 30%">Reply</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php')?>
    <div class="modal fade" id="cvDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">CV Details</h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet box red-sunglo theme-portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-newspaper-o"></i>CV Detail
                                </div>
                                <div class="DTTT btn-group">
                                </div>
                                <input type="hidden" name="custom_id" id="custom_id" value="">
                            </div>
                            <div class="portlet-body" id="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_3">
                                    <thead>
                                    <tr>
                                        <th style="width: 10%">ID</th>
                                        <th style="width: 10%">Job Position</th>
                                        <th style="width: 30%">Industry/Sector</th>
                                        <th style="width: 30%">Project Type</th>
                                        <th style="width: 30%">Active</th>
                                        <th style="width: 15%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <h3 class="page-title">
        Manage CVs URL(links)
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">CVs Url</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage CVs Url
                    </div>
                    <div class="DTTT btn-group">
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">

                     <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">CV ID</th>
                            <th style="width: 30%">Username</th>
                            <th style="width: 30%">Active</th>
                            <th style="width: 30%">CV Link</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
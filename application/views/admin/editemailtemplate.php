<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
<link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.css'); ?>"/>


   <h3 class="page-title">
        Manage Email Template
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('admin/emailtemplate/') ?>">Email Template</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Edit Email Template</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-envelope-o"></i>Manage Email Template
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <form id="form-edit">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>

                            <th style="width: 10%">Template Name</th>
                            <th style="width: 30%">Template</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input class="form-control" type="text" name="temp_title" id="edit_temp_title" value="<?php echo $edit_content->temp_title; ?>"/>
                                <input type="hidden" name="temp_id" value="<?php echo $edit_content->temp_id; ?>" id="edit_temp_id" />
                            </td>
                            <td>
                                <textarea class="ckeditor editior2 form-control" name="temp_content" id="editior2" rows="20" cols="40" style="width: 100%;height: 100%">
                                    <?php echo $edit_content->temp_content; ?>
                                </textarea>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Template</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.js"'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ckfinder/ckfinder.js'); ?>"></script>
<script src="<?php echo asset_url('inline/asset/admin/emailtemplate.index.js') ?>"></script>
<script>

    editor = CKEDITOR.replace( 'editior2', {
        filebrowserBrowseUrl : '<?php echo public_url('plugins/ckfinder/ckfinder.html')?>',
        filebrowserImageBrowseUrl : '<?php echo public_url('plugins/ckfinder/ckfinder.html?type=Images')?>',
        filebrowserFlashBrowseUrl : '<?php echo public_url('plugins/ckfinder/ckfinder.html?type=Flash')?>',
        filebrowserUploadUrl : '<?php echo public_url('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')?>',
        filebrowserImageUploadUrl : '<?php echo public_url('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images')?>',
        filebrowserFlashUploadUrl : '<?php echo public_url('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash')?>'    });



</script>




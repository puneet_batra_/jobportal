<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php')?>



       <h3 class="page-title">
        Manage Export Data
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php site_url('admin/export_data') ?>">Export Data</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Export Data
                    </div>
                    <div class="DTTT btn-group">
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">
                    <div id="sample_4_wrapper" class="dataTables_wrapper no-footer"></div>
                     <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">ID</th>
                            <th style="width: 30%">User type</th>
                            <th style="width: 15%">Actions</th>
                            <th style="width: 15%">job_position</th>
                            <th style="width: 15%">job_industry</th>
                            <th style="width: 15%">project_type</th>
                            <th style="width: 15%">username</th>
                            <th style="width: 15%">first_name</th>
                            <th style="width: 15%">middle_name</th>
                            <th style="width: 15%">last_name</th>
                            <th style="width: 15%">phone</th>
                            <th style="width: 15%">dob</th>
                            <th style="width: 15%">email</th>
                            <th style="width: 15%">address</th>
                            <th style="width: 15%">city</th>
                            <th style="width: 15%">country</th>
                            <th style="width: 15%">nationality</th>
                            <th style="width: 15%">age</th>
                            <th style="width: 15%">height</th>
                            <th style="width: 15%">weight</th>
                            <th style="width: 15%">relegion</th>
                            <th style="width: 15%">martial_status</th>
                            <th style="width: 15%">passport_num</th>
                            <th style="width: 15%">mobile_num</th>
                            <th style="width: 15%">telephone_num</th>
                            <th style="width: 15%">skype_id</th>
                            <th style="width: 15%">job_seeker_status</th>
                            <th style="width: 15%">languages</th>
                            <th style="width: 15%">hobbies_name</th>
                            <th style="width: 15%">Edu Level</th>
                            <th style="width: 15%">edu_institute_name</th>
                            <th style="width: 15%">edu_city</th>
                            <th style="width: 15%">edu_country</th>
                            <th style="width: 15%">edu_year</th>
                            <th style="width: 15%">edu_degree</th>
                            <th style="width: 15%">edu_grade</th>
                            <th style="width: 15%">exp_company_name</th>
                            <th style="width: 15%">exp_city</th>
                            <th style="width: 15%">exp_country</th>
                            <th style="width: 15%">exp_years</th>
                            <th style="width: 15%">exp_job_position</th>
                            <th style="width: 15%">exp_job_description</th>
                            <th style="width: 15%">company_reference</th>
                            <th style="width: 15%">soft_skills</th>
                            <th style="width: 15%">hard_skills</th>


                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
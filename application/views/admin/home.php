
<?php $this->load->view('layouts/admin/header') ?>
<?php $this->load->view('layouts/admin/sidebar') ?>
<h3 class="page-title">
    Dashboard
    <small>reports & statistics</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo site_url('admin/home') ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo $count_users ?>
                </div>
                <div class="desc">
                    Users Registered
                </div>
            </div>
            <!--            <a class="more" href="#">-->
            <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
            <!--            </a>-->
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-connectdevelop"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?php echo $count_companies ?>
                </div>
                <div class="desc">
                    Companies
                </div>
            </div>
            <!--            <a class="more" href="#">-->
            <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
            <!--            </a>-->
        </div>
    </div>
    <?php if ($this->auth->is_superadmin()) { ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-hdd-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        0 MB
                    </div>
                    <div class="desc">
                        Storage Used
                    </div>
                </div>
                <!--            <a class="more" href="#">-->
                <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
                <!--            </a>-->
            </div>
        </div>
    <?php } ?>
</div>
<!-- END DASHBOARD STATS -->


<?php $this->load->view('layouts/admin/footer') ?>

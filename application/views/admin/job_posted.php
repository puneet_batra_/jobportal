<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Job Post</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">Job Position</label>
                            <select name="job_pos" id="edit_job_pos" class="form-control select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <?php foreach($position as $pos) { ?>
                                    <option value="<?php echo $pos->position_id ?>"><?php echo $pos->position_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Job Industry/Sector</label>
                            <select name="ind_type" id="edit_ind_type" class="form-control select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <?php foreach($industry as $ind) {?>
                                    <option value="<?php echo $ind->industry_id ?>"><?php echo $ind->industry_type ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Project Type</label>
                            <select name="project_type" id="edit_project_type" class="form-control select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <?php foreach($project as $pro) {?>
                                    <option value="<?php echo $pro->project_type_id ?>"><?php echo $pro->project_type ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="form-group">
                            <label class="control-label">Job Salary</label>
                            <input class="form-control" type="text" name="job_salary" id="edit_job_salary" value=""/>

                        </div>
                        <div class="form-group">
                            <label class="control-label">Job Applicatioins</label>
                            <input class="form-control" type="text" name="job_app" id="edit_job_app" value=""/>
                            <input type="hidden" name="job_id" value="" id="edit_job_id" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">T.Position</label>
                            <input class="form-control" type="text" name="tot_pos" id="edit_tot_pos" value=""/>
                            <input type="hidden" name="job_id" value="" id="edit_job_id" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Filled Positions</label>
                            <input class="form-control" type="text" name="fill_pos" id="edit_fill_pos" value=""/>
                            <input type="hidden" name="job_id" value="" id="edit_job_id" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Available Positions</label>
                            <input class="form-control" type="text" name="avail_pos" id="edit_avail_pos" value=""/>
                            <input type="hidden" name="job_id" value="" id="edit_job_id" />
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <h3 class="page-title">
        Manage Posted Job
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('admin/job_post')?>">Job Post</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Posted Job</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Posted Job
                    </div>
                    <div class="DTTT btn-group">
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">
                    <input type="hidden" name="posted_job_id" value="<?php echo $posted_job_id ?>" id="posted_job_id" />
                     <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">ID</th>
                            <th style="width: 10%">Position</th>
                            <th style="width: 30%">Industry</th>
                            <th style="width: 30%">Project Type</th>
                            <th style="width: 30%">Salary</th>
                            <th style="width: 30%">Applications</th>
                            <th style="width: 30%">T.Position</th>
                            <th style="width: 30%">Filled Position</th>
                            <th style="width: 30%">Available Position</th>
                            <th style="width: 30%">Active</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
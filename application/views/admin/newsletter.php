<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Newsletter</h4>
            </div>
            <form id="form-add">
                <div class="modal-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        Your form validation is successful!
                    </div>
                    <div class="form-group">
                        <label class="control-label">Newsletter Name</label>
                        <input class="form-control" type="text" name="news_title" id="add_news_title"/>
                        <input class="form-control" type="hidden" name="news_id" id="add_news_id"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label ">Description <span class="required">* </span>
                        </label>
                        <div class="">
                            <textarea class="ckeditor form-control" name="news_content" id="editor1" rows="10" cols="20" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Newsletter</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Newsletters</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">Newsletter Name</label>
                            <input class="form-control" type="text" name="news_title" id="edit_news_title" value=""/>
                            <input type="hidden" name="news_id" value="" id="edit_news_id" />
                        </div>

                        <div class="form-group">
                            <label class="control-label ">Newsletter Description <span class="required">* </span>
                            </label>
                            <div class="">
                                <textarea class="ckeditor editor form-control" name="news_content" id="editior2" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Newsletter</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- /.modal -->

    <div class="modal fade" id="subModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">User Suggestions</h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet box red-sunglo theme-portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-newspaper-o"></i>User Suggestions
                                </div>
                                <div class="DTTT btn-group">
                                </div>
                                <input type="hidden" name="custom_id" id="custom_id" value="">
                            </div>
                            <div class="portlet-body" id="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_3">
                                    <thead>
                                    <tr>
                                        <th style="width: 10%">Subscribed User</th>
                                        <th style="width: 30%">Email Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <h3 class="page-title">
        Manage Newsletter
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Newsletter</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Newsletter
                    </div>
                    <div class="actions">
                        <a data-toggle="modal" data-target="#createModal" href="javascript:;" class="btn btn-sm btn-default">
                            <i class="fa fa-plus"></i> Add Newsletter </a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Newsletter ID</th>
                            <th style="width: 30%">Newsletter Name</th>
                            <th style="width: 30%">Newsletter Description</th>
                            <th style="width: 30%">Active</th>
                            <th style="width: 30%">Send To</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
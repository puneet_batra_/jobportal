<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Package</h4>
                </div>
                <form id="form-add">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">Package Name<span class="required">*</span></label>
                            <input class="form-control" type="text" name="package_name" id="add_package_name"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Package Facilities </label>
                            <div class="">
                                <textarea class="ckeditor form-control" name="package_facilities" id="editor1" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Select User Type <span class="required">* </span></label>
                            <div class="">
                                <select name="user_type" id="add_user_type" class="form-control select2me" data-placeholder="Select...">
                                    <option value="">Select User Type</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Package Price<span class="required">*</span></label>
                            <input class="form-control" type="text" name="package_price_in" id="add_package_price_in"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Pacakge Currency <span class="required">* </span></label>
                            <div class="">
                                <select name="package_currency" id="add_package_currency" class="form-control select2me" data-placeholder="Select...">
                                    <option value="">Pacakge Currency</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Package Validity <span class="required">*</span></label>
                            <input class="form-control" type="text" name="package_validity" id="add_package_validity"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Total Credits </label>
                            <input class="form-control" type="text" name="total_credits" id="add_total_credits"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Credits Per User Activity </label>
                            <input class="form-control" type="text" name="credit_per_user_activity" id="add_credit_per_user_activity"/>
                        </div>                
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Package</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Package</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">Package Name<span class="required">* </span></label>
                            <input type="hidden" name="package_id" value="" id="edit_package_id" />
                            <input class="form-control" type="text" name="package_name" id="edit_package_name" value=""/>                            
                        </div>

                        <div class="form-group">
                            <label class="control-label ">Package Facilities</label>
                            <div class="">
                                <textarea class="ckeditor editor form-control" name="package_faclities" id="editior2" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Select User Type <span class="required">* </span></label>
                            <div class="">
                                <select name="user_type" id="edit_user_type" class="form-control select2me" data-placeholder="Select...">
                                    <!-- <option value="">Select User Type</option> -->
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Package Price<span class="required">*</span></label>
                            <input class="form-control" type="text" name="package_price_in" id="edit_package_price_in"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Pacakge Currency <span class="required">* </span></label>
                            <div class="">
                                <select name="package_currency" id="edit_package_currency" class="form-control select2me" data-placeholder="Select...">
                                    <!-- <option value="">Pacakge Currency</option> -->
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Package Validity <span class="required">*</span></label>
                            <input class="form-control" type="text" name="package_validity" id="edit_package_validity"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Total Credits </label>
                            <input class="form-control" type="text" name="total_credits" id="edit_total_credits"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Credits Per User Activity </label>
                            <input class="form-control" type="text" name="credit_per_user_activity" id="edit_credit_per_user_activity"/>
                        </div>                
                    </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Package</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <h3 class="page-title">
        User Dashboard CMS
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">User Dashboard Package</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-grid"></i>User Dashboard Package
                    </div>
                    <div class="actions">
                        <a onclick="get_user_type()"  class="btn btn-sm btn-default">
                            <i class="fa fa-plus"></i> Add Package </a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Package ID</th>
                            <th style="width: 15%">Package Name</th>                            
                            <th style="width: 15%">User Type</th>
                            <th style="width: 15%">Package Price</th>                                                        
                            <th style="width: 15%">Validity</th>
                            <th style="width: 15%">Total Credits</th>
                            <th style="width: 15%">Credit Per User Activity</th>
                            <th style="width: 15%">Active</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
    
    <h3 class="page-title">
        Manage Purchased Packages
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Manage Purchased Packages</a>
            </li>
        </ul>
    </div>
     <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="invoice">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <center><h4 class="modal-title "><strong>Credit Pacakge Invoice</strong></h4></center>
                </div>
                <div class="portlet-body" id="invoice_div">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr><th>Package Order ID</th><td id="package_order_id"></td></tr>
                        <tr><th style="width:40%">User Type</th><td id="user_type"></td></tr>
                        <tr><th style="width: 15%">User Name</th><td id="user_name"></td></tr>

                        <tr><th style="width: 15%">Package Name</th><td id="package_name"></td></tr>
                        <tr><th style="width: 15%">Validity</th><td id="package_validity"></td></tr>
                        <tr><th style="width: 15%">Amount</th><td id="amount"></td></tr>
                        <tr><th style="width: 15%">Purchased At</th><td id="purchased_at"></td></tr>
                        <tr><th style="width: 15%">Expired At</th><td id="expired_at"></td></tr>
                            
                            
                        </tr>
                        </thead>
                        <!-- <tr><td><a href="" onclick="print_invoice()" id="btn" class="btn btn-primary pull-right"><i class="fa fa-print"></i>Print Invoice</a><td></tr> -->
                        <tbody>

                        </tbody>
                    </table>
                    <a href="" onclick="print_invoice()" id="btn" class="btn btn-primary pull-right"><i class="fa fa-print"></i>Print Invoice</a>
                    <br>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-grid"></i>Manage Purchased Packages
                    </div>                    
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Package Order ID</th>
                            <th style="width: 15%">User Type</th>
                            <th style="width: 15%">User Name</th>

                            <th style="width: 15%">Package Name</th>                   <th style="width: 15%">Validity</th>
                            <th style="width: 15%">Purchased At</th>
                            <th style="width: 15%">Expired At</th>
                            <th style="width: 15%">Amount</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
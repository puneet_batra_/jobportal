<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php')?>
    <div class="portlet box blue-hoki">
        <div class="portlet-title" style="display: block;">
            <div class="caption">
                <i class="fa fa-globe"></i>Datatable with TableTools
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">

                            <div id="sample_4_wrapper" class="dataTables_wrapper no-footer"></div>
                                  <div class="table-scrollable">
                                      <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_4" role="grid" aria-describedby="sample_4_info">
                        <thead>
                        <tr role="row">
                            <th> # </th>
                            <th> User Type </th>
                            <th> Username  </th>
                            <th> First Name</th>
                            <th> Middle Name </th>
                            <th> Last Name </th>
                            <th> Phone </th>
                            <th> Dob </th>
                            <th> Email </th>
                            <th> Address </th>
                            <th> City </th>
                            <th> Country </th>
                            <th> Nationality </th>
                            <th> Age </th>
                            <th> Height </th>
                            <th> Weight </th>
                            <th> Relegion </th>
                            <th> Martial Status </th>
                            <th> Passport No. </th>
                            <th> Mobile No. </th>
                            <th> Telephone No. </th>
                            <th> Skype Id </th>
                            <th> Job Seeker Status </th>
                            <th> Languages </th>
                            <th> Job Position </th>
                            <th> Job Industry </th>
                            <th> Project Type </th>
                            <th> Hobbies Name </th>
                            <th> Edu Level </th>
                            <th> Edu Institute Name </th>
                            <th> Edu City </th>
                            <th> Edu Country </th>
                            <th> Edu Year </th>
                            <th> Edu Degree </th>
                            <th> Edu Grade </th>
                            <th> Exp Company Name </th>
                            <th> Exp City </th>
                            <th> Exp Country </th>
                            <th> Exp Years </th>
                            <th> Exp Job Position </th>
                            <th> Exp Job Description </th>
                            <th> Company Reference </th>
                            <th> Soft Skills </th>
                            <th> Hard Skills </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($report_data as $data){ ?>
                            <tr role="row">
                                <td><?php echo $data->user_id; ?></td>
                                <td><?php echo $data->user_type_name; ?></td>
                                <td><?php echo $data->username; ?></td>
                                <td><?php echo $data->first_name; ?></td>
                                <td><?php echo $data->middle_name; ?></td>
                                <td><?php echo $data->last_name; ?></td>
                                <td><?php echo $data->phone; ?></td>
                                <td><?php echo $data->dob; ?></td>
                                <td><?php echo $data->email; ?></td>
                                <td><?php echo $data->address; ?></td>
                                <td><?php echo $data->city; ?></td>
                                <td><?php echo $data->country; ?></td>
                                <td><?php echo $data->nationality; ?></td>
                                <td><?php echo $data->age; ?></td>
                                <td><?php echo $data->height; ?></td>
                                <td><?php echo $data->weight; ?></td>
                                <td><?php echo $data->relegion; ?></td>
                                <td><?php echo $data->martial_status; ?></td>
                                <td><?php echo $data->passport_num; ?></td>
                                <td><?php echo $data->mobile_num; ?></td>
                                <td><?php echo $data->telephone_num; ?></td>
                                <td><?php echo $data->skype_id; ?></td>
                                <td><?php echo $data->job_seeker_status; ?></td>
                                <td><?php echo $data->languages; ?></td>
                                <td><?php echo $data->job_position; ?></td>
                                <td><?php echo $data->job_industry; ?></td>
                                <td><?php echo $data->project_type; ?></td>
                                <td><?php echo $data->hobbies_name; ?></td>
                                <td><?php echo $data->edu_level; ?></td>
                                <td><?php echo $data->edu_institute_name; ?></td>
                                <td><?php echo $data->edu_city; ?></td>
                                <td><?php echo $data->edu_country; ?></td>
                                <td><?php echo $data->edu_year; ?></td>
                                <td><?php echo $data->edu_degree; ?></td>
                                <td><?php echo $data->edu_grade; ?></td>
                                <td><?php echo $data->exp_company_name; ?></td>
                                <td><?php echo $data->exp_city; ?></td>
                                <td><?php echo $data->exp_country; ?></td>
                                <td><?php echo $data->exp_years; ?></td>
                                <td><?php echo $data->exp_job_position; ?></td>
                                <td><?php echo $data->exp_job_description; ?></td>
                                <td><?php echo $data->company_reference; ?></td>
                                <td><?php echo $data->soft_skills; ?></td>
                                <td><?php echo $data->hard_skills; ?></td>
                            </tr>
                        <?php } ?></tbody>
                    </table></div></div>
        </div>



<?php $this->load->view('layouts/admin/footer.php') ?>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js'); ?>"></script>
<script>
    var dataTableOptions = {

        // "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "tableTools": {
            "sSwfPath": "<?php echo public_url('plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf'); ?>",
            "aButtons": [{
                "sExtends": "print",
                "sButtonText": "Print",
                "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
                "sMessage": "Generated by DataTables"
            }]
        }
    };

    var mydatatable = $('#sample_4').dataTable(dataTableOptions);
    $('#sample_4')
        .on('preXhr.dt', function (e, settings, data) {
            uiLoader('#portlet-body', 'show');
        }).on( 'draw.dt', function () {
            uiLoader('#portlet-body', 'hide');
        });
</script>
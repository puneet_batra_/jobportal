<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Resource</h4>
            </div>
            <form id="form-add"  >
                <div class="modal-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        Your form validation is successful!
                    </div>
                    <div class="form-group">
                        <label class="control-label">Resource Category<span class="required">* </span></label>
                        <select class="form-control" name="resource_category_id" id="add_resource_category_id">
                            <option value="">Select Resource Category</option>
                        </select>                       
                    </div>
                    <div class="form-group">
                        <label class="control-label">Resource Name<span class="required">* </span></label>
                        <input type="text" class="form-control" name="resource_name" id="add_resource_name">
                                                  
                    </div>

                    <div class="form-group">
                        <label class="control-label ">Description <span class="required">* </span>
                        </label>
                        <div class="">
                            <textarea class="ckeditor form-control" name="resource_description" id="editor1" rows="10" cols="20" style="width: 100%"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Resource</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Resource</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <input class="form-control" type="hidden" name="resource_id" id="edit_resource_id" value=""/>
                        <div class="form-group">
                            <label class="control-label">Resource Category</label>
                            <select class="form-control" name="resource_category_id" id="edit_resource_category_id">
                                <option value="">Select Resource Category</option>
                            </select>                       
                        </div>
                        <div class="form-group">
                        <label class="control-label">Resource Name</label>
                        <input type="text" class="form-control" name="resource_name" id="edit_resource_name">                                                
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Resource Description <span class="required">* </span>
                            </label>
                            <div class="">
                                <textarea class="ckeditor editor form-control" name="resource_decription" id="editior2" rows="10" cols="20" style="width: 100%"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update resource</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <h3 class="page-title">
        Manage Resource
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Resource</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-newspaper-o"></i>Manage Resource
                    </div>
                    <div class="actions">
                        <a  onclick="get_category()" class="btn btn-sm btn-default ">
                            <i class="fa fa-plus"></i> Add Resource </a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">Resource ID</th>
                            <th style="width: 30%">Category Name</th>
                            <th style="width: 10%">Resource Name</th>
                            <th style="width: 30%">Created At</th>
                            <th style="width: 30%">Active</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
<?php $this->load->view('layouts/admin/header'); ?>
<?php $this->load->view('layouts/admin/sidebar'); ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Maintenance</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="">Company Name</label>
                            <input class="form-control" type="text" name="" id=""/>
                        </div>
                        <div class="form-group">
                            <label for="">Location</label>
                            <input class="form-control" type="text" name="" id=""/>
                        </div>
                        <div class="form-group">
                            <label for="">Active</label>
                            <select class="form-control" name="" id="">
                                <option value="">Yes</option>
                                <option value="">No</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue">Save changes</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <h3 class="page-title">
        Roles
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Role</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

<form id="role-form" method="post" class="form-horizontal">
    <!-- START ADD ROLE -->
    <div class="row">
        <div class="col-md-12 col-sm-12">

                <div class="form-body" id="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-2">Select Role</label>
                        <div class="col-md-4">
                            <select class="form-control select2me" name="role_id" id="select_role" data-placeholder="Select...">
                                <option value=""></option>
                                <?php foreach($roles as $role){ ?>
                                    <option value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                <?php } ?>
                            </select>

                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Role Name</label>
                        <div class="col-md-4">
                            <input type="text" id="text_role_name" class="form-control" name="role_name" placeholder="Enter Role Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Role Description</label>
                        <div class="col-md-4">
                            <textarea id="text_role_description" name="role_description" class=" form-control" rows="6"></textarea>
                        </div>
                    </div>

                </div>


        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        Manage Role Permissions
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-scrollable">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">
                                            #
                                        </th>
                                        <th style="width: 30%">
                                            Module Name
                                        </th>
                                        <th style="width: 30%">
                                            Description
                                        </th>
                                        <th style="width: 10%">
                                            No Access
                                        </th>
                                        <th style="width: 10%">
                                            View Only
                                        </th>
                                        <th style="width: 10%">
                                            Full Access
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;foreach($modules as $module) {?>
                                        <tr>
                                            <td>
                                                <?php echo $i++; ?>
                                            </td>
                                            <td>
                                                <?php echo $module->name ?>
                                            </td>
                                            <td>
                                                <?php echo $module->description; ?>
                                            </td>
                                            <td>
                                                <input type="radio" checked name="permission[<?php echo $module->module_id ?>]" value="0" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                            <td>
                                                <input type="radio" name="permission[<?php echo $module->module_id ?>]" value="1" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                            <td>
                                                <input type="radio" name="permission[<?php echo $module->module_id ?>]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">
                                <button type="submit" id="btn_update" class="btn green ladda-button" data-style="expand-right">Update Role</button>
                                <button type="submit" id="btn_add" class="btn green ladda-button" data-style="expand-right">Add Role</button>
                                <button type="submit" id="btn_delete" class="btn red ladda-button" data-style="expand-right">Delete Role</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>
</form>

<?php $this->load->view('layouts/admin/footer'); ?>
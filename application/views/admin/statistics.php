<?php $this->load->view('layouts/admin/header.php') ?>
<?php $this->load->view('layouts/admin/sidebar.php') ?>

    <h3 class="page-title">
        Site Statistics
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('admin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('admin/statistics') ?>">Statistics</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php echo $job_seeker ?>
                    </div>
                    <div class="desc">
                        Total Job Seeker
                    </div>
                </div>
                <a class="more" href="#">
                    Job Seeker  <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green-haze">
                <div class="visual">
                    <i class="fa fa-connectdevelop"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php echo $employer ?>
                    </div>
                    <div class="desc">
                        Total Employers
                    </div>
                </div>
                <a class="more" href="#">
                    Employers  <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <?php if ($this->auth->is_superadmin()) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
                <div class="dashboard-stat blue">
                    <div class="visual">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $recruiter; ?>
                        </div>
                        <div class="desc">
                            Total Recruiter
                        </div>
                    </div>
                    <a class="more" href="#">
                        Recruiter  <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        <?php } ?>

        <?php if ($this->auth->is_superadmin()) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-hdd-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $industry; ?>
                        </div>
                        <div class="desc">
                            Total Job Sectors
                        </div>
                    </div>
                    <a class="more" href="#">
                        Job Sectors  <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        <?php } ?>

        <?php if ($this->auth->is_superadmin()) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-hdd-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $project_type; ?>
                        </div>
                        <div class="desc">
                            Total Project Type
                        </div>
                    </div>
                    <a class="more" href="#">
                        Project Type  <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        <?php } ?>

        <?php if ($this->auth->is_superadmin()) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
                <div class="dashboard-stat purple-plum">
                    <div class="visual">
                        <i class="fa fa-hdd-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $position; ?>
                        </div>
                        <div class="desc">
                            Total Job Positions
                        </div>
                    </div>
                    <a class="more" href="#">
                        Positions  <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->auth->is_superadmin()) { ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="storage-block">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $total_jobs; ?>
                        </div>
                        <div class="desc">
                            Total Jobs
                        </div>
                    </div>
                                <a class="more" href="#">
                                    Jobs  <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                </div>
            </div>
        <?php } ?>
    </div>
<?php $this->load->view('layouts/admin/footer.php') ?>
<?php $this->load->view('layouts/admin/header'); ?>
<?php $this->load->view('layouts/admin/sidebar'); ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Sub Admin</h4>
                </div>
                <form  id="form-add">
                <div class="modal-body">

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">First Name <span class="required">* </span></label>
                            <input class="form-control" type="text" name="first_name" id=""/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input class="form-control" type="text" name="last_name" id=""/>
                        </div>
                    <div class="form-group">
                        <label class="control-label">Username <span class="required">* </span></label>
                        <input class="form-control" type="text" name="username" id=""/>
                    </div>
                        <div class="form-group">
                            <label class="control-label ">Email <span class="required">* </span>
                            </label>
                            <div class="">
                                <input name="email" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input class="form-control" type="password" name="password" id="add-password"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Repeat Password</label>
                            <input class="form-control" type="password" name="rpassword" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Select Role <span class="required">* </span></label>
                            <div class="">
                                <select name="role_id" class="form-control select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <?php foreach($roles as $role) {?>
                                        <option value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Active <span class="required">* </span></label>
                            <select class="form-control" name="active" id="active_select2">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <input class="form-control" type="text" name="address" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">City</label>
                            <input class="form-control" type="text" name="city" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Country </label>
                            <select name="country" id="select2_sample4" class="select2 form-control">
                                <option value=""></option>
                                <?php foreach($this->countries->getList() as $code => $country){ ?>
                                    <option value="<?php echo $code ?>"><?php echo $country ?></option>
                                <?php } ?>
                            </select>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Sub Admin</h4>
                </div>
                <form  id="form-edit">
                    <div class="modal-body">
                        <input name="uid" type="hidden" value=""/>
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">First Name <span class="required">* </span></label>
                            <input class="form-control" type="text" name="first_name" id=""/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input class="form-control" type="text" name="last_name" id=""/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Username <span class="required">* </span></label>
                            <input class="form-control" type="text" name="username" id=""/>
                        </div>
                        <div class="form-group">
                            <label class="control-label ">Email <span class="required">* </span>
                            </label>
                            <div class="">
                                <input name="email" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input class="form-control" type="password" name="password" id="edit-password"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Repeat Password</label>
                            <input class="form-control" type="password" name="rpassword" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Select Role <span class="required">* </span></label>
                            <div class="">
                                <select name="role_id" class="form-control select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <?php foreach($roles as $role) {?>
                                        <option value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Active <span class="required">* </span></label>
                            <select class="form-control" name="active" id="edit_active_select2">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <input class="form-control" type="text" name="address" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">City</label>
                            <input class="form-control" type="text" name="city" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Country </label>
                            <select name="country" id="select2_sample5" class="select2 form-control">
                                <option value=""></option>
                                <?php foreach($this->countries->getList() as $code => $country){ ?>
                                    <option value="<?php echo $code ?>"><?php echo $country ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <h3 class="page-title">
        Sub Admin <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="index.php">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Users</a>
            </li>
        </ul>

    </div>
    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red-intense theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>Sub Admin
                    </div>

                    <div class="actions">
                        <a data-toggle="modal" data-target="#createModal" href="javascript:;" class="btn btn-sm btn-default">
                            <i class="fa fa-plus"></i> Add Sub Admin </a>
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 10%">User ID</th>
                            <th style="width: 30%">Username</th>
                            <th style="width: 30%">Email</th>
                            <th style="width: 10%">Active</th>
                            <th style="width: 10%">Verified</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE HEADER-->
<?php $this->load->view('layouts/admin/footer'); ?>
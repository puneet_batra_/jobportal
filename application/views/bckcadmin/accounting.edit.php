<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>

    <h3 class="page-title">
        Accounting
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Accounting</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers"></i>
                        Edit entry
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <form method="post" class="form-horizontal" id="add-entry-form" action="<?php echo site_url('bckcadmin/accounting/update/' . $asset_meta->asset_meta_id) ?>">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Select Asset:</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="asset_id" value="<?php echo $asset->asset_id; ?>" id="select_asset" class="form-control select2">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Purchase Date:</label>
                                <div class="col-md-9">
                                    <input data-date-format="dd M, yyyy"
                                           name="purchase_date"
                                           type="text" class="form-control date-picker input-inline input-medium"
                                           value="<?php echo date('d M, Y', strtotime($asset_meta->purchase_date)) ?>"
                                           placeholder="Enter date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Purchase Value:</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="purchase_value" value="<?php echo $asset_meta->purchase_value ?>" id=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Effective Life:</label>
                                <div class="col-md-9">
                                    <input placeholder="Number of Years" class="form-control" type="text" value="<?php echo $asset_meta->effective_life ?>" name="effective_life" id=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Depreciation Method:</label>
                                <div class="col-md-9">
                                    <select class="form-control select2-styled" name="depreciation_method" id="">
                                        <option value="t1" <?php echo ($asset_meta->depreciation_method == 't1')?'selected':'' ?>>Type 1</option>
                                        <option value="t2" <?php echo ($asset_meta->depreciation_method == 't2')?'selected':'' ?>>Type 2</option>
                                        <option value="t3" <?php echo ($asset_meta->depreciation_method == 't3')?'selected':'' ?>>Type 3</option>
                                        <option value="t4" <?php echo ($asset_meta->depreciation_method == 't4')?'selected':'' ?>>Type 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input class="btn btn-success" type="submit" value="Save and return"/>
                                <a class="btn btn-default" href="<?php echo site_url('bckcadmin/accounting') ?>">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>

    <!-- Asset type add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new asset type</h4>
                </div>
                <form class="form-horizontal"
                      id="add-asset-type-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/asset_types/add') ?>" method="post" role="form">
                    <div class="modal-body" id="form-add-body">
                        <!-- start form-->

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" placeholder="Enter name">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-3 control-label">Managers</div>
                                <div class="col-md-9">
                                    <input type="hidden" id="taggable" name="managers" class="form-control select2" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Fuel</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_fuel" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_fuel" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Maintenance</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_maintenance" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_maintenance" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Exception</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_exception" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_exception" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Location</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_location" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_location" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Asset Type</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Asset Type Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Asset type</h4>
                </div>
                <form class="form-horizontal"
                      id="edit-asset-type-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/asset_types/update') ?>" method="post" role="form">
                    <div class="modal-body" id="form-edit-body">
                        <!-- start form-->
                        <input name="asset_type_id" type="hidden" value=""/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" placeholder="Enter name">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3 control-label">Managers</div>
                                <div class="col-md-9">
                                    <input type="hidden" id="edit-taggable" name="managers" class="form-control select2" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Fuel</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_fuel" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_fuel" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Maintenance</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_maintenance" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_maintenance" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Exception</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_exception" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_exception" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Location</label>

                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="feature_location" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Enabled
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="feature_location" checked="checked" value="0" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Disabled
                                        </label>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update Asset Type</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <h3 class="page-title">
        Accounting
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Accounting</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers"></i>
                        Asset Information
                    </div>
                    <div class="actions">
                        <a class="btn btn-default" href="<?php echo site_url('bckcadmin/accounting/add') ?>"><i class="fa fa-plus"></i> Add</a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="asset_types_table">
                        <thead>
                        <tr>
                            <th width="18%">Asset Name</th>
                            <th width="10%">Purchase Date</th>
                            <th width="10%">Purchase Value</th>
                            <th width="10%">Effective Life</th>
                            <th width="10%">Depreciation Method</th>
                            <th width="15%">Created</th>
                            <th width="20%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>

    <!-- Asset type add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new admin</h4>
                </div>
                <form class="form-horizontal"
                      id="add-admin-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/admins/add') ?>" method="post" role="form">
                    <div class="modal-body" id="form-add-body">
                        <!-- start form-->

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">User</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="select2_add_userid" name="user_id" placeholder="Select user">
                                </div>
                            </div>
                        </div>

                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Admin</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <h3 class="page-title">
        Admins
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Admins</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list"></i>
                        Admins
                    </div>
                    <div class="actions">
                        <a class="btn btn-default" href="#"
                           data-toggle="modal"
                           data-target="#addModal"><i class="fa fa-plus"></i> Add</a>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="admins_table">
                        <thead>
                        <tr>
                            <th width="24%">Name</th>
                            <th width="40%">Email</th>
                            <th width="20%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
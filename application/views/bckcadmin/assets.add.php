<?php $this->load->view('layouts/bckcadmin/header'); ?>
<?php $this->load->view('layouts/bckcadmin/sidebar'); ?>
    <h3 class="page-title">
        Assets
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Add Asset</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-layers"></i>Add Asset
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <div class="row">
                        <!-- Form Content Goes starts here -->
                        <div class="rows">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form" id="add-asset-form"
                                      action="<?php echo site_url('bckcadmin/assets/assetadd') ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Name
                                            </div>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="friendly_name" id=""/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Description
                                            </div>
                                            <div class="col-md-9">
    											<textarea class="wysihtml5 form-control"
                                                          name="description" id="" cols="30"
                                                              rows="4"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Barcode
                                            </div>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="nfc_uuid" id=""/>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Photo
                                            </div>
                                            <div class="col-md-9" >
                                                <input type="file" class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" name="files[]" multiple>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Type
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" name="asset_type_id" id="asset_type_id">
                                                    <option value="">Select...</option>
                                                    <?php foreach($asset_types as $asset_type){ ?>
                                                    <option value="<?php echo $asset_type->asset_type_id ?>"><?php echo $asset_type->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Features
                                            </div>
                                            <div class="col-md-9">
                                                <select multiple="multiple" class="multi-select"
                                                        id="my_multi_select1" name="features[]">
                                                    <option value="feature_maintenance">Maintenance</option>
                                                    <option value="feature_exception">Exception</option>
                                                    <option value="feature_location">Location</option>
                                                    <option value="feature_fuel">Fuel</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Enabled
                                            </div>
                                            <div class="col-md-9">
                                                <select class="form-control" name="enabled" id="enabled_select">
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green ladda-button" data-style="expand-right">Add Asset</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>


                        </div>


                        <!-- Form content ends here -->
                    </div>
                </div>
            </div>
        </div>


    </div>

    <!-- END ADD ASSET -->
<?php $this->load->view('layouts/bckcadmin/footer'); ?>
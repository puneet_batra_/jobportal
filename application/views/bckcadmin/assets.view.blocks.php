<div class="row">
    <div class="col-md-12 col-sm-12 col-xl-4" id="portlet-maintenance">
        <div class="portlet-content"></div>
    </div>

    <div class="col-md-6 col-sm-12 col-xl-4 " id="portlet-location">
        <div class="portlet-content"></div>
    </div>

    <div class="col-md-6 col-sm-12 col-xl-4 " id="portlet-fuel">
        <div class="portlet-content"></div>
    </div>

    <div class="col-md-12 col-sm-12" id="portlet-exception">
        <div class="portlet-content"></div>
    </div>
</div>
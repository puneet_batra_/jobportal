<div class="modal fade" id="exceptionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add new Exception</h4>
            </div>
            <form class="form-horizontal" action="<?php echo site_url('bckcadmin/exceptions/add') ?>"
                  id="add-exception-form"
                  enctype="multipart/form-data"
                  data-item="<?php echo $asset->asset_id; ?>"
                  method="post">
                <div class="modal-body" id="form-exception-add-body">

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="">Exception Title:</label>

                        <div class="col-md-9">
                            <input class="form-control" type="text" name="title" id=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="">Comments:</label>

                        <div class="col-md-9">
                                <textarea name="comments" id="" cols="30" rows="4"
                                          class="form-control wysihtml5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="">Attachment:</label>

                        <div class="col-md-9">
                            <input class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" type="file" name="files" id=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Subscribe:</label>

                        <div class="col-md-9">
                            <input type="checkbox" name="subscribe" id=""
                                   class="make-switch"
                                   data-on-text="&nbsp;Yes&nbsp;" data-off-text="&nbsp;No&nbsp;"
                                   value="1"
                                   checked="checked"
                                   data-size="medium"/>

                            <div class="help-block">
                                Subscribe for email alerts
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Exception</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
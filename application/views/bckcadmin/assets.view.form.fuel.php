<div class="modal fade" id="fuelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add new Fuel entry</h4>
            </div>
            <form class="form-horizontal"
                  data-item="<?php echo $asset->asset_id ?>"
                  id="add-fuel-form"
                  enctype="multipart/form-data"
                  action="<?php echo site_url('bckcadmin/fuel/add') ?>" method="post" role="form">
                <div class="modal-body" id="form-fuel-add-body">
                    <!-- start form-->

                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Odo/Hour</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="odometer" placeholder="Enter reading">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Fueled Date</label>

                            <div class="col-md-9">
                                <input data-date-format="yyyy-mm-dd"
                                       name="fueled_date"
                                       type="text" class="form-control date-picker input-inline input-medium"
                                       value="<?php echo date('Y-m-d') ?>"
                                       placeholder="Enter text">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Towing</label>

                            <div class="col-md-9">
                                <div style="padding-top: 5px;">
                                    <label>
                                        <input type="radio" name="towing" value="1" class="icheck"
                                               data-radio="iradio_flat-grey">
                                        Yes
                                    </label>
                                    &nbsp;
                                    &nbsp;
                                    <label>
                                        <input type="radio" name="towing" checked="checked" value="0" class="icheck"
                                               data-radio="iradio_flat-grey">
                                        No
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Litres</label>

                            <div class="col-md-9">
                                <input class="form-control" type="text" name="volume" placeholder="Volume of fuel"
                                       id=""/>
                            </div>
                        </div>
                        <div class="form-group" id="cost_div">
                            <label class="col-md-3 control-label">Cost</label>

                            <div class="col-md-9">
                                <input class="form-control" type="text" name="cost" id="" placeholder="Cost"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Supplier</label>

                            <div class="col-md-9">
                                <select class="form-control" name="supplier" id="supplier">
                                    <?php foreach ($suppliers as $supplier) { ?>
                                        <option
                                            value="<?php echo $supplier->supplier_id ?>"><?php echo $supplier->supplier_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Invoice</label>

                            <div class="col-md-9">
                                <input class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" type="file" name="files" id="fileupload2"/>
                                <br/>

                                <div id="dvPreview2"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Create Exception</label>

                            <div class="col-md-9">
                                <div style="padding-top: 5px;">
                                    <label>
                                        <input type="radio" name="has_exception" value="1" class="icheck"
                                               data-radio="iradio_flat-grey">
                                        Yes
                                    </label>
                                    &nbsp;
                                    &nbsp;
                                    <label>
                                        <input type="radio" name="has_exception" checked="checked" value="0"
                                               class="icheck" data-radio="iradio_flat-grey">
                                        No
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group has_exception">
                            <label class="col-md-3 control-label">Title</label>

                            <div class="col-md-9">
                                <input class="form-control" type="text" name="exception_title" id=""
                                       placeholder="Exception Title..."/>
                            </div>
                        </div>

                    </div>


                    <!-- end form -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Fuel</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
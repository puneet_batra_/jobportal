<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add new Location</h4>
            </div>
            <form class="form-horizontal" action="<?php echo site_url('bckcadmin/locations/add') ?>"
                  method="post"
                  id="add-location-form" data-item="<?php echo $asset->asset_id ?>">
                <div class="modal-body" id="form-location-add-body">
                    <div class="form-group">
                        <label class="col-md-3" for="">Type:</label>

                        <div class="col-md-9">
                            <label>
                                <input type="radio" name="type" checked="checked" value="geolocation" class="icheck"
                                       data-radio="iradio_flat-grey">
                                GeoLocation
                            </label>
                            &nbsp;
                            &nbsp;
                            <label>
                                <input type="radio" name="type" value="manual" class="icheck"
                                       data-radio="iradio_flat-grey">
                                Manual
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="latlng" id="lnglat"/>

                    <div class="form-group when-geolocation">
                        <label class="col-md-3" for="">GeoLocation:</label>

                        <div class="col-md-9">
                            <div class="input-group">
                                <input class="form-control" readonly type="text" name="geolocation"
                                       id="location_autocomplete"/>
                                <a class="btn input-group-addon blue" id="crosshairs_btn">
                                    <i style="color:#fff;" class="fa fa-crosshairs"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="form-group when-manual">
                        <label class="col-md-3" for="">Status:</label>

                        <div class="col-md-9">
                            <label>
                                <input type="radio" name="status" checked="checked" value="taken" class="icheck"
                                       data-radio="iradio_flat-grey">
                                Taken
                            </label>
                            &nbsp;
                            &nbsp;
                            <label>
                                <input type="radio" name="status" value="left" class="icheck"
                                       data-radio="iradio_flat-grey">
                                Left
                            </label>
                        </div>
                    </div>
                    <div class="form-group when-manual-left">
                        <label class="col-md-3" for="">Text Location:</label>

                        <div class="col-md-9">
                            <input class="form-control" type="text" name="manual" id="manual-location"
                                   placeholder="Manual Location"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Save changes</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
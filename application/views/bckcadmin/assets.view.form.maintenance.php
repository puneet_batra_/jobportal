<div class="modal fade" id="maintenanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post"
                  data-item="<?php echo $asset->asset_id; ?>"
                  enctype="multipart/form-data"
                  action="<?php echo site_url('bckcadmin/maintenance/add') ?>"
                  role="form" id="add-maintenance-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new Maintenance</h4>
                </div>
                <div class="modal-body" id="form-maintenance-add-body">

                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Comments</label>

                            <div class="col-md-9">
                                <textarea name="comments" class="wysihtml5 form-control" rows="6"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Notes for next service</label>

                            <div class="col-md-9">
                                <textarea name="notes_next_service" class="wysihtml5 form-control" rows="6"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Attachment</label>

                            <div class="col-md-9">
                                <input type="file" multiple="multiple" name="files[]" id="exampleInputFile1"
                                       class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox">
                                            <span class="help-block">
                                                Attachment file.
                                            </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Type</label>

                            <div class="col-md-9">
                                <select class="form-control" name="maintenance_type" id="due_fuel_entry">
                                    <?php foreach ($this->maintenance_model->get_types() as $type_id => $type) { ?>
                                        <option value="<?php echo $type_id ?>"><?php echo $type; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group conditional_div div-0">
                            <label class="control-label col-md-3">Warranty (UTC)</label>

                            <div class="col-md-3">
                                <input class="form-control" type="text" name="warranty_date" id="anytime-picker0"/>
                            </div>
                        </div>

                        <div class="form-group conditional_div div-1">
                            <label class="control-label col-md-3">Due Date (UTC)</label>

                            <div class="col-md-3">
                                <input class="form-control" type="text" name="due_date" id="anytime-picker1"/>
                            </div>
                        </div>

                        <div class="form-group conditional_div div-2">
                            <label class="col-md-3 control-label">Enter Odo/Hour</label>

                            <div class="col-md-9">
                                <input class="form-control" type="text"
                                       placeholder="Enter odo/hour to which entry is due on."
                                       name="odo_hour" id="odo_hour"/>
                            </div>
                        </div>

                        <div class="form-group conditional_div div-3">
                            <label class="control-label col-md-3">Testing (UTC)</label>

                            <div class="col-md-3">
                                <input class="form-control" type="text" name="testing_date" id="anytime-picker3"/>
                            </div>
                        </div>

                        <div class="form-group conditional_div div-4">
                            <label class="control-label col-md-3">End of Life (UTC)</label>

                            <div class="col-md-3">
                                <input class="form-control" type="text" name="end_of_life_date"
                                       id="anytime-picker4"/>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Maintenance</button>
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
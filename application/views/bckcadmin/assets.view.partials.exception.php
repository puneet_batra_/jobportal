<div class="portlet box red-sunglo theme-portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Exceptions
        </div>

        <div class="tools">
            <a title="" data-original-title="" href="" class="collapse"></a>
        </div>
        <div class="actions">
            <?php if (is_allowed(1, 'exceptions')) { ?>
                <a class="btn btn-default"
                   href="<?php echo site_url('bckcadmin/exceptions') ?>?asset_id=<?php echo $asset->asset_id ?>"><i
                        class="icon-action-redo"></i> Details</a>
            <?php } ?>

            <a class="btn btn-default" id="btn-subscribe"><span class="icon-feed"></span>&nbsp;
                Subscribe</a>
            <a class="btn btn-default" id="btn-unsubscribe"><span class="icon-feed"></span>&nbsp;
                Unsubscribe</a>
            &nbsp;
        </div>
    </div>
    <div class="portlet-body" id="form-exception-update-body">
        <div class="row">
            <div class="col-md-6" id="exception-list">
                <table class="table table-responsive table-striped"
                       data-item="<?php echo $asset->asset_id ?>"
                       id="exception_table">
                    <thead>
                    <tr>
                        <th>State</th>
                        <th>Title</th>
                        <th>Date Time</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="col-md-6" id="exception-detail">
                <form action="<?php echo site_url('bckcadmin/exceptions/update') ?>"
                      data-item="0" role="form" method="post" id="update-exception-form">
                    <?php if (is_allowed(2, 'exceptions')) { ?>
                        <div class="form-body" id="add_asset" class="table table-bordered">
                            <div class="row">
                                <div class="col-xs-4 col-md-3">
                                    <div class="form-group ">
                                        <select class="form-control" name="status"
                                                id="exception_status">
                                            <option value="new">New</option>
                                            <option value="in_progress">In Progress</option>
                                            <option value="closed">Closed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-6">
                                    <div class="form-group ">
                                        <input class="form-control" type="text" name="title"
                                               id="exception_title"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="col-xs-3 col-md-3 no-left-padding text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-primary"
                                                style="padding-left:12px; padding-right:12px"
                                                id="exception_submit" type="submit"
                                            ><i class="fa fa-check"></i></button>
                                        <?php if ($this->auth->is_companyadmin()) { ?>
                                            <a class="btn btn-danger" onclick="remove_exception()"><i
                                                    class="fa fa-trash"></i></a>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </form>
                <br/>

                <div class="scroller" id="chats-main-container" style="height: 352px;"
                     data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats" id="chats-container">


                    </ul>
                </div>
                <?php if (is_allowed(2, 'exceptions') || is_allowed(2, 'assets', '', 'exception')) { ?>
                    <div class="chat-form">
                        <form action="<?php echo site_url('bckcadmin/exceptions/do_comment') ?>"
                              id="add-exception-comment-form"
                              method="post"
                              enctype="multipart/form-data">
                            <div class="input-cont">
                                <input class="form-control" type="text" name="comments"
                                       placeholder="Type a message here..."/>
                            </div>
                            <div class="btn-cont" id="send-message-exception">
                                        <span class="arrow">
                                        </span>
                                <a class="btn blue icn-only">
                                    <i class="fa fa-check icon-white"></i>
                                </a>
                            </div>
                            <div class="btn-cont-attachment">
                                <a class="btn btn-block green icn-only" id="input-file-unselected">
                                    <i class="fa fa-paperclip icon-white"></i>
                                </a>
                                <a class="btn btn-block red icn-only" style="margin-top: 0"
                                   id="input-file-selected">
                                    <i class="fa fa-trash icon-white"></i>
                                </a>
                            </div>
                            <input type="file" style="width: 0;height: 0;overflow: hidden" name="files"
                                   id="input-attachment-exception-comments"/>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url('inline/asset/bckcadmin/assets.view.exceptions.js') ?>"></script>

<div class="portlet box red-sunglo theme-portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Fuel Entry
        </div>
        <div class="tools">
            <a title="" data-original-title="" href="" class="collapse"></a>
        </div>
        <div class="actions">
            <?php if (is_allowed(1, 'fuel')) { ?>
                <a class="btn btn-default"
                   href="<?php echo site_url('bckcadmin/fuel') ?>?asset_id=<?php echo $asset->asset_id ?>"><i
                        class="icon-action-redo"></i> Details</a>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body match-height">
        <div id="last_fueled_by"></div>

        <div id="chart_3" data-item="<?php echo $asset->asset_id; ?>" class="chart"
             style="height: 371px;">
        </div>

    </div>
</div>
<script src="<?php echo site_url('inline/asset/bckcadmin/assets.view.fuel.js') ?>"></script>

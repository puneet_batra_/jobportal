<div class="portlet box red-sunglo theme-portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Locations
        </div>

        <div class="tools">
            <a title="" data-original-title="" href="" class="collapse"></a>
        </div>
        <div class="actions">
            <?php if (is_allowed(1, 'locations')) { ?>
                <a class="btn btn-default"
                   href="<?php echo site_url('bckcadmin/locations') ?>?asset_id=<?php echo $asset->asset_id ?>"><i
                        class="icon-action-redo"></i> Details</a>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body match-height">
        <p id="location-info-text">

        </p>

        <div class="map-wrapper">
            <div id="map-canvas" data-item="<?php echo $asset->asset_id ?>" style="height: 456px"></div>
            <div class="overlay"></div>
            <div class="locker">
                <i class="icon-lock locked"></i>
                <i class="icon-lock-open unlocked"></i>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo site_url('inline/asset/bckcadmin/assets.view.location.js') ?>"></script>

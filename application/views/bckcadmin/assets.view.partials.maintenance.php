<div class="portlet box red-sunglo theme-portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Maintenance
        </div>

        <div class="tools">
            <a title="" data-original-title="" href="" class="collapse"></a>
        </div>
        <div class="actions">
            <?php if (is_allowed(1, 'maintenance')) { ?>
                <a class="btn btn-default"
                   href="<?php echo site_url('bckcadmin/maintenance') ?>?asset_id=<?php echo $asset->asset_id ?>"><i
                        class="icon-action-redo"></i> Details</a>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body" id="portlet-maintenance-body">
        <div id="maintenance-container" data-item="<?php echo $asset->asset_id; ?>">

        </div>
    </div>
</div>
<script src="<?php echo site_url('inline/asset/bckcadmin/assets.view.maintenance.js') ?>"></script>

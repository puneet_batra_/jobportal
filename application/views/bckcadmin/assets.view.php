<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

    <!-- Fuel Modal -->
<?php $this->load->view('bckcadmin/assets.view.form.fuel.php') ?>
    <!-- Maintenance Modal -->
<?php $this->load->view('bckcadmin/assets.view.form.maintenance.php') ?>
    <!-- Exception Modal -->
<?php $this->load->view('bckcadmin/assets.view.form.exception.php') ?>
    <!-- Location Modal -->
<?php $this->load->view('bckcadmin/assets.view.form.location.php') ?>

    <h3 class="page-title">
        Assets
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Asset Overview</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12" id="overview_portlet" data-item="<?php echo $asset->asset_id; ?>">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-calendar"></i>Asset Overview
                    </div>
                    <div class="actions favourite-actions" id="actions-item-<?php echo $asset->asset_id; ?>">
                        <?php if (is_allowed(2, 'assets')) { ?>
                            <a class="btn btn-default" href="#" id="enable-edit">
                                <i class="fa fa-edit"></i> <span class="edit">Edit</span> <span class="view">View</span>
                            </a>
                        <?php } ?>
                        <a class="btn btn-default" style="display: none;" href="#" id="make-unfavourite">
                            <i class="fa fa-star"></i> Favourite
                        </a>
                        <a class="btn btn-default" href="#" style="display: none;" id="make-favourite">
                            <i class="fa fa-star"></i> Make Favourite
                        </a>
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-overview-body">
                    <div class="row">
                        <!-- Form Content Goes starts here -->
                        <div class="rows">
                            <div class="col-md-12">


                                <div class="form-body" id="add_asset" class="table table-bordered">
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Name
                                            </div>
                                            <div class="col-md-9">
                                                <?php if ($has_edit_permissions) { ?>
                                                    <a href="#" class="editable" data-name="friendly_name"
                                                       data-disabled="true"
                                                       data-type="text"
                                                       data-pk="<?php echo $asset->asset_id ?>"
                                                       data-original-title="Enter Friendly Name">
                                                        <?php echo $asset->friendly_name ?> </a>
                                                <?php } else { ?>
                                                    <div class="text">
                                                        <?php echo $asset->friendly_name; ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Description
                                                <?php if ($has_edit_permissions) { ?>
                                                    <a href="#" id="pencil">
                                                        <i class="fa fa-pencil"></i> [edit] </a>
                                                <?php } ?>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php if ($has_edit_permissions) { ?>
                                                            <div class="editable" id="note"
                                                                 data-disabled="true"
                                                                 data-pk="<?php echo $asset->asset_id ?>"
                                                                 data-name="description"
                                                                 data-type="wysihtml5" data-toggle="manual"
                                                                 data-class="form-s"
                                                                 data-original-title="Enter description">
                                                                <?php echo $asset->description; ?>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="text">
                                                                <?php echo $asset->description; ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Barcode
                                            </div>
                                            <div class="col-md-9">
                                                <?php if ($has_edit_permissions) { ?>
                                                    <a href="#" class="editable"
                                                       data-disabled="true"
                                                       data-type="text"
                                                       data-pk="<?php echo $asset->asset_id ?>"
                                                       data-name="nfc_uuid"
                                                       data-original-title="Enter Barcode">
                                                        <?php echo $asset->nfc_uuid ?></a>
                                                <?php } else { ?>
                                                    <div class="text">
                                                        <?php echo $asset->nfc_uuid; ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if ($has_edit_permissions) { ?>
                                            <div class="form-group">
                                                <div class="col-md-3 control-label">
                                                    LoRa ID
                                                </div>
                                                <div class="col-md-9">

                                                    <a href="#" class="editable"
                                                       data-disabled="true"
                                                       data-type="text"
                                                       data-pk="<?php echo $asset->asset_id ?>"
                                                       data-name="device_udid"
                                                       data-original-title="Enter LoRa ID">
                                                        <?php echo $asset->device_udid ?></a>

                                                    <?php if (false) { ?>
                                                        <a class="btn btn-sm btn-success" id="channels-btn"
                                                           href="<?php echo site_url('bckcadmin/channels/asset/' . $asset->asset_id) ?>"
                                                           data-item="<?php echo $asset->asset_id ?>"
                                                           style="margin-left: 20px;">
                                                            <i class="icon-info"></i>
                                                            Channels
                                                        </a>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Photos
                                            </div>
                                            <?php if ($has_edit_permissions) { ?>
                                                <div class="col-md-9" id="asset-photos-<?php echo $asset->asset_id ?>">
                                                    <div id="asset-photos-container">

                                                    </div>
                                                    <div class="edit-control">
                                                        <form
                                                            action="<?php echo site_url('bckcadmin/assets/addphotos/' . $asset->asset_id) ?>"
                                                            method="post"
                                                            enctype="multipart/form-data"
                                                            class="dropzone" id="photos-zone">
                                                            <div class="fallback">
                                                                <input accept="image/*" capture="camera" name="photos" type="file"  multiple/>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-9">
                                                    <?php foreach ($asset->photos as $photo) { ?>
                                                        <?php if (file_exists('uploads/' . $this->auth->company_id() . '/assets/photos/' . $photo->filename)) { ?>
                                                            <div style="display: inline-block;">
                                                                <a class="fancybox" data-rel="fancybox-button"
                                                                   data-fancybox-gallery="gallery"
                                                                   rel="group1"
                                                                   href="<?php echo site_url('image/uploads/' . $this->auth->company_id() . '/assets/photos/' . $photo->filename) ?>?s=1000x1000">
                                                                    <img
                                                                        src="<?php echo site_url('image/uploads/' . $this->auth->company_id() . '/assets/photos/' . $photo->filename) ?>?s=98x98"
                                                                        alt=""/>
                                                                    <br/>
                                                                    <a class="btn btn-primary btn-sm" target="_blank"
                                                                       href="<?php echo base_url('uploads/' . $this->auth->company_id() . '/assets/photos/' . $photo->filename) ?>"><i
                                                                            class="icon-cloud-download"></i>
                                                                        Download</a>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>

                                            <?php } ?>
                                        </div>
                                        <form
                                            action="<?php echo site_url('bckcadmin/assets/update/' . $asset->asset_id) ?>"
                                            id="update-asset-form"
                                            class="form-horizonta" role="form" method="post"
                                            enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="col-md-3 control-label">
                                                    Type
                                                </div>
                                                <div class="col-md-9">
                                                    <select onchange="$('#update-asset-form').submit();" class="form-control edit-control" name="asset_type_id"
                                                        <?php if (!$has_edit_permissions) {
                                                            echo 'disabled="disabled"';
                                                        } ?>
                                                            id="asset_type_id">
                                                        <?php foreach ($asset_types as $asset_type) { ?>
                                                            <option value="<?php echo $asset_type->asset_type_id ?>"
                                                                <?php echo ($asset_type->asset_type_id == $asset->asset_type_id) ? 'selected="selected"' : '' ?>>
                                                                <?php echo $asset_type->name ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>

                                                    <div class="view-control text" id="asset_type_id_text">
                                                        <?php foreach ($asset_types as $asset_type) { ?>
                                                            <?php echo ($asset_type->asset_type_id == $asset->asset_type_id) ? $asset_type->name : '' ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if (!is_allowed(2, 'assets')) {
                                                $inline = 'style="display:none;"';
                                            } else {
                                                $inline = '';
                                            } ?>
                                            <div class="form-group hide-control" <?php echo $inline ?>>
                                                <div class="col-md-3 control-label">
                                                    Features
                                                </div>
                                                <div class="col-md-9 features-multiselect-container"
                                                     id="asset-features-item-<?php echo $asset->asset_id; ?>">
                                                    <select onchange="$('#update-asset-form').submit();" multiple="multiple" class="form-control multi-select"
                                                        <?php if (!$has_edit_permissions) {
                                                            echo 'disabled="disabled"';
                                                        } ?>
                                                            id="my_multi_select1" name="features[]">
                                                        <option
                                                            value="feature_maintenance" <?php echo ($asset->feature_maintenance == 1) ? 'selected="selected"' : '' ?>>
                                                            Maintenance
                                                        </option>
                                                        <option
                                                            value="feature_exception" <?php echo ($asset->feature_exception == 1) ? 'selected="selected"' : '' ?>>
                                                            Exception
                                                        </option>
                                                        <option
                                                            value="feature_location" <?php echo ($asset->feature_location == 1) ? 'selected="selected"' : '' ?>>
                                                            Location
                                                        </option>
                                                        <option
                                                            value="feature_fuel" <?php echo ($asset->feature_fuel == 1) ? 'selected="selected"' : '' ?>>
                                                            Fuel
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group hide-control" <?php echo $inline ?>>
                                                <div class="col-md-3 control-label">
                                                    Enabled
                                                </div>
                                                <div class="col-md-9">
                                                    <select onchange="$('#update-asset-form').submit();" class="form-control" name="enabled"
                                                        <?php if (!$has_edit_permissions) {
                                                            echo 'disabled="disabled"';
                                                        } ?>
                                                            id="enabled_select">
                                                        <option
                                                            value="1" <?php echo ($asset->enabled == 1) ? 'selected="selected"' : '' ?>>
                                                            Yes
                                                        </option>
                                                        <option
                                                            value="0" <?php echo ($asset->enabled == 0) ? 'selected="selected"' : '' ?>>
                                                            No
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="submit" value="" style="width: 0;height: 0;overflow:hidden; border: 0;padding: 0;"/>
                                        </form>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Documents
                                            </div>
                                            <div class="col-md-9">

                                                <div class=""
                                                     id="documents-list-<?php echo $asset->asset_id ?>">
                                                    <div id="documents-list-container">

                                                    </div>
                                                </div>
                                                <?php if ($has_edit_permissions) { ?>
                                                    <div class="edit-control">
                                                        <form
                                                            action="<?php echo site_url('bckcadmin/assets/adddocuments/' . $asset->asset_id) ?>"
                                                            method="post"
                                                            enctype="multipart/form-data"
                                                            class="dropzone" id="documents-zone">
                                                            <div class="fallback">
                                                                <input name="documents" type="file" multiple/>
                                                            </div>
                                                        </form>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <?php if ($has_edit_permissions && false)  { // hide ?>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green ladda-button"
                                                        data-style="expand-right">Update Asset
                                                </button>
                                                <?php if (FALSE) { ?>
                                                    <a class="btn red delete-asset-btn" href="#"
                                                       id="asset-item-<?php echo $asset->asset_id ?>">Delete
                                                        Asset</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>


                            </div>


                        </div>


                        <!-- Form content ends here -->
                    </div>
                </div>
            </div>
        </div>


    </div>

    <!-- END ADD ASSET -->


    <!-- START FEATURES ROWS -->

<?php $this->load->view('bckcadmin/assets.view.blocks.php') ?>

    <div class="floating-menu" id="features_sidebar">
        <div class="floating-menu-container">
            <a class="button-action" href="">
                <i class="icon-plus floating-btn-pink"></i>
            </a>
            <ul class="menu-links">
                <li id="btn_feature_fuel">
                    <a href="" data-toggle="modal"
                       data-target="#fuelModal">
                        <div class="icon"><i class="icon-rocket floating-btn-blue"></i></div>
                        <div class="title"><span>Add Fuel</span></div>
                    </a>
                </li>
                <li id="btn_feature_maintenance">
                    <a href="" data-toggle="modal"
                       data-target="#maintenanceModal">
                        <div class="icon"><i class="icon-refresh floating-btn-blue"></i></div>
                        <div class="title"><span>Add Maintenance</span></div>
                    </a>
                </li>
                <li id="btn_feature_exception">
                    <a href="" data-toggle="modal"
                       data-target="#exceptionModal">
                        <div class="icon"><i class="icon-support floating-btn-indigo"></i></div>
                        <div class="title"><span>Add Exception</span></div>
                    </a>
                </li>
                <li id="btn_feature_location">
                    <a href="" data-toggle="modal"
                       data-target="#locationModal">
                        <div class="icon"><i class="icon-compass floating-btn-voilet"></i></div>
                        <div class="title"><span>Add Locations</span></div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
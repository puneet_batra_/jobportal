<?php $this->load->view('layouts/bckcadmin/header.php') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.php') ?>
    <h3 class="page-title">
        Channels <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Assets</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('bckcadmin/assets/view/' . $asset->asset_id) ?>"><?php echo $asset->friendly_name ?></a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Channels management</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="portlet-api">
                <div class="heading">
                    <div class="title">Channels <small>Last updated 7 months ago</small></div>
                    <div class="actions">
                        <a class="btn green-haze" href="">
                            <i class="fa fa-share"></i>
                            Graphs
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="widget-channel">
                    <div class="overview">
                        <div class="title"><a>Orientation</a></div>
                        <div class="value">Left</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="details">
                        <div class="detail-overview">
                            <div class="name">hum</div>
                            <div class="value">Last updated a few minutes ago</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="details-data">
                            <img src="http://imgsrc.me/500x200/Graph" alt=""/>
                        </div>
                        <div class="detail-actions">
                            <a href=""><i class="fa fa-edit"></i> Edit</a>
                            <a href=""><i class="fa fa-trash"></i> Delete</a>
                        </div>
                    </div>
                </div>

                <div class="widget-channel">
                    <div class="overview">
                        <div class="title"><a>side_rotation</a></div>
                        <div class="value">back</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="details">
                        <div class="detail-overview">
                            <div class="name">hum</div>
                            <div class="value">Last updated a few minutes ago</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="details-data">
                            <img src="http://imgsrc.me/500x200/Graph" alt=""/>
                        </div>
                        <div class="detail-actions">
                            <a href=""><i class="fa fa-edit"></i> Edit</a>
                            <a href=""><i class="fa fa-trash"></i> Delete</a>
                        </div>
                    </div>
                </div>

                <div class="widget-channel">
                    <div class="overview">
                        <div class="title"><a>temperature</a></div>
                        <div class="value">35.750000</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="details">
                        <div class="detail-overview">
                            <div class="name">hum</div>
                            <div class="value">Last updated a few minutes ago</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="details-data">
                            <img src="http://imgsrc.me/500x200/Graph" alt=""/>
                        </div>
                        <div class="detail-actions">
                            <a href=""><i class="fa fa-edit"></i> Edit</a>
                            <a href=""><i class="fa fa-trash"></i> Delete</a>
                        </div>
                    </div>
                </div>



                <div class="widget-channel-add">
                    <div class="overview">
                        <a class="btn btn-block green-haze btn-open" href="">
                            <i class="fa fa-plus"></i>
                            Add
                        </a>
                    </div>
                    <div class="widget-form">
                        <form action="">
                            <fieldset>
                                <legend>Add Channel</legend>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="" class="form-control" id="" placeholder="eg sensor1"/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label for="">Tags</label>
                                        <input class="form-control" placeholder="eg energy, project" type="text" name="" id=""/>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Units</label>
                                        <input class="form-control" placeholder="eg Watts" type="text" name="" id=""/>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Symbol</label>
                                        <input class="form-control" placeholder="eg W" type="text" name="" id=""/>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <label for="">Current Value</label>
                                        <input type="text" name="" class="form-control" id="" placeholder="eg sensor1"/>
                                    </div>
                                </div>
                                <div class="row form-actions">
                                    <div class="col-sm-12">
                                        <input class="btn green-haze" type="submit" value="Save Channel"/>
                                        <a class="btn btn-default btn-cancel" href="">Cancel</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <div class="heading">
                    <div class="title">Locations <small>Last updated 7 months ago</small></div>
                    <div class="actions">
                        <a class="btn green-haze" href="">
                            <i class="fa fa-share"></i>
                            Graphs
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
        <div class="col-sm-6">
            <div class="portlet-api">
                <div class="heading">
                    <div class="title">Request Log</div>
                    <div class="actions">
                        <a class="btn green-haze" href="">
                            <i class="fa fa-pause"></i>
                            Pause
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <img src="<?php echo public_url('img/input-spinner.gif') ?>" alt=""/>
                        Waiting for Requests
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE HEADER-->
<?php $this->load->view('layouts/bckcadmin/footer.php') ?>
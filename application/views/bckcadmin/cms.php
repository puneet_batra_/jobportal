<?php $this->load->view('layouts/bckcadmin/header.php') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.php') ?>
    <h3 class="page-title">
        Dashboard CMS
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Dashboard CMS</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-grid"></i> Dashboard CMS
                    </div>
                    <div class="actions">
                        <button type="button" id="btn_update" class="btn btn-default"><i class="icon-check"></i> Save
                            Changes
                        </button>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <h4>Dashboard Content:</h4>
                    <br/>
                    <h3 id="content-title" style="border: 1px dashed #d5d5d5" class="page-title" contenteditable="true">
                        <?php echo $company->cms_dashboard_notice_title; ?>
                    </h3>
                    <div id="content-edit" class="responsive-content" style="border: 1px dashed #d5d5d5" contenteditable="true">
                        <?php echo $company->cms_dashboard_notice; ?>
                    </div>

                    <div class="clearfix"></div>
                    <br/>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/bckcadmin/footer.php') ?>
<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <h3 class="page-title">
        Expenses
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Expenses</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="form-group row">
                <div class="col-md-2 text-right">
                    <label class="control-label">Select Asset: </label>
                </div>
                <div class="col-md-10">
                    <input type="hidden" name="" value="<?php echo (int)$this->input->get('asset_id') ?>" id="select_asset" class="form-control select2">
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-md-12 col-sm-12">
            <div class="portlet box green-haze theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-graph"></i>
                        List Expenses
                    </div>
                    <div class="actions">
                        <?php if(is_allowed(1, 'assets')){ ?>
                        <a class="btn btn-default" id="asset-btn-link" href=""><i class="icon-action-redo"></i> Asset overview</a>
                        <?php } ?>
                        <?php if(is_allowed(2, 'accounting')){ ?>

                            <a class="btn btn-default custom-action"
                               data-toggle="modal"
                               data-target="#addModal"><i class="fa fa-plus"></i> Add</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table">
                        <thead>
                        <tr>
                            <th style="width: 20%">Job number</th>
                            <th style="width: 20%">Purchase Date</th>
                            <th style="width: 20%">Reimburse</th>
                            <th style="width: 20%">Photo</th>
                            <th style="width: 20%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Photo modal -->
    <div class="modal fade" id="photo-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Photo</h4>
                </div>
                <div class="modal-body">
                    <img class="img-responsive photo" src="" alt=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Asset type add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new expense</h4>
                </div>
                <form class="form-horizontal"
                      id="add-asset-type-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/expenses/add') ?>" method="post" role="form">
                    <div class="modal-body" id="form-add-body">
                        <!-- start form-->
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Number</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="job_number" placeholder="Job number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Purchase Date</label>

                                <div class="col-md-9">
                                    <input data-date-format="d M yyyy"
                                           name="purchase_date"
                                           type="text" class="form-control date-picker input-inline input-medium"
                                           value="<?php echo date('j M Y') ?>"
                                           placeholder="Enter date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Cost</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cost" placeholder="Enter cost">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Photo</label>

                                <div class="col-md-9">
                                    <input type="file" class="form-control  filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" name="files">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3 control-label">Notes</div>
                                <div class="col-md-9">
                                    <textarea class="wysihtml5 form-control" name="notes" id="" cols="30"
                                              rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Reimburse</label>

                                <div class="col-md-9">
                                    <select class="form-control select2-styled" name="reimburse" id="">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Status</label>

                                <div class="col-md-9">
                                    <select class="form-control select2-styled" name="status" id="">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>



                        </div>


                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Expense</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
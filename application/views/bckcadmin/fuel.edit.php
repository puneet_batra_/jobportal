<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <h3 class="page-title">
        Fuel
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('bckcadmin/fuel') ?>">List Fuel</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-rocket"></i>Fuel Overview
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <!-- Form Content Goes starts here -->
                        <div class="rows">
                            <div class="col-md-12">
                                <form action="<?php echo site_url('bckcadmin/fuel/update') ?>"
                                      enctype="multipart/form-data"
                                      id="update-fuel-form"
                                      data-item="<?php echo $fuel->fuel_id; ?>"
                                      class="form-horizontal" role="form" method="post">
                                    <div class="form-body" class="table table-bordered">

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Asset:
                                            </div>
                                            <div class="col-md-9">
                                                <div class="text">
                                                    <?php echo $fuel->asset->friendly_name; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Odo/Hour</label>

                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="odometer"
                                                       value="<?php echo $fuel->odometer ?>"
                                                       placeholder="Enter reading">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Fueled Date</label>

                                            <div class="col-md-9">
                                                <input data-date-format="yyyy-mm-dd"
                                                       name="fueled_date"
                                                       type="text" class="form-control date-picker input-inline input-medium"
                                                       value="<?php echo date('Y-m-d', strtotime($fuel->fueled_date)) ?>"
                                                       placeholder="Enter text">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Towing</label>

                                            <div class="col-md-9">
                                                <select class="form-control" name="towing" id="towing">
                                                    <option value="0" <?php echo ($fuel->towing == 0)?'selected="selected"':''; ?>>No</option>
                                                    <option value="1" <?php echo ($fuel->towing == 1)?'selected="selected"':''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Litres</label>

                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="volume"
                                                       value="<?php echo $fuel->volume ?>"
                                                       placeholder="Volume of fuel" id=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Pre-Purchased</label>

                                            <div class="col-md-9">
                                                <select class="form-control" name="pre_purchased" id="pre_purchased">
                                                    <option value="0" <?php echo ($fuel->pre_purchased == 0)?'selected="selected"':''; ?>>No</option>
                                                    <option value="1" <?php echo ($fuel->pre_purchased == 1)?'selected="selected"':''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="cost_div">
                                            <label class="col-md-3 control-label">Cost</label>

                                            <div class="col-md-9">
                                                <input class="form-control" type="text" name="cost" id=""
                                                       value="<?php echo $fuel->cost ?>"
                                                       placeholder="Cost"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Supplier</label>

                                            <div class="col-md-9">
                                                <select class="form-control" name="supplier" id="supplier">
                                                    <?php foreach ($suppliers as $supplier) {?>
                                                        <option value="<?php echo $supplier->supplier_id ?>" <?php echo ($fuel->supplier_id == $supplier->supplier_id)?'selected="selected"':''; ?>><?php echo $supplier->supplier_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Photo</label>

                                            <div class="col-md-9">
                                                <?php if($fuel->photo){ ?>
                                                    <img
                                                        src="<?php echo site_url('image/uploads/'. $this->auth->company_id() .'/fuel/photos/' . $fuel->photo) ?>?s=200x200"
                                                        alt=""/>
                                                    <a class="text-danger">
                                                    <i class="fa fa-trash fa-2x" id="data-item-<?php echo $fuel->fuel_id ?>" onclick="delete_photo(this)"></i>
                                                    </a>
                                                    <br/>
                                                <?php } ?>
                                                <input class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" type="file" name="files" id="fileupload2"/>
                                                <br/>

                                                <div id="dvPreview2"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Entered By
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-control no-border">
                                                    <?php echo $fuel->user->first_name . ' '. $fuel->user->last_name ?>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green ladda-button" data-style="expand-right">Update Fuel</button>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>


                        </div>


                        <!-- Form content ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('layouts/bckcadmin/footer') ?>
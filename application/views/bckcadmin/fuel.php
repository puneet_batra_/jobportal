<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <h3 class="page-title">
        Fuel
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">List Fuel</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="form-group row">
                <div class="col-md-2 text-right">
                    <label class="control-label">Select Asset: </label>
                </div>
                <div class="col-md-10">
                    <input type="hidden" name="" value="<?php echo (int)$this->input->get('asset_id') ?>" id="select_asset" class="form-control select2">
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-md-12 col-sm-12">
            <div class="portlet box green-haze theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-rocket"></i>
                        List Fuel Entries
                    </div>
                    <div class="actions">
                        <?php if(is_allowed(1, 'assets')){ ?>
                        <a class="btn btn-default" id="asset-btn-link" href=""><i class="icon-action-redo"></i> Asset overview</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table">
                        <thead>
                        <tr>
                            <th style="width: 20%">Odo/Hour</th>
                            <th style="width: 20%">Fueled Date</th>
                            <th style="width: 20%">Liters</th>
                            <th style="width: 20%">Photo</th>
                            <th style="width: 20%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Photo modal -->
    <div class="modal fade" id="photo-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Photo</h4>
                </div>
                <div class="modal-body">
                    <img class="img-responsive photo" src="" alt=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
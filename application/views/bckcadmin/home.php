<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>

<?php if ($this->auth->is_companyadmin()) { ?>
    <h3 class="page-title">
        Company Statistics
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Dashboard</a>
            </li>
        </ul>

    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php echo $count_users ?>
                    </div>
                    <div class="desc">
                        Users Registered
                    </div>
                </div>
                <!--            <a class="more" href="#">-->
                <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
                <!--            </a>-->
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat red-intense">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php echo $count_assets ?>
                    </div>
                    <div class="desc">
                        Assets Added
                    </div>
                </div>
                <!--            <a class="more" href="#" onclick="$('#typeahead_example_3').focus()">-->
                <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
                <!--            </a>-->
            </div>
        </div>


        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="storage-block">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-hdd-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        0 MB
                    </div>
                    <div class="desc">
                        Storage Used
                    </div>
                </div>
                <!--            <a class="more" href="#">-->
                <!--                View more <i class="m-icon-swapright m-icon-white"></i>-->
                <!--            </a>-->
            </div>
        </div>

    </div>
<?php } ?>

<h3 class="page-title">
    My Assets
    <small>favourite assets</small>
</h3>
<?php if(!$this->auth->company_id()) { ?>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>
<?php } else { ?>
    <br/>
<?php } ?>

<div class="row">
    <?php foreach ($favourites as $asset) { ?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a href="<?php echo site_url('bckcadmin/assets/view/' . $asset->asset_id) ?>">
                <div class="dashboard-stat favourite blue-madison">
                    <div class="visual">
                        <?php if ($asset->photo == '') { ?>
                            <img
                                src="<?php echo site_url('image/public/img/default_product.png'); ?>?s=65x65&method=crop"
                                alt=""/>
                        <?php } else { ?>
                            <img
                                src="<?php echo site_url('image/uploads/' . $this->auth->company_id() . '/assets/photos/' . $asset->photo); ?>?s=65x65&method=crop"
                                alt=""/>
                        <?php } ?>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?php echo $asset->friendly_name; ?>
                        </div>
                        <div class="desc">
                            <div class="dotdotdot">
                                <?php echo strip_tags($asset->description, '<br>') ?>
                            </div>
                        </div>
                    </div>
                    <div class="more">
                        View Overview <i class="m-icon-swapright m-icon-white"></i>
                    </div>
                </div>
            </a>
        </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="responsive-content">
            <br/>

            <h3 class="page-title">
                <?php echo $company->cms_dashboard_notice_title ?>
            </h3>
            <?php echo $company->cms_dashboard_notice; ?>
        </div>
    </div>
</div>
<?php $this->load->view('layouts/bckcadmin/footer') ?>

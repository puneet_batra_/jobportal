<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <h3 class="page-title">
        Locations
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">List Locations</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12 form-horizontal">
            <div class="form-group row">
                <div class="col-md-2 text-right">
                    <label class="control-label ">Select Asset:</label>
                </div>
                <div class="col-md-10">
                    <input type="hidden" name="" value="<?php echo (int)$this->input->get('asset_id') ?>" id="select_asset" class="form-control select2">
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-md-6 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-location-arrow"></i>
                        GeoLocation Log
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body" id="geo-portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table">
                        <thead>
                        <tr>
                            <th width="30">DateTime</th>
                            <th width="10">Latitude</th>
                            <th width="10">Longitude</th>
                            <th width="5">User</th>
                            <th width="5">HDOP</th>
                            <th width="10">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-location-arrow"></i>
                        Text Location Log
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body" id="text-portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table_2">
                        <thead>
                        <tr>
                            <th width="22%">DateTime</th>
                            <th width="22%">User</th>
                            <th width="30%">Text</th>
                            <th width="20%">Type</th>
                            <th width="5%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- END FEATURES ROWS -->

<?php $this->load->view('layouts/bckcadmin/footer') ?>
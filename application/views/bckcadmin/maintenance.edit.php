<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <h3 class="page-title">
        Maintenance
        <small>Edit</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo site_url('bckcadmin/maintenance') ?>">List Maintenance</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Edit</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-calendar"></i>Maintenance Overview
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <div class="row">
                        <!-- Form Content Goes starts here -->
                        <div class="rows">
                            <div class="col-md-12">
                                <form action="<?php echo site_url('bckcadmin/maintenance/update') ?>"
                                      id="add-maintenance-form"
                                      enctype="multipart/form-data"
                                      class="form-horizontal" role="form" method="post">
                                    <input name="maintenance_id" value="<?php echo $maintenance->maintenance_id; ?>" type="hidden"/>
                                    <input name="asset_id" value="<?php echo $maintenance->asset_id; ?>" type="hidden"/>
                                    <div class="form-body" id="add_asset" class="table table-bordered">

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Asset Name:
                                            </div>
                                            <div class="col-md-9">
                                                <div class="text">
                                                    <?php echo $maintenance->friendly_name ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Type</label>

                                            <div class="col-md-9">
                                                <select class="form-control" name="maintenance_type" id="due_fuel_entry">
                                                    <?php foreach($this->maintenance_model->get_types() as $type_id => $type){ ?>
                                                        <option value="<?php echo $type_id ?>" <?php
                                                            echo ($maintenance->maintenance_type == $type_id)? 'selected="selected"':'';
                                                        ?>><?php echo $type; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group conditional_div div-0">
                                            <label class="control-label col-md-3">Warranty</label>

                                            <div class="col-md-3">
                                                <input class="form-control" type="text" value="<?php echo $this->timezone->convertDate($maintenance->warranty_date, 'Y-m-d H:i:s', 'server-to-user') ?>" name="warranty_date" id="anytime-picker0"/>
                                            </div>
                                        </div>

                                        <div class="form-group conditional_div div-1">
                                            <label class="control-label col-md-3">Due Date</label>

                                            <div class="col-md-3">
                                                <input class="form-control" type="text" value="<?php echo $this->timezone->convertDate($maintenance->due_date, 'Y-m-d H:i:s', 'server-to-user') ?>" name="due_date" id="anytime-picker1"/>
                                            </div>
                                        </div>

                                        <div class="form-group conditional_div div-2">
                                            <label class="col-md-3 control-label">Enter Odo/Hour</label>

                                            <div class="col-md-9">
                                                <input class="form-control" type="text"
                                                       value="<?php echo $maintenance->odo_hour ?>"
                                                       placeholder="Enter odo/hour to which entry is due on."
                                                       name="odo_hour" id="odo_hour"/>
                                            </div>
                                        </div>

                                        <div class="form-group conditional_div div-3">
                                            <label class="control-label col-md-3">Testing</label>

                                            <div class="col-md-3">
                                                <input class="form-control" value="<?php echo $this->timezone->convertDate($maintenance->testing_date, 'Y-m-d H:i:s', 'server-to-user'); ?>" type="text" name="testing_date" id="anytime-picker3"/>
                                            </div>
                                        </div>

                                        <div class="form-group conditional_div div-4">
                                            <label class="control-label col-md-3">End of Life</label>

                                            <div class="col-md-3">
                                                <input class="form-control"
                                                       value="<?php echo $this->timezone->convertDate($maintenance->end_of_life_date, 'Y-m-d H:i:s', 'server-to-user') ?>"
                                                       type="text" name="end_of_life_date" id="anytime-picker4"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Comments
                                            </div>
                                            <div class="col-md-9">
                                                        <textarea class="form-control wysihtml5" name="comments" id="" cols="30"
                                                                  rows="4"><?php echo $maintenance->comments ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Notes for next service
                                            </div>
                                            <div class="col-md-9">
                                                        <textarea class="form-control wysihtml5" name="notes_next_service" id="" cols="30"
                                                                  rows="4"><?php echo $maintenance->notes_next_service ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Attachment
                                            </div>
                                            <div class="col-md-9">
                                                <div id="attachments_list" data-item="<?php echo $maintenance->maintenance_id; ?>"></div>
                                                <input type="file" class="form-control filestyle" data-buttonBefore="true" data-iconName="glyphicon-inbox" name="files[]" multiple="multiple" id="fileupload1">
                                                <br/>

                                                <div id="dvPreview1"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Entered By
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-control no-border">
                                                    <?php echo $maintenance->username ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3 control-label">
                                                Created at
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-control no-border">
                                                    <?php echo $maintenance->created_on ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn green ladda-button" data-style="expand-right">Update Maintenance</button>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>


                        </div>


                        <!-- Form content ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('layouts/bckcadmin/footer') ?>
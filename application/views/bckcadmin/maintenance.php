<?php $this->load->view('layouts/bckcadmin/header'); ?>
<?php $this->load->view('layouts/bckcadmin/sidebar'); ?>
    <h3 class="page-title">
        Maintenance
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">List Maintenances</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12 form-horizontal">
            <div class="form-group row">
                <div class="col-md-2 text-right">
                <label class="control-label">Select Asset: </label>
                </div>
                <div class="col-md-10">
                    <input type="hidden" name="" value="<?php echo (int)$this->input->get('asset_id') ?>" id="select_asset" class="form-control select2">
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-refresh"></i>
                        List Maintenance Entries
                    </div>
                    <div class="actions" style="margin-right: 205px">
                        <?php if(is_allowed(1, 'assets')){ ?>
                        <a class="btn btn-default" id="asset-btn-link" href=""><i class="icon-action-redo"></i> Asset overview</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table">
                        <thead>
                        <tr>
                            <th width="3%">#</th>
                            <th width="12%">Entered By</th>
                            <th width="15%">Date Time</th>
                            <th width="15%">Value</th>
                            <th width="5%">Maintenance Type</th>
                            <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- END FEATURES ROWS -->

<?php $this->load->view('layouts/bckcadmin/footer'); ?>
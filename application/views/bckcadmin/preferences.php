<?php $this->load->view('layouts/bckcadmin/header.php') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.php') ?>
    <h3 class="page-title">
        Preferences
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Preferences</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-check"></i>
                        Preferences
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <form method="post" action="<?php echo site_url('bckcadmin/preferences/update') ?>" id="preference-form">
                    <div class="row">
                        <div class="col-md-12">
                            <br/>
                                <div class="form-group">
                                    <div class="col-md-2 control-label">
                                        Choose Theme:
                                    </div>
                                    <div class="col-md-10">
                                        <select class="form-control" name="theme_id" id="theme-select">
                                            <?php foreach ($this->company_model->get_themes() as $key => $theme) { ?>
                                                <option data-css="<?php echo $theme['css'] ?>" value="<?php echo $key ?>" <?php if($this->company_model->get_css() == $theme['css']){echo 'selected="selected"';} ?>><?php echo $theme['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br/>

                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" class="btn green ladda-button" data-style="expand-right">Update</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
<?php $this->load->view('layouts/bckcadmin/footer.php') ?>
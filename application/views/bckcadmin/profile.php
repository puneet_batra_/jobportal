<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.welcome.php') ?>
    <h3 class="page-title">
        My Profile <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">My Profile</a>
            </li>
        </ul>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box red-intense theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>My Profile
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="profile-form form-horizontal"
                          action="<?php echo site_url('bckcadmin/profile') ?>" method="post" role="form">
                        <div class="form-body">
                            <br/>
                            <fieldset>
                                <legend>Social Connect</legend>
                                <div class="form-group">
                                    <label class="col-md-3" for=""></label>
                                    <div class="col-md-9">
                                        <?php if($connected_live){ ?>
                                            <a href="<?php echo site_url('bckcadmin/profile/unlink/Live') ?>" class="connect-btn blue unconnect">
                                                <i class="fa fa-skype"></i> Windows Live
                                                <i class="fa fa-check connected" title="Connected"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a href="<?php echo site_url('hauth/login/Live'); ?>" class="connect-btn bg-grey">
                                                <i class="fa fa-skype"></i> Windows Live
                                            </a>
                                        <?php } ?>
                                        <?php if($connected_google){ ?>
                                            <a href="<?php echo site_url('bckcadmin/profile/unlink/Google') ?>" class="connect-btn red unconnect">
                                                <i class="fa fa-google-plus"></i> Google
                                                <i class="fa fa-check connected" title="Connected"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a href="<?php echo site_url('hauth/login/Google'); ?>" class="connect-btn bg-grey">
                                                <i class="fa fa-google-plus"></i> Google
                                            </a>
                                        <?php } ?>
                                        <?php if($connected_facebook){ ?>
                                            <a href="<?php echo site_url('bckcadmin/profile/unlink/Facebook') ?>" class="connect-btn blue unconnect">
                                                <i class="fa fa-facebook"></i> Facebook
                                                <i class="fa fa-check connected" title="Connected"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a href="<?php echo site_url('hauth/login/Facebook'); ?>" class="connect-btn bg-grey">
                                                <i class="fa fa-facebook"></i> Facebook
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Information</legend>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">First Name</label>

                                    <div class="col-md-9">
                                        <input type="text" placeholder="First Name" name="first_name" value="<?php echo $user->first_name ?>" class="form-control">
                                        <?php echo form_error('first_name'); ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Last Name</label>

                                    <div class="col-md-9">
                                        <input type="text" placeholder="Last Name" name="last_name" value="<?php echo $user->last_name; ?>" class="form-control">
                                        <?php echo form_error('last_name') ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>

                                    <div class="col-md-9">
                                        <input class="form-control" type="text" placeholder="Email" value="<?php echo $user->email ?>" name="email"/>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address</label>

                                    <div class="col-md-9">
                                        <input type="text" placeholder="Address" name="address" value="<?php echo $user->address ?>" class="form-control select2">
                                        <?php echo form_error('address'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">City/Town</label>

                                    <div class="col-md-9">
                                        <input type="text" placeholder="City/Town" name="city" value="<?php echo $user->city ?>" class="form-control select2">
                                        <?php echo form_error('city'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Country</label>

                                    <div class="col-md-9">
                                        <select name="country" id="select2_sample7" class="select2 form-control">
                                            <option value=""></option>
                                            <?php foreach($this->countries->getList() as $code => $country){ ?>
                                                <option value="<?php echo $code ?>" <?php if($user->country == $code)echo 'selected="selected"' ?>><?php echo $country ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('country'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label"><b>Change Password:</b></label>

                                    <div class="col-md-9">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Password</label>

                                    <div class="col-md-9">
                                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="password"
                                               placeholder="Password" name="password"/>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Re-type Your Password</label>

                                    <div class="controls">
                                        <div class="col-md-9">
                                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                                                   placeholder="Re-type Your Password" name="rpassword"/>
                                            <?php echo form_error('rpassword'); ?>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green ladda-button" data-style="expand-right">Update Profile</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE HEADER-->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
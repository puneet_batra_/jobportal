<div class="portlet box red-sunglo theme-portlet portlet-reporting-asset">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-layers"></i>
            Assets
        </div>
        <div class="actions">
            <a class="btn btn-default export-pdf" href="#">
                                        <span class="not-loading">
                                            <i class="icon-share-alt"></i> PDF
                                        </span>
                                        <span class="loading">
                                            <i class="fa fa-circle-o-notch fa-spin"></i> Generating Report
                                        </span>
            </a>
            <a class="btn btn-default export-csv" href="#">
                                        <span class="not-loading">
                                            <i class="icon-share-alt"></i> CSV
                                        </span>
                                        <span class="loading">
                                            <i class="fa fa-circle-o-notch fa-spin"></i> Generating Report
                                        </span>
            </a>
        </div>
    </div>
    <div class="portlet-body" id="portlet-body">
        <div class="table-container">
            <div class="table-actions-wrapper">
									<span>
									</span>

            </div>
            <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                <thead>
                <tr role="row" class="heading">
                    <th></th>
                    <th width="30%">
                        Name
                    </th>

                    <th width="20%">
                        Description
                    </th>

                    <th width="20%">
                        Barcode
                    </th>

                    <th width="10%">
                        Status
                    </th>

                    <th width="10%">
                        Type
                    </th>

                    <th width="10%">
                        Feature
                    </th>

                </tr>
                <tr role="row" class="filter">
                    <td width="2%">
                        <input type="checkbox" class="check_all"/>
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="query_name">
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="query_description">
                    </td>
                    <td>
                        <input type="text" class="form-control form-filter input-sm" name="query_barcode">
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <select style="width: 120px" name="query_status" class="form-control form-filter input-sm" id="">
                                <option value="">All</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>

                        </div>
                        <!--                                    <input type="text" class="form-control form-filter input-sm" name="order_price_to" placeholder="To"/>-->
                    </td>

                    <td>
                        <select style="width: 120px" name="query_type" class="form-control form-filter input-sm">
                            <option value="">All</option>
                            <?php foreach($this->asset_types_model->getList() as $type){ ?>
                                <option value="<?php echo $type->asset_type_id ?>"><?php echo $type->name ?></option>
                            <?php } ?>
                        </select>
                    </td>

                    <td>
                        <div class="margin-bottom-5">
                            <select style="width: 120px" name="query_features" class="form-control form-filter input-sm">
                                <option value="">All</option>
                                <option value="feature_maintenance">Maintenance</option>
                                <option value="feature_fuel">Fuel</option>
                                <option value="feature_exception">Exception</option>
                                <option value="feature_location">Location</option>
                            </select>
                        </div>
                        <div class="filter-submit"></div>

                    </td>

                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>
</div>
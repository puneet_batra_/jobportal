<?php $this->load->view('layouts/bckcadmin/header.php') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.php') ?>
    <h3 class="page-title">
        Reporting <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Reporting</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#asset" aria-controls="asset" role="tab" data-toggle="tab">Assets Reporting</a></li>
                    <li role="presentation"><a href="#fuel" aria-controls="profile" role="tab" data-toggle="tab">Fuel Reporting</a></li>
                    <li role="presentation"><a href="#exception" aria-controls="messages" role="tab" data-toggle="tab">Exceptions Reporting</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="asset">
                        <?php $this->load->view('bckcadmin/reporting.partials.assets.php') ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="fuel">
                        <?php $this->load->view('bckcadmin/reporting.partials.fuel.php') ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="exception">
                        <?php $this->load->view('bckcadmin/reporting.partials.exception.php') ?>
                    </div>
                </div>

            </div>

        </div>
    </div>
<?php $this->load->view('layouts/bckcadmin/footer.php') ?>
<?php $this->load->view('layouts/bckcadmin/header'); ?>
<?php $this->load->view('layouts/bckcadmin/sidebar'); ?>

    <!-- /.modal -->
    <h3 class="page-title">
        Roles
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin/home') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Role</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

<form id="role-form" method="post" class="form-horizontal">
    <!-- START ADD ROLE -->
    <div class="row">
        <div class="col-md-12 col-sm-12">

                <div class="form-body" id="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-2">Select Role</label>
                        <div class="col-md-4">
                            <select class="form-control select2me" name="role_id" id="select_role" data-placeholder="Select...">
                                <option value=""></option>
                                <?php foreach($roles as $role){ ?>
                                    <option value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                <?php } ?>
                            </select>

                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Role Name</label>
                        <div class="col-md-4">
                            <input type="text" id="text_role_name" class="form-control" name="role_name" placeholder="Enter Role Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Role Description</label>
                        <div class="col-md-4">
                            <textarea id="text_role_description" name="role_description" class=" form-control" rows="6"></textarea>
                        </div>
                    </div>

                </div>


        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box red-sunglo theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-key"></i>
                        Manage Role Permissions
                    </div>
                </div>
                <div class="portlet-body form" id="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-scrollable">

                                <table class="table table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">
                                            #
                                        </th>
                                        <th style="width: 18%">
                                            Module Name
                                        </th>
                                        <th style="width: 22%">
                                            Description
                                        </th>
                                        <th style="width: 10%">
                                            No Access
                                        </th>
                                        <th style="width: 10%">
                                            View Only
                                        </th>
                                        <th style="width: 10%">
                                            Full Access
                                        </th>
                                        <th style="width: 30%">
                                            Exclusive Access
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;foreach($modules as $module) {?>
                                        <tr>
                                            <td>
                                                <?php echo $i++; ?>
                                            </td>
                                            <td>
                                                <?php echo $module->name ?>
                                            </td>
                                            <td>
                                                <?php echo $module->description; ?>
                                            </td>
                                            <td>
                                                <input type="radio" checked name="permission[<?php echo $module->module_id ?>]" value="0" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                            <td>
                                                <input type="radio" name="permission[<?php echo $module->module_id ?>]" value="1" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                            <td>
                                                <input type="radio" name="permission[<?php echo $module->module_id ?>]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                            </td>
                                            <td>
                                                <?php if(strtolower($module->name) == 'assets'){ ?>
                                                    <div>
                                                        <input type="checkbox" name="sub_permissions[<?php echo $module->module_id ?>][maintenance]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                                        Add Maintenance
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" name="sub_permissions[<?php echo $module->module_id ?>][location]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                                        Add Location
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" name="sub_permissions[<?php echo $module->module_id ?>][exception]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                                        Add and Comment Exception
                                                    </div>
                                                    <div>
                                                        <input type="checkbox" name="sub_permissions[<?php echo $module->module_id ?>][fuel]" value="2" class="icheck" data-radio="iradio_flat-grey">
                                                        Add Fuel
                                                    </div>
                                                <?php }  ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php if(is_allowed(2, 'roles', 'index')){ ?>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-11 col-md-offset-1">

                                <button type="submit" id="btn_update" class="btn green ladda-button" data-style="expand-right">Update Role</button>
                                <button type="submit" id="btn_add" class="btn green ladda-button" data-style="expand-right">Add Role</button>
                                <button type="submit" id="btn_delete" class="btn red ladda-button" data-style="expand-right">Delete Role</button>

                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>


        </div>

    </div>
</form>

<?php $this->load->view('layouts/bckcadmin/footer'); ?>
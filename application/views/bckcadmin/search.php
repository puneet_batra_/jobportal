<?php $this->load->view('layouts/bckcadmin/header.php') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.php') ?>
    <h3 class="page-title">
        Search Results
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Search Results</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <div class="tabbable tabbable-custom tabbable-full-width">

                <div class="row search-form-default">
                    <div class="col-md-12">
                        <form action="#" id="search-form">
                            <div class="input-group">
                                <div class="input-cont">
                                    <input type="text" value="<?php echo $this->input->get('q') ?>" placeholder="Search..." id="search-box" class="form-control"/>
                                </div>
                                <span class="input-group-btn">
                                <button type="button" class="btn green-haze" id="search-btn">
                                    Search &nbsp; <i class="m-icon-swapright m-icon-white"></i>
                                </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="results-container">

                </div>

                <div class="margin-top-20" id="end-of-search"></div>

                <div class="margin-top-20">
                    <a class="btn btn-default" href="" id="btn-load-more">
                        Load more <i class="fa fa-chevron-right"></i>
                    </a>
                </div>

            </div>
        </div>
        <!--end tabbable-->
    </div>
<?php $this->load->view('layouts/bckcadmin/footer.php') ?>
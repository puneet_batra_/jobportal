<?php $this->load->view('layouts/bckcadmin/header'); ?>
<?php $this->load->view('layouts/bckcadmin/sidebar'); ?>
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-add-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Invite User</h4>
                </div>
                <form id="form-add">
                    <div class="modal-body">

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-group">
                            <label class="control-label">First Name </label>
                            <input class="form-control" type="text" name="first_name" id=""/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input class="form-control" type="text" name="last_name" id=""/>
                        </div>

                        <div class="form-group">
                            <label class="control-label ">Email <span class="required">* </span>
                            </label>

                            <div class="">
                                <input name="email" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Select Role</label>

                            <div class="">
                                <select name="role_id" class="form-control select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <?php foreach ($roles as $role) { ?>
                                        <option value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Invite User
                        </button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <form id="form-edit">
                    <div class="modal-body" id="modal-edit-body">
                        <input name="uid" type="hidden" value=""/>

                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-3 control-label">User Type:</label>
                                <div class="col-md-9">
                                    <div style="padding-top: 5px;">
                                        <label>
                                            <input type="radio" name="company_admin" value="1" class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Company Admin
                                        </label>
                                        &nbsp;
                                        &nbsp;
                                        <label>
                                            <input type="radio" name="company_admin" checked="checked" value="0"
                                                   class="icheck"
                                                   data-radio="iradio_flat-grey">
                                            Regular User
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group role-div">
                                <label class="col-md-3 control-label">Select Role</label>
                                <div class="col-md-9">
                                    <select name="role_id" class="form-control select2me" data-placeholder="Select...">
                                        <option value=""></option>
                                        <?php foreach ($roles as $role) { ?>
                                            <option
                                                value="<?php echo $role->role_id ?>"><?php echo $role->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Update User
                        </button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Add admin -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new admin</h4>
                </div>
                <form class="form-horizontal"
                      id="add-admin-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/admins/add') ?>" method="post" role="form">
                    <div class="modal-body" id="form-add-body">
                        <!-- start form-->

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">User</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="select2_add_userid" name="user_id" placeholder="Select user">
                                </div>
                            </div>
                        </div>

                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Admin</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <h3 class="page-title">
        Users
        <small>management</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Users</a>
            </li>
        </ul>

    </div>
    <div class="row">
        <div class="col-md-12">


            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red-intense theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i>Users
                    </div>

                    <div class="actions">
                        <?php if (is_allowed(2, 'users', 'index')) { ?>
                            <a data-toggle="modal" data-target="#addModal" href="javascript:;"
                               class="btn btn-sm btn-default">
                                <i class="fa fa-plus"></i> Add Admin </a>
                            <a data-toggle="modal" data-target="#createModal" href="javascript:;"
                               class="btn btn-sm btn-default">
                                <i class="fa fa-plus"></i> Invite User </a>
                        <?php } ?>
                    </div>

                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                        <tr>
                            <th style="width: 20%">Name</th>
                            <th style="width: 20%">Email</th>
                            <th style="width: 20%">Role</th>
                            <th style="width: 10%">Status</th>
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE HEADER-->
<?php $this->load->view('layouts/bckcadmin/footer'); ?>
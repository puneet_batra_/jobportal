<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar') ?>
    <h3 class="page-title">
        Valuations
        <small></small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo site_url('bckcadmin') ?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Valuations</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE HEADER-->

    <!-- START ADD ASSET -->

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <div class="form-group row">
                <div class="col-md-2 text-right">
                    <label class="control-label">Select Asset: </label>
                </div>
                <div class="col-md-10">
                    <input type="hidden" name="" value="<?php echo (int)$this->input->get('asset_id') ?>" id="select_asset" class="form-control select2">
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="col-md-12 col-sm-12">
            <div class="portlet box green-haze theme-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-clock"></i>
                        List asset valuations
                    </div>
                    <div class="actions">
                        <?php if(is_allowed(1, 'assets')){ ?>
                            <a class="btn btn-default" id="asset-btn-link" href=""><i class="icon-action-redo"></i> Asset overview</a>
                        <?php } ?>
                        <?php if(is_allowed(2, 'accounting')){ ?>

                            <a class="btn btn-default custom-action"
                               data-toggle="modal"
                               data-target="#addModal"><i class="fa fa-plus"></i> Add</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="portlet-body" id="portlet-body">
                    <table class="table table-responsive table-striped" id="fuel_table">
                        <thead>
                        <tr>
                            <th style="width: 25%">Valuation Date</th>
                            <th style="width: 30%">Valuation Amount</th>
                            <th style="width: 20%">Created</th>
                            <th style="width: 10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Asset type add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new valuation</h4>
                </div>
                <form class="form-horizontal"
                      id="add-asset-type-form"
                      enctype="multipart/form-data"
                      action="<?php echo site_url('bckcadmin/valuations/create') ?>" method="post" role="form">
                    <div class="modal-body" id="form-add-body">
                        <!-- start form-->

                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Date</label>

                                <div class="col-md-9">
                                    <input data-date-format="d M yyyy"
                                           name="valuation_date"
                                           type="text" class="form-control date-picker input-inline input-medium"
                                           value="<?php echo date('j M Y') ?>"
                                           placeholder="Enter date">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3 control-label">Comments</div>
                                <div class="col-md-9">
                                    <textarea class="wysihtml5 form-control" name="valuation_comments" id="" cols="30"
                                              rows="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Amount</label>

                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="valuation_amount" placeholder="Enter amount">
                                </div>
                            </div>

                        </div>


                        <!-- end form -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue ladda-button" data-style="expand-right">Add Valuation</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- END FEATURES ROWS -->
<?php $this->load->view('layouts/bckcadmin/footer') ?>
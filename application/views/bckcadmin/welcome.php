<?php $this->load->view('layouts/bckcadmin/header') ?>
<?php $this->load->view('layouts/bckcadmin/sidebar.welcome.php') ?>
<h3 class="page-title">
    Welcome <small></small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo site_url('bckcadmin/home'); ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Welcome</a>
        </li>
    </ul>

</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="responsive-content">
            <br/>
            <h3 class="page-title">
                <?php echo get_option('cms_user_dashboard_notice_title') ?>
            </h3>
            <?php echo get_option('cms_user_dashboard_notice'); ?>
        </div>
    </div>
</div>
<?php $this->load->view('layouts/bckcadmin/footer') ?>

<?php $this->load->view('layouts/user/header.php') ?>

      <section>
          <div class="container">
              <div class="col-md-12 nopadding white">
                   <h4 class="heading1">Employeer/Recruiter</h4>
                  <div class="col-sm-7 col-md-7 layout sign signup">
                      <div class="col-md-12 social_button">
                          <a class="col-sm-6 col-md-6" href="<?php echo site_url('hauth/login/Google') ?>"><img class="img-responsive" src="<?php echo base_url('user/img/signupgp.png') ?>"></a>
                          <a class="col-sm-6 col-md-6" href="<?php echo site_url('hauth/login/LinkedIn') ?>"><img class="img-responsive" src="<?php echo base_url('user/img/signupin.png') ?>"></a>
                      </div>
                      <form class="formjobs" id="signup" action = "<?php echo site_url('employerrecruiter/register')?>" method="post" >
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Email</label>
                              <input class="form-control" type="email" name="email" id="email" value="<?php echo set_value('email'); ?>">
                              <?php echo form_error('email'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Password</label>
                              <input class="form-control" type="password" name="password" id="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>">
                              <?php echo form_error('password'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Confirm Password</label>
                              <input class="form-control" type="password" name="confirm_password" id="confirm_password" placeholder="Retype-Password" value="<?php echo set_value('confirm_password'); ?>">
                              <?php echo form_error('confirm_password'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> User Type</label>
                              <div class="row">
                                  <div class="col-md-3">
                                      <div class="radio">
                                          <label><input type="radio" name="user_type"  id="user_type" value="2">Employer</label>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                        <div class="radio">
                                            <label><input type="radio" name="user_type"  id="user_type" value="3">Recruiter</label>
                                         </div>
                                  </div>

                              </div>
                              <?php echo form_error('user_type'); ?>
                          </div>

                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Company Name</label>
                              <input class="form-control" type="text" name="company_name" id="company_name" value="<?php echo set_value('company_name'); ?>">
                              <?php echo form_error('company_name'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Industry Type</label>
                              <select class="form-control" name="recruit_industry" id="recruit_industry" value="<?php echo set_value('recruit_industry'); ?>">
                                  <option>First</option>
                                  <?php foreach($job_industry as $job_industries) { ?>
                                        <option value="<?php echo $job_industries->industry_id ?>"  <?php echo set_select('recruit_industry',$job_industries->industry_id, TRUE); ?>><?php echo $job_industries->industry_type ?></option>
                                  <?php }?>
                              </select>
                              <?php echo form_error('recruit_industry'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Address</label>
                              <textarea class="form-control" name="address" id="address"><?php echo set_value('address'); ?></textarea>
                              <?php echo form_error('address'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Country</label>
                              <select class="_dropdown" id="country_id" name="country">
                                  <option value="">Select Country</option>
                                  <?php foreach($countries as $country) {?>
                                  <option value="<?php echo $country->location_id ; ?>"<?php echo set_select('country',$country->location_id, TRUE); ?> ><?php echo $country->name; ?></option>
                                  <?php } ?>
                              </select>
                              <?php echo form_error('country'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> State</label>
                              <select class="_dropdown" id="state_id" name="state">
                                  <option value="">Select State</option>

                              </select>
                              <?php echo form_error('state'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> City</label>
                              <select class="_dropdown" id="city_id"  name= "city">
                                  <option>First</option>
                              </select>
                              <?php echo form_error('city') ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Contact Number</label>
                              <input class="form-control" type="number" name="phone" id="phone" value="<?php echo set_value('phone'); ?>">
                              <?php echo form_error('phone'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Company Person Name</label>
                              <input class="form-control" type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
                              <?php echo form_error('first_name'); ?>
                          </div>
                          <div class="form-group">
                              <input type="checkbox" name="tnc" id="tnc" /> I agree to Terms & Conditions<br>
                              <input class="col-sm-12 col-md-12 submit blue" type="submit" name="submit" value="Submit">
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </section>

<?php $this->load->view('layouts/user/latest.php') ?>
<script type="text/javascript" src="<?php echo public_url('plugins/bootbox/bootbox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/spin.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/ladda-bootstrap/dist/ladda.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-validation/js/additional-methods.min.js'); ?>"></script>
<script src="<?php echo asset_url('inline/asset/user/employerrecruiter.index.js') ?>"></script>
<?php $this->load->view('layouts/user/footer.php') ?>

<?php $this->load->view('layouts/user/header.php') ?>
     
      <section>
          <div class="container">
              <div class="col-md-12 nopadding white">
                  <h4 class="heading1">Job Seeker</h4>
                  <div class="col-sm-7 col-md-7 layout sign signup">
                      <div class="col-md-12 social_button">
                          <a class="col-sm-6 col-md-6" href="<?php echo site_url('hauth/login/Google') ?>"><img class="img-responsive" src="<?php echo base_url('user/img/signupgp.png') ?>"></a>
                          <a class="col-sm-6 col-md-6" href="#"><img class="img-responsive" src="<?php echo base_url('user/img/signupin.png') ?>"></a>
                      </div>
                      <form class="formjobs">
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Email</label>
                              <input class="form-control" type="email" name="email" id="email" value="<?php echo set_value('email'); ?>">
                              <?php echo form_error('email'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Password</label>
                              <input class="form-control" type="password" name="password" id="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>">
                              <?php echo form_error('password'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Confirm Password</label>
                              <input class="form-control" type="password" name="confirm_password" id="confirm_password" placeholder="Retype-Password" value="<?php echo set_value('confirm_password'); ?>">
                              <?php echo form_error('confirm_password'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Mobile Number</label>
                              <input class="form-control" type="number" name="phone" id="phone" value="<?php echo set_value('phone'); ?>">
                              <?php echo form_error('phone'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span>First Name</label>
                              <input class="form-control" type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
                              <?php echo form_error('first_name'); ?>
                          </div>
                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span>Last Name</label>
                              <input class="form-control" type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>">
                              <?php echo form_error('last_name'); ?>
                          </div>
                          <input type="hidden" name="user_type"  id="user_type" value="1">

                          <div class="form-group">
                              <label class=""><span class="text-warning">*</span> Address</label>
                              <textarea class="form-control" name="address" id="address"><?php echo set_value('address'); ?></textarea>
                              <?php echo form_error('address'); ?>
                          </div>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class=""><span class="text-warning">*</span> Country</label>
                                      <select class="_dropdown" id="country_id" name="country">
                                          <option value="">Select Country</option>
                                          <?php foreach($countries as $country) {?>
                                              <option value="<?php echo $country->location_id ; ?>"<?php echo set_select('country',$country->location_id, TRUE); ?> ><?php echo $country->name; ?></option>
                                          <?php } ?>
                                      </select>
                                      <?php echo form_error('country'); ?>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class=""><span class="text-warning">*</span> State</label>
                                      <select class="_dropdown" id="state_id" name="state">
                                          <option value="">Select State</option>

                                      </select>
                                      <?php echo form_error('state'); ?>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class=""><span class="text-warning">*</span> City</label>
                                      <select class="_dropdown" id="city_id"  name= "city">
                                          <option>First</option>
                                      </select>
                                      <?php echo form_error('city') ?>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label class=""><span class="text-warning">*</span>Year Of Experience</label>
                                      <select class="_dropdown" id="exp_years" name="exp_years">
                                          <option value="">Select</option>
                                          <option value="99">Fresher</option>
                                          <?php for($i = 0; $i <= 30; $i++) {?>
                                              <option value="<?php echo $i ; ?>"<?php echo set_select('exp_years',$i, TRUE); ?> ><?php echo $i; ?></option>
                                          <?php } ?>
                                      </select>
                                      <?php echo form_error('exp_years'); ?>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <input type="checkbox" name="tnc" id="tnc" /> I agree to Terms & Conditions<br>
                          <input class="col-sm-12 col-md-12 submit blue" type="submit" name="submit" value="Submit">
                      </form>
                  </div>
                   

              </div>
          </div>
      </section>

<?php $this->load->view('layouts/user/latest.php') ?>

<?php $this->load->view('layouts/user/footer.php') ?>

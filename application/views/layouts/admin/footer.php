</div>
</div>
</div>
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?php echo date('Y'); ?> &copy; IJC.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo public_url('plugins/respond.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?php echo public_url('plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo public_url('plugins/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php
$this->minify->js(array(
    'scripts/metronic.js',
    'scripts/layout.js',
    'scripts/quick-sidebar.js',
    'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
    'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'plugins/jquery.blockui.min.js',
    'plugins/jquery.cokie.min.js',
    'plugins/uniform/jquery.uniform.min.js',
    'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    'plugins/jquery-idle-timeout/jquery.idletimeout.js',
    'plugins/jquery-idle-timeout/jquery.idletimer.js'
));
?>
<?php echo $this->minify->deploy_js(false, 'admin.layout.js') ?>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- START SELECT2 PLUGINS -->
<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-multi-select/js/jquery.multi-select.min.js'); ?>"></script>
<!-- END SELECT2 PLUGINS -->

<script src="<?php echo public_url('plugins/typeahead/handlebars.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/typeahead/typeahead.bundle.min.js'); ?>" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/actual/jquery.actual.min.js'); ?>"></script>
<script src="<?php echo asset_url('inline/asset/admin/common.js') ?>"></script>
<?php echo get_page_js(); ?>
</body>
<!-- END BODY -->
</html>
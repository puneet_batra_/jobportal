<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Admin Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/select2/select2.min.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/jquery-multi-select/css/multi-select.css'); ?>"/>

    <link href="<?php echo public_url('css/components.min.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/plugins.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/layout.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/themes/default.css'); ?>" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo public_url('css/custom.css'); ?>" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="<?php echo public_url('img/favicon.png') ?>"/>
    <?php echo get_page_css(); ?>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo site_url('admin/home'); ?>">
                <img src="<?php echo public_url('img/logo-big.svg'); ?>" alt="logo" height="46" class="logo-default"/>
            </a>
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->


        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <?php if(count($this->auth->get()->companies) > 0) {?>
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username"><i class="fa fa-building-o"></i>
                        <?php if($this->auth->company()){ ?>
                            <?php echo $this->auth->company()->name ?>
                        <?php } else { ?>
                             Select
                        <?php } ?>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-company">
                        <li class="external">
                            <h3><span class="bold"><?php echo count($this->auth->get()->companies) ?> Companies</span>

                            </h3>

                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                <?php foreach($this->auth->get()->companies as $my_joined_company){ ?>
                                <li>
                                    <?php if ($this->auth->company() && $this->auth->company()->company_id == $my_joined_company->company_id) { ?>
                                        <a href="<?php echo site_url('bckcadmin/home') ?>">
                                            <span class="time">Current</span>
                                            <span class="details">
                                            <span class="label label-sm label-icon label-success">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                                <?php echo $my_joined_company->name ?> </span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo site_url('company/login/' . $my_joined_company->company_id) ?>">
                                            <span class="time"></span>
                                            <span class="details">
                                            <span class="label label-sm label-icon label-success">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                                <?php echo $my_joined_company->name ?> </span>
                                        </a>
                                    <?php } ?>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN INBOX DROPDOWN -->
                <?php } ?>

                <!-- BEGIN TODO DROPDOWN -->

                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <?php if ($this->auth->get()->profile_pic){ ?>
                            <img alt="" class="img-circle" src="<?php echo $this->auth->get()->profile_pic; ?>"/>
                        <?php } else { ?>
                            <img alt="" class="img-circle" src="<?php echo public_url('img/avatar.png'); ?>"/>
                        <?php } ?>
					<span class="username ">
					<?php echo $this->auth->get()->first_name; ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="<?php echo site_url('admin/profile') ?>">
                                <i class="icon-user"></i> My Profile </a>
                        </li>

                        <li class="divider">
                        </li>
                        <li>
                            <a href="<?php echo site_url('logout') ?>">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>

<div class="page-container">
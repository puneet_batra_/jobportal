<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->

            <li>&nbsp;</li>
            <li class="start <?php echo (in_array($this->uri->segment(2), array('', 'home', 'welcome')))?'active':'' ?>">
                <a href="<?php echo site_url('admin/home') ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php if (is_allowed(1, 'users', 'index')) { ?>
            <li class="start <?php echo ($this->uri->segment(2) == 'users'
                && ($this->uri->segment(3) == 'index' || $this->uri->segment(3) == '')
                )?'active':'' ?>">
                <a href="<?php echo site_url('admin/users') ?>">
                    <i class="icon-user"></i>
                    <span class="title">Manage Sub Admin</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php } ?>
            <?php if (is_allowed(1, 'users', 'cusers')){ ?>
            <li class="start <?php echo ($this->uri->segment(3) == 'cusers')?'active':'' ?>">
                <a href="<?php echo site_url('admin/users/cusers') ?>">
                    <i class="icon-user"></i>
                    <span class="title">Manage Users</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php } ?>
<!--            --><?php //if(is_allowed( 1, 'companies')) { ?>
<!--            <li class="start --><?php //echo ($this->uri->segment(2) == 'companies')?'active':'' ?><!--">-->
<!--                <a href="--><?php //echo site_url('admin/companies') ?><!--">-->
<!--                    <i class="icon-briefcase"></i>-->
<!--                    <span class="title">Companies</span>-->
<!--                    <span class="selected"></span>-->
<!--                </a>-->
<!--            </li>-->
<!--            --><?php //} ?>
            <?php if (is_allowed(2, 'cms')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'cms')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/cms') ?>">
                        <i class="icon-grid"></i>
                        <span class="title">Manage CMS</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(2, 'emailtemplate')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'emailtemplate')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/emailtemplate') ?>">
                        <i class="fa fa-envelope-o"></i>
                        <span class="title">Manage Email Template</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(2, 'newsletter')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'newsletter')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/newsletter') ?>">
                        <i class="fa fa-newspaper-o"></i>
                        <span class="title">Manage Newsletter</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(2, 'subscriber')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'subscriber')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/subscriber') ?>">
                        <i class="fa fa-users"></i>
                        <span class="title">Manage Subscriber</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php
            if (is_allowed(2, 'jobportal')){
                $arr = array(
                    "industry_sector" => "industry_sector",
                    "project_type" => "project_type",
                    "job_position" => "job_position",
                    "job_post" => "job_post",
                    "cv_url" => "cv_url",
                    "export_data" => "export_data",
                    "job_posted" => "job_posted"
                );
                ?>

                <li class="start <?php if(in_array($this->uri->segment(2),$arr)){echo 'active open';} ?>">
                    <a href="<?php echo site_url('admin/jobportal') ?>">
                        <i class="fa fa-suitcase"></i>
                        <span class="title">Manage Job Portal</span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu" style=<?php if(in_array($this->uri->segment(2),$arr)){ echo "display: block;"; }else{ echo "display: none"; } ?>>
                        <li class="start <?php echo ($this->uri->segment(2) == 'industry_sector')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/industry_sector') ?>">
                                <i class="icon-paper-plane"></i>
                                Industry/Sector</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'project_type')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/project_type') ?>">
                                <i class="icon-paper-plane"></i>
                                Project Type</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'job_position')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/job_position') ?>">
                                <i class="icon-paper-plane"></i>
                                Job Position</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'job_post' || $this->uri->segment(2) == 'job_posted')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/job_post') ?>">
                                <i class="icon-paper-plane"></i>
                                Job Post</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'cv_url')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/cv_url') ?>">
                                <i class="icon-paper-plane"></i>
                              Manage CV</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'export_data')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/export_data') ?>">
                                <i class="icon-paper-plane"></i>
                              Manage Export Data</a>
                        </li>
                    </ul>

                </li>
            <?php } ?>
            <?php if (is_allowed(2, 'package')){
                $package_arr = array(
                    "packages" => "packages",
                    "packages_order" => "packages_order",
                );
                ?>
                <li class="start <?php if(in_array($this->uri->segment(2),$package_arr)){echo 'active open';} ?>">
                    <a href="<?php echo site_url('admin/packages') ?>">
                        <i class="fa fa-money"></i>
                        <span class="title">Manage Credit Package</span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu" style=<?php if(in_array($this->uri->segment(2),$package_arr)){ echo "display: block;"; }else{ echo "display: none"; } ?>>
                        <li class="start <?php echo ($this->uri->segment(2) == 'packages')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/packages') ?>">
                                <i class="icon-paper-plane"></i>
                               Add Packages</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'packages_order')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/packages_order') ?>">
                                <i class="icon-paper-plane"></i>
                                View Packages</a>
                        </li>
                    </ul>
                </li>
            <?php } ?>

            <?php if(is_allowed( 2, 'region')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'region')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/region') ?>">
                        <i class="fa fa-university"></i>
                        <span class="title">Manage Region</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if(is_allowed( 2, 'currency')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'currency')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/currency') ?>">
                        <i class="fa fa-money"></i>
                        <span class="title">Manage Currency</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>

            <?php if(is_allowed( 2, 'resourcecategory')){

                $resource_arr = array(
                    "resourcecategory" => "resourcecategory",
                    "resource" => "resource",
                );

                ?>
                <li class="start <?php if(in_array($this->uri->segment(2),$resource_arr)){echo 'active open';} ?>">
                    <a href="<?php echo site_url('admin/resourcecategory') ?>">
                        <i class="fa fa-book"></i>
                        <span class="title">Resources</span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu" style=<?php if(in_array($this->uri->segment(2),$resource_arr)){ echo "display: block;"; }else{ echo "display: none"; } ?>>
                        <li class="start <?php echo ($this->uri->segment(2) == 'resourcecategory')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/resourcecategory') ?>">
                                <i class="icon-paper-plane"></i>
                                Resource Category</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'resource')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/resource') ?>">
                                <i class="icon-paper-plane"></i>
                                Resources</a>
                        </li>
                    </ul>
                </li>
            <?php } ?>


            <?php if(is_allowed( 2, 'blogcategory')){
                $blog_arr = array(
                    "blogcategory" => "blogcategory",
                    "blog" => "blog",
                );
                ?>
                <li class="start  <?php if(in_array($this->uri->segment(2),$blog_arr)){echo 'active open';} ?>">
                    <a href="<?php echo site_url('admin/blogcategory') ?>">
                        <i class="fa fa-rss
                        "></i>
                        <span class="title">Blog</span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu" style=<?php if(in_array($this->uri->segment(2),$blog_arr)){ echo "display: block;"; }else{ echo "display: none"; } ?>>
                        <li class="start <?php echo ($this->uri->segment(2) == 'blogcategory')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/blogcategory') ?>">
                                <i class="icon-paper-plane"></i>
                                Blog Category</a>
                        </li>
                        <li class="start <?php echo ($this->uri->segment(2) == 'blog')?'active':'' ?>">
                            <a href="<?php echo site_url('admin/blog') ?>">
                                <i class="icon-paper-plane"></i>
                                Blogs</a>
                        </li>
                    </ul>
                </li>
            <?php } ?>

            <?php if(is_allowed( 2, 'site_statistics')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'statistics')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/statistics') ?>">
                        <i class="fa fa-line-chart"></i>
                        <span class="title">View Site Statistics</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if(is_allowed( 2, 'contact')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'contact')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/contact') ?>">
                        <i class="icon-key"></i>
                        <span class="title">Manage Contact Us</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if(is_allowed( 1, 'roles')){ ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'roles')?'active':'' ?>">
                    <a href="<?php echo site_url('admin/roles') ?>">
                        <i class="icon-key"></i>
                        <span class="title">Roles and Permissions</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>


        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content">

        <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success') ?>
        </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-error">
            <i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('error') ?>
        </div>
        <?php } ?>

<!--        --><?php //if ($this->auth->get()->is_verified == false) { ?>
<!--        <div class="alert alert-warning">-->
<!--            <i class="fa fa-info-circle"></i> You need to verify your email to continue using the system.-->
<!--            Click-->
<!--            <a href="--><?php //echo site_url('admin/profile/reverify'); ?><!--">here</a>-->
<!--            to send email again.-->
<!--        </div>-->
<!--        --><?php //} ?>
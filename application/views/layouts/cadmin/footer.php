</div>
</div>
</div>
</div>
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        <?php echo date('Y') ?> &copy; AIR.
    </div>
    <!--
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
    -->
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo public_url('plugins/respond.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?php echo public_url('plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo public_url('plugins/jquery-ui/jquery-ui.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<script type="text/javascript" src="<?php echo public_url('js/custom_fileupload.js'); ?>"></script>
<script src="<?php echo public_url('scripts/metronic.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('scripts/layout.min.js'); ?>" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/jquery-multi-select/js/jquery.multi-select.min.js'); ?>"></script>

<script src="<?php echo public_url('plugins/typeahead/handlebars.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/typeahead/typeahead.bundle.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/actual/jquery.actual.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/jquery-idle-timeout/jquery.idletimeout.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-idle-timeout/jquery.idletimer.min.js'); ?>" type="text/javascript"></script>

<script src="<?php echo public_url('plugins/icheck/icheck.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/bootstrap-filestyle.min.js'); ?>"></script>
<script src="<?php echo public_url('scripts/form-icheck.js'); ?>"></script>

<script src="<?php echo asset_url('inline/asset/bckcadmin/common.js') ?>"></script>
<?php echo get_page_js(); ?>
</body>
<!-- END BODY -->
</html>
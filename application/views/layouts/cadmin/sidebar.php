<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->

            <li>&nbsp;</li>

            <li class="start <?php echo (in_array($this->uri->segment(2), array('', 'home', 'welcome'))) ? 'active' : '' ?>">
                <a href="<?php echo site_url('bckcadmin/home') ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>

            <?php if (is_allowed(2, 'assets', 'add')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'assets') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/assets/add') ?>">
                        <i class="icon-layers"></i>
                        <span class="title">Add Assets</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>

            <?php if (is_allowed(2, 'asset_types', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'asset_types') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/asset_types') ?>">
                        <i class="icon-layers"></i>
                        <span class="title">Asset Types</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>

            <?php if ($this->auth->is_companyadmin()) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'suppliers') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/suppliers') ?>">
                        <i class="icon-list"></i>
                        <span class="title">Suppliers</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>

            <?php if (is_allowed(1, 'fuel', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'fuel') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/fuel') ?>">
                        <i class="icon-rocket"></i>
                        <span class="title">Fuel</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'maintenance', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'maintenance') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/maintenance') ?>">
                        <i class="icon-refresh"></i>
                        <span class="title">Maintenance</span>
                        <span class="selected"></span>
                    </a>

                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'exceptions', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'exceptions') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/exceptions') ?>">
                        <i class="icon-support"></i>
                        <span class="title">Exceptions</span>
                        <span class="selected"></span>
                    </a>

                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'locations', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'locations') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/locations') ?>">
                        <i class="fa fa-location-arrow"></i>
                        <span class="title">Locations</span>
                        <span class="selected"></span>
                    </a>

                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'admins', 'index') && false) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'admins') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/admins') ?>">
                        <i class="icon-users"></i>
                        <span class="title">Admins</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'users', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'users') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/users') ?>">
                        <i class="icon-user"></i>
                        <span class="title">Users</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'reporting', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'reporting') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/reporting') ?>">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Reporting</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'accounting', 'index')) { ?>
                <li class="<?php echo (in_array($this->uri->segment(2),array('accounting', 'valuations', 'expenses'))) ? 'active' : '' ?>">
                    <a href="javascript:;">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Accounting</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php echo ($this->uri->segment(2) == 'accounting') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('bckcadmin/accounting') ?>">
                                Accounting
                            </a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == 'valuations') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('bckcadmin/valuations') ?>">
                                Valuations
                            </a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == 'expenses') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('bckcadmin/expenses') ?>">
                                Expenses
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($this->auth->is_companyadmin()) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'cms') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/cms') ?>">
                        <i class="icon-grid"></i>
                        <span class="title">Dashboard CMS</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <?php if (is_allowed(1, 'roles', 'index')) { ?>
                <li class="start <?php echo ($this->uri->segment(2) == 'roles') ? 'active' : '' ?>">
                    <a href="<?php echo site_url('bckcadmin/roles') ?>">
                        <i class="icon-key"></i>
                        <span class="title">Roles and Permissions</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php } ?>
            <li class="start <?php echo ($this->uri->segment(2) == 'preferences') ? 'active' : '' ?>">
                <a href="<?php echo site_url('bckcadmin/preferences') ?>">
                    <i class="icon-check"></i>
                    <span class="title">Preferences</span>
                    <span class="selected"></span>
                </a>
            </li>


        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content" id="page-block">
        <div id="internal-page-content">
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="alert alert-success">
                    <i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success') ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-error">
                    <i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('error') ?>
                </div>
            <?php } ?>

            <?php if ($this->auth->get()->is_verified == false) { ?>
            <div class="alert alert-warning">
                <i class="fa fa-info-circle"></i> You need to verify your email to continue using the system.
                Click
                <a href="<?php echo site_url('bckcadmin/profile/reverify'); ?>">here</a>
                to send email again.
            </div>
<?php } ?>
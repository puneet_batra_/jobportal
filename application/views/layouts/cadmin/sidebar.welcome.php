<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->

            <li>&nbsp;</li>
            <li class="start <?php echo (basename($_SERVER['SCRIPT_FILENAME']) == 'index.php')?'active':'' ?>">
                <a href="<?php echo site_url('bckcadmin/home/welcome') ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>



        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content">

        <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success') ?>
        </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-error">
            <i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('error') ?>
        </div>
        <?php } ?>

        <?php if ($this->auth->get()->is_verified == false) { ?>
        <div class="alert alert-warning">
            <i class="fa fa-info-circle"></i> You need to verify your email to continue using the system.
            Click
            <a href="<?php echo site_url('bckcadmin/profile/reverify'); ?>">here</a>
            to send email again.
        </div>
        <?php } ?>
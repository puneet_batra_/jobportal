<?php
$description_plain = preg_replace('#<[^>]+>#', ' ', $row->description);
?>
<table style="border:1px solid #ccc" width="100%" nobr="true" cellpadding="7">
    <tr>
        <td rowspan="4" width="20%" style="border:1px solid #ccc">
            <?php if ($row->filename != '') {
                $filename = FCPATH . 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $row->filename;
                if (!file_exists($filename)) {
                    $filename = FCPATH . 'public/img/default_product.png';
                }
            } else {
                $filename = FCPATH . 'public/img/default_product.png';
            }
            ?>
            <img src="<?php echo $filename; ?>"/>
        </td>
        <td rowspan="1" width="20%" style="border:1px solid #ccc">Name</td>
        <td rowspan="1" width="60%" style="border:1px solid #ccc"><?php echo $row->friendly_name ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Description</td>
        <td style="border:1px solid #ccc"><?php echo $row->description; ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Type</td>
        <td style="border:1px solid #ccc"><?php echo $row->asset_type; ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Barcode</td>
        <td style="border:1px solid #ccc"><?php echo $row->barcode; ?></td>
    </tr>
</table>
<br/>
<br/>
<?php
$description_plain = preg_replace('#<[^>]+>#', ' ', $row->description);
?>
<table style="border:1px solid #ccc" width="100%" nobr="true" cellpadding="7">
    <tr>
        <td rowspan="6" width="20%" style="border:1px solid #ccc">
            <?php if ($row->filename != '') {
                $filename = FCPATH . 'uploads/' . $this->auth->company_id() . '/assets/photos/' . $row->filename;
                if (!file_exists($filename)) {
                    $filename = FCPATH . 'public/img/default_product.png';
                }
            } else {
                $filename = FCPATH . 'public/img/default_product.png';
            }
            ?>
            <img src="<?php echo $filename; ?>"/>
        </td>
        <td rowspan="1" width="20%" style="border:1px solid #ccc">Name</td>
        <td rowspan="1" width="60%" style="border:1px solid #ccc"><?php echo $row->friendly_name ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Description</td>
        <td style="border:1px solid #ccc"><?php echo $row->description; ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Last Fueled</td>
        <td style="border:1px solid #ccc">
            <?php if (strtotime($row->last_fueled_date) > 0) {
                echo $this->timezone->convertDate($row->last_fueled_date, 'd M Y, g:ia');
            } else {
                echo $row->last_fueled_date;
            } ?>
        </td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Km till next</td>
        <td style="border:1px solid #ccc"><?php echo $row->km_left; ?></td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Exceptions</td>
        <td style="border:1px solid #ccc"><?php echo $row->exceptions_open_count; ?> New / <?php echo $row->exceptions_progress_count ?> In Progress</td>
    </tr>
    <tr>
        <td style="border:1px solid #ccc">Average LPHK</td>
        <td style="border:1px solid #ccc"><?php echo $row->l_per_n_km; ?></td>
    </tr>
</table>
<br/>
<br/>
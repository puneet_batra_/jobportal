<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 links">
                <h4>Quick Links</h4>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Recruits</a></li>
                    <li><a href="#">Employers</a></li>
                    <li><a href="#">Job Seekers</a></li>
                    <li><a href="#">Resources</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Ready to Apply</a></li>
                </ul>

            </div>

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <h4>From the Blog</h4>
                <div class="blog">
                    <p> Ipsum is simply dummy </p>
                    <p> Ipsum is simply dummy </p>
                    <p> Ipsum is simply dummy </p>
                    <p> Ipsum is simply dummy </p>
                </div>
            </div>

            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 for">
                <div>
                    <h5>For Recruits</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>

                <div class="text-right">
                    <h5>For Recruits</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>


                <div class="">
                    <h5>For Recruits</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <a class="text-right more" href="#">MORE</a>
                </div>


            </div>


        </div>

    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('user/js/bootstrap.min.js') ?>"></script>

<!-- Button trigger modal -->
<!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>-->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Registration</h4>
            </div>
            <div class="modal-body">
                <div>
                    <label class="">Name:</label>
                    <input class="form-control" type="text" name="user_name">
                    <label>Email:</label>
                    <input class="form-control" type="text" name="user_name">
                    <label>Password:</label>
                    <input class="form-control" type="text" name="user_name">
                    <label>Re-Type Password:</label>
                    <input class="form-control" type="text" name="user_name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Register</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sign In</h4>
            </div>
            <div class="modal-body">
                <div>

                    <label>Email:</label>
                    <input class="form-control" type="text" name="user_name">
                    <label>Password:</label>
                    <input class="form-control" type="text" name="user_name">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Sign In</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
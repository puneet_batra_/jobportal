
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Job Seeker</title>

    <!-- Bootstrap -->
    <link href="<?php echo  base_url('user/css/bootstrap.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo public_url('plugins/bootstrap-select/bootstrap-select.min.css'); ?>"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo  base_url('user/css/style.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo  base_url('user/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('user/css/responsive.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('user/css/select2.css') ?>" rel="stylesheet" type="text/css"/>
    <script src="<?php echo base_url('user/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('user/js/select2.min.js') ?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic|Oswald|Roboto+Condensed:400,400italic|Source+Sans+Pro:400,400italic' rel='stylesheet' type='text/css'>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <figure class="col-xs-12 col-sm-3 col-md-3 col-lg-3 lg">
                <img class="img-responsive" src="<?php echo base_url('user/img/1.svg') ?>" >
            </figure>

            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 fright social">
                <a href="#"><span class="fa fa-facebook-official"></span></a>
                <a href="#"><span class="fa fa-twitter-square"></span></a>
                <a href="#"><span class="fa  fa-pinterest"></span></a>
                <a href="#"><span class="fa  fa-google-plus-square"></span></a>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 toplinks">
                <ul class="list-unstyled ">

                    <li><a href="#" data-toggle="modal" data-target="#myModal1">About us </a></li>
                    <li><a href="#" data-toggle="modal" data-target="#myModal">Contacts </a></li>
                </ul>
            </div>
            <nav class="navbar navbar-inverse navbar-static-top col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div class="">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!--          <a href="#" class="navbar-brand">Bootstrap theme</a>-->
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo site_url();?>">Home</a></li>

                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Post Job<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider" role="separator"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Post Find<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider" role="separator"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Post Resume<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider" role="separator"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">Find Resume<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider" role="separator"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">


                                <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <?php if($this->session->userdata('user_id') && $this->session->userdata('is_superuser') == 0) {
                                    if($this->auth->get()->user_type == '1'){echo  $this->auth->get()->first_name." " .$this->auth->get()->last_name; }
                                    if($this->auth->get()->user_type == '2'){ echo $this->auth->get()->first_name." " .$this->auth->get()->last_name;}
                                    if($this->auth->get()->user_type == '3') {echo $this->auth->get()->first_name." " .$this->auth->get()->last_name; } } else{
                                        echo "My Account";
                                    }?>
                                    <span class="caret"></span>
                                </a>
                                <?php if($this->session->userdata('user_id')) { ?>
                                <ul class="dropdown-menu">
                                    <li><a href="#">
                                            <?php if($this->auth->get()->user_type == '1'){echo "Job Seeker"; }?>
                                            <?php if($this->auth->get()->user_type == '2'){echo "Employer"; }?>
                                            <?php if($this->auth->get()->user_type == '3'){echo "Recruiter"; }?>
                                    </a></li>
                                    <li><a href="#">Change Password</a></li>
                                    <li><a href="<?php echo site_url('logout/userlogout')?>">Logout</a></li>
                                    <!--                <li class="divider" role="separator"></li>-->
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                                <?php } else{ ?>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo site_url('employerrecruiter') ?>"><i class="fa fa-sign-in"></i> Sign In </a></li>
                                        <li><a href="<?php echo site_url('registration') ?>" ><i class="fa fa-user-plus"></i> Sign Up</a></li>
                                        <!--<li><a href="#">Somthing Else Here</a></li>
                                        <li class="divider" role="separator"></li>
                                        <li class="dropdown-header">Nav header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>-->
                                    </ul>
                                <?php } ?>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success">
        <i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success') ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-error">
        <i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('error') ?>
    </div>
<?php } ?>

<?php /*if ($this->auth->get()->is_verified == false) { */?><!--
    <div class="alert alert-warning">
        <i class="fa fa-info-circle"></i> You need to verify your email to continue using the system.
        Click
        <a href="<?php /*echo site_url('bckcadmin/profile/reverify'); */?>">here</a>
        to send email again.
    </div>
--><?php /*} */?>
     
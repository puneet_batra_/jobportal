<section class="ban">
    <div class="container">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide col-xs-12 col-sm-12 col-md-12 col-lg-12" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner slider_message" role="listbox">
                    <div class="item active">
                        <img src="<?php echo base_url('user/img/business.jpg') ?>" alt="banner">
                        <div class="carousel-caption">
                            <h4>Welcome to</h4>
                            <h1>International Job Centre</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                            <h4><a class="register" href="#">Register Now</a></h4>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url('user/img/business.jpg') ?>" alt="banner">
                        <div class="carousel-caption">
                            <h4>Welcome to</h4>
                            <h1>International Job Centre</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                            <h4><a class="register" href="#">Register Now</a></h4>
                        </div>
                    </div>

                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

</section>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo public_url('plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo public_url('plugins/select2/select2.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/login3.min.css'); ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo public_url('plugins/icheck/skins/all.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo public_url('css/components.min.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/themes/darkblue.css'); ?>" rel="stylesheet" type="text/css" id="style_color"/>
    <?php $this->minify->css(array(
        'css/plugins.css',
        'css/layout.css',
        'css/custom.css'
    )) ?>
    <?php echo $this->minify->deploy_css(false, 'login.index.css') ?>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo public_url('img/favicon.png') ?>"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?php echo site_url(); ?>">
        <img src="<?php echo public_url('img/logo-big.svg'); ?>" height="75" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="<?php echo site_url('login' . $return_url) ?>" method="post">
        <h3 class="form-title">Login to your account</h3>

        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php } ?>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>
			Enter your email and password. </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>

            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"
                       name="email" value="<?php echo set_value('email') ?>"/>
            </div>
            <?php echo form_error('email'); ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>

            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password"
                       name="password"/>
            </div>
            <?php echo form_error('password') ?>
        </div>
        <div class="form-actions">
            <label class="remember-me-tick">
                <input type="checkbox" name="remember" class="icheck" data-radio="iradio_flat-grey" value="1"/> Remember me </label>

            <button type="submit" class="btn green-haze pull-right">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        <!--ME-->
       <!--div class="login-options">
            <h4>Or login with</h4>
            <ul class="social-icons">
                <li>
                    <a class="googleplus" data-original-title="Google Plus" href="<?php /*echo site_url('hauth/login/Google') */?>">
                    </a>
                </li>
                <li>
                    <a class="skype" data-original-title="Skype" href="<?php /*echo site_url('hauth/login/Live') */?>">
                    </a>
                </li>
                <li>
                    <a class="facebook" data-original-title="Facebook" href="<?php /*echo site_url('hauth/login/Facebook') */?>">
                    </a>
                </li>
            </ul>
        </div>
        <div class="forget-password">
            <h4>Forgot your password ?</h4>

            <p>
                No worries, click <a href="javascript:;" id="forget-password">
                    here </a>
                to reset your password.
            </p>
        </div>
        <div class="create-account">
            <p>
                Don't have an account yet ?&nbsp; <a href="javascript:;" id="register-btn">
                    Create an account </a>
            </p>
        </div-->
        <!--ME-->
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="<?php echo site_url('login/forgot') ?>" method="post">
        <h3>Forget Password ?</h3>

        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php } ?>

        <p>
            Enter your e-mail address below to reset your password.
        </p>

        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"
                       name="email_forgot" value="<?php echo set_value('email_forgot') ?>"/>
            </div>
            <?php echo form_error('email_forgot') ?>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i> Back
            </button>
            <button type="submit" class="btn green-haze pull-right">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
    <!-- BEGIN REGISTRATION FORM -->
    <form class="register-form" action="<?php echo site_url('register') ?>" method="post">
        <h3>Sign Up <small>with</small></h3>
        <div class="login-options register-fix">
            <ul class="social-icons">
                <li>
                    <a class="googleplus" data-original-title="Google Plus" href="<?php echo site_url('hauth/login/Google') ?>">
                    </a>
                </li>
                <li>
                    <a class="skype" data-original-title="Skype" href="<?php echo site_url('hauth/login/Live') ?>">
                    </a>
                </li>
                <li>
                    <a class="facebook" data-original-title="Facebook" href="<?php echo site_url('hauth/login/Facebook') ?>">
                    </a>
                </li>
            </ul>
        </div>

        <p>
            Or Sign-up with following form:
        </p>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">First Name</label>

            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" value="<?php echo set_value('first_name') ?>" placeholder="First Name" name="first_name"/>
            </div>
            <?php echo form_error('first_name') ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Last Name</label>

            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" value="<?php echo set_value('last_name') ?>" placeholder="Last Name" name="last_name"/>
            </div>
            <?php echo form_error('last_name') ?>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>

            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Email" value="<?php echo set_value('email') ?>" name="email"/>
            </div>
            <?php echo form_error('email') ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Address</label>

            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Address" value="<?php echo set_value('address') ?>" name="address"/>
            </div>
            <?php echo form_error('address') ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">City/Town</label>

            <div class="input-icon">
                <i class="fa fa-location-arrow"></i>
                <input class="form-control placeholder-no-fix" type="text" value="<?php echo set_value('city') ?>" placeholder="City/Town" name="city"/>
            </div>
            <?php echo form_error('city') ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Country</label>
            <select name="country" id="select2_sample4" class="select2 form-control">
                <option value=""></option>
                <?php foreach($this->countries->getList() as $code => $country){ ?>
                    <option value="<?php echo $code ?>" <?php if(set_value('country') == $code)echo 'selected="selected"' ?>><?php echo $country ?></option>
                <?php } ?>
            </select>
            <?php echo form_error('country') ?>
        </div>
        <p>
            Enter your account details below:
        </p>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>

            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password"
                       placeholder="Password" name="password"/>
            </div>
            <?php echo form_error('password') ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>

            <div class="controls">
                <div class="input-icon">
                    <i class="fa fa-check"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                           placeholder="Re-type Your Password" name="rpassword"/>
                </div>
            </div>
            <?php echo form_error('rpassword') ?>
        </div>

        <!-- BEGIN REPCAPTCHA -->
        <div id="recaptcha_widget" class="form-recaptcha">
            <div class="form-recaptcha-img" style="width: 300px">
                <a id="recaptcha_image" href="#">
                </a>

                <div class="recaptcha_only_if_incorrect_sol display-none" style="color:red">
                    Incorrect please try again
                </div>
            </div>
            <div class="input-group" style="width: 300px">
                <input type="text" class="form-control" id="recaptcha_response_field" name="recaptcha_response_field">

                <div class="input-group-btn">
                    <a class="btn default" href="javascript:Recaptcha.reload()">
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a class="btn default recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')">
                        <i title="Get an audio CAPTCHA" class="fa fa-headphones"></i>
                    </a>
                    <a class="btn default recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')">
                        <i title="Get an image CAPTCHA" class="fa fa-picture-o"></i>
                    </a>
                    <a class="btn default" href="javascript:Recaptcha.showhelp()">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <p class="help-block">
                <span class="recaptcha_only_if_image">
                    Enter the words above </span>
                <span class="recaptcha_only_if_audio">
                    Enter the numbers you hear </span>
            </p>
            <div id="captcha-error"></div>
            <?php echo form_error('recaptcha_response_field') ?>
        </div>
        <!-- END REPCAPTCHA -->

        <div class="form-group">
            <label>
                <input type="checkbox" name="tnc"/> I agree to the
                <a href="#" data-toggle="modal" data-target="#terms-and-conditions-modal">
                    Terms of Service </a>
                and <a href="#" data-toggle="modal" data-target="#privacy-policy-modal">
                    Privacy Policy </a>
            </label>

            <div id="register_tnc_error">
            </div>
            <?php echo form_error('tnc') ?>
        </div>
        <div class="form-actions">
            <button id="register-back-btn" type="button" class="btn">
                <i class="m-icon-swapleft"></i> Back
            </button>
            <button type="submit" id="register-submit-btn" class="btn green-haze pull-right">
                Sign Up <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->

    <!-- Privacy Policy Modal -->
    <div class="modal fade" id="privacy-policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
                </div>
                <div class="modal-body">
                    <p>This following document sets forth the Privacy Policy for the Air website, airapp.io.</p>

                    <p>Air is committed to providing you with the best possible customer service experience.<br />
                        Air is bound by the Privacy Act 1988 (Crh), which sets out a number of principles concerning the privacy of individuals.</p>

                    <p>Collection of your personal information</p>

                    <p>There are many aspects of the site which can be viewed without providing personal information, however, for access to future Air customer support features you are required to submit personally identifiable information. This may include but not limited to a unique email and password, or provide sensitive information in the recovery of your lost password.</p>

                    <p>Sharing of your personal information</p>

                    <p>We may occasionally hire other companies to provide services on our behalf, including but not limited to handling customer support enquiries, processing transactions or customer freight shipping. Those companies will be permitted to obtain only the personal information they need to deliver the service. Air takes reasonable steps to ensure that these organisations are bound by confidentiality and privacy obligations in relation to the protection of your personal information.</p>


                    <p>Use of your personal information</p>

                    <p>For each visitor to reach the site, we expressively collect the following non-personally identifiable information, including but not limited to browser type, version and language, operating system, pages viewed while browsing the Site, page access times and referring website address. This collected information is used solely internally for the purpose of gauging visitor traffic, trends and delivering personalized content to you while you are at this Site.</p>


                    <p>From time to time, we may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will use for these new purposes only, data collected from the time of the policy change forward will adhere to our updated practices.</p>


                    <p>Changes to this Privacy Policy</p>

                    <p>Air reserves the right to make amendments to this Privacy Policy at any time. If you have objections to the Privacy Policy, you should not access or use the Site.</p>


                    <p>Accessing Your Personal Information</p>

                    <p>You have a right to access your personal information, subject to exceptions allowed by law. If you would like to do so, please let us know. You may be required to put your request in writing for security reasons. Air reserves the right to charge a fee for searching for, and providing access to, your information on a per request basis.</p>


                    <p>Contacting us</p>

                    <p>Air welcomes your comments regarding this Privacy Policy. If you have any questions about this Privacy Policy and would like further information, please contact us by any of the following means during business hours Monday to Friday.</p>

                    <hr/>

                    <p>Call: (0407) 098 970</p>


                    <p>Post: Attn: Privacy Policy,</p>

                    <p>Air,</p>

                    <p>22 Anderson St,</p>

                    <p>Horsham, Vic 3400,</p>

                    <p>AUSTRALIA</p>

                    <p>&nbsp;</p>

                    <p>E-mail: help@airapp.io</p>

                    <h1>&nbsp;</h1>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok, Got it</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Terms and Conditions Modal -->
    <div class="modal fade" id="terms-and-conditions-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
                </div>
                <div class="modal-body">
                    <p>This following document sets forth the Terms and Conditions for the Air website, airapp.io.</p>


                    <hr/>

                    <p>Call: (0407) 098 970</p>


                    <p>Post: Attn: Terms and Conditions,</p>

                    <p>Air,</p>

                    <p>22 Anderson St,</p>

                    <p>Horsham, Vic 3400,</p>

                    <p>AUSTRALIA</p>

                    <p>&nbsp;</p>

                    <p>E-mail: help@airapp.io</p>

                    <h1>&nbsp;</h1>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok, Got it</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    <?php echo date('Y') ?> &copy; Jobportal.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo public_url('plugins/respond.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?php echo public_url('plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/icheck/icheck.min.js'); ?>"></script>
<script src="<?php echo public_url('scripts/form-icheck.js'); ?>"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php $this->minify->js(array(
    'scripts/metronic.min.js',
    'scripts/layout.min.js',
    'scripts/login.min.js'
)); ?>
<?php echo $this->minify->deploy_js(false, 'login.index.js') ?>

<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo site_url('inline/asset/login.index.js') ?>"></script>

<?php if (isset($active_form)) { ?>
    <script type="text/javascript">
        $(window).load(function(){
            $('.login-form').hide();
            $('.register-form').hide();
            $('.forget-form').hide();
            $('.<?php echo $active_form ?>').show();
        })
    </script>
<?php } ?>

<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=<?php echo $this->config->item('google_captcha_public_key') ?>"></script>
<!-- END GOOGLE RECAPTCHA -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
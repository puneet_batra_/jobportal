<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Reset Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo public_url('plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('plugins/uniform/css/uniform.default.css'); ?>" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo public_url('plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/login3.css'); ?>" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo public_url('css/components.css'); ?>" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/plugins.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/layout.css'); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo public_url('css/themes/darkblue.css'); ?>" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo public_url('css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo public_url('img/favicon.png') ?>"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?php echo site_url(); ?>">
        <img src="<?php echo public_url('img/logo-new.png'); ?>" alt=""/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">

    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="<?php echo site_url('login/recover/' . $id . '/' .$secret) ?>" method="post">
        <h3>Change Password </h3>

        <p>
            Enter your new password below to continue.
        </p>

        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-key"></i>
                <input class="form-control placeholder-no-fix" id="password" type="password" autocomplete="off" placeholder="New Password"
                       name="password" value=""/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-key"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Repeat New Password"
                       name="rpassword" value=""/>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn green-haze pull-right">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        <br/>
        <p>

        </p>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    <?php echo date('Y') ?> &copy; Air.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo public_url('plugins/respond.min.js'); ?>"></script>
<script src="<?php echo public_url('plugins/excanvas.min.js'); ?>"></script>
<![endif]-->
<script src="<?php echo public_url('plugins/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery-migrate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/uniform/jquery.uniform.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('plugins/jquery.cokie.min.js'); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo public_url('plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo public_url('plugins/select2/select2.min.js'); ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo public_url('scripts/metronic.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('scripts/layout.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('scripts/demo.js'); ?>" type="text/javascript"></script>
<script src="<?php echo public_url('scripts/login.js'); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo site_url('inline/asset/login.recover.js') ?>"></script>

</body>
<!-- END BODY -->
</html>
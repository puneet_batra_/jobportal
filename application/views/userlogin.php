<?php $this->load->view('layouts/user/header.php') ?>

<section>
    <div class="container">
        <div class="col-md-12 nopadding white">
            <h4 class="heading1 text-center">Sign Up</h4>
            <div class="col-sm-7 col-md-7 layout sign signup">
                <div class="col-md-12 social_button">
                    <a class="col-sm-6 col-md-6" href="#"><img class="img-responsive" src="<?php echo site_url('user/img/gp.png') ?>"></a>
                    <a class="col-sm-6 col-md-6" href="#"><img class="img-responsive" src="<?php echo site_url('user/img/in.png') ?>"></a>
                </div>
                <div class="col-md-12 form">
                    <form  method="post" action="<?php echo site_url('employerrecruiter'.$return_url) ?>">
                        <input class="form-control" type="text" placeholder="Email" name="email" value="<?php set_value('email') ?>">
                        <?php echo form_error('email'); ?>
                        <input class="form-control" type="password" placeholder="Password" name="password" value="<?php set_value('password') ?>">
                        <?php echo form_error('password'); ?>
                        <div class="form-group">
                            <!-- <label class=""><span class="text-warning">*</span> Country</label>
                            <select class="_dropdown" id="user_type" name="user_type">
                                <option value="">Select Country</option>
                                <?php /*foreach($user_type as $user) { */?>
                                    <option value="<?php /*echo $country->location_id ; */?>"<?php /*echo set_select('country',$country->location_id, TRUE); */?> ><?php /*echo $country->name; */?></option>
                                <?php /*} */?>
                            </select>
                            --><?php /*echo form_error('country'); */?>
                        </div>



                        <input class="col-sm-12 col-md-12 submit blue" type="submit" name="submit" value="Submit">
                    </form>

                </div>

            </div>


        </div>
    </div>
</section>

<?php $this->load->view('layouts/user/latest.php') ?>

<?php $this->load->view('layouts/user/footer.php') ?>

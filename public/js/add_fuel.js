$(function(){
    // Hide when pre purchased
    $('#pre_purchased').change(function(){
        if($(this).val() == 'yes')
        {
            $('#cost_div').hide();
        } else {
            $('#cost_div').show();
        }
    });
    // fire on load event
    $('#pre_purchased').change();

    // Hide when no exception
    $('#has_exception_switch').change(function(){
        if($(this).is(':checked'))
        {
            $('.hide-no-exception').show();
        } else {
            $('.hide-no-exception').hide();
        }
    });
    // fire on load event
    $('#has_exception_switch').change();

    // handle via bootstrap switch
    $('#has_exception_switch').on('switchChange.bootstrapSwitch',function(event, state){
        $('#has_exception_switch').change();
    })
});
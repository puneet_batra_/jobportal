$(function(){
    $('#select_role').change(function () {
        var val = $(this).val();
        if (val == false) {
            $('#text_role_name').val('');
            $('#text_role_description').val('');
            $('#btn_update').hide();
            $('#btn_add').show();
        } else {
            $('#text_role_name').val(val);
            $('#text_role_description').val('Description of role: ' + val);
            $('#btn_add').hide();
            $('#btn_update').show();
        }
    });
    $('#select_role').change();
});
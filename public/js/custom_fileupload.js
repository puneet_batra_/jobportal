$.fn.fileSelector = function() {

    return this.each(function() {
        // Do something to each element here.
        // get parent node
        var files;
        var writable_filelist = [];
        var element = this;
        var parentnode = element.parentNode;
        // Hide the input type file
        element.style.display = 'none';

        // create container
        var container = document.createElement('div');
        container.className = 'multifile-container';

        // create button
        var button = document.createElement('button');
        button.className = 'btn red multifile-select-btn';
        button.innerHTML = '<i class="icon-cloud-upload"></i> Choose files';
        button.type = 'button';
        button.addEventListener('click', function(){
            element.click();
        });

        // create files display container
        var files_container = document.createElement('div');
        files_container.className = 'multifile-files-container';

        // Customize the view
        // show container
        parentnode.appendChild(container);
        // append element
        container.appendChild(element);
        // append new button
        container.appendChild(button);
        container.appendChild(button);
        // append files container box
        container.appendChild(files_container);


        // Attach event Listener to input type file
        element.addEventListener('change', function(){
            files_container.innerHTML = '';
            files = this.files;
            var i = 0;
            for(; i < files.length; i++) {
                var filename = files[i].name + "<br />";
                writable_filelist.push(files[i]);

                var single_file = document.createElement('div');
                single_file.className = 'multifile-filename';

                var single_filename = document.createElement('span');
                single_filename.className = 'single-filename';
                single_filename.innerHTML = '<i class="icon-paper-clip text-primary"></i> ' + filename;
                single_file.appendChild(single_filename);

                files_container.appendChild(single_file);
            }
        })
    });

};
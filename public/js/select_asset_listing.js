/**
 Custom module for you to write your own javascript functions
 **/
var Custom = function () {

    // private functions & variables

    var myFunc = function (text) {

        //Add Asset Starts Here

        function formatAsset(asset) {

            return "<img class='flag' src='img/" + asset.id.toLowerCase() + ".jpg'/>&nbsp;&nbsp;" + asset.name;
        };
        function formatSelect(asset) {

            return asset.name;
        };

        var names = [

            {id: "avatar1", name: "Asset 5"},
            {id: "avatar2", name: "Asset 2"},
            {id: "avatar3", name: "Asset 1"},
            {id: "avatar4", name: "Asset 3"}

        ]
        $("#add_asset").select2({
            placeholder: "Select an Asset",
            data: {results: names, text: 'name'},
            formatSelection: formatSelect,
            formatResult: formatAsset

        });
        //Add employee Admin Ends here


    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.          
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

}();

/***
 Usage
 ***/
//Custom.init();
//Custom.doSomeStuff();
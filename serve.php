<?php
define('BASEPATH', __DIR__);
include BASEPATH . '/application/config/config.php';

function removeComments($input, $type)
{
    if ($type == 'css') {
        $pattern = '!/\*[^*]*\*+([^/][^*]*\*+)*/!';
    } else if ($type == 'js') {
        $pattern = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
    }
    $output = preg_replace($pattern, '', $input);
    return $output;
}

function compress($output)
{
    ini_set("pcre.recursion_limit", "16777");
    $buffer = $output;

    $re = '%# Collapse whitespace everywhere but in blacklisted elements.
        (?>             # Match all whitespans other than single space.
          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
        | \s{2,}        # or two or more consecutive-any-whitespace.
        ) # Note: The remaining regex consumes no text at all...
        (?=             # Ensure we are not in a blacklist tag.
          [^<]*+        # Either zero or more non-"<" {normal*}
          (?:           # Begin {(special normal*)*} construct
            <           # or a < starting a non-blacklist tag.
            (?!/?(?:textarea|pre|script)\b)
            [^<]*+      # more non-"<" {normal*}
          )*+           # Finish "unrolling-the-loop"
          (?:           # Begin alternation group.
            <           # Either a blacklist start tag.
            (?>textarea|pre|script)\b
          | \z          # or end of file.
          )             # End alternation group.
        )  # If we made it here, we are not in a blacklist tag.
        %Six';

    $new_buffer = preg_replace($re, " ", $buffer);

    // We are going to check if processing has working
    if ($new_buffer === null)
    {
        $new_buffer = $buffer;
    }

    return $new_buffer;
}

$uri =  '.' . explode('?', $_SERVER['REQUEST_URI'])[0];
$ext = strtolower(end(explode('.', $uri)));

$mimes = array(
    'css' => 'text/css',
    'js' => 'text/javascript',
    'jpg' => 'image/jpg',
    'jpeg' => 'image/jpeg',
    'png' => 'image/png'
);

$output = file_get_contents($uri);
if (in_array($ext, array('css'))) {
    $output = compress(removeComments($output, $ext));
}

header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + $config['cache_length']));
header("Cache-Control: max-age={$config['cache_length']}, must-revalidate");
header('Content-Type: '. $mimes[$ext]);

$last_modified_time = filemtime($uri);
$etag = md5_file($uri);

header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
header("Etag: $etag");

if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start("ob_gzhandler");
} else {
    ob_start();
}
echo $output;
ob_flush();
